// YY Knowledge Board用変数
var boards = [];                // ボード格納用の配列
var selectStickyNote;           // 選択中の付箋情報
var selectCell;                 // 選択中のオブジェクト情報
var notesResData;               // 発言内容(付箋)情報
var noDispRowsIndexes;          // 表示しない発言内容(付箋)のインデックスのリスト
var resHtmlStrList;             // 付箋追加画面用のテーブル情報(html形式)
var userOperationLogs = [];     // ユーザの操作情報(「戻る」「進む」機能)
var userOperationIndex = -1;    // 「戻る」「進む」機能のインデックス
var contenntEditBef;            // 編集前の発言内容
var moveBefore;                 // 付箋の移動前の座標
var participantsData;           // 参加者データ(名前、コード、顔画像、付箋の色)
var userOperationLogAddLock = false; // 操作ログを追加しない用のフラグ
var linkAddFlg = false;         // 線の追加フラグ(操作ログ用)
var moveLinkBefore;             // 線の操作前の情報
var memoCotents = [];           // メモの発言内容の配列
var nowNoteColorType = 0;       // 今の付箋の色の割当てタイプ　0：デフォルト 1：ユーザー別 2：ネガポジ別 3：疑問質問別
var lastAddNoteId = "";         // 最後に追加した付箋のElementID
var noEmotion_RemarkIDList = [];// 表情データがない発言IDのリスト
var timerIdMousePointerSend;    // マウスカーソル共有化用タイマー
var timerIdBoardThumbnail;      // ボードのサムネイル画像更新タイマー
var timerClicked = undefined;
var timerKeikajikan = null;
var contentsList = [];          // 発言内容のリスト
var selectContentsCellInfo = undefined;
var groupingInfo = undefined;
// var jsonSaveValidFlg = true;
var loadingFlg = false;
var activeBoardId = undefined;
var cellCripBoard = [];
var commandManagers = [];
var userInfo = undefined;
var meetingRelatedInfo = undefined;

var boardListInfo = [];
var mousePointerInfo = undefined;
var objectPastePosition = undefined; // Add 20201111 kobayashi

// 画像追加処理改善 -->
var imageList = [];
// <-- 画像追加処理改善

//var note_header_visible = false; // ヘッダの表示/非表示切替 ※デバッグ用
//ymd st
var circleContentsList = [];
var squareContentsList = [];
var addSquareSelectFlag = 0;          　 //ツールバー矩形描画
var addCircleSelectFlag = 0;
var figureFlag = 0;                      //図形
var shapeFlag = 0;
var toolbarFlag = 0;                     //ツールバー
var lineMenuFlag = 0;                    //線のメニュー
var lineTypeFlag = 0;                    //線のtype
var lineStyleFlag = 0;                   //線のstyle
var lineLeftFlag = 0;                    // <= のメニュー
var lineRightFlag = 0;                   // => のメニュー

//初期表示判定フラグ
var lineStyleSettingMenubarDisplay = -1;　//矢印のメニュー
var lineLeftMenubarDisplay = -1;    　　　//矢印のメニュー
var lineRightMenubarDisplay = -1;    　　 //矢印のメニュー
var addFigureMenubarDisplay = -1;         //図形のメニュー
var beforeToolbarSelect = 0;              //前回の選択したボタン保持用

var lineColor = "#000000";
const TRANSPARENT = "transparent";        //透明

const TOOLBAR_UNSELECTED = 0;             //ツールバー：未選択
const TOOLBAR_CARD_SELECT = 1;            //ツールバー：付箋
const TOOLBAR_TEXT_SELECT = 2;            //ツールバー：テキスト
const TOOLBAR_LINK_SELECT = 3;            //ツールバー：矢印
const TOOLBAR_FIGURE_SELECT = 4;          //ツールバー：図形
const TOOLBAR_PEN_SELECT = 5;             //ツールバー：ペン
const TOOLBAR_TEMPLATE_SELECT = 6;        //ツールバー：テンプレート
const TOOLBAR_SORT_SELECT = 7;            //ツールバー：ソート
const TOOLBAR_UNDO_SELECT = 8;            //ツールバー：undo
const TOOLBAR_REDO_SELECT = 9;            //ツールバー：redo
const TOOLBAR_EXPANSIONSELECT = 10;       //ツールバー：拡張機能
const TOOLBAR_VOICE_NOTE_SELECT = 11;            //ツールバー：付箋(音声認識入力)

const TOOLBAR_Y_OFFSET = 50;              //ADD 20201022

const FIGURE_MENU_BAR_UNSELECTED = 0;     //図形：未選択
const FIGURE_MENU_BAR_RECTANGLE = 1;      //図形：矩形(枠のみ)
const FIGURE_MENU_BAR_RECTANGLE_FILL = 2; //図形：矩形
const FIGURE_MENU_BAR_CIRCLE = 3;         //図形：円(枠のみ)
const FIGURE_MENU_BAR_CIRCLE_FILL = 4;    //図形：円

/*new Design*/
const SHAPE_MENU_BAR_SQUARE = 1;          //図形：四角
const SHAPE_MENU_BAR_CIRCLE = 2;          //図形：円
const SHAPE_MENU_BAR_CLOUD = 3;           //図形：雲
const SHAPE_MENU_BAR_STAR = 4;            //図形：星
const SHAPE_MENU_BAR_SQUARE2 = 5;         //図形：四角　角丸
const SHAPE_MENU_BAR_TRIANGLE = 6;        //図形：三角
const SHAPE_MENU_BAR_CHAT_BUBBLE = 7;     //図形：吹き出し
const SHAPE_MENU_BAR_HEART = 8;　　　　  　//図形：三角

const LINE_CONNECT_STRAIGHT = 1;
const LINE_CONNECT_ARROW　= 2;
const LINE_CONNECT_CIRCLE = 3;

const ARROW_NONE = 1;
const ARROW_RIGHT = 2;
const ARROW_LEFT = 3;
const ARROW_BOTH = 4;

const LINE_TYPE = 0;
const LINE_TYPE_STRAIGHT = 1;
const LINE_TYPE_KAGI = 2;
const LINE_TYPE_CURVE = 3;

const LINE_STYLE = 0;
const LINE_STYLE_STRAIGHT = 1;
const LINE_STYLE_DASHED = 2;
const LINE_STYLE_DOTTED = 3;

const LINE_STYLE_DASHED_STROKE_DASHARRAY = "10 2";
const LINE_STYLE_DOTTED_STROKE_DASHARRAY = "3";
const LINE_STYLE_STRAIGHT_STROKE_DASHARRAY = "none";

const CONNECTOR_TYPE_STRAIGHT =　"normal";
const CONNECTOR_TYPE_KAGI = "rounded";
const CONNECTOR_TYPE_CURVE = "smooth";

const ROUTER_TYPE_KAGI = "orthogonal";

const LINE_SOURCE_MARKER_FILL_PATH = "line/sourceMarker/fill";
const LINE_SOURCE_MARKER_STROKE_PATH = "line/sourceMarker/stroke";
const LINE_SOURCE_MARKER_TYPE_PATH = "line/sourceMarker/type";
const LINE_SOURCE_MARKER_R_PATH = "line/sourceMarker/r";
const LINE_SOURCE_MARKER_D_PATH = "line/sourceMarker/d";
const LINE_SOURCE_MARKER_CX_PATH = "line/sourceMarker/cx";

const LINE_TARGET_MARKER_FILL_PATH = "line/targetMarker/fill";
const LINE_TARGET_MARKER_STROKE_PATH = "line/targetMarker/stroke";
const LINE_TARGET_MARKER_TYPE_PATH = "line/targetMarker/type";
const LINE_TARGET_MARKER_R_PATH = "line/targetMarker/r";
const LINE_TARGET_MARKER_D_PATH = "line/targetMarker/d";
const LINE_TARGET_MARKER_CX_PATH = "line/targetMarker/cx";

const ID_KNOWLEDGEBOARD_FRAME = 'yyKnowledgeBoard-frame-';
const ID_KOJIN_BOARDS = 'yyKnowledgeBoard-kojin-boards';
const ID_KNOWLEDGEBOARD_BODY　= 'yyKnowledgeBoard-body';
const ID_LINE_TYPE_SETTING_MENUBAR　="lineTypeSettingMenubar-container-";
const ID_ADD_LINE_MENU = "addLineMenu-container-";
const ID_LINE_MENUBAR = "lineMenubar-container-";
const ID_FUSEN_MENUBAR = "fusenMenubar-container-";
const ID_VOICE_FUSEN_MENUBAR = "voiceFusenMenubar-container-";
const ID_FUSEN_SUB_MENUBAR = "fusenSubMenubar-container-";
const ID_TEXT_MENUBAR = "textMenubar-container-";
const ID_ADD_FIGURE_MENUBAR = "addFigureMenubar-container-";
const ID_SHAPE_SUB_MENUBAR = "shapeSubMenubar-container-";
const ID_LINE_LEFT_MENUBAR　= "lineLeftMenubar-container-";
const ID_LINE_RIGHT_MENUBAR　= "lineRightMenubar-container-";
const ID_MULTI_SELECT_MENUBAR = "multiSelectMenubar-container-";
const ID_PAPER_SCROLLER　= "paperScroller-";
const ID_REACTION_STICKY_NOTE　= "reaction-sticky-note-";
const ID_CHANGE_COLOR_STICKY_NOTE　= "change-color-sticky-note-";

var double_arrow_svg = {
  left:{id:0, src:'images/double_arrow_left.svg'},
  right:{id:1, src:'images/double_arrow_right.svg'},
  up:{id:2, src:"images/double_arrow_up.svg"},
  down:{id:3, src:"images/double_arrow_down.svg"}
};

var TOOLBAR_MENU =
  {
    TOOLBAR_UNSELECTED : { no : 0},
    TOOLBAR_CARD : { no : 1, name : 'addNote'},
    TOOLBAR_TEXT : { no : 2, name : 'addTextLabel'},
    TOOLBAR_LINK : { no : 3, name : 'addLine'},
    TOOLBAR_FIGURE : { no : 4, name : 'addSharp'},
    TOOLBAR_FREE_SHARP : { no : 5, name : 'addFreeShape'},
    TOOLBAR_TEMPLATE : { no : 6, name : 'template'},
    TOOLBAR_SORT : { no : 7, name : 'sort'},
    TOOLBAR_UNDO  : { no : 8, name : 'undo'},
    TOOLBAR_REDO  : { no : 9, name : 'redo'},
    TOOLBAR_REDO  : { no : 10, name : 'expansion'}
  };

var fusenColor =
  {
    fusen1 : {color:"#FFF6D5"},
    fusen2 : {color:"#E7F3F2"},
    fusen3 : {color:"#D1DFF4"},
    fusen4 : {color:"#FFE9E9"},
    fusen5 : {color:"#E1DFF7"},
    fusen6 : {color:"#FFE3B5"},
    fusen7 : {color:"#9DEFD0"},
    fusen8 : {color:"#B3CCFF"},
    fusen9 : {color:"#FFBCBC"},
    fusen10 : {color:"#F6F9FB"},
  };
//ymd end

// Add Start 20200826 Y.Akasaka
var stickyNoteType = {
  normal      : 0,
  image       : 6,
  movie       : 7,
  sound       : 8,
  document    : 9,
}

var stickyNoteHeaderType = {
  headerHidden    : 0,
  headerVisible   : 1,
  headerIconOnly  : 2
}
var note_header_type = stickyNoteHeaderType.headerVisible;
// Add End

var contentsEditFlg = false;

let room = undefined;
let scheduled_at;
let add_note_color = undefined;
let add_voice_note_color = undefined;
let mouseCursorTimerCount = 0;
let mouseCursorDataList = [];
let changeObjectDataList = [];
let lockObjectDataList = [];
let reciveDataStock = [];
let textEditerBef = {};
let selectMeeting = undefined;
let textLockTimer;
let boardThumbnailDBUpdateTimeCount = 0;
let mousePointerNoChangeCount = 0;
let resizeFinishTimer = undefined;
let changeingCell;
let boardEditMode = false; // true:編集モード false:移動モード
let commentTargetCell = undefined; // 一時対応(関数化したときに削除する)
let clickedCellView = undefined;
let nowPasteingFlg = false;
let old_cellview = undefined;    // ポインターダウンイベント前回値
let oldTextArea;
let onseiNinshikiCommentStopFnc = undefined;
let isChangeSizeSticky = true;

const KOJIN_BOARD_NAME = "個人ボード";
const SPEECH_BOARD_NAME = "発言ボード";
const CARD_MEMO_NAME = "メモ";  // メモ用付箋の左上に表示するテキスト
const CARD_WIDTH = 175;         //付箋の幅
const CARD_HEIGHT = 175;         //付箋の高さ
const CARD_OMISSION_NEWLINE_LIMIT = 0;    // 付箋の発言内容を省略表示したときの行数の上限
const CARD_WORD_HEIGHT = 21;    // 発言内容1行分の高さ
const NEXT_NEWLINE_CARD = 8;    // 1行に表示する付箋の数
const CARD_ROW_MAX = 300;       // 1列に表示する付箋の数
const BOARD_WIDTH_OFFSET = 16;  // ボード幅微調整用
const GROUP_LINE_SOURCE = { x:300, y:100 };  // 領域線のソース側の初期値
const GROUP_LINE_TARGET = { x:300, y:600 };  // 領域線のターゲット側の初期値
const CARD_X_PEDDING = 10;      //付箋の表示位置の余白(横)
const CARD_Y_PEDDING = 10;      //付箋の表示位置の余白(縦)
const CARD_SHADOW_WIDTH = CARD_WIDTH; // 付箋の影の横幅
const CARD_SHADOW_HEIGHT = CARD_HEIGHT; // 付箋の影の縦幅
const CARD_CONTENTS_LINEHEIGHT = "1.3em"; // 付箋の影の縦幅
const WORD_DISP_LIMIT = 160;              // 付箋1行表示するときの1行当たりの文字の描画幅のリミット
const WORD_DISP_ALL_LIMIT = 175;          // 付箋全表示するときの1行当たりの文字の描画幅のリミット
const MENU_NODISP_OFFSET = 103;           // 会議選択メニュー表示/非表示切替時にボードの高さを増減させる量
const HIGHLIGHTKEYWORD_LABEL_MAX = 15;    // 「ハイライト対象キーワード」に表示するキーワードの文字数上限
const CONTENTS_FONTSIZE = 16;             // 発言内容のフォントサイズ
const Menber_font = 'Source Han Sans JP';
const BOARD_WIDTH = 8000;                 // ボードの横幅
const BOARD_HEIGHT = 6000;                // ボードの縦幅
const TRIANGLE_POINT = 'M 2 -4 L -6 0 L 2 4 Z';

const CONTENTS_HIGHLIGHT_SPAN_START = '<span class="searchTargetFont">';
const CONTENTS_HIGHLIGHT_SPAN_END = '</span>';

const MOUSEPOINTER_SEND_TIMER = 100;
const MOUSEPOINTER_SEND_TIMER_COUNT_MAX = 10;
const MOUSEPOINTER_ANIMETION_INTERVAL = 90;
const KEIKA_JIKAN = 1000;

// 付箋に表示する文字を制御する処理のモード
const MODE_OMISSION = "omission";
const MODE_ALL = "all";

// 操作ログ情報の操作内容
const USER_OPERATION_ADD = "add";
const USER_OPERATION_DELETE = "delete";
const USER_OPERATION_EDIT = "edit";
const USER_OPERATION_MOVE = "move";
const USER_OPERATION_LINK_ADD = "linkAdd";
const USER_OPERATION_LINK_DELETE = "linkDelete";
const USER_OPERATION_LINK_MOVE = "linkMove";
const USER_OPERATION_RANGEDELETE = "rangeDelete";

// 付箋の種別
const NOTE_KIND_MEMO = "memo";

// 色選択項目
const NOTECOLOR_USER_MODE = 0;      // ユーザー別
const NOTECOLOR_NEGAPOSI_MODE = 1;  // ネガポジ別
const NOTECOLOR_QUESTION_MODE = 2;  // 疑問・質問別
const NOTECOLOR_MEETING_MODE = 3;   // 会議別

// ネガポジ値
const NEGAPOSI_NEGATIVE = 1;
const NEGAPOSI_POSITIVE = 2;
const NEGAPOSI_OTHER = 3;
const NEGAPOSI_UNDETERMINED = 0;

// 疑問・質問値
const IS_QUESTION_NORMAL = 0;
const IS_QUESTION_QUESTION = 1;

// 色編集の自動割当ての色
const NOTECOLOR_DEFAULT_NEGATIVE = "#A0E0E0";
const NOTECOLOR_DEFAULT_POSITIVE = "#E0C0C0";
const NOTECOLOR_DEFAULT_UNDETERMINED = "#F0F0A0";
const NOTECOLOR_DEFAULT_OTHER = "#DEDEDE";
const NOTECOLOR_DEFAULT_NORMAL = "#F0F0A0";
const NOTECOLOR_DEFAULT_QUESTION = "#C06080";
const NOTECOLOR_DEFAULT_MEMO = "#c6c7e2";
const NOTECOLOR_DEFAULT_KEYWORD = "#F0A040";

// JointJS(ライブラリ)の要素のタイプ
const CELL_TYPE_NOTE = "org.Member";
const CELL_TYPE_TEXT = "standard.Rectangle";
const CELL_TYPE_LINK = "link";
const CELL_TYPE_AREALINE = "org.Arrow";
const CELL_TYPE_RECT = "standard.Rectangle";
const CELL_TYPE_STANDARD_LINK = "standard.Link";
const CELL_TYPE_CIRCLE = "basic.Circle";//add ymd
const CELL_TYPE_PATH = "standard.Path";//add ymd
const CELL_TYPE_IMAGE = "standard.Image";

// 表情情報関係
// 種別
const EMOTION_KIND_DEFAULT = 0;   // デフォルト
const EMOTION_KIND_HAPPINESS = 1; // 笑顔
const EMOTION_KIND_NEUTRAL = 2;   // ナチュラル(真顔)
const EMOTION_KIND_SURPRISE = 3;  // 驚き
const EMOTION_KIND_ANGER = 4;     // 怒り
const EMOTION_KIND_SCARED = 5;    // 恐怖
const EMOTION_KIND_SADNESS = 6;   // 悲しみ
const EMOTION_KIND_CONTEMPT = 7;  // 考え中
const EMOTION_KIND_DISGUST = 8;   // 嫌悪
const EMOTION_KIND_NOTHING = 9;   // NOTHING(データ取得無し)

// 表示する絵文字
const EMOTION_DEFAULT = "😃";   // デフォルト
const EMOTION_HAPPINESS = "😊"; // 笑顔
const EMOTION_NEUTRAL = "😐";   // ナチュラル(真顔)
const EMOTION_SURPRISE = "😲";  // 驚き
const EMOTION_ANGER = "👿";     // 怒り
const EMOTION_SCARED = "😖";    // 恐怖
const EMOTION_SADNESS = "😢";   // 悲しみ
const EMOTION_CONTEMPT = "🤔";  // 考え中
const EMOTION_DISGUST = "😨";   // 嫌悪
const EMOTION_NOTHING = "😶";   // NOTHING(データ取得無し)

const KEYCODE_DELETE = 46;
const KEYCODE_C = 67;
const KEYCODE_V = 86;

const OPACITY_DISP = 0.9;
const OPACITY_NODISP = 0.2;

// 顔画像のデフォルトアイコンの色
const DEFAULT_FACE_IMAGE_BACKCOLOR = '#000000'; // 背景色
const DEFAULT_FACE_IMAGE_FONTCOLOR = '#FFFFFF'; // 文字色

// オブジェクト種別
const OBJECT_KIND_STICKY_NOTE = 0;　// 付箋
const OBJECT_KIND_LINE = 1;　// 線
const OBJECT_KIND_SHAPE = 2;　// 図形
const OBJECT_KIND_COMMENT = 3;　// コメント
const OBJECT_KIND_TEXT = 4;　// テキスト
const OBJECT_KIND_PEN = 5;　// ペン
const OBJECT_KIND_STICKY_NOTE_FILE = 6;　// 付箋(ファイル)

// ボード種別
const BOARD_KIND_SHARE = 0;
const BOARD_KIND_KOJIN = 1;
const BOARD_KIND_REMARK = 2;

// マウスカーソルの色
const MOUSECORSOR_COLOR_RED = "#E95A56";
const MOUSECORSOR_COLOR_BLUE = "#4B71A6";
const MOUSECORSOR_COLOR_YELLOW = "#FDC465";
const MOUSECORSOR_COLOR_GREEN = "#0C8467";
const MOUSECORSOR_COLOR_PURPLE = "#796EF8";
const MOUSECORSOR_COLOR_GRAY = "#3B4043";

const CELL_SUBTYPE_SELECT_RECT = "selectRectangle";

//線の太さ
const LINE_THICKNESS_DEFAULT = 1; //add 20201023 YMD Requa問題点ID：314
// 線オブジェクトの最小値設定対応 -->
const LINE_OBJECT_DEFAULT_SIZE = {WIDTH:50, HEIGHT: 50};
const LINE_OBJECT_MIN_SIZE = { WIDTH:20, HEIGHT:20};
// <-- 線オブジェクトの最小値設定対応

const COMMENT_WORD_PX = 18; // 音声認識時の1文字当たりのオブジェクトサイズ

const AUTO_STOP_REC_TIME = 10; // 音声認識をONのままにした時の自動停止時間(秒)

const BOARD_THUMBNAIL_RESIZE_MAX_WIDTH = 400;  // ボードのサムネイル画像リサイズ時の最大幅
const BOARD_THUMBNAIL_RESIZE_MAX_HEIGHT = 225; // ボードのサムネイル画像リサイズ時の最大高さ

const BOARD_THUMBNAIL_UPDATE_TIME = 30 * 60;  // ボードのサムネイル画像更新時間(秒)
const BOARD_THUMBNAIL_DB_UPDATE_TIME = 30 * 60;  // ボードのサムネイル画像のDBの更新時間(秒)
const TEXT_LOCK_MAX_TIME = 5 * 60;  // テキスト編集時にロックをかける最大時間(秒)

// 付箋追加で取り扱えるファイル拡張子
const DOCUMENT_EXTENTIONS = {
  PDF:['pdf'],
  WORD:['doc', 'docx'],
  EXCEL :['xls', 'xlsx'],
  POWERPOINT: ['ppt', 'pptx'],
  ALL : ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', ]
};
// const VALID_MOVIE_EXTENTIONS = ['mp4', 'mov'];
const VALID_MOVIE_EXTENTIONS = [];
// const VALID_SOUND_EXTENTIONS = ['mp3','wav', 'ogg','m4a'];
const VALID_SOUND_EXTENTIONS = [];
const VALID_IMAGE_EXTENTIONS = ['jpg', 'jpeg', 'png', 'gif'];
const MEGA_BYTE = 1024 * 1024;
const MAX_UPLOAD_FILE_SIZE = 100 * MEGA_BYTE; // 100MB

const SELECTED_TOOLBAR_COLOR = "#707F89"; // ツールバーのコントロール選択時の背景色
const SELECTED_THUMBNAIL_COLOR = "rgba(209, 223, 244, 0.55)"; // ボード一覧のサムネイル選択時の枠色

const TEMPLATE_WIDTH = 1860;
const TEMPLATE_HEIGHT = 1046;

// 文字以外の付箋のサムネイル画像の最大サイズ
const IMAGE_THUMBNAIL_WIDTH = 1280;
const IMAGE_THUMBNAIL_HEIGHT = 1280;
let mainRoomId = "";
const listColorSticky = ['#FFF6D5', '#E7F3F2', '#D1DFF4', '#FFE9E9', '#E1DFF7', '#FFE3B5', '#9DEFD0', '#B3CCFF',
  '#FFBCBC', '#F6F9FB'];

// Y.Akasaka 20201023
// 図形のデフォルトサイズ
var shapeDefault = {
  // 外枠
  strokeWidth    : LINE_THICKNESS_DEFAULT,
  // 矩形
  rect           : {width : 50, height : 50},
}

const TOOLBAR_ICON_CHANGE_ONSEININSHIKI = `<style>button.joint-widget.joint-theme-modern[data-name="onseiNinshiki"]::after {background-image: url(/images/voice_recognition_on.svg);}</style>`;
const TOOLBAR_ICON_CHANGE_MOVE_EDIT_MODE = `<style>.joint-toolbar.joint-theme-modern button[data-name="move-edit"]::after {background-image: url(/images/icon-toolbar/pointer.svg); background-repeat: no-repeat}</style>`;

const MAX_WORD_LENGTH = 1000;
const MAX_WORD_LENGTH_TEXT = 250;
const MIN_OBJECT_WIDTH = 150;
const MIN_OBJECT_HEIGHT = 75;
const MAX_OBJECT_WIDTH = BOARD_WIDTH / 2;
const MAX_OBJECT_HEIGHT = BOARD_HEIGHT / 2;
const OBJECT_SELECT_MAX = 100;

// 1度に選択出来る画像の上限設定 -->
const OBJECT_SELECT_IMAGE_MAX = 20;
// <-- 1度に選択出来る画像の上限設定

const WORD_RETURN_MAX = 20; // コメントの折り返し(改行)する文字数

let roleUser;
let viewMenuTop;
let textEditedLine;

const TYPE_BOARD_REMARK_STICKY = 'type-board-remark-sticky';
const TYPE_BOARD_REMARK_STICKY_AUDIO = 'type-board-remark-sticky-audio';
const TYPE_BOARD_REMARK_COMMENT = 'type-board-remark-comment';
const TYPE_BOARD_REMARK_COMMENT_AUDIO = 'type-board-remark-comment-audio';
const TYPE_BOARD_REMARK_AUDIO = 'type-board-remark-audio';


async function yyKnowledgeBoard_load(option){
  
  // 2020/10/14 微調整
  commonDefine.setVisible('#yykb-loading', true);
  $('#btnMainRoom').prop('disabled', true);
  // 2020/10/14 微調整
  
  // 選択した会議のボード情報を取得
  $.ajax({
    type: 'POST',
    url: '/yyknowledgeboard/load-boardlist',
    processData: false,
    data : JSON.stringify(option),
    contentType: "application/json; charset=utf-8",
    success: async function (response) {
      userInfo = response.userInfo;
      // document.getElementById("userName-lbl").innerText = userInfo.response[0].name;
      boardListInfo = response.boardListInfo;
      selectMeeting = response.selectMeeting;
      meetingRelatedInfo = response.meetingRelatedInfo;
      
      // 取得したデータを連結
      participantsData = [];
      updateNotesData(response.participantsData);
      
      // 割り当てられた付箋の色をユーザー情報に追加
      let idx = response.participantsData.findIndex(a => a.code == userInfo.response[0].code);
      userInfo.sticky_note_color = fusenColor.fusen1.color; //デフォルト
      userInfo.mouseCursorColor = "#E95856"; //デフォルト
      if(idx != -1){
        userInfo.sticky_note_color = participantsData[idx].sticky_note_color;
        userInfo.mouseCursorColor = participantsData[idx]["mouse-corsor-color"];
        userInfo.role = participantsData[idx].role;
        userInfo.display_status = participantsData[idx].display_status;
      }
      setUserNoteImage(userInfo.sticky_note_color);
      
      // チーム情報反映
      // if(response.teamInfo != undefined){
      //     if(response.teamInfo.length > 0){
      //         setTeamInfo(response.teamInfo[0]);
      //     }
      // }
      
      // 自分のビデオ通話のラベルの色を変更
      let color = "rgba(253, 196, 101, 0.35)";
      let ix = participantsData.findIndex(a => a.code == userInfo.response[0].code);
      if(ix != -1){
        if(participantsData[ix].sticky_note_color != null && participantsData[idx].sticky_note_color != undefined){
          color = hex2rgb(participantsData[ix]["mouse-corsor-color"], 0.35);
        }
      }
      document.getElementById("my-video-thumbnail-user-plate").style["background-color"] = color;
      
      
      // ビデオ通話初期化
      let meetingId = [];
      meetingId.push(response.selectMeeting);
      
      let param = {
        meetingId : meetingId, // 会議ID(配列)
        apiKey : response.videoCallApiKey, // SkyWay APIキー
        micImgID : "my-micImg", // マイクON/OFFアイコン
        cameraImgID : "my-videoImg",　// カメラON/OFFアイコン
        desktopImgID : "my-desktopImg",　// 画面共有ON/OFFアイコン
        myVideoID : "my-video",　// 自分のカメラ画像(videoタグ)
        videosDisp : "videocall-main", // ビデオ通話表示エリア
        myNameLbl : "lbl-my-video", // 自分の名前表示
        myName : userInfo.response[0].name, // 自分の名前
        myUserCode : userInfo.response[0].code, // 自分のユーザーコード
        myRole : userInfo.role // 自分のロール
      };
      if(userInfo.response[0].nickname != null && userInfo.response[0].nickname != ""){
        param.myNickname = userInfo.response[0].nickname;
      }
      
      // 2020/10/14 微調整 -->
      // initVideoCall(param, mainRoomTransition);
      // 2020/10/15 再対応
      // await initVideoCall(param, mainRoomTransition, initFinishNotify);
      await initVideoCall(param, initFinishNotify);
      // 2020/10/15 再対応
      // <-- 2020/10/14 微調整
      
      // ボード一覧更新
      $('#boardSelect-main').children().remove();
      $('#yyKnowledgeBoard-hidden-boards').children().remove();
      $('#yyKnowledgeBoard-td1').children().remove();
      $('#yyKnowledgeBoard-td0').children().remove();
      boards = [];
      let len = boardListInfo.length;
      let firstBoardLoadComp = false;
      for(let i = 0; i < len; i++){
        
        // 発言ボードは一旦なし
        if(boardListInfo[i].kind == BOARD_KIND_REMARK){
          continue;
        }
        
        let op = Object.assign({}, option);
        addSelectBoard(boardListInfo[i].boardId, boardListInfo[i].boardName,boardListInfo[i].kind, boardListInfo[i].visible, boardListInfo[i].thumbnail_path);
        boardListInfo.dispBoardId = "";
        // if(i == 0){
        if(!firstBoardLoadComp && boardListInfo[i].visible && boardListInfo[i].kind == BOARD_KIND_SHARE){
          let board = document.getElementById("yyKnowledgeBoard-frame-" + boardListInfo[i].boardId);
          // document.getElementById(boardListInfo[i].boardId + "-disp").src = "images/visibility.svg";
          document.getElementById("yyKnowledgeBoard-td1").appendChild(board);
          board.style.display = "";
          document.getElementById(`toolbar-container-${boardListInfo[i].boardId}`).style.display = "";
          document.getElementById(`zoomArea-container-${boardListInfo[i].boardId}`).style.display = "";
          document.getElementById(`boardNameArea-container-${boardListInfo[i].boardId}`).style.display = "";
          activeBoardId = boardListInfo[i].boardId;
          
          // Add 20201027 ボード一覧　初期選択枠追加
          let thumb = document.getElementById(`${activeBoardId}-img`);
          if(thumb != undefined){
            thumb.style.border = "solid 5px";
            thumb.style.borderColor = SELECTED_THUMBNAIL_COLOR;
          }
          firstBoardLoadComp = true;
        }
        
        // set board background image
        if(isBlank(boardListInfo[i].background_image_path) == false){
          drawBoardBackgroundImage(boardListInfo[i].boardId, boardListInfo[i].background_image_path);
        }
        
        op.jsonFileName = boardListInfo[i].fileName;
        op.boardId =  boardListInfo[i].boardId;
        if(boardListInfo[i].kind == BOARD_KIND_REMARK){
          initLoad(op);
        } else {
          notesObjectsLoad(op);
        }
      }
      
      // 編集モードでスタート
      changeBoardEditMode(true);
      
      //add ST YMD
      // if(len>1){
      //     addSelectBoard(1, KOJIN_BOARD_NAME,1);
      // }
      //add END YMD
      initToolbar(); //add ymd
      
      // マウスカーソル共有化のタイマー開始
      if(userInfo.role != 3){
        mousePointerTimerEnable(true);
      }
      
      // サムネイル更新用タイマー起動
      // boardThumbnailTimerEnable(true);
      // await yyKnowledgeBoard.checkRoleUserYYKB(response.mainMeetingId);
      // if(response.mainMeetingId != -1){
      //     await yyKnowledgeBoard.checkRoleUserYYKB(userInfo.role);
      // }
      let iconReaction = await yyKnowledgeBoard.getIconReaction();
      let html = '';
      iconReaction.forEach(icon => {
        html += `<div class="icon-reaction-sticky-note flex-layout" data-id="${icon.id}"><div class="icon-reaction"> ${icon.reaction_icon} </div></div>`;
      });
      $('.reaction-sticky').html(html);
      
      await yyKnowledgeBoard.appendLayoutChangeColor();
      
      if(response.mainMeetingId != -1){
        await yyKnowledgeBoard.ajaxGetListSubRoom(response.mainMeetingId, response.selectMeeting);
        // document.getElementById("btnMainRoom").style.display = "";
      }
      
      analysisCount.init();
      // 音認Stopの対応2 -->
      voiceRemark.setStopFunc(onseininshikiStickyNote.stop, true);
      // 常時音認を一時的に停止 -->
      // await speechRecognitionMng.incessantlyRecStart();
      // <-- 常時音認を一時的に停止
      // <-- 音認Stopの対応2
      // 制限機能対応 -->
      limitFunctions.init();
      // <-- 制限機能対応
    }
  });
}

// 付箋のユーザー色(左上)をセットします
function setUserNoteImage(color){
  let filename = "/images/fusen1.png";
  let voice_filename = "/images/voiceFusen1.svg";
  
  switch(color.toUpperCase()){
    
    case fusenColor.fusen1.color:
      filename = "/images/fusen1.png";
      voice_filename = "/images/voiceFusen1.svg";
      break;
    case fusenColor.fusen2.color:
      filename = "/images/fusen2.png";
      voice_filename = "/images/voiceFusen2.svg";
      break;
    case fusenColor.fusen3.color:
      filename = "/images/fusen3.png";
      voice_filename = "/images/voiceFusen3.svg";
      break;
    case fusenColor.fusen4.color:
      filename = "/images/fusen4.png";
      voice_filename = "/images/voiceFusen4.svg";
      break;
    case fusenColor.fusen5.color:
      filename = "/images/fusen5.png";
      voice_filename = "/images/voiceFusen5.svg";
      break;
    case fusenColor.fusen10.color:
      filename = "/images/fusen10.png";
      voice_filename = "/images/voiceFusen10.svg";
      break;
    default:
      filename = "/images/fusen5.png";
      voice_filename = "/images/voiceFusen5.svg";
      break;
  }
  
  $('head').append(`<style>button.joint-widget.joint-theme-modern[data-name="fusen1"]::after { background-image: url(${filename}); } </style>`);
  $('head').append(`<style>button.joint-widget.joint-theme-modern[data-name="voiceFusen1"]::after { background-image: url(${voice_filename}); } </style>`);
  
}

//共有Boardの表示・非表示切り替え
function shareBoardDisplayChange(boardId){
  
  let kojinBoardId =　getKojinBoardId();
  let boardDispChgCtrl = document.getElementById('boardDispChange-'+ boardId);
  
  if(boardDispChgCtrl != null){ // 矢印ボタンの有無チェック
    
    //個人ボード非表示
    if(getKojinBoardDisplayStatus() == false){
      
      setKojinBoardDisplay("disable",kojinBoardId);
      
      //共有ボード非表示
      if(boardDispChgCtrl.getAttribute("data-name") == "boardDisp-enable"){
        
        boardDispChgCtrl.setAttribute("data-name" , "boardDisp-disable");
        setShareBoardDisplay("disable",boardId,"");
        document.getElementById("yyKnowledgeBoard-sharearea").style.height = "100%";
        
      }else{
        //共有ボード表示
        boardDispChgCtrl.setAttribute("data-name" , "boardDisp-enable");
        if(getVideoAreaDisplayStatus() == false){
          setShareBoardDisplay("default",boardId,"");
        }else{
          setShareBoardDisplay("defaultAndVideo",boardId,"");
        }
      }
    }else{
      //個人ボード表示
      //共有ボード非表示
      if(boardDispChgCtrl.getAttribute("data-name") == "boardDisp-enable"){
        
        boardDispChgCtrl.setAttribute("data-name" , "boardDisp-disable");
        setShareBoardDisplay("disable",boardId,"");
        setKojinBoardDisplay("default",kojinBoardId);
        
      }else{
        //共有ボード表示
        boardDispChgCtrl.setAttribute("data-name" , "boardDisp-enable");
        setKojinBoardDisplay("defaultAndShare",kojinBoardId);
        
        if(getVideoAreaDisplayStatus() == false){
          setShareBoardDisplay("defaultAndKojin",boardId,"");
        }else{
          setShareBoardDisplay("ALL",boardId,"");
        }
      }
    }
  }
}

function stopKeikajikan(){
  clearInterval(timerKeikajikan);
}

/****************************************
 * チーム情報表示
 ****************************************/
// function setTeamInfo(teamInfo){
//
//     // アイコン
//     if(teamInfo.icon_file_path != undefined){
//         document.getElementById("teamIcon-img").src = teamInfo.icon_file_path;
//     } else {
//         return;
//     }
//
//     // チーム名
//     let team_name = "";
//     if(teamInfo.team_name == undefined || teamInfo.team_name == ""){
//         team_name = teamInfo.default_team_name;
//     } else {
//         team_name = teamInfo.team_name;
//     }
//
//     document.getElementById("teamName-lbl").innerText = team_name;
//
// }

/****************************************
 * 付箋をDBから読み込み処理
 ****************************************/
async function notesObjectsLoad(option){
  
  try{
    
    // コントロール操作無効
    loadingContralDisabled(true);
    
    // 初期化処理
    initYYKnowledgeBoard();
    
    // ボードの初期位置中央にする
    let boardIndex = getBoardsIndex(option.boardId);
    boards[boardIndex].paperScroller.center();
    
    let res = undefined;
    
    // 選択した会議のIDを取得
    $.ajax({
      type: 'POST',
      url: '/yyknowledgeboard/load',
      processData: false,
      data : JSON.stringify(option),
      contentType: "application/json; charset=utf-8",
      success: async function (response) {
        res = response;
        
        // コンフィグ情報をhiddenに格納
        document.getElementById("hKeywordRelationWords").value = res.keywordRelationWords;
        
        // エラーチェック
        if(checkResponseErrer("", res)== false){
          document.getElementById('yyKnowledgeBoard-body').style.display = "none";
          loadingContralDisabled(false);
          return;
        }
        
        // 会議名表示
        document.getElementById("meetingInfoStr").innerText = res.meetingString;
        
        // 主催者名表示
        document.getElementById("meetingOrganizer").innerText = res.meetingRecord[0].meetingOrganizer + "のボード";
        
        // 会議開始終了日時表示
        let scheduled_at = new Date(res.meetingRecord[0].scheduled_at);
        scheduled_at_str = dateToStr24HPad0(scheduled_at, 'YYYY/MM/DD hh:mm');
        let scheduled_end_at_str = "";
        if(res.meetingRecord[0].scheduled_end_at != null){
          let scheduled_end_at = new Date(res.meetingRecord[0].scheduled_end_at);
          let format = "";
          if(scheduled_at.getFullYear() == scheduled_end_at.getFullYear() &&
            scheduled_at.getMonth() == scheduled_end_at.getMonth() &&
            scheduled_at.getDate() == scheduled_end_at.getDate() ){
            format = "hh:mm";
          } else {
            format = 'YYYY/MM/DD hh:mm';
          }
          scheduled_end_at_str = dateToStr24HPad0(scheduled_end_at, format);
        }
        document.getElementById("meetingDateStr").innerText = scheduled_at_str + " ～ " + scheduled_end_at_str;
        document.getElementById("meetingInfoIcon").style.display = "block";
        document.getElementById("meetingElapsedTime").style.display = "block";
        
        //経過時間
        if(timerKeikajikan == null){
          timerKeikajikan = setInterval(function(){
            let e_time = new Date();
            if(res.meetingRecord[0].scheduled_at == null) {
              return;
            } else {
              let sa = new Date(res.meetingRecord[0].scheduled_at);
              let diff = e_time.getTime() - sa.getTime();
//--Add　START 20201015
              let keika_jikan_flag = 0;
              if(diff < 0){
                diff = sa.getTime() - e_time.getTime();
                keika_jikan_flag = 1;
              }
//--Add　End　20201015
              let t2 = Math.round(diff / 1000);
              let h = Math.floor(t2 / 3600);
              let m = Math.floor((t2 - h * 3600) / 60);
              let s = Math.floor(t2 - h * 3600 - m * 60);

//--Add　START 20201015
              if(keika_jikan_flag == 1){
                // document.getElementById("meetingElapsedTime").innerText = "-" + h + "時間" + m + "分" + s + "秒  経過";
                document.getElementById("meetingElapsedTime").innerText = "開催前";
              }else{
//--Add　END 20201015
                document.getElementById("meetingElapsedTime").innerText=h + "時間" + m + "分" + s + "秒  経過";
              }//add  20201015
            }
          },KEIKA_JIKAN);
        }
        
        // 発言データの付箋作成
        addLoadNotesData(res.data, res.limit, res.boardId);
        
        // 現状のボードのサムネイル画像を表示
        // if(getBoardKind(option.boardId) != BOARD_KIND_KOJIN){
        //     await boardThumbnailTimer(100,option.boardId);
        // }
        
      }
    });
    
  }
  catch(err){
    
    writeYYKnowledgeBoardErrorLog(err);
  }
  finally
  {
    linkAddFlg = false;
  }
  
}

//ツールバーの初期化処理
function initToolbar(){
  lineLeftFlag = LINE_TYPE_STRAIGHT;
  lineRightFlag = LINE_TYPE_STRAIGHT;
//Change　YMD　Requa問題点ID：309
  //    lineTypeFlag = LINE_TYPE_STRAIGHT;
  lineTypeFlag =  LINE_TYPE_CURVE;
//Change　YMD
  lineStyleFlag = LINE_STYLE_STRAIGHT;
  shapeFlag　=　SHAPE_MENU_BAR_SQUARE;
}

/****************************************
 * (YY Knowledge Board) レスポンスエラーチェック
 ****************************************/
function checkResponseErrer(mode, res){
  
  // サーバエラーチェック
  if(res.status == -1){
    // Toastify.error(res.errMessage, {
    //     showHideTransition : 'slide',
    //     position: 'top-center',
    // });
    // document.getElementById('error_table_YYKB').innerText = res.errMessage;
    return false;
  }
  
  // count:件数取得時
  // data :発言データ取得時
  if(mode == "count"){
    
    // 発言件数チェック
    if(res.count == 0){
      document.getElementById('error_table_YYKB').innerText = res.errMessage;
      document.getElementById('yyKnowledgeBoard-body').style.display = "none";
      return false;
    }
    
  } else if (mode == "data"){
    
    // limitGetCountが異常な場合にエラーメッセージを表示する。
    if(res.limitGetCount == -1){
      document.getElementById('error_table_YYKB').innerText = "通信パラメータが異常です。";
      return false;
    }
  }
  
  return true;
  
}


/****************************************
 * (YY Knowledge Board) 初期化処理
 ****************************************/
function initYYKnowledgeBoard(){
  
  // メッセージ初期化
  document.getElementById('error_table_YYKB').innerText = "";
  
  // ボードの初期化
  yyKnowledgeBoardInit();
  nowNoteColorType = 0;
  
  // 操作ログリストをリセットします。
  userOperationLogs = [];
  userOperationIndex = -1;
  
  // 会議データ初期化
  notesResData = [];
  resHtmlStrList = [];
  noDispRowsIndexes = [];
  noEmotion_RemarkIDList = [];
  contentsList = [];
  
  // json保存停止
  // jsonSaveValidFlg = false;
  
  // 画面表示初期化
  if($("#boardSelect-double-angle").hasClass("fa-angle-double-left")){
    boardSelectDoubleAngle_click();
  }
  
  document.getElementById("yyKnowledgeBoard-td1").removeAttribute('hidden');
  document.getElementById("yyKnowledgeBoard-td0").setAttribute('hidden', 'hidden');
  
}


// 初期状態でボード表示する
async function initLoad(option, initBtnFlg){
  
  try
  {
    
    loadingContralDisabled(true);
    
    // 選択した会議の発言数
    let remarkCountObj = undefined;
    
    // 選択した会議のIDを取得
    $.ajax({
      type: 'POST',
      url: '/yyknowledgeboard/count-remark',
      processData: false,
      data : JSON.stringify(option),
      contentType: "application/json; charset=utf-8",
      success: function (response) {
        remarkCountObj = response;
        
        document.getElementById("hDemoEmotionEnable").value = String(remarkCountObj.demoEmotionEnable);
        document.getElementById("hKeywordRelationWords").value = remarkCountObj.keywordRelationWords;
        let count = Number(remarkCountObj.count);
        let limit = Number(remarkCountObj.limit);
        
        // 発言が無ければ発言データ取得処理をスキップ
        if(count == 0){
          // error_table.innerText = remarkCountObj.errMessage;
          loadingContralDisabled(false);
          return;
        }
        
        // 会議データ取得処理
        setTimeout(() => {
          getMeetingDataYYKB(0, count, false, limit, initBtnFlg, option.boardId)
        }, 10);
      }
    });
    
  } catch (err) {
    writeYYKnowledgeBoardErrorLog(err);
    loadingContralDisabled(false);
  }
  finally
  {
    // setVisible('#loading', false);
  }
}

// DBから発言データを取得する処理
function getMeetingDataYYKB(limitGetCount, count, autoSearchFlg, getlimit, initBtnFlg, boardId){
  
  let url = `/yyknowledgeboard/get-remark-data`
    + `?limitGetCount=${limitGetCount}`;
  
  // 選択した会議のIDを取得
  let res = undefined;
  $.ajax({
    type: 'POST',
    url: url,
    processData: false,
    contentType: false,
    success: function (response) {
      
      try {
        
        res = response;
        
        // エラーチェック
        if(checkResponseErrer("data", res)== false){
          result = false;
          loadingContralDisabled(false);
          return;
        }
        
        
        // 取得データ無い場合
        if(res.resData.length == 0){
          loadingContralDisabled(false);
          return;
        }
        
        // 既に処理済みのデータを取得したときは処理をスキップする。
        if(notesResData != undefined){
          if(notesResData.findIndex(a => a.remark_id == res.resData[0].remark_id) != -1){
            return;
          }
        }
        
        // 取得したデータを連結
        // updateNotesData(res);
        
        // 発言データの付箋作成
        createYYKBNotes(res.resData, limitGetCount, autoSearchFlg, count, initBtnFlg, boardId);
        
        if(count > limitGetCount){
          
          let lgcnt = limitGetCount + getlimit; // getlimitはconfigのGET_LIMIT
          getMeetingDataYYKB(lgcnt, count, autoSearchFlg, getlimit, undefined, boardId);
        }
        
      } catch (err) {
        writeYYKnowledgeBoardErrorLog(err);
        loadingContralDisabled(false);
      }
    }
  });
  
}


/****************************************
 * (YY Knowledge Board)メモリ保持している会議データ更新
 ****************************************/
function updateNotesData(pData){
  
  // 参加者情報更新。
  let parti_len = pData.length;
  for(let i = 0; i < parti_len; i++){
    
    // 顔画像がない場合はコードカラーと名前の1文字目から画像を作成します。
    if(pData[i].face_image == null){
      
      let canvasInfo = {
        // 画像のサイズ
        // 幅
        WIDTH: 60,
        // 高さ
        HEIGHT: 60,
        // 背景色
        BACK_COLOR: "",
        // 文字色
        FORE_COLOR: "",
        // 文字サイズ(px)
        FONT_SIZE: 44,
        // フォント
        FONT: 'sans-serif',
        // ベースラインの位置ぞろえ
        TEXT_BASE_LINE: 'hanging'
      }
      
      // コードカラーもない場合はデフォルト色に設定します。
      if(pData[i].code_color == null){
        canvasInfo.BACK_COLOR = DEFAULT_FACE_IMAGE_BACKCOLOR;
        canvasInfo.FORE_COLOR = DEFAULT_FACE_IMAGE_FONTCOLOR;
      } else {
        canvasInfo.BACK_COLOR = pData[i].code_color;
        canvasInfo.FORE_COLOR = fontColorBlackorWhite(pData[i].code_color);
      }
      
      // 顔画像作成
      pData[i].face_image = createImageFromText(canvasInfo, pData[i].name.substr(0, 1));
    }
    
    let idx = participantsData.findIndex(a => a.code == pData[i].code);
    if(idx == -1){
      
      // 参加者を追加
      participantsData.push(pData[i]);
      
    } else {
      participantsData[idx].face_image = pData[i].face_image;
    }
  }
  
}

// ボードのIDからボードの配列のインデックスを取得する処理
function getBoardsIndex(boardId){
  
  // return boards.findIndex(a => a.paper.el.id == boardId);
  return boards.findIndex(a => a.id == boardId);
}

/****************************************
 * ナレッジボードの初期化処理
 ****************************************/
function yyKnowledgeBoardInit(){
  
  try{
    
    // ボードの初期化処理
    document.getElementById('yyKnowledgeBoard-body').style.display = "block";
    joint.setTheme('modern');
    if(note_header_type == stickyNoteHeaderType.headerHidden){
      
      joint.shapes.org.Member.prototype.markup = '<filter id="filter1"><feGaussianBlur stdDeviation="5" /></filter>'
        + '<g class="scalable">'
        + '<rect class="shadow"/>'
        + '<rect class="card"/>'
        //  + '<defs> <clipPath id="clip-path"><rect class="clipPath" width="40px" height="40px" ref-x="8" ref-y="5" rx="20" ry="20" /></clipPath></defs>'
        + '<image/>'
        + '<image class="background"/>'
        + `<text class="name" style="margin-left: 50px; font-size: 14; font-family: ${Menber_font};"/>`
        + `<text class="date" style="margin-left: 20px; font-size: 11; font-family: ${Menber_font};"/>`
        + `<text class="meeting_id" style="font-size: 11; font-family: ${Menber_font};" />`
        + '<text class="emotion"/>'
        + '<foreignObject class = "foreignObject" width="260" height="260">'
        + `<div contenteditable = "true" class="contents custom-scrollbar" xmlns="http://www.w3.org/1999/xhtml" onblur = "contents_onblur()" style = "resize: none; font-family: ${Menber_font}; cursor:text; overflow-x:hidden; overflow-y:auto; background-color:transparent; border:none; margin-top: 55px; margin-left: 5px; width:254px; height:204px; outline:none;" ></div>`
        + '</foreignObject>'
        + '</g>'
        //+ '<g class="btn del"><circle class="del"/><text class="del" style = "cursor: hand; font-size: 28; font-family: Times New Roman">×</text></g>'
        + '<g class="btn audioPlay"><circle class="audioPlay"/><text class="audioPlay" style = "cursor: hand; font-size: 18; font-family: Times New Roman"></text></g>'
      ;
    } else {
      joint.shapes.org.Member.prototype.markup = '<filter id="filter1"><feGaussianBlur stdDeviation="5" /></filter>'
        + '<g class="scalable" transform="scale(1,1)">'
        + '<rect class="shadow"/>'
        + '<rect class="card"/>'
        + '</g>'
        + '<foreignObject class = "foreignObject"  y="17"  width="260" height="260">'
        + `<div contenteditable = "true" class="contents note-contents-corsor custom-scrollbar" xmlns="http://www.w3.org/1999/xhtml" onblur = "contents_onblur()" style = "resize: none; font-family: ${Menber_font}; cursor:text; overflow-x:hidden; overflow-y:auto; background-color:transparent; border:none; margin-left: 5px; width:254px; height:244px; outline:none;" ></div>`
        + '</foreignObject>'
      ;
    }
    
  } catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// Utility function for normalizing marker's path data.
// Translates the center of an arbitrary path at <0 + offset,0>.
function normalizeMarker(d, offset) {
  var path = new g.Path(V.normalizePathData(d));
  var bbox = path.bbox();
  var ty = - bbox.height / 2 - bbox.y;
  var tx = - bbox.width / 2 - bbox.x;
  if (typeof offset === 'number') tx -= offset;
  path.translate(tx, ty);
  return path.serialize();
}

//線描画のSTYLEの設定を取得
function getStrokeDasharrayValue(){
  let strokeDasharrayValue = LINE_STYLE_STRAIGHT_STROKE_DASHARRAY;
  if(lineStyleFlag == LINE_STYLE_DASHED){
    strokeDasharrayValue = LINE_STYLE_DASHED_STROKE_DASHARRAY;
  }else if(lineStyleFlag == LINE_STYLE_DOTTED){
    strokeDasharrayValue = LINE_STYLE_DOTTED_STROKE_DASHARRAY;
  }
  return strokeDasharrayValue;
}

function paperEvent(paper, jointJSGraph, boardCtrlId){
  
  /*---------------
    * イベント登録
    -----------------*/
  // 付箋のスピーカーアイコンクリック時の処理
  paper.on('element:audioPlay', function(selectCellView){
    
    // テキスト直接入力対応 -->
    jointUiTextEditorClose(activeBoardId);
    // <-- テキスト直接入力対応
    
    if(boardEditMode == false){
      return;
    }
    
    let audio = document.getElementById("comment-play");
    let audioPath = "";
    try {
      if(audio){
        audio.pause();
        audio.currentTime = 0;
        
        if(selectCellView.model.attributes.attrs.audioPlay){
          audioPath = selectCellView.model.attributes.attrs.audioPlay.audioPath;
        } else if(selectCellView.model.attributes.attrs['.btn.audioPlay']) {
          audioPath = selectCellView.model.attributes.attrs['.btn.audioPlay'].audioPath;
        }
        audio.src = "";
        if(audioPath){
          audio.src = audioPath;
        }
      }
    } catch (err) {
    
    }
    // let label = document.getElementById("labelaudioPlayYYKBInfo");
    
    // // audioPlayStop();
    
    // let index = notesResData.findIndex(a => a.remark_id == selectCellView.model.attributes.attrs[".id"].text.replace("ID:",""));
    // if(notesResData[index].audioPath == null){
    //     // エラー処理
    //     label.innerText = `発言ID:${notesResData[index].remark_id}の音声ファイルがありません。`;
    
    // } else {
    //     label.innerText = "再生中の発言ID:" + notesResData[index].remark_id;
    //     audio.src = notesResData[index].audioPath;
    // }
    
    // // 音声再生中の枠付け
    // selectCellView.model.attributes.attrs[".card"].stroke = "#C06080";
    // selectCellView.model.attributes.attrs[".card"]['stroke-width'] = "3";
    // selectCellView.update();
    
  });
  
  // コメント編集アイコンクリック時の処理
  paper.on('element:commentEdit', function(cellView, evt){
    evt.stopPropagation();
    
    // テキスト直接入力対応 -->
    jointUiTextEditorClose(activeBoardId);
    // <-- テキスト直接入力対応
    
    addTextFromTextMenuBar(evt, paper);
    // マウスカーソルが復帰しないバグ対応 -->
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    releaseSelectToolbar();
    changeCursor();
    $('.joint-toolbar.joint-theme-modern button').removeClass('active bg-btn-active');
    $('.yyKnowledgeBoard-teamName .my-board-bar-button#btn-edit-team').removeClass('active');
    // <-- マウスカーソルが復帰しないバグ対応
    window_blur();
    
    // 選択状態にする
    let cells = [];
    cells.push(cellView.model);
    selectedObject(cells);
    
    // ロック状態にする。
    noteObjectLock(cells, true);
    
    // コメント入力欄表示
    if($('.text-edit-area-custom').hasClass('d-none')){
      addTextFromTextMenuBar(evt, paper);
      getTextEditedLine(evt, paper);
      $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').removeClass('bg-btn-active');
      // $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').click();
      // $('.input-wrapper .textarea').focus();
      addTextLabel_Click();
    }
  });
  
  // オブザーバーはここまで
  if(userInfo.role == 3){
    return paper;
  }
  
  // 発言内容エリアクリック
  paper.on('element:contents', function(elementView, evt){
    getTextEditedLine(evt, paper);
    let color = $(`#${elementView.id}`).find('.card').attr('fill');
    let colorKey = listColorSticky.indexOf(color);
    $('.joint-toolbar.joint-theme-modern button[data-name="changeColor"]').attr('class', `joint-widget joint-theme-modern color-${colorKey}`);
    
    // ロック解除
    // window_blur();
    let boardchange = false;
    if(activeBoardId != boardCtrlId){
      boardchange = true;
    }
    
    let parentCell = elementView.model.getParentCell();
    let embeddedCells = elementView.model.getEmbeddedCells();
    let group = false;
    if(parentCell != null || embeddedCells.length > 0){
      group = true;
    }
    let groupCheck = false;
    if(boardchange == true || (evt.shiftKey == false && evt.ctrlKey == false)){
      if(group == false){
        // window_blur();
        groupCheck = true;
      }
    } else {
      contents_onblur();
      noteObjectLock(undefined, false);
    }
    
    if(groupCheck){
      window_blur();
    } else {
      if(onseiNinshikiCommentStopFnc){
        onseiNinshikiCommentStopFnc(false);
      }
    }
    
    let graphIndex = getBoardsIndex(activeBoardId);
    let selectlist = getBoundaryCellList(graphIndex, 'all');
    let selectCount = selectlist.length;
    if(selectCount > OBJECT_SELECT_MAX - 1){
      // Toastify.warning(`一度に${OBJECT_SELECT_MAX}個以上選択出来ません。`, {
      //     showHideTransition : 'slide',
      //     position: 'top-center',
      // });
      return;
    }
    
    selectStickyNote = elementView.model;
    selectCell = elementView.model;
    
    if(boardEditMode == false){
      return;
    }
    
    // // 編集中の発言内容保存
    // contents_onblur();
    if(clickedCellView){
      if(clickedCellView.model.attributes.attrs[".objectId"].value !=
        elementView.model.attributes.attrs[".objectId"].value){
        document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id).style.cursor = "move";
        clickedCellView = undefined;
      }
    }
    
    
    if (clickedCellView) {
      
      if(timerClicked){
        clearTimeout(timerClicked);
      }
      
      evt.stopPropagation();
      
      // 選択状態にする
      let cells = [];
      cells.push(elementView.model);
      selectedObject(cells);
      
      // ロックかける
      let selectList = getBoundaryCellList(getBoardsIndex(activeBoardId) ,"all");
      noteObjectLock(selectList, true, elementView.model.attributes.attrs[".objectId"].value);
      
      // 選択中のelementViewと発言内容を保持する
      selectContentsCellInfo = {cell: elementView, contents_bef: document.getElementById(elementView.model.attributes.attrs[".contents"].id).innerText};
      
      // 編集中フラグON
      contentsEditFlg = true;
      
      let contentCtrl = document.getElementById(elementView.model.attributes.attrs[".contents"].id);
      if(contentCtrl){
        contentCtrl.style.cursor = "text";
      }
      
      // 付箋のプレースホルダ対応 -->
      contentCtrl.focus();
      // <-- 付箋のプレースホルダ対応
      
      // ロックする制限時間のタイマー起動
      textLockTimer = setTimeout(() => {
        
        // 編集中の発言内容保存
        contents_onblur();
        
        // ロック解除
        contentsEditFlg = false;
        noteObjectLock(undefined, false);
        // }, 5 * 60 * 1000);
      }, TEXT_LOCK_MAX_TIME * 1000);
      
      return;
    }
    
    clickedCellView = elementView;
    timerClicked = setTimeout(function () {
      clickedCellView = undefined;
    }, 300);
    
  });
  
  // paper.on('paper:mouseleave', function(evt, x, y){
  //     // ロック外す
  //     noteObjectLock(undefined, false);
  // });
  
  paper.on('blank:pointerclick', function(evt, x, y){
    
    // クリック処理
  });
  
  // 付箋がない箇所でのポインターダウンイベント
  paper.on('blank:pointerdown', async function(evt, x, y){
    textEditedLine = '';
    getTextEditedLine(evt, paper);
    
    addTextFromTextMenuBar(evt, paper);
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    $('.yyKnowledgeBoard-teamName .my-board-bar-button#btn-edit-team').removeClass('active');
    let boardchange = false;
    if(activeBoardId != boardCtrlId){
      boardchange = true;
    }
    
    activeBoardId = boardCtrlId;
    if(boardchange == true || (evt.shiftKey == false && evt.ctrlKey == false)){
      window_blur();
    } else {
      if(onseiNinshikiCommentStopFnc){
        onseiNinshikiCommentStopFnc(false);
      }
    }
    
    // ホイールでクリックした時は抜ける
    if(!(evt.button == 0 && evt.buttons == 1)){
      return;
    }
    
    // // ロック外す
    // noteObjectLock(undefined, false);
    
    // アクティブ表示用の枠を全ての付箋から消す
    // if(boardchange == true || (evt.shiftKey == false && evt.ctrlKey == false)){
    // paper.removeTools();
    // }
    
    // 編集中の発言内容保存
    contents_onblur();
    if(clickedCellView){
      document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id).style.cursor = "move";
      clickedCellView = undefined;
    }
    
    if(add_note_color != undefined){
      noteAddImg_Click(add_note_color, x, y);
      add_note_color = undefined;
    } else if(add_voice_note_color) {
      let param = {
        note_header_type: note_header_type,
        color: add_voice_note_color,
        x: x,
        y: y
      }
      onseininshikiStickyNote.add(param);
      add_voice_note_color = undefined;
    }
    
    // 矩形が既にあれば削除
    let graphIndex = getBoardsIndex(evt.currentTarget.id);
    if(graphIndex == -1){
      return;
    }
    
    let rect_list = boards[graphIndex].graph.attributes.cells.models.filter((val) => {
      if(val.attributes.type != CELL_TYPE_RECT ){
        return;
      }else if(val.attributes.attrs.body.fill == TRANSPARENT) {
        return;
      }else if(val.attributes.attrs.body.opacity == null){
        return;
      }
      return  val.attributes.type == CELL_TYPE_RECT;
    });
    document.getElementById(ID_REACTION_STICKY_NOTE + boards[graphIndex].id).style.display = "none";
    document.getElementById(ID_CHANGE_COLOR_STICKY_NOTE + boards[graphIndex].id).style.display = "none";
    
    let rect_list_len = rect_list.length;
    for(let i = 0; i < rect_list_len; i++){
      let cellView = paper.findViewByModel(rect_list[i]);
      let childCell_list  =  rect_list[i].getEmbeddedCells();
      let childCell_list_len = childCell_list.length;
      for(let n = 0; n < childCell_list_len; n++){
        rect_list[i].unembed(childCell_list[n]);
      }
      cellView.model.remove();//dell yamada
    }

//CHG ST ymd
    
    if(toolbarFlag != TOOLBAR_UNSELECTED){
      changeBoardEditMode(true);
      boards[graphIndex].paperScroller.stopPanning(evt, x, y);
    }
    
    if(boardEditMode){
      
      if(toolbarFlag == TOOLBAR_UNSELECTED && figureFlag == FIGURE_MENU_BAR_UNSELECTED)
      {
        // 矩形作成
        let data = evt.data = {};
        let cell;
        cell = new joint.shapes.standard.Rectangle();
        cell.attr('body/fill', '#0000FF');
        cell.attr('body/opacity', '0.1');
        cell.attr('body/stroke', '#000000');
        //Change START 20201023 Requa問題点ID：314
        //cell.attr('body/strokeWidth', '3');
        cell.attr('body/strokeWidth', LINE_THICKNESS_DEFAULT);
        //Change END 20201023
        cell.position(x, y);
        cell.attributes.subtype = CELL_SUBTYPE_SELECT_RECT;
        data.x = x;
        data.y = y;
        boards[graphIndex].graph.addCell(cell);
        data.cell = cell;
      }else if(toolbarFlag == TOOLBAR_TEXT_SELECT){
        //テキスト
        let data = evt.data = {};
        let param = {
          x: x,
          y: y,
          width: 120,
          height: 80,
          text: 'text'
        };
        let res = await createTextCell(param);
        
        data.x = x;
        data.y = y;
        data.cell = res.cell;
        
        AddObject(res.sendData, res.cell);
        
      }else if(toolbarFlag == TOOLBAR_LINK_SELECT){
        
        var connector_type;
        let strokeDasharrayValue = getStrokeDasharrayValue();
        
        if(lineTypeFlag ==　LINE_TYPE_STRAIGHT){
          
          smooth_type = false;
          connector_type = CONNECTOR_TYPE_STRAIGHT;
          
        }else if(lineTypeFlag == LINE_TYPE_CURVE){
          
          smooth_type = true;
          connector_type = CONNECTOR_TYPE_CURVE;
          
        }else{//lineTypeFlag == LINE_TYPE_KAGI
          
          smooth_type = false;
          connector_type = CONNECTOR_TYPE_KAGI;
        }
        
        let data = evt.data = {};
        let param = {
          smooth_type: smooth_type,
          connector_type: connector_type,
          strokeDasharrayValue: strokeDasharrayValue,
        }
        
        let res = await createLinkCell(param);
        
        data.x = x;
        data.y = y;
        data.cell = res.cell;
        
        AddObject(res.sendData, res.cell);
        
      }else if(toolbarFlag == TOOLBAR_FIGURE_SELECT){
        if(figureFlag == FIGURE_MENU_BAR_RECTANGLE || figureFlag == FIGURE_MENU_BAR_RECTANGLE_FILL){
          let data = evt.data = {};
          let fill = "";
          if(figureFlag == FIGURE_MENU_BAR_RECTANGLE){
            fill = TRANSPARENT;
          }else{
            fill = '#000000';
          }
          
          let param = {
            x: x - shapeDefault.rect.width,
            y: y - shapeDefault.rect.height,
            width: shapeDefault.rect.width,
            height: shapeDefault.rect.height,
            fill: fill,
            stroke: "#000000",
            strokeWidth: `${shapeDefault.strokeWidth}`
          };
          
          let res = await createRectangleCell(param);
          data.x = x;
          data.y = y;
          data.cell = res.cell;
          AddObject(res.sendData, res.cell);
          
          figureFlag = FIGURE_MENU_BAR_UNSELECTED;
          
        }else if(figureFlag == FIGURE_MENU_BAR_CIRCLE || figureFlag == FIGURE_MENU_BAR_CIRCLE_FILL){
          let data = evt.data = {};
          let cell;
          let bbox = new g.Rect(data.x, data.y, x - data.x, y - data.y);
          var constraint = g.ellipse(bbox);
          cell = new joint.shapes.basic.Circle();
          if(figureFlag == FIGURE_MENU_BAR_CIRCLE){
            cell.attr('circle/fill', TRANSPARENT);
          }else{
            cell.attr('circle/fill', '#000000');
          }
          cell.attr('circle/stroke', '#000000');
          cell.attr('circle/strokeWidth', '3');
          cell.position(constraint.intersectionWithLineFromCenterToPoint(g.point(x, y)).offset(-10, -10));
          data.x = x;
          data.y = y;
          boards[graphIndex].graph.addCell(cell);
          data.cell = cell;
        }else if (shapeFlag == SHAPE_MENU_BAR_STAR){
          //Star
          let data = evt.data = {};
          
          var cell = new joint.shapes.standard.Path();
          cell.attr('root/title', 'joint.shapes.standard.Path');
          cell.attr('body/fill', TRANSPARENT);
          cell.attr('body/stroke', '#000000');
          cell.attr('body/refD', normalizeMarker('M14.615,4.928c0.487-0.986,1.284-0.986,1.771,0l2.249,4.554c0.486,0.986,1.775,1.923,2.864,2.081l5.024,0.73c1.089,0.158,1.335,0.916,0.547,1.684l-3.636,3.544c-0.788,0.769-1.28,2.283-1.095,3.368l0.859,5.004c0.186,1.085-0.459,1.553-1.433,1.041l-4.495-2.363c-0.974-0.512-2.567-0.512-3.541,0l-4.495,2.363c-0.974,0.512-1.618,0.044-1.432-1.041l0.858-5.004c0.186-1.085-0.307-2.6-1.094-3.368L3.93,13.977c-0.788-0.768-0.542-1.525,0.547-1.684l5.026-0.73c1.088-0.158,2.377-1.095,2.864-2.081L14.615,4.928z'));
          cell.position(x, y);
          data.x = x;
          data.y = y;
          boards[graphIndex].graph.addCell(cell);
          data.cell = cell;
          
        }else if (shapeFlag == SHAPE_MENU_BAR_TRIANGLE){
          //三角形
          let data = evt.data = {};
          let cell = new joint.shapes.standard.Path();
          cell.attr('body/refD', normalizeMarker('M250,10 L200,100 L300,100 Z'));
          cell.attr('body/fill', TRANSPARENT);//'#F08080'
          cell.attr('body/stroke', '#000000');// '#CD5C5C'
          cell.attr('body/stroke-width', 4);
          cell.position(x, y);
          data.x = x;
          data.y = y;
          boards[graphIndex].graph.addCell(cell);
          data.cell = cell;
        }else if (shapeFlag == SHAPE_MENU_BAR_HEART){
          //heart
          let data = evt.data = {};
          let cell = new joint.shapes.standard.Path();
          cell.attr('body/refD', normalizeMarker('M20 70 A20 25 0 1 1 50 40 A20 25 0 1 1 80 70 L50,110 Z'));
          cell.attr('body/fill', TRANSPARENT);   //'#FF0000'
          cell.attr('body/stroke', '#000000'); //'#CD5C5C'
          cell.attr('body/stroke-width', 4);
          cell.position(x, y);
          data.x = x;
          data.y = y;
          boards[graphIndex].graph.addCell(cell);
          data.cell = cell;
        }else if (shapeFlag == SHAPE_MENU_BAR_CHAT_BUBBLE){
          //吹き出し
          let data = evt.data = {};
          let cell = new joint.shapes.standard.Path();
          cell.attr('body/refD', normalizeMarker('M1,6 A6,6 0 0 1 6,1 L154,1 A6,6 0 0 1 160,6 L160,42 A6,6 0 0 1 154,48 L70,48 L80,60 L55,48 L6,48 A6,6 0 0 1 1,42  Z'));
          cell.attr('body/fill', TRANSPARENT);
          cell.attr('body/stroke', '#000000');
          cell.attr('body/stroke-width', 4);
          cell.position(x, y);
          data.x = x;
          data.y = y;
          boards[graphIndex].graph.addCell(cell);
          data.cell = cell;
        }else if(shapeFlag == SHAPE_MENU_BAR_CLOUD){
          //雲
          let data = evt.data = {};
          let cell = new joint.shapes.standard.Path();
          cell.attr('body/refD', normalizeMarker('d="M-21499,13641s-2.2-21.447,24-24,32,24,32,24,18.227-18.269,41.17-8.173S-21379,13665-21379,13665s17.533-5.526,24,16-16,32-16,32,11.135,23.069-8,32-32,0-32,0,3.09,22.7-22.83,23.827S-21467,13753-21467,13753s-23.5,14.117-38.83,7.827S-21523,13737-21523,13737s-15.121,6.028-24,0,0-24,0-24-18.447-4.307-16-24,24-16,24-16-7.148-23.733,8-32S-21499,13641-21499,13641Z"'));
          cell.attr('body/fill', TRANSPARENT);
          cell.attr('body/stroke', '#000000');
          cell.attr('body/stroke-width', 4);
          cell.position(x, y);
          data.x = x;
          data.y = y;
          boards[graphIndex].graph.addCell(cell);
          data.cell = cell;
        }
      }
    }
    // テキスト直接入力対応 -->
    jointUiTextEditorClose(evt.currentTarget.id);
    // <-- テキスト直接入力対応
    
    if(textEditerBef.objectId != undefined){
      
      let objIdx = boards[graphIndex].graph.attributes.cells.models.findIndex(a => {
        let res = false;
        if(a.attributes.attrs[".objectId"] != undefined){
          if(a.attributes.attrs[".objectId"].value == textEditerBef.objectId){
            res = true;
          }
        }
        return res;
      });
      
      if(objIdx != -1){
        let cellView = boards[graphIndex].paper.findViewByModel(boards[graphIndex].graph.attributes.cells.models[objIdx]);
        
        if(cellView.model.attributes.attrs.label.text != textEditerBef.text){
          
          let d = {
            kind: USER_OPERATION_EDIT,
            selectObjectId: cellView.model.attributes.attrs[".objectId"].value,
            cell : cellView.model
          }
          
          // オブジェクト情報保存
          addUserOperationLogs(d);
        }
      }
    }
    textEditerBef = {};

//CHG end ymd
  });
  
  paper.on('link:pointerdown', function(linkView, evt, x, y) {
    addTextFromTextMenuBar(evt, paper);
    // マウスカーソルが復帰しないバグ対応 -->
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    releaseSelectToolbar();
    changeCursor();
    $('.joint-toolbar.joint-theme-modern button').removeClass('active bg-btn-active');
    $('.yyKnowledgeBoard-teamName .my-board-bar-button#btn-edit-team').removeClass('active');
    // <-- マウスカーソルが復帰しないバグ対応
    $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"]').addClass('d-none');
    $('.lineMenubar-container .joint-widget.joint-theme-modern[data-name="addTextLabel').removeClass('active bg-btn-active');
    var tools;
    let boardchange = false;
    if(activeBoardId != boardCtrlId){
      boardchange = true;
    }
    
    activeBoardId = boardCtrlId;
    
    let graphIndex = getBoardsIndex(activeBoardId);
    if(boardEditMode == false){
      evt.stopPropagation();
      boards[graphIndex].paperScroller.startPanning(evt, x, y);
      return;
    }
    
    if(clickedCellView){
      document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id).style.cursor = "move";
      clickedCellView = undefined;
    }
    
    // アクティブ表示用の枠を全ての付箋から消す
    if(boardchange == true || (evt.shiftKey == false && evt.ctrlKey == false)){
      // paper.removeTools();
      window_blur();
    } else {
      if(onseiNinshikiCommentStopFnc){
        onseiNinshikiCommentStopFnc(false);
      }
    }
    
    // ロックかける
    if(linkView._toolsView == undefined){
      // let graphIndex = getBoardsIndex(activeBoardId);
      let selectlist = getBoundaryCellList(graphIndex, 'all');
      let selectCount = selectlist.length;
      if(selectCount > OBJECT_SELECT_MAX - 1){
        Toastify.warning(`一度に${OBJECT_SELECT_MAX}個以上選択出来ません。`, {
          showHideTransition : 'slide',
          position: 'top-center',
        });
        return;
      }
    }
    
    if(evt.shiftKey == true || evt.ctrlKey == true){
      
      if(linkView._toolsView == undefined){
        //矢印メニュー表示
        // setDisplayNone(ID_SHAPE_SUB_MENUBAR, activeBoardId);
        // setDisplayNone(ID_TEXT_MENUBAR, activeBoardId);
        // setDisplayNone(ID_FUSEN_SUB_MENUBAR, activeBoardId);
        menuDisplayDisabled(activeBoardId);
        setDisplay(ID_LINE_MENUBAR, activeBoardId);
      } else {
        // cellView._toolsView.tools[0].options.vertexAdding = false;
        // cellView.update();
        menuDisplayDisabled(activeBoardId);
        
      }
    } else {
      // setDisplayNone(ID_SHAPE_SUB_MENUBAR, activeBoardId);
      // setDisplayNone(ID_TEXT_MENUBAR, activeBoardId);
      // setDisplayNone(ID_FUSEN_SUB_MENUBAR, activeBoardId);
      menuDisplayDisabled(activeBoardId);
      setDisplay(ID_LINE_MENUBAR, activeBoardId);
    }
    selectCell = linkView.model;
    noteBoundary(linkView);
    toolsViewDisp(linkView);
    
    let selectList = getBoundaryCellList(graphIndex, "all");
    if(selectList.length > 0){
      noteObjectLock(selectList, true, linkView.model.attributes.attrs[".objectId"].value);
    } else {
      noteObjectLock(undefined, false);
    }
    
    // 矢印の状態を取得
    if(linkView.model.attributes.attrs.line.targetMarker.stroke == TRANSPARENT &&
      linkView.model.attributes.attrs.line.targetMarker.fill == TRANSPARENT){
      lineRightFlag = LINE_CONNECT_STRAIGHT;
    } else {
      lineRightFlag = LINE_CONNECT_ARROW;
    }
    
    if(linkView.model.attributes.attrs.line.sourceMarker.stroke == TRANSPARENT &&
      linkView.model.attributes.attrs.line.sourceMarker.fill == TRANSPARENT){
      lineLeftFlag = LINE_CONNECT_STRAIGHT;
    } else {
      lineLeftFlag = LINE_CONNECT_ARROW;
    }
    
    // zインデックス更新
    let index = boards[graphIndex].graph.attributes.cells.models.findIndex(a => a.id == linkView.model.id);
    let z_max = boards[graphIndex].graph.maxZIndex();
    if(boards[graphIndex].graph.attributes.cells.models[index].attributes.z != z_max || z_max == 0){
      boards[graphIndex].graph.attributes.cells.models[index].toFront();
    }
    
    // 変更前情報取得
    let source = undefined;
    let target = undefined;
    let vertices = [];
    
    // ソース側のデータを取得
    if(linkView.model.attributes.source.x != undefined && linkView.model.attributes.source.y != undefined){
      source = {
        x : linkView.model.attributes.source.x,
        y : linkView.model.attributes.source.y
      }
    } else {
      source = {
        id : linkView.model.attributes.source.id,
        selector : linkView.model.attributes.source.selector
      }
    }
    
    // ターゲット側のデータを取得
    if(linkView.model.attributes.target.x != undefined && linkView.model.attributes.target.y != undefined){
      target = {
        x : linkView.model.attributes.target.x,
        y : linkView.model.attributes.target.y
      }
    } else {
      target = {
        id : linkView.model.attributes.target.id,
        selector : linkView.model.attributes.target.selector
      }
    }
    
    // データ格納
    moveLinkBefore = {
      source: source,
      target: target,
      vertices: vertices
    };
    let lineMenuBarName = ID_LINE_MENUBAR + activeBoardId;
    let lineMenuChildrenList = document.getElementById(lineMenuBarName).children[0].children;
    textEditedLine = lineMenuChildrenList[0].children[8].childNodes[1].firstElementChild.value;
    
  });
  
  paper.on('link:pointermove', function(linkView, evt) {
    // menuDisplayDisabled(activeBoardId);
  });
  
  
  paper.on('link:pointerup', function(linkView) {
    if(boardEditMode == false){
      return;
    }
    
    // Add start 20201111 kobayashi
    if(linkView._toolsView != undefined){
      setDisplay(ID_LINE_MENUBAR, activeBoardId);
      // noteBoundary(linkView);
      toolsViewDisp(linkView);
    }
    // Add end 20201111
    let moveLinkAfter = undefined;
    
    let source = undefined;
    let target = undefined;
    let vertices = [];
    let error_move = false;
    let board_width =  Number(paper.options.width);
    let board_height =  Number(paper.options.height);
    
    // ソース側のデータを取得
    if(linkView.model.attributes.source.x != undefined && linkView.model.attributes.source.y != undefined){
      source = {
        x : linkView.model.attributes.source.x,
        y : linkView.model.attributes.source.y
      }
      
      // 領域外の場合はエラーとする
      if(source.x < 0
        || source.y < 0
        || source.x > board_width
        || source.y > board_height){
        error_move = true;
      }
      
    } else {
      source = {
        id : linkView.model.attributes.source.id,
        selector : linkView.model.attributes.source.selector
      }
    }
    
    // ターゲット側のデータを取得
    if(linkView.model.attributes.target.x != undefined && linkView.model.attributes.target.y != undefined){
      target = {
        x : linkView.model.attributes.target.x,
        y : linkView.model.attributes.target.y
      }
      
      // 領域外の場合はエラーとする
      if(target.x < 0
        || target.y < 0
        || target.x > board_width
        || target.y > board_height){
        error_move = true;
      }
    } else {
      target = {
        id : linkView.model.attributes.target.id,
        selector : linkView.model.attributes.target.selector
      }
    }
    
    // データ格納
    moveLinkAfter = {
      source: source,
      target: target,
      vertices: vertices
    };
    
    
    // 差分チェック
    let update = false;
    
    if(moveLinkBefore){
      
      // source
      if(moveLinkAfter.source.id != undefined){ // id
        
        if(moveLinkBefore.source.id == undefined){
          update = true;
        } else if(moveLinkAfter.source.id != moveLinkBefore.source.id ){
          update = true;
        }
        
      } else { // 座標
        
        if(moveLinkBefore.source.id != undefined){
          update = true;
        } else if(moveLinkAfter.source.x != moveLinkBefore.source.x ||
          moveLinkAfter.source.y != moveLinkBefore.source.y){
          update = true;
        }
      }
      
      // target
      if(moveLinkAfter.target.id != undefined){ // id
        
        if(moveLinkBefore.target.id == undefined){
          update = true;
        } else if(moveLinkAfter.target.id != moveLinkBefore.target.id ){
          update = true;
        }
        
      } else { // 座標
        
        if(moveLinkBefore.target.id != undefined){
          update = true;
        } else if(moveLinkAfter.target.x != moveLinkBefore.target.x ||
          moveLinkAfter.target.y != moveLinkBefore.target.y){
          update = true;
        }
      }
      
      // vertices
      let v_len = moveLinkAfter.vertices.length;
      if(moveLinkAfter.vertices.length == moveLinkBefore.vertices.length){
        for(let i = 0; i > v_len; i++){
          if(moveLinkAfter.vertices[i].x != moveLinkBefore.vertices[i].x){
            update = true;
          }
          if(moveLinkAfter.vertices[i].y != moveLinkBefore.vertices[i].y){
            update = true;
          }
        }
      } else {
        update = true;
      }
      
    } else {
      update = true;
    }
    
    if(update){
      addUserOperationLogLink(USER_OPERATION_LINK_MOVE, linkView.model, moveLinkBefore, moveLinkAfter);
    }
    joint.ui.FreeTransform.clear(paper);
    // // 領域外に線がある場合は元の位置に戻す
    // if(error_move){
    //     // redoUndo_LineMove(linkView.model.cid, moveLinkBefore.source, moveLinkBefore.target);
    
    // } else {
    //     addUserOperationLogLink(USER_OPERATION_LINK_MOVE, linkView, moveLinkBefore, moveLinkAfter);
    // }
    
  });
  
  paper.on('link:mouseleave', function(linkView) {
    //linkView.removeTools();
  });
  
  // 付箋がない箇所でのポインタームーブイベント
  paper.on('blank:pointermove', function(evt, x, y) {
    // ホイールでクリックした時は抜ける
    if(!(evt.button == 0 && evt.buttons == 1)){
      
      return;
    }
    
    if(evt.data.cell == undefined){
      return;
    }
    
    // 矩形のサイズ調整
    let data = evt.data;
    let cell = data.cell;
    //CHG YMD
    if (cell.isLink()) {
      let bbox = new g.Rect(data.x, data.y, x - data.x, y - data.y);
      bbox.normalize();
      
      if(cell.attributes.source.x == undefined){
        cell.set({
          target:({ x: x, y: y }),
          source:({ x: bbox.x, y: bbox.y })
        });
      }else{
        cell.set({
          target:({ x: x, y: y })
        });
      }
    } else {
      //CHG END YMD
      if(cell.attributes.subtype == CELL_SUBTYPE_SELECT_RECT){
        
        let bbox = new g.Rect(data.x, data.y, x - data.x, y - data.y);
        bbox.normalize();
        // cell.set({
        //     position: { x: bbox.x - shapeDefault.rect.width, y: bbox.y - shapeDefault.rect.height},
        //     size: { width: Math.max(bbox.width, 1), height: Math.max(bbox.height, 1) },
        // });
        cell.set({
          position: { x: bbox.x, y: bbox.y},
          size: { width: Math.max(bbox.width, 1), height: Math.max(bbox.height, 1) },
        });
      } else {
        let bbox = new g.Rect(data.x, data.y, x - data.x + shapeDefault.rect.width, y - data.y + shapeDefault.rect.height);
        bbox.normalize();
        cell.set({
          position: { x: bbox.x - shapeDefault.rect.width, y: bbox.y - shapeDefault.rect.height},
          size: { width: Math.max(bbox.width, 1), height: Math.max(bbox.height, 1) },
        });
        changeingCell = cell;
      }
      
      //CHG ymd
    }
  });
  
  // 付箋がない箇所でのポインターアップイベント
  paper.on('blank:pointerup', async function(evt, x, y) {
    let graphIndex = getBoardsIndex(activeBoardId);
    
    //add ymd
    clearFlag();
    setDisplayNone(ID_FUSEN_MENUBAR,activeBoardId);
    setDisplayNone(ID_ADD_LINE_MENU,activeBoardId);
    setDisplayNone(ID_ADD_FIGURE_MENUBAR,activeBoardId);
    // jsonSave(evt.data.cell);
    
    if(evt.data.cell == undefined){
      return;
    }
    let cell = evt.data.cell;
    
    menuDisplayDisabled(boards[graphIndex].id);
    
    if(cell.attributes.subtype != CELL_SUBTYPE_SELECT_RECT){
      if(!cell.isLink()){
        let cells = [];
        cells.push(cell);
        selectedObject(cells);
      } else {
        setDisplay(ID_LINE_MENUBAR, activeBoardId);
        
        let cv = boards[graphIndex].paper.findViewByModel(cell);
        
        // 無ければ描画されるまで待つ
        while(cv == undefined){
          await new Promise((resolve) => {
            setTimeout(() => resolve(), 100);
          });
          cv = boards[graphIndex].paper.findViewByModel(cell)
        }
        
        if(cv._toolsView){
          let len = cv._toolsView.el.children.length;
          for(let i = 0; i < len; i++){
            let ctrl = cv._toolsView.el.children[i];
            if(ctrl.tagName == "rect"){
              
              if(ctrl.x.baseVal.value < 0){
                ctrl.x.baseVal.value = x;
                ctrl.width.baseVal.value = 0;
                ctrl.height.baseVal.value = 0;
              }
              if(ctrl.y.baseVal.value < 0){
                ctrl.y.baseVal.value = y;
                ctrl.width.baseVal.value = 0;
                ctrl.height.baseVal.value = 0;
              }
            }
          }
          toolsViewDisp(cv);
        }
      }
    }
    
    // 矩形の座標セット
    if (cell.isLink()){
      setTimeout(() => {
        if(cell.attributes.target.x == undefined &&
          cell.attributes.target.y == undefined &&
          cell.attributes.target.id == undefined){
          
          cell.attributes.target.x = x + LINE_OBJECT_DEFAULT_SIZE.WIDTH;
          cell.attributes.target.y = y + LINE_OBJECT_DEFAULT_SIZE.HEIGHT;
          
        }
        if(cell.attributes.source.x == undefined &&
          cell.attributes.source.y == undefined &&
          cell.attributes.source.id == undefined){
          
          cell.attributes.source.x = x;
          cell.attributes.source.y = y;
          
        }
        
        // 線オブジェクトの最小値設定対応 -->
        let result = calcStandardLinkSize(cell);
        cell = result.cell;
        // <-- 線オブジェクトの最小値設定対応
        let linkView = boards[graphIndex].paper.findViewByModel(cell);
        linkView.update();
        
        addUserOperationLogLink(USER_OPERATION_LINK_MOVE, cell, undefined, undefined);
      }, 200);
      return;
    };
    
    let scale = boards[graphIndex].paperScroller.zoom();
    let start_x = evt.data.x * scale;
    let start_y = evt.data.y * scale;
    let end_x = x * scale;
    let end_y = y * scale;
    
    // 値が小さいほうをStartにする
    if(start_x > end_x){
      let t_x = start_x;
      start_x = end_x;
      end_x = t_x;
    }
    if(start_y > end_y){
      let t_y = start_y;
      start_y = end_y;
      end_y = t_y;
    }
    
    // オフセット分拡張する
    let offset = 2;
    start_x -= offset;
    start_y -= offset;
    end_x += offset;
    end_y += offset;
    
    
    if(cell.attributes.subtype == CELL_SUBTYPE_SELECT_RECT){
      //　矩形削除
      try {
        userOperationLogAddLock = true;
        if(graphIndex != -1){
          let rectView = boards[graphIndex].paper.findViewByModel(cell);
          rectView.model.remove();
        }
        userOperationLogAddLock = false;
        
        let lockDivs = document.getElementsByClassName("object-lock");
        let lockDivs_len = lockDivs.length;
        
        // chg start 20201019 kobayashi
        let removeList = boards[graphIndex].graph.attributes.cells.models.filter((val) => {
          let ret = false;
          if(val.attributes.type == CELL_TYPE_NOTE){
            
            // if(lockObjectIds.indexOf(val.attributes.attrs[".objectId"].value) != -1){
            //     return false;
            // }
            
            if(val.attributes.position.x * scale > start_x && end_x > val.attributes.position.x * scale &&
              val.attributes.position.y * scale > start_y && end_y > val.attributes.position.y * scale &&
              (val.attributes.position.x + val.attributes.size.width) * scale > start_x && end_x > (val.attributes.position.x + val.attributes.size.width) * scale &&
              (val.attributes.position.y + val.attributes.size.height) * scale > start_y && end_y > (val.attributes.position.y + val.attributes.size.height) * scale ){
              ret = true;
            }
            
            // ロック表示内にあるかチェック
            for(let i = 0; i < lockDivs_len; i++){
              if(lockDivs[i].style.display == "none"){
                continue;
              }
              let lockdiv_start_x = Number(lockDivs[i].style.left.replace("px",""));
              let lockdiv_start_y = Number(lockDivs[i].style.top.replace("px",""));
              let lockdiv_end_x = lockdiv_start_x + lockDivs[i].clientWidth;
              let lockdiv_end_y = lockdiv_start_y + lockDivs[i].clientHeight;
              
              if(val.attributes.position.x * scale > lockdiv_start_x && lockdiv_end_x > val.attributes.position.x * scale &&
                val.attributes.position.y * scale > lockdiv_start_y && lockdiv_end_y > val.attributes.position.y * scale &&
                (val.attributes.position.x + val.attributes.size.width) * scale > lockdiv_start_x && lockdiv_end_x > (val.attributes.position.x + val.attributes.size.width) * scale &&
                (val.attributes.position.y + val.attributes.size.height) * scale  > lockdiv_start_y && lockdiv_end_y > (val.attributes.position.y + val.attributes.size.height) * scale ){
                ret = false;
              }
            }
            
          } else if(val.attributes.type == CELL_TYPE_RECT || val.attributes.type == CELL_TYPE_IMAGE){
            
            // if(lockObjectIds.indexOf(val.attributes.attrs[".objectId"].value) != -1){
            //     return false;
            // }
            
            if(val.attributes.position.x * scale > start_x && end_x > val.attributes.position.x * scale &&
              val.attributes.position.y * scale > start_y && end_y > val.attributes.position.y * scale &&
              (val.attributes.position.x + val.attributes.size.width) * scale > start_x && end_x > (val.attributes.position.x + val.attributes.size.width) * scale &&
              (val.attributes.position.y + val.attributes.size.height) * scale > start_y && end_y > (val.attributes.position.y + val.attributes.size.height) * scale){
              ret = true;
            }
            
            // ロック表示内にあるかチェック
            for(let i = 0; i < lockDivs_len; i++){
              if(lockDivs[i].style.display == "none"){
                continue;
              }
              let lockdiv_start_x = Number(lockDivs[i].style.left.replace("px",""));
              let lockdiv_start_y = Number(lockDivs[i].style.top.replace("px",""));
              let lockdiv_end_x = lockdiv_start_x + lockDivs[i].clientWidth;
              let lockdiv_end_y = lockdiv_start_y + lockDivs[i].clientHeight;
              
              if(val.attributes.position.x * scale > lockdiv_start_x && lockdiv_end_x > val.attributes.position.x * scale &&
                val.attributes.position.y * scale > lockdiv_start_y && lockdiv_end_y > val.attributes.position.y * scale &&
                (val.attributes.position.x + val.attributes.size.width) * scale > lockdiv_start_x && lockdiv_end_x > (val.attributes.position.x + val.attributes.size.width) * scale &&
                (val.attributes.position.y + val.attributes.size.height) * scale > lockdiv_start_y && lockdiv_end_y > (val.attributes.position.y + val.attributes.size.height) * scale ){
                ret = false;
              }
            }
          }
          return ret;
        });
        
        let removeList_Link = boards[graphIndex].graph.attributes.cells.models.filter((val) => {
          
          if(val.attributes.type == CELL_TYPE_LINK || val.attributes.type == CELL_TYPE_AREALINE || val.attributes.type == CELL_TYPE_STANDARD_LINK){
            
            let ret = false;
            
            // if(lockObjectIds.indexOf(val.attributes.attrs[".objectId"].value) != -1){
            //     return false;
            // }
            
            // source
            let ret_source = false;
            if(val.attributes.source.x != undefined && val.attributes.source.y != undefined){
              
              if(val.attributes.source.x  * scale > start_x && end_x > val.attributes.source.x  * scale &&
                val.attributes.source.y * scale > start_y && end_y > val.attributes.source.y * scale){
                ret_source = true;
              }
              
            } else if(val.attributes.source.id != undefined){
              
              if(removeList.findIndex(a => a.id == val.attributes.source.id) != -1){
                ret_source = true;
              }
            }
            
            // target
            let ret_target = false;
            if(val.attributes.target.x != undefined && val.attributes.target.y != undefined){
              
              if(val.attributes.target.x * scale > start_x && end_x > val.attributes.target.x * scale &&
                val.attributes.target.y * scale > start_y && end_y > val.attributes.target.y * scale){
                ret_target = true;
              }
              
            } else if(val.attributes.target.id != undefined){
              
              if(removeList.findIndex(a => a.id == val.attributes.target.id) != -1){
                ret_target = true;
              }
            }
            
            // ロック表示内にあるかチェック
            for(let i = 0; i < lockDivs_len; i++){
              if(lockDivs[i].style.display == "none"){
                continue;
              }
              let lockdiv_start_x = Number(lockDivs[i].style.left.replace("px",""));
              let lockdiv_start_y = Number(lockDivs[i].style.top.replace("px",""));
              let lockdiv_end_x = lockdiv_start_x + lockDivs[i].clientWidth;
              let lockdiv_end_y = lockdiv_start_y + lockDivs[i].clientHeight;
              let source_lock_out = true;
              let target_lock_out = true;
              
              if(val.attributes.source.x != undefined && val.attributes.source.y != undefined){
                if(val.attributes.source.x * scale > lockdiv_start_x && lockdiv_end_x > val.attributes.source.x * scale &&
                  val.attributes.source.y * scale > lockdiv_start_y && lockdiv_end_y > val.attributes.source.y * scale){
                  source_lock_out = false;
                }
              }
              
              if(val.attributes.target.x != undefined && val.attributes.target.y != undefined){
                if(val.attributes.target.x * scale > lockdiv_start_x && lockdiv_end_x > val.attributes.target.x * scale &&
                  val.attributes.target.y * scale > lockdiv_start_y && lockdiv_end_y > val.attributes.target.y * scale){
                  target_lock_out = false;
                }
              }
              
              // 始点も終点もロック表示に入っていたら選択対象外にする。
              if(source_lock_out == false && target_lock_out == false){
                ret_source = false;
                ret_target = false;
              }
            }
            
            // 始点と終点両方対象である時のみ抽出
            if(ret_source == true && ret_target == true){
              ret = true;
            }
            
            return ret;
          }
        });
        
        let selectCount = removeList_Link.length + removeList.length;
        
        // 1度に選択出来る画像の上限設定 -->
        let selectImage = removeList.filter((val) => {
          let res = false;
          if(val.attributes.type == CELL_TYPE_IMAGE){
            res = true;
          }
          return res;
        });
        let imageCount = selectImage.length;
        // <-- 1度に選択出来る画像の上限設定
        
        if(selectCount > OBJECT_SELECT_MAX){
          Toastify.warning(`一度に${OBJECT_SELECT_MAX}個以上選択出来ません。`, {
            showHideTransition : 'slide',
            position: 'top-center',
          });
          return;
          
          // 1度に選択出来る画像の上限設定 -->
        } else if(imageCount > OBJECT_SELECT_IMAGE_MAX){
          Toastify.warning(`一度に${OBJECT_SELECT_IMAGE_MAX}個以上画像を選択出来ません。`, {
            showHideTransition : 'slide',
            position: 'top-center',
          });
          return;
        }
        // <-- 1度に選択出来る画像の上限設定
        
        let noteBoundarys = async function(list){
          
          let list_len = list.length;
          for(let i = 0; i < list_len; i++){
            let cellView = paper.findViewByModel(list[i]);
            noteBoundary(cellView);
          }
        };
        
        await noteBoundarys(removeList_Link);
        await noteBoundarys(removeList);
        
        let selectlist = getBoundaryCellList(graphIndex, 'all');
        if(selectlist.length == 0){
          noteObjectLock(undefined, false);
        } else {
          noteObjectLock(selectlist, true, selectlist[0].attributes.attrs[".objectId"].value);
          
          // 範囲選択時にメニューバー表示
          let cv = paper.findViewByModel(selectlist[0]);
          if(cv._toolsView != undefined){
            cv.removeTools();
          }
          let array = [];
          array.push(selectlist[0]);
          selectedObject(array);
          
          // グループ化解除
          if(old_cellview != undefined){
            let embedList = old_cellview.model.getEmbeddedCells();
            let len = embedList.length;
            for(let i = 0; i < len; i++){
              if(embedList[i]){
                old_cellview.model.unembed(embedList[i]);
              }
            }
          }
          
          // 親になるオブジェクトがグループ化されていたら解除
          let cellView = boards[graphIndex].paper.findViewByModel(selectlist[0]);
          let pC = cellView.model.getParentCell();
          if(pC){
            let eL = pC.getEmbeddedCells();
            let len = eL.length;
            for(let i = 0; i < len; i++){
              if(eL[i]){
                pC.unembed(eL[i]);
              }
            }
          }
          
          let selectlist_len = selectlist.length;
          for(let i = 0; i < selectlist_len; i++){
            if(i != 0){
              if(cellView.model !== selectlist[i]){
                cellView.model.embed(selectlist[i]);
              }
            }
          }
          old_cellview = cellView
        }
        
      } catch (err) {
        writeYYKnowledgeBoardErrorLog(err);
      }
    } else {
      if(cell.attributes.attrs[".objectId"]){
        let logObj = {
          kind: USER_OPERATION_MOVE,
          selectObjectId : cell.attributes.attrs[".objectId"].value,
          cell : cell
        }
        addUserOperationLogs(logObj);
      }
    }
  });
  
  // 付箋：ポインターダウンイベント
  paper.on('element:pointerdown', function(cellView,evt,x,y){
    addTextFromTextMenuBar(evt, paper);
    getTextEditedLine(evt, paper);
    releaseSelectToolbar();
    // マウスカーソルが復帰しないバグ対応 -->
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    changeCursor();
    // <-- マウスカーソルが復帰しないバグ対応
    $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').removeClass('bg-btn-active');
    let color = $(`#${cellView.id}`).find('.card').attr('fill');
    let colorKey = listColorSticky.indexOf(color);
    $('.joint-toolbar.joint-theme-modern button[data-name="changeColor"]').attr('class', `joint-widget joint-theme-modern color-${colorKey}`);
    let graphIndex = boards.findIndex(a => a.graph.cid == cellView.model.graph.cid);
    
    if(boardEditMode == false){
      evt.stopPropagation();
      boards[graphIndex].paperScroller.startPanning(evt, x, y);
      return;
    }
    
    let boardchange = false;
    if(activeBoardId != boardCtrlId){
      boardchange = true;
    }
    
    activeBoardId = boardCtrlId;
    
    if(clickedCellView){
      if(cellView.model.attributes.type != CELL_TYPE_NOTE){
        if(document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id)){
          document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id).style.cursor = "move";
        }
        clickedCellView = undefined;
      } else {
        if(clickedCellView.model.attributes.attrs[".objectId"].value !=
          cellView.model.attributes.attrs[".objectId"].value){
          
          if(document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id)){
            document.getElementById(clickedCellView.model.attributes.attrs[".contents"].id).style.cursor = "move";
          }
          clickedCellView = undefined;
        }
      }
    }
    
    // 一時的に追加するテンプレートの場合はリターンする
    if(cellView.model.attributes.kind == "template"){
      return;
    }
    
    // アクティブ表示用の枠を全ての付箋から消す
    let parentCell = cellView.model.getParentCell();
    let embeddedCells = cellView.model.getEmbeddedCells();
    let group = false;
    if(parentCell != null || embeddedCells.length > 0){
      group = true;
    }
    
    let groupCheck = false;
    if(boardchange == true || (evt.shiftKey == false && evt.ctrlKey == false)){
      if(group == false){
        // paper.removeTools();
        // window_blur();
        groupCheck = true;
      }
    }
    if(groupCheck){
      window_blur();
    } else {
      if(onseiNinshikiCommentStopFnc){
        onseiNinshikiCommentStopFnc(false);
      }
    }
    
    // ロックかける
    // noteObjectLock(cellView.model, true);
    
    // 編集中の発言内容保存
    contents_onblur();
    
    // 選択数チェック
    if (cellView._toolsView == undefined){
      let selectlist = getBoundaryCellList(graphIndex, 'all');
      let selectCount = selectlist.length;
      if(selectCount > OBJECT_SELECT_MAX - 1){
        Toastify.warning(`一度に${OBJECT_SELECT_MAX}個以上選択出来ません。`, {
          showHideTransition : 'slide',
          position: 'top-center',
        });
        return;
      }
      
      // 1度に選択出来る画像の上限設定 -->
      if(cellView.model.attributes.type == CELL_TYPE_IMAGE){
        
        let selectImage = selectlist.filter((val) => {
          let res = false;
          if(val.attributes.type == CELL_TYPE_IMAGE){
            res = true;
          }
          return res;
        });
        let imageCount = selectImage.length;
        if(imageCount > OBJECT_SELECT_IMAGE_MAX - 1){
          Toastify.warning(`一度に${OBJECT_SELECT_IMAGE_MAX}個以上画像を選択出来ません。`, {
            showHideTransition : 'slide',
            position: 'top-center',
          });
          return;
        }
      }
      // <-- 1度に選択出来る画像の上限設定
    }
    
    // グループ化解除
    if(old_cellview != undefined){
      let embedList = old_cellview.model.getEmbeddedCells();
      let len = embedList.length;
      for(let i = 0; i < len; i++){
        if(embedList[i]){
          old_cellview.model.unembed(embedList[i]);
        }
      }
    }
    
    moveBefore = undefined;
    moveBefore = {
      cellView: cellView,
      x: cellView.model.attributes.position.x,
      y: cellView.model.attributes.position.y,
      width : cellView.model.attributes.size.width,
      height : cellView.model.attributes.size.height,
      embedCells : []
    }
    
    // 付箋の中のZ軸のMAX値を探す
    let z_max = boards[graphIndex].graph.maxZIndex();
    
    // 同じZ値があるかチェック
    let index = boards[graphIndex].graph.attributes.cells.models.findIndex(a => a.id == cellView.model.id);
    let z_max_cells = boards[graphIndex].graph.attributes.cells.models.filter((val) => {
      return val.attributes.z == z_max;
    });
    
    // Z更新(MAX値と同値の時は更新しない)
    if(boards[graphIndex].graph.attributes.cells.models[index].attributes.z != z_max || z_max == 0 || z_max_cells.length > 1){
      boards[graphIndex].graph.attributes.cells.models[index].toFront();
    }
    
    // アクティブ表示用の枠を付箋に付ける。
    if(evt.shiftKey == true || evt.ctrlKey == true){
      noteBoundary(cellView);
      menuDisplayDisabled(boards[graphIndex].id);
    } else {
      if (cellView._toolsView == undefined){
        noteBoundary(cellView);
        
        // メニューバー表示
        // document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.display = "none";
        // document.getElementById(ID_TEXT_MENUBAR + boards[graphIndex].id).style.display = "none";
        // document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id).style.display = "none";
        // document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.display = "none";
        
      }
      menuDisplayDisabled(boards[graphIndex].id);
      setMenuBarPosition(cellView);
    }
    selectStickyNote = cellView.model;
    selectCell = cellView.model;
    
    
    // 親になるオブジェクトがグループ化されていたら解除
    let pc = cellView.model.getParentCell();
    if(pc){
      let eL = pc.getEmbeddedCells();
      let len = eL.length;
      for(let i = 0; i < len; i++){
        if(eL[i]){
          pc.unembed(eL[i]);
        }
      }
    }
    
    // 枠線付きの付箋をグループ化
    let selectList = getBoundaryCellList(graphIndex ,"all");
    let selectList_len = selectList.length;
    for(let i = 0; i < selectList_len; i++){
      
      let parentCell = selectList[i].getParentCell();
      if(parentCell){
        parentCell.unembed(selectList[i]);
      }
      
      if(cellView.model !== selectList[i]){
        cellView.model.embed(selectList[i]);
        // moveBefore.embedCells.push({
        //     cellView : paper.findViewByModel(selectList[i]),
        //     x: selectList[i].attributes.position.x,
        //     y: selectList[i].attributes.position.y,
        // });
      }
    }
    noteObjectLock(selectList, true, cellView.model.attributes.attrs[".objectId"].value);
    
    old_cellview = cellView;
    let textMenuBarName = ID_TEXT_MENUBAR + activeBoardId;
    let textMenuChildrenList = document.getElementById(textMenuBarName).children[0].children[1].children;
    oldTextArea = textMenuChildrenList[0].childNodes[1].firstElementChild.value;
    $('.text-edit-area-custom .textarea').css('height', '52px');
  });
  
  
  // 付箋：ポインターアップイベント
  paper.on('element:pointerup', function(cellView){
    
    if(boardEditMode == false){
      return;
    }
    
    // 一時的に追加するテンプレートの場合はリターンする
    if(cellView.model.attributes.kind == "template"){
      return;
    }
    
    //let halo = new joint.ui.Halo({ cellView: cellView });
    //halo.render();
    let graphIndex = boards.findIndex(a => a.graph.cid == cellView.model.graph.cid);
    
    if(graphIndex != -1){
      if(cellView._toolsView != undefined){
        menuDisplayDisabled(boards[graphIndex].id);
        
        // メニューバーの位置合わせ
        setMenuBarPosition(cellView);
      }
    }
    selectCell = cellView.model;
    
    if(moveBefore != undefined){
      
      // 座標位置が前回値と異なる場合のみ保存
      if(moveBefore.x != cellView.model.attributes.position.x ||
        moveBefore.y != cellView.model.attributes.position.y){
        
        // 選択中のオブジェクト全て取得
        let cells = getBoundaryCellList(graphIndex, "all");
        
        let len = moveBefore.embedCells.length;
        let after_embedCells = [];
        for(let i = 0; i < len; i++){
          
          // after_embedCells.push({
          //     cellView : moveBefore.embedCells[i].cellView,
          //     x : moveBefore.embedCells[i].cellView.model.attributes.position.x,
          //     y : moveBefore.embedCells[i].cellView.model.attributes.position.y
          // });
          
        }
        
        let logObj = {
          kind: USER_OPERATION_MOVE,
          cell : cells,
          // cell : cellView.model,
          selectObjectId: cellView.model.attributes.attrs[".objectId"].value,
          beforeData: {
            content : undefined,
            x : moveBefore.x,
            y : moveBefore.y,
            embedCells : moveBefore.embedCells
          },
          afterData: {
            content : undefined,
            x : cellView.model.attributes.position.x,
            y : cellView.model.attributes.position.y,
            embedCells : after_embedCells
          }
        }
        
        // 操作ログ情報追加処理
        addUserOperationLogs(logObj);
      }
    }
    
    // オブジェクトに繋がっている線のロックオブジェクトの位置を更新します
    try {
      
      if(cellView.model.isLink() == false){
        let lockDivs = document.getElementsByClassName("object-lock");
        let links = boards[graphIndex].graph.getConnectedLinks(cellView.model);
        let len = lockDivs.length;
        for(let i = 0; i < len; i++){
          if(lockDivs[i].style.display == "none"){
            continue;
          }
          if(lockDivs[i].value){
            let lockDivVal =  JSON.parse(lockDivs[i].value);
            // let lockDivUserCode = Json.parse(lockDivs[i].value);
            let links_len = links.length;
            let link_on = false;
            for(let n = 0; n < links_len; n++){
              // let index = lockDivs[i].lockObjectid.findIndex(a => a.attributes.attrs[".objectId"].value == links[i].attributes.attrs[".objectId"].value);
              let index = lockDivs[i].lockObjectid.indexOf(links[i].attributes.attrs[".objectId"].value);
              if(index != -1){
                link_on = true;
              }
            }
            
            // let index = links.findIndex(a => a.attributes.attrs[".objectId"].value == lockDivVal.objectId);
            if(link_on){
              reciveNoteObjectLock({
                lock: true,
                userCode: lockDivVal.userCode,
                userName: lockDivVal.userName,
                boardId: activeBoardId,
                objectIds: lockDivs[i].lockObjectid,
              });
            }
          }
          // let linkView =  boards[graphIndex].paper.findViewByModel(links[i]);
          // linkView.update();
        }
      }
      
    } catch (err) {
      writeYYKnowledgeBoardErrorLog(err);
    }
    
  });
  
  paper.on('cell:pointerdown', function(cellView, evt) {
    // テキスト直接入力対応 -->
    jointUiTextEditorClose(activeBoardId);
    // <-- テキスト直接入力対応
    getTextEditedLine(evt, paper);
  });
  
  //add ymd サイズ変更イベント
  paper.on('cell:pointerup', function(cellView, evt) {
    if(boardEditMode == false){
      return;
    }
    
    if(cellView.model.attributes.kind == "template"){
      return;
    }
    
    // We don't want to transform links.
    if (cellView.model instanceof joint.dia.Link) return;
    
    // let param = {
    //     cellView: cellView,
    //     allowRotation: false
    // };
    
    // if(cellView.model.attributes.type == CELL_TYPE_NOTE){
    //     param.minWidth = 150;
    //     param.minHeight = 75;
    
    //     if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
    //         param.preserveAspectRatio= true;
    //     }
    // };
    
    
    if(evt.shiftKey == true || evt.ctrlKey == true){
      
      let listeners = Object.entries(cellView.model._listeners).map(([key, value]) => (value));
      let len = listeners.length;
      let transformRemove = false;
      for(let i = 0; i < len; i++){
        let className = listeners[i].listener.el.className.toString();
        if(className.indexOf("joint-free-transform") != -1){
          transformRemove = true;
        }
      }
      if(transformRemove){
        joint.ui.FreeTransform.clear(paper);
        return;
      }
    }
    
    // let freeTransform = new joint.ui.FreeTransform(param);
    // //自由変形ウィジェットをレンダリング
    // freeTransform.render();
    if(cellView._toolsView != undefined){
      
      setFreeTransform(cellView);
      
      // // リサイズ開始イベント登録
      // $('.resize').on('pointerdown', function(){
      //     changeingCell = cellView.model;
      // });
    }
  });
  
  // テキスト直接入力対応 -->
  paper.on('element:pointerdblclick', function(elementView, evt) {
    
    if(boardEditMode == false){
      return;
    }
    
    if(elementView.model.attributes.type == CELL_TYPE_RECT){
      if(elementView.model.attributes.kind != OBJECT_KIND_TEXT){
        return;
      }
    } else {
      return;
    }
    
    if(elementView.model.attributes.attrs.label == undefined){
      return;
    }
    
    if( elementView.model.attributes.attrs[".objectId"] != undefined){
      
      textEditerBef = {
        objectId: elementView.model.attributes.attrs[".objectId"].value,
        befText: elementView.model.attributes.attrs.label.text
      };
    }
    
    contentsEditFlg = true;
    joint.ui.TextEditor.edit(evt.target, {
      
      cellView: elementView,
      textProperty: ['attrs','label','text'],
      /* textProperty: ['model','attributes','attrs','label','text'], */
      annotationsProperty: ['attrs', 'label', 'annotations'],
      textareaAttributes: {
        maxlength: MAX_WORD_LENGTH_TEXT,
        autocorrect: 'off',
        autocapitalize: 'off',
        spellcheck: 'false',
        tabindex: '0'
      }
    });
  });
  // <-- テキスト直接入力対応
  
  return paper;
}

//矢印の移動イベント add YMD
// 緑枠の表示位置の調整(線用)
let toolsViewDisp = function(linkView, multiSelect = false){
  
  if(linkView._toolsView != null){
    let graphIndex = getBoardsIndex(activeBoardId);
    let len = linkView._toolsView.el.children.length;
    let zoomRange = document.getElementById("zoom-range-" + boards[graphIndex].id);
    let scale = Number(zoomRange.value) / 100;
    let lineToolbar = document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id);
    
    let selectCells = getBoundaryCellList(graphIndex, "all");
    if(selectCells.length > 1 || multiSelect){
      menuDisplayDisabled(boards[graphIndex].id);
      lineToolbar = document.getElementById(ID_MULTI_SELECT_MENUBAR + boards[graphIndex].id);
      lineToolbar.style.display = "";
    }
    
    for(let i = 0; i < len; i++){
      if(linkView._toolsView.el.children[i].tagName == "rect"){
        let ctrl = linkView._toolsView.el.children[i];
        ctrl.style.display = "";
        
        //change Start 20201016　YMD Requa問題点ID284、302　
        // lineToolbar.style.left = (ctrl.x.baseVal.value) * scale;
        
        let posX = (ctrl.x.baseVal.value) * scale;
        if(boards[graphIndex].paper.options.width < posX + lineToolbar.clientWidth){
          posX = boards[graphIndex].paper.options.width - lineToolbar.clientWidth;
        }
        lineToolbar.style.left = posX;
        
        //document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.top =  (ctrl.y.baseVal.value + 15) * scale;
        //if(((ctrl.y.baseVal.value - 55) * scale) > 0){
        if((((ctrl.y.baseVal.value - 55) - TOOLBAR_Y_OFFSET)* scale) > 0){
          //document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.top =  (ctrl.y.baseVal.value - 55) * scale;
          // document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.top =  (ctrl.y.baseVal.value - 55 - TOOLBAR_Y_OFFSET) * scale;
          lineToolbar.style.top =  (ctrl.y.baseVal.value) * scale  - 55 - TOOLBAR_Y_OFFSET;
        }else{
          //document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.top =  (ctrl.y.baseVal.value + 15 + ctrl.height.baseVal.value) * scale;
          // document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.top =  (ctrl.y.baseVal.value + 15 + TOOLBAR_Y_OFFSET + ctrl.height.baseVal.value) * scale;
          lineToolbar.style.top =  (ctrl.y.baseVal.value + ctrl.height.baseVal.value) * scale + TOOLBAR_Y_OFFSET;
        }
        //change END 20201016　YMD
        // document.getElementById(ID_LINE_MENUBAR + boards[graphIndex].id).style.transform = `scale(${scale})`;
        
      }
    }
    
    // 複数選択時の場合はここでリターン
    if(lineToolbar.id == ID_MULTI_SELECT_MENUBAR + boards[graphIndex].id){
      return;
    }
    
    //add Start 20201020　ツールバーのテキストエリアにテキストコンポーネントの値を表示
    var textMenuChildrenList = lineToolbar.children[0].children;
    
    var lineLabel = "";
    if(linkView.model._previousAttributes.labels != null || linkView.model._previousAttributes.labels != undefined){
      lineLabel = linkView.model._previousAttributes.labels[0].attrs.text.text.trim();
    } else if (linkView.model.attributes.labels != null || linkView.model.attributes.labels != undefined) {
      lineLabel = linkView.model.attributes.labels[0].attrs.text.text.trim();
    }
    textMenuChildrenList[0].children[8].childNodes[1].firstElementChild.value = lineLabel;
    //textMenuChildrenList[0].children[6].childNodes[1].firstElementChild.value = lineLabel;
    
    //var nodeVal = textMenuChildrenList[0].children[8].childNodes[1].firstElementChild.attributes[2].Value;
    //textMenuChildrenList[0].children[8].childNodes[1].firstElementChild.attributes[2].nodeValue =  nodeVal +" height:100px;";
    
    //textMenuChildrenList[0].children[8].childNodes[1].firstElementChild.attributes[0].ownerElement.offsetHeight = "100px;";
    textMenuChildrenList[0].childNodes[8].children[1].children[0].offsetHeight= "100px;";
    //add End 20201020
  }
};

// 図形(四角)を作成する処理
// パラメータ：
// {   x: 表示位置(x座標),
//     y: 表示位置(y座標),
//     width: 横幅
//     height: 縦幅
//     fill: 塗りつぶし色,
//     stroke: 枠の色,
//     strokeWidth: 枠の幅
// }
async function createRectangleCell(param){
  let res = {};
  let cell;
  cell = new joint.shapes.standard.Rectangle();
  cell.attr('body/fill', param.fill);
  cell.attr('body/stroke', param.stroke);
  cell.attr('body/strokeWidth', param.strokeWidth);
  cell.attr('body/strokeWidth', `${shapeDefault.strokeWidth}`);
  cell.position(param.x, param.y);
  cell.size(param.width, param.height);
  cell.attributes.kind = OBJECT_KIND_SHAPE;
  
  // Add start 20201111 kobayashi
  if(param.copyObjectId){
    cell.attr('copyObjectId/value', param.copyObjectId);
  }
  // Add end 20201111 kobayashi
  
  // DBに付箋情報保存
  let cellinfo = {
    x: cell.attributes.position.x,
    y: cell.attributes.position.y,
    z: cell.attributes.z,
    width: cell.attributes.size.width,
    height: cell.attributes.size.height,
    fill: cell.attributes.attrs.body.fill,
    stroke: cell.attributes.attrs.body.stroke,
    strokeWidth: cell.attributes.attrs.body.strokeWidth,
    kind:OBJECT_KIND_SHAPE
  };
  let sendData = {
    data : cellinfo,
    boardId : activeBoardId,
    remark_id : undefined,
    kind: OBJECT_KIND_SHAPE
  };
  res = {
    sendData: sendData,
    cell: cell
  };
  
  return res;
  
}

// テキストを作成する処理
// パラメータ：
// {   x: 表示位置(x座標),
//     y: 表示位置(y座標),
//     width: 横幅
//     height: 縦幅
//     text: テキスト
// }
async function createTextCell(param){
  let res = {};
  let cell;
  cell = new joint.shapes.standard.Rectangle({
    kind: OBJECT_KIND_TEXT,
    position: { x: param.x, y: param.y },
    size: { width: param.width, height: param.height },
    attrs:{
      label: {
        text: param.text,
        fill: '#000000',
        fontSize: 28,
        fontFamily: 'Montserrat',
        fontWeight: 'normal',
        textAnchor: 'start',
        refX: 0,
      },
      body: {
        fill: TRANSPARENT,
        stroke: TRANSPARENT,
        rx: 5,
        ry: 5
      },
    }
  });
  
  // Add start 20201111 kobayashi
  if(param.copyObjectId){
    cell.attr('copyObjectId/value', param.copyObjectId);
  }
  // Add end 20201111 kobayashi
  
  // DBに付箋情報保存
  let cellinfo = {
    x: cell.attributes.position.x,
    y: cell.attributes.position.y,
    z: 0,
    width: cell.attributes.size.width,
    height: cell.attributes.size.height,
    attrs_body : cell.attributes.attrs.body,
    attrs_label : cell.attributes.attrs.label,
    type: cell.attributes.type,
    kind: OBJECT_KIND_TEXT
  };
  let sendData = {
    data : cellinfo,
    boardId : activeBoardId,
    remark_id : undefined,
    kind: OBJECT_KIND_TEXT
  };
  
  res = {
    sendData: sendData,
    cell: cell
  };
  
  
  return res;
}

// コメント(音声認識)を作成する処理
// パラメータ：
// {   x: 表示位置(x座標),
//     y: 表示位置(y座標),
//     width: 横幅
//     height: 縦幅
//     text: テキスト
// }
// soundDataURL : 録音データ
// async function createCommentCell(param, reader){
async function createCommentCell(param, soundDataURL){
  
  return new Promise((resolve, reject) => {
    
    let data = {
      kind: OBJECT_KIND_COMMENT,
      type: CELL_TYPE_RECT,
      position: { x: param.x, y: param.y },
      size: { width: param.width, height: param.height },
      attrs:{
        label: {
          text: param.contents,
          fill: '#000000',
          fontSize: 16,
          fontFamily: 'Montserrat',
          fontWeight: 'normal',
          textAnchor: 'start',
          refX: 10
          
        },
        body: {
          fill: "#fff",
          stroke: "#D1DFF4",
          'stroke-width': 10
        },
        audioPlay: {
          'xlink:href' : "images/voice_recognition.svg",
          audioPath : param.audioPath,
          event: 'element:audioPlay',
        }
      },
      remark_id: "",
      contents: param.contents
    };
    
    if(!soundDataURL){
      if(param.audioPath == undefined || param.audioPath == ""){
        data.attrs.audioPlay["xlink:href"] = "images/commentEdit.svg";
        data.attrs.audioPlay.event = 'element:commentEdit';
      }
    }
    
    let cell = new joint.shapes.standard.Rectangle(data);
    
    // Add start 20201111 kobayashi
    if(param.copyObjectId){
      cell.attr('copyObjectId/value', param.copyObjectId);
    }
    // Add end 20201111 kobayashi
    
    if(cell.markup.length == 2){
      
      cell.markup.push({
        tagName: "g",
        children:[{
          selector: "audioPlay",
          tagName: "image"
        }]
      });
    }
    
    // DBに保存するオブジェクトデータから発言内容は削除する
    // data.contents = undefined;　// サーバ側で制御する
    data.attrs.label.text = undefined;
    // let reader_res = undefined;
    // if (soundDataURL) {
    //     reader_res = soundDataURL;
    // }
    let sendData = {
      kind: OBJECT_KIND_COMMENT,
      boardId: activeBoardId,
      remark_id : "",
      data: data,
      user_code: userInfo.response[0].code,
      meetingId: selectMeeting,
      speechedAt: param.speechedAt
    };
    
    result = {
      sendData: sendData,
      cell: cell,
      // reader_res: reader_res
    }
    
    // let reader_res = undefined;
    if (soundDataURL) {
      result.reader_res = soundDataURL;
    }
    
    resolve(result);
  });
  
}

// 線を作成する処理
// パラメータ：
async function createLinkCell(param){
  let res = {};
  let cellData = {
    kind: OBJECT_KIND_LINE,
    connector: { name: param.connector_type },
    smooth: param.smooth_type,
    attrs: {}
  };
  
  if(param.addselect){
    cellData.addselect = param.addselect;
  }
  
  let cell = undefined;
  
  if(param.attrs_line){
    cellData.attrs.line = param.attrs_line;
    cell = new joint.shapes.standard.Link(cellData);
    if(param.target){
      cell.target(param.target);
    }
    
    if(param.source){
      cell.source(param.source);
    }
    
  } else {
    cellData.attrs = {
      line: {
        strokeDasharray: param.strokeDasharrayValue,
        stroke: lineColor,
        //Change　START　20201019
        strokeWidth: LINE_THICKNESS_DEFAULT,
        /* strokeWidth: 1, Requa問題点ID：314*/
        /* strokeWidth: 3, Requa問題点ID：309*/
        //Change　END　20201019
        sourceMarker: {
          //Change　START　20201019
          /* 'stroke-width': 5 Requa問題点ID：309*/
          strokeWidth: 0.1
          //Change　END　20201019
        },
        targetMarker: {
          //Change　START　20201019
          /* 'stroke-width': 5 Requa問題点ID：309*/
          strokeWidth: 0.1
          //Change　END　20201019
        }
      }
    }
    cell = new joint.shapes.standard.Link(cellData);
    cell.attributes.attrs.wrapper.class = "link-corsor";
    
    //Add 20201019 ymd　Requa問題点ID：309
    
    if(lineLeftFlag == LINE_CONNECT_STRAIGHT){
      cell.attr(LINE_SOURCE_MARKER_FILL_PATH,TRANSPARENT);
      cell.attr(LINE_SOURCE_MARKER_STROKE_PATH ,TRANSPARENT);
      //change　Start 20201019 Requa問題点ID：309
      cell.attr(LINE_SOURCE_MARKER_D_PATH, TRIANGLE_POINT);
      //cell.attr(LINE_SOURCE_MARKER_D_PATH, 'M 5 -10 L -15 0 L 5 10 Z');
      //change　END 20201019
    }else if(lineLeftFlag == LINE_CONNECT_CIRCLE){
      cell.attr(LINE_SOURCE_MARKER_FILL_PATH,lineColor);
      cell.attr(LINE_SOURCE_MARKER_STROKE_PATH ,lineColor);
      cell.attr(LINE_SOURCE_MARKER_TYPE_PATH ,'circle');
      cell.attr(LINE_SOURCE_MARKER_R_PATH,10);
      cell.attr(LINE_SOURCE_MARKER_CX_PATH,5);
    } else {//LINE_CONNECT_ARROW
      cell.attr(LINE_SOURCE_MARKER_FILL_PATH,lineColor);
      cell.attr(LINE_SOURCE_MARKER_STROKE_PATH ,lineColor);
      //change　Start 20201019 Requa問題点ID：309
      cell.attr(LINE_SOURCE_MARKER_D_PATH, TRIANGLE_POINT);
      //cell.attr(LINE_SOURCE_MARKER_D_PATH, 'M 5 -10 L -15 0 L 5 10 Z');
      //change　END 20201019
    }
    
    if(lineRightFlag == LINE_CONNECT_STRAIGHT){
      cell.attr(LINE_TARGET_MARKER_FILL_PATH,TRANSPARENT);
      cell.attr(LINE_TARGET_MARKER_STROKE_PATH,TRANSPARENT);
      //change　Start 20201019 Requa問題点ID：309
      cell.attr(LINE_TARGET_MARKER_D_PATH, TRIANGLE_POINT);
      //cell.attr(LINE_TARGET_MARKER_D_PATH, 'M 5 -10 L -15 0 L 5 10 Z');
      //change　END 20201019
    }else if(lineRightFlag == LINE_CONNECT_CIRCLE){
      cell.attr(LINE_TARGET_MARKER_FILL_PATH,lineColor);
      cell.attr(LINE_TARGET_MARKER_STROKE_PATH,lineColor);
      cell.attr(LINE_TARGET_MARKER_TYPE_PATH,'circle');
      cell.attr(LINE_TARGET_MARKER_R_PATH,10);
      cell.attr(LINE_TARGET_MARKER_CX_PATH,5);
    } else {//LINE_CONNECT_ARROW
      cell.attr(LINE_TARGET_MARKER_FILL_PATH, lineColor);
      cell.attr(LINE_TARGET_MARKER_STROKE_PATH, lineColor);
      //change　Start 20201019 Requa問題点ID：309
      cell.attr(LINE_TARGET_MARKER_D_PATH, TRIANGLE_POINT);
      //cell.attr(LINE_TARGET_MARKER_D_PATH, 'M 5 -10 L -15 0 L 5 10 Z');
      //change　END 20201019
    }
    
    if(lineTypeFlag == LINE_TYPE_KAGI){
      cell.router(ROUTER_TYPE_KAGI);
    }
  }
  
  // Add Start 20201111 kobayashi
  let linkLabel = ' ';
  if(param.label){
    linkLabel = param.label;
  }
  cell.appendLabel({
    attrs: {
      text: {
        type: 'textarea',
        text: linkLabel,
        style:{whiteSpace: 'pre',
          'xml:space': "preserve",
          'word-break': 'break-all'},
      },
      rect: {
        class: "link-label-corsor",
        fill: TRANSPARENT,
      },
      position: {
        distance: 0.3,
        args: {
          keepGradient: true
        }
      }
    }
  });
  if(param.label_position){
    cell.attributes.labels[0].position = param.label_position;
  }
  if(param.vertices){
    cell.attributes.vertices = param.vertices;
  }
  // Add End 20201111 kobayashi
  
  // DBに線情報保存
  let cellinfo = {
    target: getLinkTargetSourceSaveData(cell.attributes.target),
    source: getLinkTargetSourceSaveData(cell.attributes.source),
    z: cell.attributes.z,
    attrs_line : cell.attributes.attrs.line,
    type: cell.attributes.type,
    smooth: cell.attributes.smooth,
    connector: cell.attributes.connector,
    kind: OBJECT_KIND_LINE
  };
  let sendData = {
    data : cellinfo,
    boardId : activeBoardId,
    remark_id : undefined,
    kind: OBJECT_KIND_LINE
  };
  
  res = {
    sendData: sendData,
    cell: cell
  };
  
  return res;
  // AddObject(sendData, cell);
}

async function createCommentLinkCell(param){
  
  let result = undefined;
  let targetObj = getLinkTargetSourceSaveData({id: param.targetId});
  let sourceObj = getLinkTargetSourceSaveData({id: param.sourceId});
  
  // ターゲットとソースどちらか不足していたら線作成をやめる
  if(targetObj.objectId == undefined || targetObj.objectId == "" ||
    sourceObj.objectId == undefined || sourceObj.objectId == ""){
    return result;
  }
  
  let link = new joint.shapes.standard.Link({
    kind: OBJECT_KIND_LINE,
    connector: { name: CONNECTOR_TYPE_STRAIGHT },
    smooth: false,
    addselect: false,
    attrs: {
      line: {
        strokeDasharray: LINE_STYLE_DASHED_STROKE_DASHARRAY,
        stroke: "#4B71A6",
        strokeWidth: 1,
        sourceMarker: {
          strokeWidth: 0.1,
          stroke : TRANSPARENT,
          fill : TRANSPARENT,
          d : TRIANGLE_POINT
        },
        targetMarker: {
          strokeWidth: 0.1,
          stroke : TRANSPARENT,
          fill : TRANSPARENT,
          d : TRIANGLE_POINT
        }
      }
    },
    source: {id: param.sourceId},
    target: {id: param.targetId}
  });
  link.appendLabel({
    attrs: {
      text: {
        type: 'textarea',
        text: ' ',
        style:{whiteSpace: 'pre',
          'xml:space': "preserve",
          'word-break': 'break-all'},
      },
      rect: {
        fill: TRANSPARENT,
      },
      position: {
        distance: 0.3,
        args: {
          keepGradient: true
        }
      }
    }
  });
  
  // DBに付箋情報保存
  let cellinfo = {
    target: targetObj,
    source: sourceObj,
    z: link.attributes.z,
    attrs_line : link.attributes.attrs.line,
    type: link.attributes.type,
    smooth: link.attributes.smooth,
    connector: link.attributes.connector,
    kind: OBJECT_KIND_LINE
  };
  let sendData = {
    data : cellinfo,
    boardId : activeBoardId,
    remark_id : undefined,
    kind: OBJECT_KIND_LINE
  };
  
  result = {
    sendData: sendData,
    cell: link
  };
  
  return result;
}


async function createNoteForFile(fd, param){
  return new Promise((resolve, reject) => {
    
    $.ajax({
      type: "POST",
      url: '/yyknowledgeboard/add-note-for-files',
      data: fd,
      processData: false,
      contentType: false,
      dataType: 'html',
      success: function (response) {
        let result = {};
        let resObj = JSON.parse(response);
        if (resObj.status === -1) $('#error-system').modal('show');
        else{
          
          let boardIndex = getBoardsIndex(activeBoardId);
          let posZ = boards[boardIndex].graph.maxZIndex() + 1;
          
          // set param
          let stickyType = param.remarkType;
          let image_str = GetUserIconStr();
          let x = param.x;
          let y = param.y;
          let z = posZ;
          let now_str = dateToStr24HPad0(new Date(), 'YYYY/MM/DD hh:mm:ss');
          
          let srcfile = 'images/files_icon/default.png';
          // ドキュメントの場合 ファイル名からドキュメントアイコンを取得
          if(stickyType == stickyNoteType.document){
            srcfile = getDocumentFileIconPath(resObj.originalname);
            // srcfile = getDocumentFileIconPath(param.fileName);
            // 動画、画像、音声の場合 コンテンツのURLを指定
          }else{
            srcfile = resObj.url;
            // srcfile = "";
          }
          
          // update sicky note templete ファイル用にテンプレートを更新する
          let templete = createStickyNoteTemplete(note_header_type, stickyType);
          
          let cell;
          let cellInfo;
          if(stickyType == stickyNoteType.image){
            
            cell = new joint.shapes.standard.Image();
            cell.position(x, y);
            cell.attributes.z = z;
            cell.size(150, 150);
            cell.attr('.objectId/value', undefined);
            // del start 20201111 kobayashi
            // cell.attr('root/title', resObj.originalname);
            // cell.attr('label/text', getWrapFileName(resObj.originalname));
            // del end 20201111 kobayashi
            cell.attr('image/xlinkHref', param.thumbnail_image);
            cell.attributes.kind = OBJECT_KIND_STICKY_NOTE_FILE;
            cell.attributes.remark_type = stickyType;
            // cell.attributes.file_info = {file_name: resObj.originalname ,src:srcfile, url:resObj.url};
            cell.attributes.file_info = {file_name: param.fileName, src:srcfile, url:srcfile};
            
            cellInfo = {
              type: cell.attributes.type,
              x : cell.attributes.position.x,
              y : cell.attributes.position.y,
              z : cell.attributes.z,
              kind : OBJECT_KIND_STICKY_NOTE_FILE,
              // id : resObj.insertData[0].remark_id,
              id : "",
              name : userInfo.response[0].name,
              meeting_id : resObj.meeting_id,
              // meeting_id : selectMeeting,
              date : now_str,
              remark_type : stickyType,
              file_name : resObj.originalname,
              // file_name : param.fileName,
              src : srcfile,
              download_url : srcfile,
              thumbnail_image : param.thumbnail_image
            };
          } else {
            
            // create cell
            cell = AddMemberForFiles(
              x, y, z,
              // resObj.insertData[0].remark_id,
              "",
              userInfo.response[0].name,
              now_str,
              resObj.originalname,
              // param.fileName,
              srcfile,
              image_str,
              param.sticky_note_color,
              OBJECT_KIND_STICKY_NOTE_FILE,
              "会議ID:" + resObj.meeting_id,
              // "会議ID:" + selectMeeting,
              OPACITY_DISP,
              undefined,
              stickyType,
              resObj.url,
              // "",
              param.width,
              param.height,
              param.thumbnail_image
            );
            
            cell.markup = templete;
            
            // 付箋情報
            cellInfo = {
              type: cell.attributes.type,
              x : cell.attributes.position.x,
              y : cell.attributes.position.y,
              z : 0,
              kind : cell.attributes.kind,
              // id : resObj.insertData[0].remark_id,
              id : "",
              name : cell.attributes.attrs[".name"].text,
              background : cell.attributes.attrs[".card"].fill,
              meeting_id : resObj.meeting_id,
              // meeting_id : selectMeeting,
              date : now_str,
              opacity : OPACITY_DISP,
              remark_type : stickyType,
              file_name : resObj.originalname,
              // file_name : param.fileName,
              src : srcfile,
              download_url : resObj.url
              // download_url : ""
              //imageは容量が大きいので参加者情報で保持する。
            };
          }
          
          // DBに付箋情報保存
          let data = {
            data : cellInfo,
            boardId : activeBoardId,
            // remark_id : resObj.insertData[0].remark_id,
            remark_id : "",
            user_code: userInfo.response[0].code,
            meetingId: selectMeeting,
            url: resObj.url,
            kind: OBJECT_KIND_STICKY_NOTE_FILE
          };
          
          // Add start 20201111 kobayashi
          if(param.copyObjectId){
            cell.attr('copyObjectId/value', param.copyObjectId);
          }
          // Add end 20201111
          
          // fd.append('sendData', JSON.stringify(data));
          
          result = {
            sendData: data,
            cell: cell
          };
          
          resolve(result);
        }
      }
    });
  });
  
}

async function createNoteForFile_Copy(fd, param){
  return new Promise((resolve, reject) => {
    
    let boardIndex = getBoardsIndex(activeBoardId);
    let posZ = boards[boardIndex].graph.maxZIndex() + 1;
    
    // set param
    let stickyType = param.remarkType;
    let image_str = GetUserIconStr();
    let x = param.x;
    let y = param.y;
    let z = posZ;
    let now_str = dateToStr24HPad0(new Date(), 'YYYY/MM/DD hh:mm:ss');
    
    let srcfile = 'images/files_icon/default.png';
    // ドキュメントの場合 ファイル名からドキュメントアイコンを取得
    if(stickyType == stickyNoteType.document){
      // srcfile = getDocumentFileIconPath(resObj.originalname);
      srcfile = getDocumentFileIconPath(param.fileName);
      // 動画、画像、音声の場合 コンテンツのURLを指定
    }else{
      // srcfile = resObj.url;
      srcfile = param.filePath;
      
    }
    
    // update sicky note templete ファイル用にテンプレートを更新する
    let templete = createStickyNoteTemplete(note_header_type, stickyType);
    
    let cell;
    let cellInfo;
    if(stickyType == stickyNoteType.image){
      
      cell = new joint.shapes.standard.Image();
      cell.position(x, y);
      cell.attributes.z = z;
      cell.size(param.width, param.height);
      cell.attr('.objectId/value', undefined);
      // del start 20201111 kobayashi
      // cell.attr('root/title', resObj.originalname);
      // cell.attr('label/text', getWrapFileName(resObj.originalname));
      // del end 20201111 kobayashi
      cell.attr('image/xlinkHref', param.thumbnail_image);
      cell.attributes.kind = OBJECT_KIND_STICKY_NOTE_FILE;
      cell.attributes.remark_type = stickyType;
      // cell.attributes.file_info = {file_name: resObj.originalname ,src:srcfile, url:resObj.url};
      cell.attributes.file_info = {file_name: param.fileName, src:srcfile, url:srcfile};
      
      cellInfo = {
        type: cell.attributes.type,
        x : cell.attributes.position.x,
        y : cell.attributes.position.y,
        z : cell.attributes.z,
        kind : OBJECT_KIND_STICKY_NOTE_FILE,
        // id : resObj.insertData[0].remark_id,
        id : "",
        name : userInfo.response[0].name,
        // meeting_id : resObj.meeting_id,
        meeting_id : selectMeeting,
        date : now_str,
        remark_type : stickyType,
        // file_name : resObj.originalname,
        file_name : param.fileName,
        src : srcfile,
        download_url : srcfile,
        thumbnail_image : param.thumbnail_image
      };
    } else {
      
      // create cell
      cell = AddMemberForFiles(
        x, y, z,
        // resObj.insertData[0].remark_id,
        "",
        userInfo.response[0].name,
        now_str,
        // resObj.originalname,
        param.fileName,
        srcfile,
        image_str,
        param.sticky_note_color,
        OBJECT_KIND_STICKY_NOTE_FILE,
        // "会議ID:" + resObj.meeting_id,
        "会議ID:" + selectMeeting,
        OPACITY_DISP,
        undefined,
        stickyType,
        // resObj.url,
        param.filePath,
        param.width,
        param.height,
        param.thumbnail_image
      );
      
      cell.markup = templete;
      
      // 付箋情報
      cellInfo = {
        type: cell.attributes.type,
        x : cell.attributes.position.x,
        y : cell.attributes.position.y,
        z : 0,
        kind : cell.attributes.kind,
        // id : resObj.insertData[0].remark_id,
        id : "",
        name : cell.attributes.attrs[".name"].text,
        background : cell.attributes.attrs[".card"].fill,
        // meeting_id : resObj.meeting_id,
        meeting_id : selectMeeting,
        date : now_str,
        opacity : OPACITY_DISP,
        remark_type : stickyType,
        // file_name : resObj.originalname,
        file_name : param.fileName,
        src : srcfile,
        // download_url : resObj.url
        download_url :  param.filePath
        //imageは容量が大きいので参加者情報で保持する。
      };
    }
    
    // DBに付箋情報保存
    let data = {
      data : cellInfo,
      boardId : activeBoardId,
      // remark_id : resObj.insertData[0].remark_id,
      remark_id : "",
      user_code: userInfo.response[0].code,
      meetingId: selectMeeting,
      kind: OBJECT_KIND_STICKY_NOTE_FILE
    };
    
    // Add start 20201111 kobayashi
    if(param.copyObjectId){
      cell.attr('copyObjectId/value', param.copyObjectId);
    }
    // Add end 20201111
    
    result = {
      sendData: data,
      cell: cell
    };
    
    resolve(result);
  });
  
}

//add ymd
function clearFlag(){
  toolbarFlag = TOOLBAR_UNSELECTED;
  //figureFlag = FIGURE_MENU_BAR_UNSELECTED;
  
}

//ツールバーを非表示にする
function menuDisplayDisabled(boardId){
  
  try{
    setDisplayNone(ID_ADD_LINE_MENU,boardId);
    setDisplayNone(ID_LINE_MENUBAR,boardId);
    setDisplayNone(ID_LINE_TYPE_SETTING_MENUBAR,boardId);
    setDisplayNone(ID_LINE_LEFT_MENUBAR,boardId);
    setDisplayNone(ID_LINE_RIGHT_MENUBAR,boardId);
    setDisplayNone(ID_SHAPE_SUB_MENUBAR,boardId);
    setDisplayNone(ID_ADD_FIGURE_MENUBAR,boardId);
    setDisplayNone(ID_FUSEN_MENUBAR,boardId);
    setDisplayNone(ID_VOICE_FUSEN_MENUBAR,boardId);
    setDisplayNone(ID_FUSEN_SUB_MENUBAR,boardId);
    setDisplayNone(ID_TEXT_MENUBAR,boardId);
    setDisplayNone(ID_REACTION_STICKY_NOTE,boardId);
    setDisplayNone(ID_CHANGE_COLOR_STICKY_NOTE,boardId);
    setDisplayNone(ID_MULTI_SELECT_MENUBAR,boardId);
  }
  catch (err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

//表示の場合は非表示にする
function setDisplayNone(str,boardId){
  
  // Chg Y.Akasaka 20201022
  let element = document.getElementById(`${str}${boardId}`);
  if(element == undefined){
    return;
  }
  
  if(element.style.display == "" || element.style.display === 'flex'){
    element.style.display = "none";
  }
}

//非表示の場合は表示にする
function setDisplay(str,boardId){
  
  // Chg Y.Akasaka 20201022
  let element = document.getElementById(`${str}${boardId}`);
  if(element == undefined){
    return;
  }
  
  if(element.style.display == "none"){
    element.style.display = "";
  }
}

//add end ymd
function contentsDivResize(cell, saveEnable, isResize, isShare){
  
  if(isShare == false){
    menuDisplayDisabled(activeBoardId);
  }
  
  if (cell.attributes.attrs[".contents"] !== undefined){
    if (isResize) {
      $(`.${cell.attributes.attrs['.contents'].id}`).attr('oninput', '');
      isChangeSizeSticky = false;
    }
    let ctrl = document.getElementById(cell.attributes.attrs[".contents"].id);
    if(ctrl != null){
      ctrl.style.width = cell.attributes.size.width - 10;
      ctrl.style.height = cell.attributes.size.height - 35;
      cell.attributes.attrs['.foreignObject'].width = cell.attributes.size.width - 5;
      cell.attributes.attrs['.foreignObject'].height = cell.attributes.size.height - 35;
    }
    setTimeout(function () {
      $(`#area-reaction-${cell.attributes.attrs[".contents"].id}`).attr('transform',`translate (0, ${cell.attributes.size.height})`);
    })
  }
}

function changeVertices(cell){
  changeingCell = cell;
  if(cell.changed.source != undefined && cell.changed.target != undefined){
    changeingCell.attributes.isAllMove = true;
  }
}

function addBoard(boardCtrlId, boardName, boardID, boardKind, paperScrollAppendFunc){
  
  let jointJSGraph = new joint.dia.Graph();
  // let commandManager = new joint.dia.CommandManager({ graph: jointJSGraph });
  
  jointJSGraph.on('change:vertices', function(element){
    changeVertices(element);
  });
  
  jointJSGraph.on('change:source', function(element){
    changeVertices(element);
  });
  
  jointJSGraph.on('change:target', function(element){
    changeVertices(element);
  });
  
  jointJSGraph.on('change:size', function(element){
    contentsDivResize(element, true, true, true);
  });
  
  let width = BOARD_WIDTH;
  let height = BOARD_HEIGHT;
  if(boardName == SPEECH_BOARD_NAME){
    width = (CARD_WIDTH * NEXT_NEWLINE_CARD + NEXT_NEWLINE_CARD * CARD_X_PEDDING);
    height = ((CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)) * CARD_ROW_MAX + CARD_ROW_MAX * CARD_Y_PEDDING);
  }
  
  let paper = new joint.dia.Paper({
    el: document.getElementById(boardCtrlId),
    width: width,
    height: height,
    model: jointJSGraph,
    perpendicularLinks: false,
    restrictTranslate: true,
    interactive : paperInteractive,
    gridSize: 20,
    // drawGrid: { color: 'white', thickness: 1 }
  });
  
  let paperScroller = new joint.ui.PaperScroller({
    id: ID_PAPER_SCROLLER + boardCtrlId,
    // padding: 0,
    // contentOptions: {maxWidth: BOARD_WIDTH, maxHeight: BOARD_HEIGHT},
    paper: paper
  });
  // paperScroller.center();
  
  $(`#${ID_KNOWLEDGEBOARD_FRAME + boardCtrlId}`).append(paperScroller.render().el);
  paperScroller.setCursor('grab');
  paperScrollAppendFunc();
  
  paper.on('blank:pointerdown', function(evt, x, y){
    if(boardEditMode == false){
      paperScroller.startPanning(evt, x, y);
    }
  });
  
  paper = paperEvent(paper, jointJSGraph, boardCtrlId);
  
  // if(userInfo.role == 3){
  paper.setInteractivity(false);
  // }
  
  // ツールバー作成
  // 横のツールバー
  let sideToolbarShort = new joint.ui.Toolbar({
    tools: [
      {
        type: 'button',
        name: 'arrowUp',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'addNote',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'arrowDown',
      }
      ,
      { type: 'separator' },
      {
        type: 'button',
        name: 'doubleArrowDown'
      },
    ],
  });
  
  
  // ツールバー作成
  // 横のツールバー
  let sideToolbar = new joint.ui.Toolbar({
    tools: [
      // { type: 'separator', name: 'topSeparator'},
      {
        type: 'button',
        name: 'move-edit',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'addNote',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'voice',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'chat',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'addTextLabel',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'addLine',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'addSharp',
      },
      { type: 'separator' },
      {
        type: 'button',
        name: 'edit',
      },
      { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'addFreeShape',
      // },
      // { type: 'separator' },
      {
        type: 'button',
        name: 'template',
      },
      { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'sort',
      // },
      // { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'undo',
      // },
      // { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'redo',
      // },
      // { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'expansion',
      // },
      // { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'boardSave',
      // },
      {
        type: 'button',
        name: 'aspectRatio',
      },
      // { type: 'separator' },
      // {   type: 'button',
      //     name: 'doubleArrowUp'
      // }
      // * nakamatsu
      // { type: 'separator' },
      // {
      //     type: 'button',
      //     name: 'analysisCount',
      // },
      { type: 'separator' },
      {
        type: 'button',
        name: 'more',
      },
      // nakamatsu *
      { type: 'separator' },
      {
        type: 'button',
        name: 'collapseMenu',
      },
    
    ],
  });
  
  //add ymd
  let fusenMenubar = new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'fusen1',
        group: 'fusengroup1'
      }, {
        type: 'button',
        name: 'fusen2',
        group: 'fusengroup1'
      }, {
        type: 'button',
        name: 'fusen3',
        group: 'fusengroup1'
      }, {
        type: 'button',
        name: 'fusen4',
        group: 'fusengroup1'
      }, {
        type: 'button',
        name: 'fusen5',
        group: 'fusengroup1'
      }, {
        type: 'button',
        name: 'fusen6',
        group: 'fusengroup2'
      }, {
        type: 'button',
        name: 'fusen7',
        group: 'fusengroup2'
      }, {
        type: 'button',
        name: 'fusen8',
        group: 'fusengroup2'
      },{
        type: 'button',
        name: 'fusen9',
        group: 'fusengroup2'
      }, {
        type: 'button',
        name: 'fusen10',
        group: 'fusengroup2'
      }
    ],
    fusengroup1:{aline:'left'},
    fusengroup2:{aline:'right'}
  });
  
  let voiceFusenMenubar = new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'voiceFusen1',
        group: 'voicefusengroup1'
      }, {
        type: 'button',
        name: 'voiceFusen2',
        group: 'voicefusengroup1'
      }, {
        type: 'button',
        name: 'voiceFusen3',
        group: 'voicefusengroup1'
      }, {
        type: 'button',
        name: 'voiceFusen4',
        group: 'voicefusengroup1'
      }, {
        type: 'button',
        name: 'voiceFusen5',
        group: 'voicefusengroup1'
      }, {
        type: 'button',
        name: 'voiceFusen6',
        group: 'voicefusengroup2'
      }, {
        type: 'button',
        name: 'voiceFusen7',
        group: 'voicefusengroup2'
      }, {
        type: 'button',
        name: 'voiceFusen8',
        group: 'voicefusengroup2'
      },{
        type: 'button',
        name: 'voiceFusen9',
        group: 'voicefusengroup2'
      }, {
        type: 'button',
        name: 'voiceFusen10',
        group: 'voicefusengroup2'
      }
    ],
    voicefusengroup1:{aline:'left'},
    voicefusengroup2:{aline:'right'}
  });
  
  let fusenSubMenubar = new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'onseiNinshiki'
      },
      // {
      //     type: 'button',
      //     name: 'colorPalette',
      //     //colorPalette
      // },
      // {
      //     type: 'button',
      //     name: 'fontDownload'
      // },
      // {
      //     type: 'button',
      //     name: 'fontSize'
      // },
      // {
      //     type: 'button',
      //     name: 'fontColorPalette',
      // },
      // {
      //     type: 'button',
      //     name: 'fontBold'
      // },
      // {
      //     type: 'button',
      //     name: 'fontItalic'
      // },
      // {
      //     type: 'button',
      //     name: 'fontUnderline'
      // },
      // {
      //     type: 'button',
      //     name: 'formatClear'
      // },
      {
        type: 'button',
        name: 'changeColor',
      },
      {
        type: 'button',
        name: 'dustBox',
      },
      {
        type: 'button',
        name: 'download',
      },
      {
        type: 'button',
        name: 'reactionStickyNote',
      },
      // * nakamatsu
      {
        type: 'button',
        name: 'imageSearch',
      },
      // nakamatsu *
      {
        type: 'button',
        name: 'comment',
      },
    ]
  });
  
  let textMenubar = new joint.ui.Toolbar({
    tools:[
      // {
      //     type: 'button',
      //     name: 'fontDownload'
      // },
      // {
      //     type: 'button',
      //     name: 'fontSize'
      // },
      // {
      //     type: 'button',
      //     name: 'fontColorPalette',
      // },
      // {
      //     type: 'button',
      //     name: 'fontBold'
      // },
      // {
      //     type: 'button',
      //     name: 'fontItalic'
      // },
      // {
      //     type: 'button',
      //     name: 'fontUnderline'
      // },
      // {
      //     type: 'button',
      //     name: 'formatClear'
      // },
      //Add　Start 20201020　ツールバーでテキスト入力
      
      {
        type: 'label',
        name: 'commentlabel',
        text: 'comment'
      },
      {
        type: 'label',
        name: 'dummySpace'
      },
      {
        type: 'textarea',
        name: 'textEditArea',
        
      },
      {
        type: 'button',
        name: 'OK',
        text: 'OK',
        
      },
      /*             {
                type: 'button',
                name: 'Cancel',
                text: 'Cancel',
            }, */
      //Add　END 20201020　ツールバーでテキスト入力
      {
        type: 'button',
        name: 'dustBox',
      }
    ]
  });
  
  let addFigureMenubar= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'addCircle',
        group: 'group1'
      },
      {
        type: 'button',
        name: 'addSquare',
        group: 'group1'
      },
      {
        type: 'button',
        name: 'addCircle2',
        group: 'group2'
      },
      {
        type: 'button',
        name: 'addSquare2',
        group: 'group2'
      }
    ],
    group1:{aline:'left'},
    group2:{aline:'right'}
  });
  
  let lineMenubar= new joint.ui.Toolbar({
    tools:[
      // {
      //     type: 'button',
      //     name: 'lineType',
      // },
      {
        type: 'button',
        name: 'lineTypeRight'
      },
      {
        type: 'button',
        name: 'lineMiddle'
      },
      {
        type: 'button',
        name: 'colorPalette_black',
      },
      {
        type: 'button',
        name: 'lineSize',
      },
      {
        type: 'button',
        name: 'addTextLabel',
      },
      {
        type: 'button',
        name: 'fontColorPalette'
      },
      {
        type: 'button',
        name: 'fontSize'
      },
      {
        type: 'button',
        name: 'ellipsis'
      },
      // {
      //     type: 'button',
      //     name: 'lineTypeLeft',
      // },
      // {
      //     type: 'button',
      //     name: 'lineTypeRight',
      // },{
      //     type: 'button',
      //     name: 'lineTypeBoth',
      // },{
      //     type: 'button',
      //     name: 'colorPalette',
      //     //colorPalette
      // },
      
      //Add　Start 20201020　ツールバーでテキスト入力
      
      // {
      //     type: 'label',
      //     name: 'commentlabel',
      //     text: 'comment'
      // },
      // {
      //     type: 'label',
      //     name: 'dummySpace'
      // },
      {
        type: 'textarea',
        name: 'textEditAreaLine',
        
      },
      // {
      //     type: 'button',
      //     name: 'OK',
      //     text: 'OK',
      //
      // },
      /*             {
                type: 'button',
                name: 'Cancel',
                text: 'Cancel',
            }, */
      //Add　END 20201020　ツールバーでテキスト入力
      // nakamatsu 未実装メニューの押下不可対応 -->
      {
        type: 'button',
        name: 'dustBox',
      },
      // <-- nakamatsu 未実装メニューの押下不可対応
    ]
  });
  
  let lineTypeSettingMenubar= new joint.ui.Toolbar({
    tools:[
      {   type: 'range',
        name: 'LineVolume',
        min: 0,
        max: 10,
        step: 1,
        group: 'group1'
      }]
  });
  
  let lineTypeSettingMenubar3= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'lineStyleStraight',
        group: 'group3'
      },
      {
        type: 'button',
        name: 'lineStyleDashed',
        group: 'group3'
      },
      {
        type: 'button',
        name: 'lineStyleDotted',
        group: 'group3'
      }
    ]
  });
  
  let lineTypeSettingMenubar2= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'lineTypeStraight',
        group: 'group2'
      },
      {
        type: 'button',
        name: 'lineTypeKagi',
        group: 'group2'
      },
      {
        type: 'button',
        name: 'lineTypeCurve',
        group: 'group2'
      }]
  });
  
  let lineLeftMenubar= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'type_straight',
      },
      {
        type: 'button',
        name: 'type_arrow',
      },
      {
        type: 'button',
        name: 'type_cycle',
      }
    ]
  });
  
  let lineRightMenubar= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'type_straight',
      },
      {
        type: 'button',
        name: 'type_arrow',
      },
      {
        type: 'button',
        name: 'type_cycle',
      }
    ]
  });
  
  let addLineMenu= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'lineStyleStraight1',
        group: 'group1'
      },
      {
        type: 'button',
        name: 'lineStyleDashed',
        group: 'group1'
      },
      // {
      //     type: 'button',
      //     name: 'lineStyleStraight2',
      //     group: 'group2'
      // },
      // {
      //     type: 'button',
      //     name: 'lineStyleDotted',
      //     group: 'group2'
      // }
      {
        type: 'button',
        name: 'lineStyleDotted',
        group: 'group1'
      }
    ],
    group1:{aline:'left'},
    // group2:{aline:'right'}
  });
  
  let addLineMenu2 =new joint.ui.Toolbar({
    tools:[
      {
        type: 'inputNumber',
        value: 3,
        min: 1
      }]
  });
  
  let addLineMenu3 =new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'colorPalette',
      },{
        type: 'button',
        name: 'colorPalette',
      },{
        type: 'button',
        name: 'colorPalette',
      }]
  });
  
  
  let shapeMenubar = new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'shapeSquare',
        group: 'group1'
      },
      {
        type: 'button',
        name: 'shapeCircle',
        group: 'group1'
      },
      {
        type: 'button',
        name: 'shapeCloud',
        group: 'group1'
      },
      {
        type: 'button',
        name: 'shapeStar',
        group: 'group1'
      },
      // {
      //     type: 'button',
      //     name: 'shapeSquare',
      //     group: 'group2'
      // },
      {
        type: 'button',
        name: 'shapeTriangle',
        group: 'group2'
      },
      {
        type: 'button',
        name: 'chatBubble',
        group: 'group2'
      },
      {
        type: 'button',
        name: 'shapeHeart',
        group: 'group2'
      },
      {
        type: 'button',
        name: 'noshape',
        group: 'group2'
      },
    ]
  });
  
  //矩形クリック時に表示されるメニュー
  let shapeSubMenubar= new joint.ui.Toolbar({
    tools:[{
      type: 'button',
      name: 'colorPalette',
    },
      {
        type: 'button',
        name: 'shape_colorPalette_black',
      },
      {
        type: 'button',
        name: 'dustBox',
      }
    ]
  });
  
  //複数選択時に表示されるメニュー
  let multiSelectMenubar= new joint.ui.Toolbar({
    tools:[
      {
        type: 'button',
        name: 'dustBox',
      }
    ]
  });
  
  var colorPalette = new joint.ui.ColorPalette({
    options: [
      { content: '#000000' },
      { content: '#FFFFFF' },
      { content: TRANSPARENT, icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGoAAABrCAYAAACffRcyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABPxJREFUeNrsnc9rU1kUx+/LS2LTBtI4VMiqjYjiQgQ3IoIUuhvGUmYzUBgV8SdWLS7sooLgxm0XgjKg0P+gMDC7ggjDzGwGOt1psS2WBptioiYVmrbxnviiSc17eaPvvdzz7vcLlyTk5vVyPtzzzv3mtDWq1arYrVwuNyAfxuUYkaNfQEFoWY4ZOaYymczS7jeNRlASUC9NlOMs4tZRTdNGkcCKX4GyID2V4yjipITm5Bisw4oAkrIiFk8tNp9AWekOkNSENVVLfaurq1Q4LCImSisbtao7R5mmWTQMYxPx8l6yRohvb2/3tpk2HrVKcFtA6XT6WSwWKyOk/qlSqfQUCoVTDsBGIk7nJEAKRhRjirXDlP6I024CpGBhUczt3rcFhXtS8HKKeQTh4aHot35Q3vyObW1tpdvNSyQSi8lk8iWu1yFQtGg59rWbJyuZNVzv+4XUx0QABVCa68+/0mJi8oj4/Y99Xlwuioj6BOn8lSHx4UOs9vr0j2vYUYrJ/G++6zOkRKIihn9aQepTzV14viB6Lo5lP0N68mhWnDxRACjFIP1w9YYwNjZMryEBlNeQSmVR7e7e9hoSib44rLasMqLRtb6+vlm7D5ZKpf3ycNfj4qCY2tnZ2dM2t5tmSY4NbtejexKlO9pJBOndbw/ym4cPtd0ArRyMfD4/ZHeo/uaqz61N4vTDGxWPx9dSqdQ8q+tRdXfhGhUOtXRnPH44u3nwwDE/HAykPi9KcB/uSQDFEBJAMYEEUEwgAdT3OA4BQgIoBRwHgGLiOAAUE8fBrXzvmaATPR0W3TgEdPhU5Xo1x+Hy9WbHQR5m5QnZk/XJOa8DAeW2h0A3x8Ht+pD6GJfgAMUcEkDZQSLdmfhbFUgAZQfp9q1/xK+jKyotUXtQTY5DHdLYlZeqrVNrUE2Og8KQtAbV5DgoDklbUI2OAwdIJO16Jhp7HOh1eXLizcbPw1tBrw89E24dB2snSUjZTqwPPRP/pwRXPN3pB4o5JD1AhQBS+EGFBFKoQXFxHLQGxclx0BYUN8dBS1CR9yWRuneflePgVqHpmTCKb83kxWvZyOJSV4PjkJUn0KwK62txgNawZyKfj4vRc0Pi1UpXpx0H9EzYRzAuRn4hSL1hS3fhAaUJJN6gNILEF5RmkFiCoupON0jsQNE5KXnmQlY3SKxAEaS9V2+K+jlJJ0hsQNUhxV4sCB0h1VK+6j0TNcdBprtdjkPgPQ7omQiZ46Bfz4SGJTg/UIDEABQgMQAFSOqD0tVxYAVKZ8eBDSjdHQcWoOA4uFfHeia49Tjo2TMRYschPD0TKMEZgAIkBqAAiQEoQFIfFBwHBqDgODAABceBASg4Dj7cQrzumQhrj0O4eibgODDomUAJzuAeBUhMQF0aOw5IHECVy3FA8lfe/HvXu5P/1h4V+husANVKAMTXmYAACqnPD9EJ3M3hzm0PgW7XCwyU29/6wPWQ+nCPghQAVa1W4whPsHKKuS0oeUPsrVQqPQhfMKJYU8ydQC3bvVkoFE4BVjCQKNYOU5ap6puR46bdrlpfXx82TbNoGMYmQupPunPaSZZm6BveAflkESFTWtlIJpNZkk+mEQtlNU2M6sXEuBxziIlymrPYfKr6JLGifBgELOUgDVpsvpTnDbCQBhVId42QSIasOr6alcvlBqwtNyJHP+IWiJatCnzKqhua1BIUpJ4+CjAAVnYzLhKE5pcAAAAASUVORK5CYII=' },
      { content: '#B3B3B3' },
      { content: '#808080' },
      { content: '#4D4D4D' },
      { content: '#E6E6E6' },
      { content: '#FFC7C9' },
      { content: '#FFA0A4' },
      { content: '#E3686D' },
      { content: '#D71920' },
      { content: '#FFE3D1' },
      { content: '#FFCBA8' },
      { content: '#FFAB73' },
      { content: '#F58235' }
    ]
  });
  
  let selectedToolbar = function(){
    let targetDataName = "";
    
    switch(toolbarFlag){
      
      // 付箋
      case 1:
        targetDataName = "addNote";
        break;
      
      // テキスト
      case 2:
        targetDataName = "addTextLabel";
        break;
      
      // 矢印
      case 3:
        targetDataName = "addLine";
        break;
      
      // 図形
      case 4:
        targetDataName = "addSharp";
        break;
      
      // 付箋(音声入力)
      case TOOLBAR_VOICE_NOTE_SELECT:
        targetDataName = "voice";
        break;
      
      default:
        break;
    }
    
    if(targetDataName == ""){
      return;
    }
    
    let sideToolBarCtrl = sideToolbar.el.children[0];
    let sideToolBarCtrlCld_len = sideToolBarCtrl.children.length;
    for(let i = 0; i < sideToolBarCtrlCld_len; i++){
      let dataname = sideToolBarCtrl.children[i].dataset.name;
      if(dataname == targetDataName){
        sideToolBarCtrl.children[i].style.backgroundColor = SELECTED_TOOLBAR_COLOR;
        sideToolBarCtrl.children[i].style["margin-right"] = "1px";
        sideToolBarCtrl.children[i].classList.add('active');
      }
    }
    
    // ショート
    let sideToolbarShortCtrl = sideToolbarShort.el.children[0];
    let sideToolbarShortCtrlCld_len = sideToolbarShortCtrl.children.length;
    for(let i = 0; i < sideToolbarShortCtrlCld_len; i++){
      let dataname = sideToolbarShortCtrl.children[i].dataset.name;
      if(dataname == targetDataName){
        sideToolbarShortCtrl.children[i].style.backgroundColor = SELECTED_TOOLBAR_COLOR;
        sideToolBarCtrl.children[i].style["margin-right"] = "1px";
      }
    }
    
  };
  
  let selectedSubToolbarFusen = function(selectfusen){
    
    let selectedFusen = "";
    if(selectfusen){
      selectedFusen = selectfusen;
    } else {
      if(add_note_color){
        
        switch(add_note_color){
          case fusenColor.fusen1.color:
            selectedFusen = "fusen1";
            break;
          case fusenColor.fusen2.color:
            selectedFusen = "fusen2";
            break;
          case fusenColor.fusen3.color:
            selectedFusen = "fusen3";
            break;
          case fusenColor.fusen4.color:
            selectedFusen = "fusen4";
            break;
          case fusenColor.fusen5.color:
            selectedFusen = "fusen5";
            break;
          case fusenColor.fusen6.color:
            selectedFusen = "fusen6";
            break;
          case fusenColor.fusen7.color:
            selectedFusen = "fusen7";
            break;
          case fusenColor.fusen8.color:
            selectedFusen = "fusen8";
            break;
          case fusenColor.fusen9.color:
            selectedFusen = "fusen9";
            break;
          case fusenColor.fusen10.color:
            selectedFusen = "fusen10";
            break;
          default:
            break;
        }
      }
    }
    
    
    if(selectedFusen != ""){
      let ctrl_len = fusenMenubar.el.children.length;
      for(let n = 0; n < ctrl_len; n++){
        let fusenMenuBarCtrl = fusenMenubar.el.children[n];
        let fusenMenuBarCtrlCld_len = fusenMenuBarCtrl.children.length;
        for(let i = 0; i < fusenMenuBarCtrlCld_len; i++){
          let dataname = fusenMenuBarCtrl.children[i].dataset.name;
          if(dataname == selectedFusen){
            fusenMenuBarCtrl.children[i].style.backgroundColor = SELECTED_TOOLBAR_COLOR;
            fusenMenuBarCtrl.children[i].style["margin-right"] = "1px";
          }
        }
      }
    }
    
  };
  
  let selectedSubToolbarVoiceFusen = function(selectVoiceFusen){
    
    let selectedVoiceFusen = "";
    if(selectVoiceFusen){
      selectedVoiceFusen = selectVoiceFusen;
    } else {
      if(add_voice_note_color){
        
        switch(add_voice_note_color){
          case fusenColor.fusen1.color:
            selectedVoiceFusen = "voiceFusen1";
            break;
          case fusenColor.fusen2.color:
            selectedVoiceFusen = "voiceFusen2";
            break;
          case fusenColor.fusen3.color:
            selectedVoiceFusen = "voiceFusen3";
            break;
          case fusenColor.fusen4.color:
            selectedVoiceFusen = "voiceFusen4";
            break;
          case fusenColor.fusen5.color:
            selectedVoiceFusen = "voiceFusen5";
            break;
          case fusenColor.fusen6.color:
            selectedVoiceFusen = "voiceFusen6";
            break;
          case fusenColor.fusen7.color:
            selectedVoiceFusen = "voiceFusen7";
            break;
          case fusenColor.fusen8.color:
            selectedVoiceFusen = "voiceFusen8";
            break;
          case fusenColor.fusen9.color:
            selectedVoiceFusen = "voiceFusen9";
            break;
          case fusenColor.fusen10.color:
            selectedVoiceFusen = "voiceFusen10";
            break;
          default:
            break;
        }
      }
    }
    
    if(selectedVoiceFusen != ""){
      let ctrl_len = voiceFusenMenubar.el.children.length;
      for(let n = 0; n < ctrl_len; n++){
        let voiceFusenMenubarCtrl = voiceFusenMenubar.el.children[n];
        let voiceFusenMenubarCtrlCld_len = voiceFusenMenubarCtrl.children.length;
        for(let i = 0; i < voiceFusenMenubarCtrlCld_len; i++){
          let dataname = voiceFusenMenubarCtrl.children[i].dataset.name;
          if(dataname == selectedVoiceFusen){
            voiceFusenMenubarCtrl.children[i].style.backgroundColor = SELECTED_TOOLBAR_COLOR;
            voiceFusenMenubarCtrl.children[i].style["margin-right"] = "1px";
          }
        }
      }
    }
  };
  
  let selectedSubToolbarLine = function(){
    
    let selectedLine = "";
    
    switch(lineStyleFlag){
      case LINE_STYLE_STRAIGHT:
        selectedLine = "lineStyleStraight1";
        break;
      case LINE_STYLE_DASHED:
        selectedLine = "lineStyleDashed";
        break;
      case LINE_STYLE_DOTTED:
        selectedLine = "lineStyleDotted";
        break;
      default:
        break;
    }
    
    if(selectedLine != ""){
      let ctrl_len = addLineMenu.el.children.length;
      for(let n = 0; n < ctrl_len; n++){
        let addLineMenuCtrl = addLineMenu.el.children[n];
        let addLineMenuCtrlCld_len = addLineMenuCtrl.children.length;
        for(let i = 0; i < addLineMenuCtrlCld_len; i++){
          let dataname = addLineMenuCtrl.children[i].dataset.name;
          if(dataname == selectedLine){
            addLineMenuCtrl.children[i].style.backgroundColor = SELECTED_TOOLBAR_COLOR;
            addLineMenuCtrl.children[i].style["margin-right"] = "1px";
            addLineMenuCtrl.children[i].classList.add('active');
          }
        }
      }
    }
    
  };
  
  let selectedSubToolbarShape = function(){
    
    let selectedShape = "";
    
    switch(figureFlag){
      case FIGURE_MENU_BAR_RECTANGLE:
        selectedShape = "shapeSquare";
        break;
      case FIGURE_MENU_BAR_CIRCLE:
        selectedShape = "shapeCircle";
        break;
      
      case FIGURE_MENU_BAR_UNSELECTED:
        switch(shapeFlag){
          
          case SHAPE_MENU_BAR_CLOUD:
            selectedShape = "shapeCloud";
            break;
          case SHAPE_MENU_BAR_STAR:
            selectedShape = "shapeStar";
            break;
          case SHAPE_MENU_BAR_TRIANGLE:
            selectedShape = "shapeTriangle";
            break;
          case SHAPE_MENU_BAR_CHAT_BUBBLE:
            selectedShape = "chatBubble";
            break;
          case SHAPE_MENU_BAR_HEART:
            selectedShape = "shapeHeart";
            break;
          default:
            break;
        };
        break;
      default:
        break;
    }
    
    if(selectedShape != ""){
      let ctrl_len = shapeMenubar.el.children.length;
      for(let n = 0; n < ctrl_len; n++){
        let shapeMenubarCtrl = shapeMenubar.el.children[n];
        let shapeMenubarCtrlCld_len = shapeMenubarCtrl.children.length;
        for(let i = 0; i < shapeMenubarCtrlCld_len; i++){
          let dataname = shapeMenubarCtrl.children[i].dataset.name;
          shapeMenubarCtrl.children[i].classList.remove('active');
          if(dataname == selectedShape){
            shapeMenubarCtrl.children[i].style.backgroundColor = SELECTED_TOOLBAR_COLOR;
            shapeMenubarCtrl.children[i].classList.add('active');
            // shapeMenubarCtrl.children[i].style["margin-right"] = "1px";
          }
        }
      }
    }
  };
  sideToolbar.on('move-edit:pointerclick', function(event, x, y) {
    
    changeBoardEditMode(!boardEditMode);
    
  });

//add end ymd
  sideToolbar.on('addNote:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    if(userInfo.role == 3){
      return;
    }
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    
    if(toolbarFlag == TOOLBAR_CARD_SELECT){
      releaseSelectToolbar();
      menuDisplayDisabled(boardCtrlId);
      changeCursor();
      return;
    }
    
    releaseSelectToolbar();
    //add st ymd
    toolbarFlag = TOOLBAR_CARD_SELECT;
    selectedToolbar();
    menuDisplayDisabled(boardCtrlId);
    setDisplay(ID_FUSEN_MENUBAR,boardCtrlId);
    
    // ユーザ色選択
    if(userInfo.sticky_note_color != undefined){
      add_note_color = userInfo.sticky_note_color;
    } else {
      add_note_color = fusenColor.fusen1.color;
    }
    changeCursor('add-note-cursor');
    selectedSubToolbarFusen("fusen1");
  });
  
  sideToolbar.on('voice:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    if(userInfo.role == 3){
      return;
    }
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    
    if(toolbarFlag == TOOLBAR_VOICE_NOTE_SELECT){
      releaseSelectToolbar();
      menuDisplayDisabled(boardCtrlId);
      changeCursor();
      return;
    }
    
    releaseSelectToolbar();
    //add st ymd
    toolbarFlag = TOOLBAR_VOICE_NOTE_SELECT;
    selectedToolbar();
    menuDisplayDisabled(boardCtrlId);
    setDisplay(ID_VOICE_FUSEN_MENUBAR,boardCtrlId);
    
    if(userInfo.sticky_note_color != undefined){
      add_voice_note_color = userInfo.sticky_note_color;
    } else {
      add_voice_note_color = fusenColor.fusen1.color;
    }
    
    changeCursor('voice-cursor')
    selectedSubToolbarVoiceFusen("voiceFusen1");
    
    // setDisplay(ID_FUSEN_MENUBAR,boardCtrlId);
    
    // // ユーザ色選択
    // if(userInfo.sticky_note_color != undefined){
    //     add_note_color = userInfo.sticky_note_color;
    // } else {
    //     add_note_color = fusenColor.fusen1.color;
    // }
    // selectedSubToolbarFusen("fusen1");
  });
  
  
  sideToolbar.on('addLine:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    
    if(userInfo.role == 3){
      return;
    }
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    if(toolbarFlag == TOOLBAR_LINK_SELECT){
      releaseSelectToolbar();
      menuDisplayDisabled(boardCtrlId);
      changeCursor();
      return;
    }

//        test(event,TOOLBAR_LINK_SELECT);
    
    releaseSelectToolbar();
    toolbarFlag = TOOLBAR_LINK_SELECT;
    selectedToolbar();
    changeCursor('add-line-cursor');
    menuDisplayDisabled(boardCtrlId);
    
    lineLeftFlag = LINE_CONNECT_STRAIGHT;
    lineStyleFlag = LINE_STYLE_STRAIGHT;
    selectedSubToolbarLine();
    
    //初回表示時のみ実行
    // if(addLineMenubarDisplay == -1){
    //lineMenubar.render().$el.appendTo('#'+ ID_LINE_MENUBAR + boardID);
    //     addLineMenubarDisplay = 0;
    // }
    
    setDisplay(ID_ADD_LINE_MENU,boardCtrlId);
  });
//add Start yamada
  
  addLineMenu.on('lineStyleStraight1:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleDotted"]').removeClass('active');
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleDashed"]').removeClass('active');
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleStraight1"]').addClass('active');
    releaseSelectSubToolbar();
    lineLeftFlag = LINE_CONNECT_STRAIGHT;
    lineStyleFlag = LINE_STYLE_STRAIGHT;
    selectedSubToolbarLine();
  });
  
  addLineMenu.on('lineStyleStraight2:pointerclick', function(event) {
    lineLeftFlag = LINE_CONNECT_STRAIGHT;
    lineStyleFlag = LINE_STYLE_STRAIGHT;
  });
  
  addLineMenu.on('lineStyleDotted:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleDotted"]').addClass('active');
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleStraight1"]').removeClass('active');
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleDashed"]').removeClass('active');
    releaseSelectSubToolbar();
    lineStyleFlag = LINE_STYLE_DOTTED;
    selectedSubToolbarLine();
  });
  
  addLineMenu.on('lineStyleDashed:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleDotted"]').removeClass('active');
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleStraight1"]').removeClass('active');
    $('.joint-toolbar.joint-theme-modern button[data-name="lineStyleDashed"]').addClass('active');
    releaseSelectSubToolbar();
    lineStyleFlag = LINE_STYLE_DASHED;
    selectedSubToolbarLine();
  });
  
  /*ライン　ストレート*/
  lineMenubar.on('lineType:pointerclick', function(event) {
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
    
    lineLeftFlag = LINE_CONNECT_STRAIGHT;
    lineRightFlag = LINE_CONNECT_STRAIGHT;
    
    event.stopPropagation();
    setArrow(boradId, false);
    
  });
  
  lineMenubar.on('lineTypeLeft:pointerclick', function(event) {
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
    
    lineLeftFlag = LINE_CONNECT_ARROW;
    lineRightFlag = LINE_CONNECT_STRAIGHT;
    
    event.stopPropagation();
    setArrow(boradId, false);
    
  });
  
  lineMenubar.on('lineTypeRight:pointerclick', function(event) {
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
    
    lineLeftFlag = LINE_CONNECT_STRAIGHT;
    lineRightFlag = LINE_CONNECT_ARROW;
    
    event.stopPropagation();
    setArrow(boradId, false);
    
  });
  
  
  lineMenubar.on('lineTypeBoth:pointerclick', function(event) {
    
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
    lineLeftFlag = LINE_CONNECT_ARROW;
    lineRightFlag = LINE_CONNECT_ARROW;
    
    event.stopPropagation();
    setArrow(boradId, false);
    
  });
  
  lineMenubar.on('colorPalette:pointerclick', function(event) {
    
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
    lineColor = "#ff0000";
    
    event.stopPropagation();
    setArrow(boradId, true);
  });
  
  lineMenubar.on('colorPalette_black:pointerclick', function(event) {
    
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
    lineColor = "#000000";
    
    event.stopPropagation();
    setArrow(boradId, true);
  });
  
  lineMenubar.on('addTextLabel:pointerclick', function (e) {
    e.target.attributes[0].value = 'joint-widget joint-theme-modern active bg-btn-active';
    let img;
    let textarea = $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] .input-wrapper .textarea');
    $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] img').remove();
    let val = textarea.val().trim().length;
    if (val > 0) img = `<img src="/images/icon-toolbar/keyboard_return-active.png">`;
    else img = `<img src="/images/icon-toolbar/keyboard_return.png">`;
    textarea.css('height', 'auto');
    textarea.css('height',`${textarea.scrollHeight}px`);
    $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"]').removeClass('d-none').append(img);
    textarea.focus();
    contentsEditFlg = true;
  });
  
  function setArrow(boradId, chageColor){
    
    // 操作ログ用の発言内容を削除前に保持
    let graphIndex = getBoardsIndex(boradId);
    let list_arrow = getBoundaryCellList(graphIndex,CELL_TYPE_STANDARD_LINK);
    
    // ライン
    let list_arrow_len = list_arrow.length;
    for(let i = 0; i < list_arrow_len; i++){
      
      let cellView = paper.findViewByModel(list_arrow[i]);
      
      // 色変更する場合は選択した色情報セット
      let color = cellView.model.attributes.attrs.line.stroke;
      if(chageColor){
        color = lineColor;
      }
      
      cellView.model.attributes.attrs.line.stroke = color;
      if(lineRightFlag == LINE_CONNECT_STRAIGHT){
        cellView.model.attributes.attrs.line.targetMarker.stroke = TRANSPARENT;
        cellView.model.attributes.attrs.line.targetMarker.fill = TRANSPARENT;
        
      }else{
        cellView.model.attributes.attrs.line.targetMarker.stroke = color;
        cellView.model.attributes.attrs.line.targetMarker.fill = color;
        //change　Start 20201019
        //cellView.model.attributes.attrs.line.targetMarker.d = 'M 5 -10 L -15 0 L 5 10 Z';
        cellView.model.attributes.attrs.line.targetMarker.d = TRIANGLE_POINT;
        //change　END 20201019
      }
      
      if(lineLeftFlag == LINE_CONNECT_STRAIGHT){
        cellView.model.attributes.attrs.line.sourceMarker.stroke = TRANSPARENT;
        cellView.model.attributes.attrs.line.sourceMarker.fill = TRANSPARENT;
      }else{
        cellView.model.attributes.attrs.line.sourceMarker.stroke = color;
        cellView.model.attributes.attrs.line.sourceMarker.fill = color;
        //change　Start 20201019
        //cellView.model.attributes.attrs.line.sourceMarker.d = 'M 5 -10 L -15 0 L 5 10 Z';
        cellView.model.attributes.attrs.line.sourceMarker.d = TRIANGLE_POINT;
        //change　END 20201019
      }
      cellView.update();
      addUserOperationLogs({
        cell: cellView.model,
        selectObjectId: cellView.model.attributes.attrs[".objectId"].value,
        kind: USER_OPERATION_LINK_MOVE
      });
    }
    
  }
  
  sideToolbar.on('addSharp:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    if(userInfo.role == 3){
      return;
    }
    
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    
    if(toolbarFlag == TOOLBAR_FIGURE_SELECT){
      releaseSelectToolbar();
      menuDisplayDisabled(boardCtrlId);
      changeCursor();
      return;
    }
    
    releaseSelectToolbar();
    figureFlag = FIGURE_MENU_BAR_RECTANGLE;
    toolbarFlag = TOOLBAR_FIGURE_SELECT;
    selectedToolbar();
    changeCursor('add-sharp-cursor');
    menuDisplayDisabled(boardCtrlId);
    selectedSubToolbarShape();
    
    // //初回表示時のみ実行
    // if(addFigureMenubarDisplay == -1){
    //     shapeMenubar.render().$el.appendTo('#'+ ID_ADD_FIGURE_MENUBAR + boardID);
    
    //     //addFigureMenubar.render().$el.appendTo('#'+ ID_ADD_FIGURE_MENUBAR + boardID);
    //     addFigureMenubarDisplay = 0;
    // }
    
    setDisplay( ID_ADD_FIGURE_MENUBAR,boardCtrlId);
    
    toolbarFlag = TOOLBAR_FIGURE_SELECT;
    
  });
  
  shapeMenubar.on('shapeSquare:pointerclick', function(event) {
    releaseSelectSubToolbar();
    figureFlag = FIGURE_MENU_BAR_RECTANGLE;
    selectedSubToolbarShape();
  });
  
  shapeMenubar.on('shapeCircle:pointerclick', function(event) {
    return;
    releaseSelectSubToolbar();
    figureFlag = FIGURE_MENU_BAR_CIRCLE;
    selectedSubToolbarShape();
  });
  //星
  shapeMenubar.on('shapeStar:pointerclick', function(event) {
    return;
    releaseSelectSubToolbar();
    shapeFlag = SHAPE_MENU_BAR_STAR;
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    selectedSubToolbarShape();
  });
  
  //三角
  shapeMenubar.on('shapeTriangle:pointerclick', function(event) {
    return;
    releaseSelectSubToolbar();
    shapeFlag = SHAPE_MENU_BAR_TRIANGLE;
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    selectedSubToolbarShape();
  });
  
  //heart
  shapeMenubar.on('shapeHeart:pointerclick', function(event) {
    return;
    releaseSelectSubToolbar();
    shapeFlag = SHAPE_MENU_BAR_HEART;
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    selectedSubToolbarShape();
  });
  
  //吹き出し
  shapeMenubar.on('chatBubble:pointerclick', function(event) {
    return;
    releaseSelectSubToolbar();
    shapeFlag = SHAPE_MENU_BAR_CHAT_BUBBLE;
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    selectedSubToolbarShape();
  });
  
  //雲
  shapeMenubar.on('shapeCloud:pointerclick', function(event) {
    return;
    releaseSelectSubToolbar();
    shapeFlag = SHAPE_MENU_BAR_CLOUD;
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    selectedSubToolbarShape();
  });
//add End yamada
  sideToolbar.render().$el.appendTo('#toolbar-container-' + boardID);
  // 常時音認を一時的に停止 -->
  $('.joint-toolbar.joint-theme-modern button[data-name="chat"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'right'});
  // <-- 常時音認を一時的に停止
  $('.joint-toolbar.joint-theme-modern button[data-name="edit"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'right'});
  $('.joint-toolbar.joint-theme-modern button[data-name="template"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'right'});
  $('.joint-toolbar.joint-theme-modern button[data-name="aspectRatio"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'right'});
  $('.joint-toolbar.joint-theme-modern button[data-name="more"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'right'});
  $(`#toolbar-container-${boardID} .joint-toolbar`).append(`
        <div class="joint-toolbar-group joint-theme-modern" data-group="extends">
           <button class="joint-widget joint-theme-modern" data-type="button" data-name="expandMenu" id="expandMenu"></button>
        </div>`
  );
  // bottomToolbar.render().$el.appendTo('#bottomToolbar-container-' + boardID);
  // nakamatsu 未実装メニューの押下不可対応 (処理タイミングを修正) -->
  // new joint.ui.Tooltip({
  //     target: '[data-tooltip]',
  //     content: $(this).data('tooltip'),
  //     padding: 10,
  //     // trigger: 'click',
  //     // hideTrigger: 'mouseout'
  // });
  // <-- nakamatsu 未実装メニューの押下不可対応 (処理タイミングを修正)
//add st ymd
  //サイドツールバー・short版
  sideToolbarShort.render().$el.appendTo('#toolbarShort-container-' + boardID);
  //付箋メニューバー
  fusenMenubar.render().$el.appendTo('#'+ ID_FUSEN_MENUBAR + boardID);
  
  fusenMenubar.on('fusen1:pointerclick', function(event) {
    //add end ymd
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen1.color;
    if(userInfo.sticky_note_color != undefined){
      add_note_color = userInfo.sticky_note_color;
    }
    selectedSubToolbarFusen("fusen1");
  });
  
  fusenMenubar.on('fusen2:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen2.color;
    selectedSubToolbarFusen();
  });
  
  
  fusenMenubar.on('fusen3:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen3.color;
    selectedSubToolbarFusen();
    
  });
  
  fusenMenubar.on('fusen4:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen4.color;
    selectedSubToolbarFusen();
  });
  
  
  fusenMenubar.on('fusen5:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen5.color;
    selectedSubToolbarFusen();
  });
  
  fusenMenubar.on('fusen6:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen6.color;
    selectedSubToolbarFusen();
  });
  
  fusenMenubar.on('fusen7:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen7.color;
    selectedSubToolbarFusen();
  });
  
  fusenMenubar.on('fusen8:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen8.color;
    selectedSubToolbarFusen();
  });
  
  
  fusenMenubar.on('fusen9:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen9.color;
    selectedSubToolbarFusen();
  });
  
  fusenMenubar.on('fusen10:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_note_color = fusenColor.fusen10.color;
    selectedSubToolbarFusen();
  });
  
  voiceFusenMenubar.render().$el.appendTo('#'+ ID_VOICE_FUSEN_MENUBAR + boardID);
  
  voiceFusenMenubar.on('voiceFusen1:pointerclick', function(event) {
    //add end ymd
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen1.color;
    if(userInfo.sticky_note_color != undefined){
      add_voice_note_color = userInfo.sticky_note_color;
    }
    selectedSubToolbarVoiceFusen("voiceFusen1");
  });
  
  voiceFusenMenubar.on('voiceFusen2:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen2.color;
    selectedSubToolbarVoiceFusen();
  });
  
  
  voiceFusenMenubar.on('voiceFusen3:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen3.color;
    selectedSubToolbarVoiceFusen();
    
  });
  
  voiceFusenMenubar.on('voiceFusen4:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen4.color;
    selectedSubToolbarVoiceFusen();
  });
  
  
  voiceFusenMenubar.on('voiceFusen5:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen5.color;
    selectedSubToolbarVoiceFusen();
  });
  
  voiceFusenMenubar.on('voiceFusen6:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen6.color;
    selectedSubToolbarVoiceFusen();
  });
  
  voiceFusenMenubar.on('voiceFusen7:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen7.color;
    selectedSubToolbarVoiceFusen();
  });
  
  voiceFusenMenubar.on('voiceFusen8:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen8.color;
    selectedSubToolbarVoiceFusen();
  });
  
  
  voiceFusenMenubar.on('voiceFusen9:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen9.color;
    selectedSubToolbarVoiceFusen();
  });
  
  voiceFusenMenubar.on('voiceFusen10:pointerclick', function(event) {
    releaseSelectSubToolbar();
    add_voice_note_color = fusenColor.fusen10.color;
    selectedSubToolbarVoiceFusen();
  });
  
  //付箋のサブメニュー
  fusenSubMenubar.render().$el.appendTo('#'+ ID_FUSEN_SUB_MENUBAR + boardID);
  // リアクションアイコンの無効化対応 -->
  $('.joint-toolbar.joint-theme-modern button[data-name="reactionStickyNote"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  // <-- リアクションアイコンの無効化対応
  //テキストメニュー
  textMenubar.render().$el.appendTo('#'+ ID_TEXT_MENUBAR + boardID);
  
  // deha_eqr-239 start
  textMenubar.el.innerHTML =
    '<div class="joint-toolbar-group joint-theme-modern" data-group="default">' +
    // '<button class="joint-widget joint-theme-modern" data-type="button" data-name="addTextLabel"></button>' +
    `<button id = "textMenubar-${boardID}-addTextLabel" class="joint-widget joint-theme-modern" data-type="button" data-name="addTextLabel"></button>` +
    // nakamatsu 未実装メニューの押下不可対応 -->
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="fontDownload"></button>' +
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="fontSize"></button>' +
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="fontColorPalette"></button>' +
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="fontBold"></button>' +
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="fontItalic"></button>' +
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="fontUnderline"></button>' +
    '<button class="joint-widget joint-theme-modern disableMenuYYkb" data-type="button" data-tooltip="まもなく！" data-tooltip-position="top" data-name="formatClear"></button>' +
    // <-- nakamatsu 未実装メニューの押下不可対応
    '<button class="joint-widget joint-theme-modern" data-type="button" data-name="dustBox"></button>' +
    '</div>' +
    '<div class="text-edit-area-custom d-none">' +
    '<div class="joint-widget joint-theme-modern" data-type="textarea" data-name="textEditArea">' +
    '<label></label>' +
    '<div class="input-wrapper d-flex justify-content-between align-items-end">' +
    // '<textarea class="textarea"></textarea>' +
    // '<img src="/images/keyboard_return.png">' +
    `<textarea id = "text-edit-area-custom-${boardID}-textarea" class="textarea"></textarea>` +
    `<img id = "textMenubar-${boardID}-keyboard-return" src="/images/keyboard_return.png">` +
    '</div>' +
    '</div>' +
    '</div>';
  //線のメニュー
  
  // $(document).on('click', '.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]', function() {
  $(`#textMenubar-${boardID}-addTextLabel`).on('click', function() {
    // $('.input-wrapper .textarea').focus();
    // $('.text-edit-area-custom').removeClass('d-none');
    // $('.joint-them-modern').css('background', 'none');
    // $(this).addClass('active bg-btn-active');
    // let text = $(this).parent().parent().find('.text-edit-area-custom').children().find('.input-wrapper').find('.textarea').val();
    // if (text && text.length > 0) $('.input-wrapper img').attr('src', '/images/keyboard_return-active.png');
    // else $('.input-wrapper img').attr('src', '/images/keyboard_return.png');
    // contentsEditFlg = true;
    // $('.text-edit-area-custom .textarea').css('height', '52px');
    
    
    // $('.textMenubar-container .textarea').on('input', function () {
    //     this.style.height = 'auto';
    //     this.style.height = (this.scrollHeight) + 'px';
    //     if ($(this).val().length > 0) $('.input-wrapper img').attr('src', '/images/keyboard_return-active.png');
    //     else $('.input-wrapper img').attr('src', '/images/keyboard_return.png');
    // });
    
    // テキスト直接入力対応 -->
    jointUiTextEditorClose(activeBoardId);
    // <-- テキスト直接入力対応
    
    addTextLabel_Click();
  });
  
  // $(document).on('dblclick', '.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]', function () {
  $(`#textMenubar-${boardID}-addTextLabel`).on('dblclick', function () {
    $('.text-edit-area-custom').addClass('d-none');
    $(this).removeClass('active bg-btn-active');
    $(this).parent().parent().find('.text-edit-area-custom').children().find('.input-wrapper').find('.textarea').val(oldTextArea);
  });
  
  // $(document).on('click', '.text-edit-area-custom img', function (event) {
  $(`#textMenubar-${boardID}-keyboard-return`).on('click', function () {
    if (paper.$el[0].id === activeBoardId) addTextFromTextMenuBar(event, paper);
  });
  
  // $(document).on('click', '.text-edit-area-custom .textarea', function () {
  $(`#text-edit-area-custom-${boardID}-textarea`).on('click', function () {
    this.style.height = (this.scrollHeight) + 'px';
    if ($(this).val().length > 0) $('.input-wrapper img').attr('src', '/images/keyboard_return-active.png');
    else $('.input-wrapper img').attr('src', '/images/keyboard_return.png');
  });
  
  // $(document).on('click', '.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]', function () {
  // $(`#textMenubar-${boardID}-addTextLabel`).on('click', function () {
  //     $(this).addClass('bg-btn-active');
  // });
  
  $(document).on('click', '.toolbar-container .joint-widget.joint-theme-modern[data-name="addTextLabel"]', function () {
    $(this).toggleClass('active bg-btn-active');
    $('.text-edit-area-custom .textarea').css('height', '52px');
  })
  // nakamatsu 未実装メニューの押下不可対応 -->
  // $(document).on('click', '.textMenubar-container', function () {
  //     $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').addClass('bg-btn-active');
  // })
  // <-- nakamatsu 未実装メニューの押下不可対応
  
  // nakamatsu 未実装メニューの押下不可対応 -->
  // $(document).on('click', '.textMenubar-container', function () {
  //     $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').addClass('bg-btn-active');
  // })
  // <-- nakamatsu 未実装メニューの押下不可対応
  
  lineMenubar.render().$el.appendTo('#'+ ID_LINE_MENUBAR + boardID);
  // nakamatsu 未実装メニューの押下不可対応 -->
  $('.joint-toolbar.joint-theme-modern button[data-name="lineTypeRight"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="lineMiddle"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="colorPalette_black"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="lineSize"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="fontColorPalette"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="fontSize"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="ellipsis"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  $('.joint-toolbar.joint-theme-modern button[data-name="textEditAreaLine"]').attr({'data-tooltip':'まもなく！','data-tooltip-position':'top'}).addClass('disableMenuYYkb');
  // $('.joint-toolbar.joint-theme-modern button[data-name="addTextLabel"]').attr({'data-tooltip':'テキスト','data-tooltip-position':'top'});
  new joint.ui.Tooltip({
    target: '[data-tooltip]',
    content: $(this).data('tooltip'),
    padding: 10,
  });
  // <-- nakamatsu 未実装メニューの押下不可対応
  
  addLineMenu.render().$el.appendTo('#'+ ID_ADD_LINE_MENU + boardID);
  // addLineMenu2.render().$el.appendTo('#'+ ID_ADD_LINE_MENU + boardID);
  // addLineMenu3.render().$el.appendTo('#'+ ID_ADD_LINE_MENU + boardID);
  // addLineMenu3.render().$el.appendTo('#'+ ID_ADD_LINE_MENU + boardID);
  // addLineMenu3.render().$el.appendTo('#'+ ID_ADD_LINE_MENU + boardID);
  //矩形のメニュー
  shapeSubMenubar.render().$el.appendTo('#'+ ID_SHAPE_SUB_MENUBAR + boardID);
  //矩形のサブメニュー
  shapeMenubar.render().$el.appendTo('#'+ ID_ADD_FIGURE_MENUBAR + boardID);
  // 複数選択時のメニュー
  multiSelectMenubar.render().$el.appendTo('#'+ ID_MULTI_SELECT_MENUBAR + boardID);
  
  // textareaの文字数制限付加
  let inputWrappers =  document.getElementsByClassName("input-wrapper");
  for(let i = 0; i < inputWrappers.length; i++){
    
    if(inputWrappers[i].firstChild.type == "textarea"){
      inputWrappers[i].firstChild.maxLength = MAX_WORD_LENGTH_TEXT;
    }
  }
  
  //付箋
  fusenSubMenubar.on('colorPalette:pointerclick', function(event) {
    // 操作ログ用の発言内容を削除前に保持
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_FUSEN_SUB_MENUBAR,"");
    let graphIndex = getBoardsIndex(boradId);
    let removeList_Note = getBoundaryCellList(graphIndex ,CELL_TYPE_NOTE);
    
    // 付箋の色変更
    let removeList_Note_len = removeList_Note.length;
    for(let i = 0; i < removeList_Note_len; i++){
      
      // 付箋
      let cellView = paper.findViewByModel(removeList_Note[i]);
      // let childrenList = cellView.el.lastChild.children;
      // let cardId;
      // for(let j = 0; j < childrenList.length;j++){
      //     if(childrenList[j].classList.value =="card"){
      //         cardId = childrenList[j].id
      //         break;
      //     }
      // }
      // document.getElementById(cardId).style.fill = "#FF0000";
      cellView.model.attributes.attrs[".card"].fill = "#FF0000";
      cellView.update();
    }
  });
  
  //*付箋の削除(ツールバー：ゴミ箱) */
  fusenSubMenubar.on('dustBox:pointerclick', function(event) {
    keyDownDelete();
  });
  
  //*付箋のダウンロード(ツールバー：ダウンロード) */
  fusenSubMenubar.on('download:pointerclick', function(event) {
    try
    {
      event.stopPropagation();
      
      let boradId = (event.currentTarget.offsetParent.id).replace(ID_FUSEN_SUB_MENUBAR,"");
      let graphIndex = getBoardsIndex(boradId);
      
      let selectList = getBoundaryCellList(graphIndex ,CELL_TYPE_NOTE);
      for(let i = 0; i < selectList.length; i++){
        
        let cellView = paper.findViewByModel(selectList[i]);
        
        if(cellView.model.attributes.kind != OBJECT_KIND_STICKY_NOTE_FILE) {
          continue;
        }
        
        // image以外の要素をダウンロードする
        let url = cellView.model.attributes.file_info.url;
        let name = cellView.model.attributes.file_info.file_name;
        $("body").append(`<a id='download-file' href='${url}' download='${name}'>Donload File</a>`);
        $("#download-file")[0].click();
        $("#download-file").remove();
      }
      
      // 選択注の画像ファイルのリスト
      let selectList_Image = getBoundaryCellList(graphIndex ,CELL_TYPE_IMAGE);
      for(let i = 0; i < selectList_Image.length; i++){
        
        let cellView = paper.findViewByModel(selectList_Image[i]);
        
        // 画像のダウンロードの処理
        let url = cellView.model.attributes.file_info.url;
        let name = cellView.model.attributes.file_info.file_name;
        $("body").append(`<a id='download-file' href='${url}' download='${name}'>Donload File</a>`);
        $("#download-file")[0].click();
        $("#download-file").remove();
      }
      
    } catch (err) {
      writeYYKnowledgeBoardErrorLog(err);
    }
  });
  
  // リアクションアイコンの無効化対応 -->
  // fusenSubMenubar.on('reactionStickyNote:pointerclick', function(event) {
  //     event.stopPropagation();
  
  //     let boardID = (event.currentTarget.offsetParent.id).replace(ID_FUSEN_SUB_MENUBAR,"");
  //     let graphIndex = getBoardsIndex(boardID);
  //     let listStickyNote = getBoundaryCellList(graphIndex ,CELL_TYPE_NOTE);
  
  //     for(let i = 0; i < listStickyNote.length; i++){
  //         let cellView = paper.findViewByModel(listStickyNote[i]);
  //         yyKnowledgeBoard.reactionStickyNote(cellView, graphIndex);
  //     }
  
  //     $(`#${ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id}`).css('display','none');
  // });
  // <-- リアクションアイコンの無効化対応
  
  fusenSubMenubar.on('changeColor:pointerclick', function(event) {
    event.stopPropagation();
    $('.custom-select-wrapper').removeClass('color-active');
    let colorKey = event.target.classList[2];
    $(`.custom-color-select[data-color-key= ${colorKey}]`).parent().addClass('color-active');
    
    let boardID = (event.currentTarget.offsetParent.id).replace(ID_FUSEN_SUB_MENUBAR,"");
    let graphIndex = getBoardsIndex(boardID);
    let listStickyNote = getBoundaryCellList(graphIndex ,CELL_TYPE_NOTE);
    
    for(let i = 0; i < listStickyNote.length; i++){
      let cellView = paper.findViewByModel(listStickyNote[i]);
      let scale = $(`#zoom-range-${boards[graphIndex].id}`).val() / 100;
      let offsetY = 60;
      let changeColor = $(`#${ID_CHANGE_COLOR_STICKY_NOTE + boards[graphIndex].id}`);
      
      if(scale > 1){
        offsetY += (scale - 1) * 15;
      }
      
      changeColor.attr('style', `left: ${(cellView.model.attributes.position.x) * scale}px; top: ${(cellView.model.attributes.position.y) * scale - offsetY}px; display: flex;`);
    }
  });
//add 20201020
  
  // * nakamatsu
  fusenSubMenubar.on('imageSearch:pointerclick', (e) => {
    
    e.stopPropagation();
    analysisCount.getConcernedImage();
    
  }) ;
  // nakamatsu *
  
  fusenSubMenubar.on('comment:pointerclick', async (e) => {
    e.stopPropagation();
    window_blur();
    // let targetId = selectCell.attributes.attrs[".objectId"].value;
    let targetId = selectStickyNote.id;
    let x = selectStickyNote.attributes.position.x;
    let y = selectStickyNote.attributes.position.y;
    
    // ボード外に出ないように調整
    let posX = x + 270;
    let posY = y + 50;
    if(posX + MIN_OBJECT_WIDTH > BOARD_WIDTH){
      posX = BOARD_WIDTH - MIN_OBJECT_WIDTH;
    }
    if(posY + MIN_OBJECT_HEIGHT > BOARD_HEIGHT){
      posY = BOARD_HEIGHT - MIN_OBJECT_HEIGHT;
    }
    
    // コメントオブジェクト作成
    let param = {
      x: posX,
      y: posY,
      width: MIN_OBJECT_WIDTH,
      height: MIN_OBJECT_HEIGHT,
      audioPath: "",
      contents: " ",
    }
    let resData = await createCommentCell(param, undefined);
    
    // コメント作成後に処理する関数(線作成とか)
    let finishNotify = async function(cells){
      
      setTimeout(async () => {
        
        // コメント追加時に編集用テキストボックス表示 -->
        addTextLabel_Click();
        // <-- コメント追加時に編集用テキストボックス表示
        
        let sourceCell = cells[0];
        let sourceId = resData.cell.id;
        
        let linkParam = {
          targetId: targetId,
          sourceId: sourceId,
        };
        let commentLinkData = await createCommentLinkCell(linkParam);
        if(!commentLinkData){
          return;
        }
        AddObject(commentLinkData.sendData, commentLinkData.cell);
        
      },300)
    };
    
    // DB登録
    AddObject(resData.sendData, resData.cell, undefined, finishNotify);
    
  });
  
  textMenubar.on('OK:pointerclick', function(event) {
    //var textMenuBarName = event.target.childNodes[0].parentElement.childNodes[0].parentNode.childNodes[0].parentElement.nextElementSibling.offsetParent.attributes[0].nodeValue;
    var textMenuBarName = event.currentTarget.offsetParent.id;
    var textMenuChildrenList = document.getElementById(textMenuBarName).children[0].children;
    //矢印のメニューとの表示統一化
    //var editComment = textMenuChildrenList[0].children[0].childNodes[1].firstElementChild.value;
    var editComment = textMenuChildrenList[0].children[2].childNodes[1].firstElementChild.value;
    //矢印のメニューとの表示統一化
    
    let boardId = (event.currentTarget.offsetParent.id).replace(ID_TEXT_MENUBAR,"");
    let graphIndex = getBoardsIndex(boardId);
    let removeList_Note = getBoundaryCellList(graphIndex,CELL_TYPE_TEXT);
    if(removeList_Note[0].collection.models[1]._previousAttributes.attrs.label != null){
      removeList_Note[0].collection.models[1]._previousAttributes.attrs.label.text = editComment;
    }
    let cellView = paper.findViewByModel(removeList_Note[0]);
    cellView.selectors.label.textContent = editComment;
    cellView.model.attributes.attrs.label.text = editComment;
    
    addUserOperationLog(USER_OPERATION_EDIT, cellView.model, undefined);
    
  });
  /*     textMenubar.on('Cancel:pointerclick', function(event) {
        alert("キャンセル!！");
    }); */
  //*textの削除(ツールバー：ゴミ箱) */
  textMenubar.on('dustBox:pointerclick', function(event) {
    keyDownDelete();
  });
  
  $('#yyKnowledgeBoard-videocall, #meeting-records-top, #my-board-bar, #yyKnowledgeBoard-teamName, .zoomArea-container, .toolbar-container, .boardNameArea-container')
    .mouseup(event => {
      getTextEditedLine(event, paper);
      setDisplayNone(ID_LINE_MENUBAR, activeBoardId);
      addTextFromTextMenuBar(event, paper);
    });
  
  $(document).on('click', '.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] img', function (evt) {
    if (paper.$el[0].id === activeBoardId) {
      getTextEditedLine(evt, paper);
      setDisplayNone(ID_LINE_MENUBAR, activeBoardId);
    }
  });
  
  $(document).on('click', '.joint-toolbar-group .joint-theme-modern[data-name=dustBox]', function() {
    keyDownDelete();
  });
  
  //add 20201021 矢印＞コメント追加
  // lineMenubar.on('OK:pointerclick', function(event) {
  //
  //     var lineMenuBarName = event.currentTarget.offsetParent.id;
  //     var lineMenuChildrenList = document.getElementById(lineMenuBarName).children[0].children;
  //     //var editComment = lineMenuChildrenList[0].children[6].childNodes[1].firstElementChild.value;
  //     var editComment = lineMenuChildrenList[0].children[8].childNodes[1].firstElementChild.value;
  //
  //     let boradId = (event.currentTarget.offsetParent.id).replace(ID_LINE_MENUBAR,"");
  //     let graphIndex = getBoardsIndex(boradId);
  //     let removeList_Note = getBoundaryCellList(graphIndex,CELL_TYPE_STANDARD_LINK);
  //     removeList_Note[0].attributes.labels[0].attrs.text.text = editComment;
  //
  //     let cellView = paper.findViewByModel(removeList_Note[0]);
  //     cellView.el.children[2].lastElementChild.lastChild.textContent= editComment;
  //
  //     // Del Start 20201111 kobayashi
  //     // if(deleteSpace(editComment).length > 0){
  //     //     cellView.el.firstChild.attributes[0].nodeValue = "#ffffff";
  //     //     //cellView.el.children[2].firstChild.childNodes[0].attributes[2].nodeValue = "#ffffff";
  //     // }else{
  //     //     cellView.el.firstChild.attributes[0].nodeValue = TRANSPARENT;
  //     //     //cellView.el.children[2].firstChild.childNodes[0].attributes[2].nodeValue = TRANSPARENT;
  //     // }
  //     // Del End 20201111
  //
  //     addUserOperationLogLink(USER_OPERATION_EDIT, cellView.model); // Add 20201111 kobayahsi
  //
  // });
  //矢印＞ゴミ箱
  lineMenubar.on('dustBox:pointerclick', function(event) {
    
    keyDownDelete();
  });
  
  //テキスト
  sideToolbar.on('addTextLabel:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    if(userInfo.role == 3){
      return;
    }
    figureFlag = FIGURE_MENU_BAR_UNSELECTED;
    
    if(toolbarFlag == TOOLBAR_TEXT_SELECT){
      releaseSelectToolbar();
      menuDisplayDisabled(boardCtrlId);
      changeCursor();
      return;
    }
    
    releaseSelectToolbar();
    toolbarFlag = TOOLBAR_TEXT_SELECT;
    selectedToolbar();
    changeCursor('add-text-label-cursor');
    menuDisplayDisabled(boardCtrlId);
    
  });
  
  sideToolbar.on('doubleArrowUp:pointerclick', function(event) {
    $('.joint-toolbar.joint-theme-modern button').removeClass('active');
    setDisplayNone('toolbar-container-',boardCtrlId);
    setDisplay('toolbarShort-container-',boardCtrlId);
    
  });
  
  //サイドツールバー・short版
  sideToolbarShort.on('doubleArrowDown:pointerclick', function(event) {
    setDisplay('toolbar-container-',boardCtrlId);
    setDisplayNone('toolbarShort-container-',boardCtrlId);
  });
  
  // background template ※※※
  // sideToolbar.on('template:pointerclick', function(event) {
  //     changeCursor();
  //     $('.joint-toolbar.joint-theme-modern button').removeClass('active');
  //     if(userInfo.role == 3){
  //         return;
  //     }
  //     releaseSelectToolbar();
  //     toolbarFlag = TOOLBAR_TEMPLATE_SELECT;
  //     showTemplateModal();
  //     menuDisplayDisabled(boardCtrlId);
  // });
  
  //add end ymd
  /*****************************/
  //矩形のカラー変更
  /*****************************/
  shapeSubMenubar.on('colorPalette:pointerclick', function(event) {
    
    // 操作ログ用の発言内容を削除前に保持
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_SHAPE_SUB_MENUBAR,"");
    let graphIndex = getBoardsIndex(boradId);
    let shapeList_Note = getBoundaryShapeCellList(graphIndex);
    
    // 付箋の色変更
    let shapeList_Note_len = shapeList_Note.length;
    for(let i = 0; i < shapeList_Note_len; i++){
      
      // 付箋
      let cellView = paper.findViewByModel(shapeList_Note[i]);
      
      cellView.model.attributes.attrs.body.stroke = "#FF0000";
      cellView.update();
    }
    addUserOperationLog(USER_OPERATION_EDIT, shapeList_Note, undefined);
    
  });
  
  shapeSubMenubar.on('shape_colorPalette_black:pointerclick', function(event) {
    
    // 操作ログ用の発言内容を削除前に保持
    let boradId = (event.currentTarget.offsetParent.id).replace(ID_SHAPE_SUB_MENUBAR,"");
    let graphIndex = getBoardsIndex(boradId);
    let shapeList_Note = getBoundaryShapeCellList(graphIndex);
    
    // 付箋の色変更
    let shapeList_Note_len = shapeList_Note.length;
    for(let i = 0; i < shapeList_Note_len; i++){
      
      // 付箋
      let cellView = paper.findViewByModel(shapeList_Note[i]);
      
      cellView.model.attributes.attrs.body.stroke = "#000000";
      cellView.update();
    }
    addUserOperationLog(USER_OPERATION_EDIT, shapeList_Note, undefined);
  });
  
  /*****************************/
  //矩形のゴミ箱
  /*****************************/
  shapeSubMenubar.on('dustBox:pointerclick', function(event) {
    
    keyDownDelete();
  });
  
  // 複数選択時のゴミ箱アイコンクリック
  multiSelectMenubar.on('dustBox:pointerclick', function(event) {
    
    keyDownDelete();
  });
  
  boards.push({
    graph : jointJSGraph,
    // commandManager: commandManager,
    paperScroller : paperScroller,
    paper : paper,
    name : boardName,
    id: boardID,
    jsonSaveValid: false,
    kind: boardKind
  });
  
  document.getElementById(boardCtrlId).addEventListener('click', (e) => {
    activeBoardId = e.currentTarget.id;
  });
  
  // 音声認識
  // 音認のStop対応 -->
  fusenSubMenubar.on('onseiNinshiki:pointerclick', async function(event) {
    // <-- 音認のStop対応
    
    // * nakamatsu 常時音声認識対応 -->
    analysisCount.stop();
    voiceRemark.stop();
    // nakamatsu * <-- 常時音声認識対応
    
    await onseininshikiStickyNote.stop();
    // 既に音声認識がONになっていたらリターンする。
    let headChildren = document.head.children;
    let head_len = headChildren.length;
    for(let i = 0; i < head_len; i++){
      if(headChildren[i].innerHTML.indexOf('[data-name="onseiNinshiki"]') != -1){
        return;
      }
    }
    
    commentTargetCell = selectStickyNote;
    let x = selectStickyNote.attributes.position.x;
    let y = selectStickyNote.attributes.position.y;
    let boardFrame = document.getElementById(`yyKnowledgeBoard-frame-${activeBoardId}`);
    let textLbl = document.getElementById('onseiNinshiki');
    if(boardFrame != null && textLbl != null){
      textLbl.style.display = "";
      boardFrame.appendChild(textLbl);
    }
    
    let commentLblHide = () => {
      textLbl.innerHTML = "";
      textLbl.style.display = "none";
    };
    
    let recResultNotify = async (arg) => {
      
      textLbl.innerHTML = `<span class="onseiNinshiki-span">${arg.text}</span>`;
      if (!arg.isFinal) {
        return;
      }
      
      setTimeout(commentLblHide, 1000);
      setToolbarIcon(false, "onseiNinshiki", TOOLBAR_ICON_CHANGE_ONSEININSHIKI);
      
      if (!arg.text) {
        return;
      }
      let text = arg.text.substring(0, MAX_WORD_LENGTH_TEXT);
      
      // 指定文字数で改行入れる。
      let text_len = text.length
      let text_tmp = "";
      let returnCount = 0;
      for(let i = 0; i < text_len; i++){
        if(text_len > i + WORD_RETURN_MAX){
          text_tmp += text.substring(i,  i + WORD_RETURN_MAX) + '\n';
          i += WORD_RETURN_MAX - 1;
          returnCount++;
        } else {
          text_tmp += text.substring(i,  text_len);
          break;
        }
      }
      
      // ボード外に出ないように調整
      let posX = x + 270;
      let posY = y + 50;
      let width = returnCount === 0 ? COMMENT_WORD_PX * text_len : COMMENT_WORD_PX * WORD_RETURN_MAX;
      let height = 80 + 24 * returnCount;
      if(posX + width > BOARD_WIDTH){
        posX = BOARD_WIDTH - width;
      }
      if(posY + height > BOARD_HEIGHT){
        posY = BOARD_HEIGHT - height;
      }
      
      let param = {
        x: posX,
        y: posY,
        width: width,
        height: height,
        audioPath: "",
        contents: text_tmp,
        speechedAt: arg.speechedAt.format('YYYY-MM-DD HH:mm:ss.SSS')
      }
      let resData = await createCommentCell(param, arg.soundDataURL);
      AddObject(resData.sendData, resData.cell, resData.reader_res);
    };
    
    let startTimeoutNotify = () => {
      setToolbarIcon(false, "onseiNinshiki", TOOLBAR_ICON_CHANGE_ONSEININSHIKI);
    };
    
    speechRecognitionMng.oneShotStart(AUTO_STOP_REC_TIME, startTimeoutNotify, recResultNotify, true);
    
    setToolbarIcon(true, "onseiNinshiki", TOOLBAR_ICON_CHANGE_ONSEININSHIKI);
    
    let stopFunc = (selectRemove = true) => {
      commentLblHide();
      setToolbarIcon(false, "onseiNinshiki", TOOLBAR_ICON_CHANGE_ONSEININSHIKI);
      if(selectRemove){
        let graphIndex = getBoardsIndex(activeBoardId);
        joint.ui.FreeTransform.clear(boards[graphIndex].paper);
        boards[graphIndex].paper.removeTools();
        menuDisplayDisabled(boards[graphIndex].id);
      }
    };
    // 常時音声認識対応 -->
    voiceRemark.setStopFunc(stopFunc, false);
    // <-- 常時音声認識対応
    onseiNinshikiCommentStopFnc = stopFunc;
    onseininshikiStickyNote.setStopFunc(stopFunc);
  });
  
  sideToolbar.on('boardSave:pointerclick', function(event) {
    knowledgeDlImg_Click();
  });
  
  // 常時音認を一時的に停止 -->
  // sideToolbar.on('chat:pointerclick', (e) => {
  //     analysisCount.buttonClick();
  // });
  // <-- 常時音認を一時的に停止
  
  // Add Tooltip
  // $('.joint-toolbar.joint-theme-modern button[data-name="move-edit"]').attr({'data-tooltip':'ま編集モード','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="addNote"]').attr({'data-tooltip':'付せん','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="voice"]').attr({'data-tooltip':'付せん  (音声入力)','data-tooltip-position':'right'});
  // $('.toolbar-container .joint-toolbar.joint-theme-modern button[data-name="addTextLabel"]').attr({'data-tooltip':'テキスト','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="addLine"]').attr({'data-tooltip':'線・矢印','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="addSharp"]').attr({'data-tooltip':'図形','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="template"]').attr({'data-tooltip':'テンプレート','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="boardSave"]').attr({'data-tooltip':'ボードの画像保存','data-tooltip-position':'right'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="analysisCount"]').attr({'data-tooltip':'音声認識','data-tooltip-position':'right'});
  
  // $('.joint-toolbar.joint-theme-modern button[data-name="onseiNinshiki"]').attr({'data-tooltip':'コメント (音声入力)','data-tooltip-position':'top'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="changeColor"]').attr({'data-tooltip':'色','data-tooltip-position':'top'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="dustBox"]').attr({'data-tooltip':'削 除','data-tooltip-position':'top'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="reactionStickyNote"]').attr({'data-tooltip':'リアクション','data-tooltip-position':'top'});
  // $('.joint-toolbar.joint-theme-modern button[data-name="imageSearch"]').attr({'data-tooltip':'関連画像検索','data-tooltip-position':'top'});
  
  // $('#btn-edit-team').attr({'data-tooltip':'編集','data-tooltip-position':'top'});
  
  // $('#my-board-name-edit').attr({'data-tooltip':'編集','data-tooltip-position':'bottom'});
  // $('#my-board-add').attr({'data-tooltip':'追加','data-tooltip-position':'bottom'});
  // $('#my-board-delete').attr({'data-tooltip':'削除','data-tooltip-position':'bottom'});
  
  sideToolbar.on('edit:pointerclick', (e) => {
  
  });
  
  sideToolbar.on('aspectRatio:pointerclick', (e) => {
  
  });
  
  sideToolbar.on('more:pointerclick', (e) => {
  
  });
  
  sideToolbar.on('collapseMenu:pointerclick', (e) => {
    $(`#toolbar-container-${activeBoardId} .joint-toolbar .joint-toolbar-group[data-group="default"]`).css('display', 'none');
    $(`.joint-toolbar-group[data-group="extends"]`).css('display', 'block');
  });
  
}

// ツールバーのアイコン変更処理
function setToolbarIcon(enable, dataname, appendHtml){
  let headChildren = document.head.children;
  let head_len = headChildren.length;
  for(let i = 0; i < head_len; i++){
    if(headChildren[i].innerHTML.indexOf(`[data-name="${dataname}"]`) != -1){
      headChildren[i].remove();
    }
  }
  if(enable){
    $('head').append(appendHtml);
  }
}

// ツールバーの選択状態を解除します。
function releaseSelectToolbar(){
  
  add_note_color = undefined;
  add_voice_note_color = undefined;
  clearFlag();
  
  let len = boards.length;
  for(let n = 0; n < len; n++){
    let ctrl = document.getElementById("toolbar-container-" + boards[n].id);
    if(ctrl != null){
      let sideToolBarCtrl = ctrl.children[0].children[0];
      let sideToolBarCtrlCld_len = sideToolBarCtrl.children.length;
      for(let i = 0; i < sideToolBarCtrlCld_len; i++){
        let datatype = sideToolBarCtrl.children[i].dataset.type;
        if(datatype == "button" && sideToolBarCtrl.children[i].dataset.name != "move-edit"){
          sideToolBarCtrl.children[i].style.backgroundColor = "";
          sideToolBarCtrl.children[i].style.margin = "";
        }
      }
    }
  }
  
  // // ショート
  // let sCtrl = document.getElementById("toolbarShort-container-" + activeBoardId);
  // if(sCtrl != null){
  //     let sideToolbarShortCtrl = sCtrl.children[0].children[0];
  //     let sideToolbarShortCtrlCld_len = sideToolbarShortCtrl.children.length;
  //     for(let i = 0; i < sideToolbarShortCtrlCld_len; i++){
  //         let datatype = sideToolbarShortCtrl.children[i].dataset.type;
  //         if(datatype == "button"){
  //             sideToolbarShortCtrl.children[i].style.backgroundColor = "";
  //             sideToolbarShortCtrl.children[i].style.margin = "";
  //         }
  //     }
  // }
  
  // サブツールバーの選択状態解除
  releaseSelectSubToolbar();
};

function changeBoardEditMode(enable){
  if(userInfo.role == 3){
    return;
  }
  
  for(let i = 0; i < boards.length; i++){
    if(enable){
      boardEditMode = true;
      boards[i].paperScroller.setCursor('auto');
      boards[i].paper.setInteractivity(paperInteractive);
      $('.joint-toolbar.joint-theme-modern button[data-name="move-edit"]').removeClass('pointer-hand');
      // $('.joint-toolbar.joint-theme-modern button[data-name="move-edit"]').attr('data-tooltip', '編集モード');
      $('.joint-element').css('cursor','move');
      $('.note-contents-cursor').css('cursor','move');
      $('.link-corsor').css('cursor','pointer');
      $('.link-label-corsor').css('cursor','move');
      $('.audioPlay').css('cursor','hand');
      $('.total-reaction').css('cursor','pointer');
    } else {
      boardEditMode = false;
      boards[i].paper.removeTools();
      joint.ui.FreeTransform.clear(boards[i].paper);
      menuDisplayDisabled(boards[i].id);
      boards[i].paperScroller.setCursor('grab');
      boards[i].paper.setInteractivity(false);
      $('.joint-toolbar.joint-theme-modern button[data-name="move-edit"]').addClass('pointer-hand');
      $('.joint-element').css('cursor','grab');
      $('.joint-type-standard-link').css('cursor','grab');
      $('.note-contents-cursor').css('cursor','grab');
      $('.link-corsor').css('cursor','grab');
      $('.link-label-corsor').css('cursor','grab');
      $('.audioPlay').css('cursor','grab');
      $('.total-reaction').css('cursor','grab');
      
    }
  }
  
  setToolbarIcon(boardEditMode, "move-edit", TOOLBAR_ICON_CHANGE_MOVE_EDIT_MODE);
}

// サブツールバーの選択状態を解除します。
function releaseSelectSubToolbar(){
  
  // 解除する対象のサブツールバーリスト
  let targetSubToolbarList = [ID_FUSEN_MENUBAR, ID_ADD_LINE_MENU, ID_ADD_FIGURE_MENUBAR,ID_VOICE_FUSEN_MENUBAR ];
  let len = targetSubToolbarList.length;
  let board_len = boards.length;
  
  for(let i = 0; i < board_len; i++){
    for(let m = 0; m < len; m++){
      let ctrl = document.getElementById(targetSubToolbarList[m] + boards[i].id);
      if(ctrl != null){
        if(ctrl.children.length > 0){
          let ctrl_len = ctrl.children[0].children.length;
          for(let n = 0; n < ctrl_len; n++){
            let sideToolBarCtrl = ctrl.children[0].children[n];
            let sideToolBarCtrlCld_len = sideToolBarCtrl.children.length;
            for(let i = 0; i < sideToolBarCtrlCld_len; i++){
              let datatype = sideToolBarCtrl.children[i].dataset.type;
              if(datatype == "button"){
                sideToolBarCtrl.children[i].style.backgroundColor = "";
                sideToolBarCtrl.children[i].style.margin = "";
              }
            }
          }
        }
      }
    }
  }
}

/****************************************
 * 付箋クリックに枠を付ける処理
 ****************************************/
function noteBoundary(cellView){
  
  // 既に枠が付いていたら消す
  if(cellView._toolsView != undefined){
    cellView.removeTools();
  } else {
    
    // 枠作成
    let tools = {};
    if(cellView.model.attributes.type == CELL_TYPE_STANDARD_LINK){
      if(cellView.model.attributes.connector.name == CONNECTOR_TYPE_STRAIGHT){
        //直線の場合**矢印自体を移動させる
        tools = new joint.dia.ToolsView({
          tools: [new joint.linkTools.SourceArrowhead(),
            new joint.linkTools.TargetArrowhead(),
            new joint.elementTools.Boundary({
              padding : 7,
              attributes: {
                'fill': 'none',
                //change　ST yamada　Requa問題点ID：309
                'stroke-width': 4,
                // strokeWidth: 1,
                //change　END yamada
                'stroke': ' #31d0c6',
                'z-index': 3,
                'rx': 5,
                'ry': 5
              }
            })
          ]
        });
      }else{
        //カギ線、曲線の場合**頂点の追加
        let event = window.event;
        let vertexAdding = true;
        if(event){
          if(event.ctrlKey == true || event.shiftKey == true){
            vertexAdding = false;
          }
        }
        tools = new joint.dia.ToolsView({
          tools: [
            new joint.linkTools.Vertices({
              vertexAdding: vertexAdding,
            }),
            new joint.linkTools.SourceArrowhead(),
            new joint.linkTools.TargetArrowhead(),
            new joint.elementTools.Boundary({
              padding : 7,
              attributes: {
                'fill': 'none',
                //change　ST yamada　Requa問題点ID：309
                'stroke-width': 4,
                // strokeWidth: 1,
                //change　END yamada
                'stroke': ' #31d0c6',
                'z-index': 3,
                'rx': 5,
                'ry': 5
              }
            })
          ]
        });
      }
    } else {
      tools = new joint.dia.ToolsView({
        tools: [new joint.elementTools.Boundary({
          useModelGeometry: true,
          padding : 7,
          attributes: {
            'fill': 'none',
            /*   'pointer-events': 'none',//フォントの選択ができないので削除　YMD*/
            'stroke-width': 4,
            'stroke': ' #31d0c6',
            'z-index': 3,
            'rx': 5,
            'ry': 5
          }
        })]
      });
    }
    
    // 枠追加
    cellView.addTools(tools);
    
  }
}

/****************************************
 * 音声再生停止処理
 ****************************************/
function audioPlayStop(){
  let audio = document.getElementById("audioPlayYYKB");
  let label = document.getElementById("labelaudioPlayYYKBInfo");
  audio.pause();
  audio.currentTime = 0;
  audio.src = "";
  
  // 再生中の付箋を検索
  let remark_id = "";
  if(label.innerText.indexOf("再生中の発言ID:") != -1){
    
    remark_id = label.innerText.replace("再生中の発言ID:", "");
    
  } else if(label.innerText.indexOf("の音声ファイルがありません。") != -1){
    
    label.innerText = label.innerText.replace("の音声ファイルがありません。", "");
    remark_id = label.innerText.replace("発言ID:", "");
    
  }
  
  // 再生中の付箋の枠線を元に戻す。
  if(remark_id != ""){
    
    let index = jointJSGraph.attributes.cells.models.findIndex(a => {
      if(a.attributes.type == CELL_TYPE_NOTE){
        if(a.attributes.kind == OBJECT_KIND_STICKY_NOTE){
          return a.attributes.attrs[".id"].text.replace("ID:","") == remark_id;
        }}});
    let cellView = paper.findViewByModel(jointJSGraph.attributes.cells.models[index]);
    cellView.model.attributes.attrs[".card"].stroke = "none";
    cellView.model.attributes.attrs[".card"]['stroke-width'] = "1";
    cellView.update();
  }
  
  // ラベル表示初期化
  document.getElementById("labelaudioPlayYYKBInfo").innerText = "";
  
}

/****************************************
 * DBデータから付箋情報を作成する処理
 ****************************************/
function createYYKBNotes(data, stratIndex, autoLoadFlg, datalen, initBtnFlg, boardId){
  
  try
  {
    let templete = createStickyNoteTemplete(note_header_type, stickyNoteType.normal);
    
    let len = data.length;
    let cells = [];
    for(let i = 0; i < len; i++){
      
      let index = i + stratIndex;
      let opacity = OPACITY_DISP;
      
      // ユーザーの付箋の色と顔画像を取得
      let userData = getParticipantsData(data[i].name, data[i].code);
      
      // 付箋追加
      let noteXY = getNoteXY(index, boardId);
      
      // Chg Start 20200902 Y.Akasaka
      //cells.push(AddMember(noteXY.x, noteXY.y, index, data[i].remark_id, data[i].name, data[i].speeched_at, data[i].contents, userData.face_image, userData.color, OBJECT_KIND_STICKY_NOTE, "会議ID:" + data[i].meeting_id, data[i].emotion, opacity, 0));
      let cell = AddMember(noteXY.x, noteXY.y, index, data[i].remark_id, data[i].name, data[i].speeched_at, data[i].contents, userData.face_image, userData.color, OBJECT_KIND_STICKY_NOTE, "会議ID:" + data[i].meeting_id, data[i].emotion, opacity, 0);
      cell.markup = templete;
      cells.push(cell);
    }
    
    // 作成した付箋をボードに追加
    addNotes(cells, datalen, false, boardId);
    
  } catch(err){
    writeYYKnowledgeBoardErrorLog(err);
  }
  
}


/****************************************
 * 付箋のXY座標を取得
 ****************************************/
function getNoteXY(index, boardId){
  
  let bIdx = getBoardsIndex(boardId);
  let res = {
    x : 0,
    y : 0
  }
  
  res.x = (index % NEXT_NEWLINE_CARD) * CARD_WIDTH + CARD_X_PEDDING * (index % NEXT_NEWLINE_CARD);
  res.y = Math.floor(index / NEXT_NEWLINE_CARD) * (CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)) + Math.floor(index / NEXT_NEWLINE_CARD) * CARD_Y_PEDDING + 5;
  
  // let height = Number(boards[bIdx].paper.options.height.replace("px", ""));
  let height = Number(boards[bIdx].paper.options.height);
  while(res.y >= height){
    res.y -= height;
    if(res.y < 0){
      res.y = 0;
    }
  }
  
  return res;
}

/****************************************
 * 参加者順の時の座標位置取得処理
 ****************************************/
function getNoteXYTypeUser(index, userIndex){
  
  let bIdx = getBoardsIndex("yyKnowledgeBoard-main");
  let res = {
    x : 0,
    y : 0
  }
  
  res.x = userIndex * CARD_WIDTH + userIndex * CARD_X_PEDDING;
  res.y = index * (CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)) + index * CARD_Y_PEDDING;
  
  let height = Number(boards[bIdx].paper.options.height);
  while(res.y >= height){
    res.y -= height;
    if(res.y < 0){
      res.y = 0;
    }
  }
  
  return res;
}

/****************************************
 * グループ分け時の座標位置取得処理
 ****************************************/
function getNoteXYTypeGrouping(index, startY){
  
  let bIdx = getBoardsIndex("yyKnowledgeBoard-main");
  let res = {
    x : 0,
    y : 0
  };
  
  res.x = (index % NEXT_NEWLINE_CARD) * CARD_WIDTH + CARD_X_PEDDING * (index % NEXT_NEWLINE_CARD);
  res.y = Math.floor(index / NEXT_NEWLINE_CARD) * (CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)) + Math.floor(index / NEXT_NEWLINE_CARD) * CARD_Y_PEDDING + startY + 5;
  
  let height = Number(boards[bIdx].paper.options.height.replace("px", ""));
  while(res.y >= height){
    res.y -= height;
    if(res.y < startY){
      res.y = startY;
    }
  }
  
  return res;
}

/****************************************
 * 参加者情報取得
 ****************************************/
function getParticipantsData(name, userCode){
  
  let res = {
    face_image : "",
    color : "",
    userIndex : ""
  }
  
  // 付箋追加
  let userIndex = participantsData.findIndex(a => a.code ==  userCode);
  res.userIndex = userIndex;
  
  // 会議参加者テーブルにない発言者の場合は参加者リストに追加します(参加者リストにない人は顔画像は出ない)
  if(userIndex == -1){
    
    // 参加者データ作成
    let participant = createParticipantsData(name, userCode);
    res.color = participant.color;
    
    // 参加者リストに追加
    participantsData.push(participant);
    
  } else {
    res.face_image = participantsData[userIndex].face_image;
    res.color = participantsData[userIndex].sticky_note_color;
  }
  
  return res;
  
}
/****************************************
 * 文字列から空白削除
 ****************************************/
function deleteSpace(str) {
  
  var str2 = str.replace(/\s+/g, "");
  return str2
}
/****************************************
 * 参加者追加処理
 ****************************************/
function createParticipantsData(name, code){
  
  let res = undefined;
  let color = "";
  
  // 付箋の色割り当て
  color =  getUserColor(participantsData.length);
  
  res = {
    code : code,
    name : name,
    face_image : "",
    color : color
  };
  
  return res;
}

/****************************************
 * ユーザー別のデフォルト色情報取得
 ****************************************/
function getUserColor(number){
  
  let result = "";
  
  // 付箋の色割り当て
  let h = 0;
  if(number > 9){
    h = (Math.random() * 360);
  } else {
    h = (36 * number);
  }
  result =  `hsl(${h}, 80%, 70%)`;
  
  return result;
}

/****************************************
 * 付箋追加(発言内容)
 ****************************************/
function AddMember(x, y, z, id, name, date, contents, image, background, kind, meetingId, emotion, opacity, objectId, width, height, audioPath = "") {
  
  let textColor = "#000";
  
  if(width == undefined){
    width = CARD_SHADOW_WIDTH;
  }
  if(height == undefined){
    height = CARD_SHADOW_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT);
  }
  
  let cell = new joint.shapes.org.Member({
    position: { x: x, y: y },
    //angle: Math.floor( Math.random() * (5 + 1 - (-5)) ) - 5,
    z: z,
    size : {width : width, height : height},
    kind: kind,
    attrs: {
      '.shadow': { fill: '#000000', 'fill-opacity': 0.15, rx: 0, ry: 0, filter: "url(#filter1)", 'style':`width: ${CARD_SHADOW_WIDTH}px; height: ${CARD_SHADOW_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)}px;`},
      '.card': {fill: background, stroke: 'none', 'stroke-width': 3, 'style':`width: ${CARD_WIDTH}px; height: ${(CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT))}px;`, opacity:opacity,  'pointer-events': 'visiblePainted', ref: '.shadow', rx: 0, ry: 0},
      image: {
        'xlink:href':  image,
        ref: '.card',
        opacity: opacity,
        width: "28px",
        height:"28px",
        // 'clip-path':"url(#clip-path)",
        'ref-x': 4,
        'ref-y': 4
      },
      '.background': {
        'xlink:href':  'images/dummy.png',
        'ref-x': 8,
        'ref-y': 5,
        width: (CARD_WIDTH - 30) + "px",
        height: ((CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)) - 14) + "px"
      },
      '.id' : { ref: '.card', text: "ID:" + id, fill: textColor, 'x':240, 'y':30, 'text-anchor': 'end', opacity:opacity},
      '.objectId' : {value : objectId},
      '.meeting_id' : { text: meetingId, fill: textColor, 'text-anchor': 'end', 'font-family': Menber_font, 'font-size': '11px', 'x':200, 'y':15, opacity:opacity},
      '.name': { "font-weight":"0", ref:".card","ref-x":0,"ref-y":0, "text-anchor":"start", text: name, fill: textColor, 'x': 65, 'y':33, opacity:opacity},
      '.date': { ref: '.card', text: date, fill: textColor, 'x': 40, 'y': 15, 'letter-spacing': 0, opacity:opacity},
      '.contents': {id:"contents-"+ id, class: "contents custom-scrollbar "+ "contents-"+ id, event:'element:contents'},
      '.foreignObject':{width:width-5, height:height-35},
      '.emotion' : { text: getEmotion(emotion), 'font-size': 20, 'x': 47, 'y':42, opacity:opacity},
      '.btn.del': { 'ref-x': 242,'ref-y': 20, 'ref': '.card', event: 'element:delete', opacity:opacity},
      '.btn.del>text': { fill: textColor,'font-size': 28, 'font-weight': 500, stroke: '#000', 'font-family': 'Times New Roman' },
      '.btn.big' : {'ref': '.card', 'ref-dx': -20,'ref-y': (CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)) - 20, event: 'element:big', opacity:opacity},
      '.btn.big>text': { text: "▼", fill: textColor,'font-size': 18, 'font-weight': 500, stroke: '#000',  'font-family': 'Times New Roman' },
      '.btn.audioPlay' : {'ref': '.card', 'ref-x': 0,'ref-y': 0, event: 'element:audioPlay', opacity:opacity, audioPath: audioPath},
      '.btn.audioPlay>image': {'ref-x': 0,'ref-y': 0, width: 22, height:22, 'xlink:href':  'images/dummy.png' },
      // '.btn.audioPlay>text': { text: "🔊", fill: textColor,'font-size': 18, 'font-weight': 500, stroke: 'none', },
      '.areaReaction': { id: 'area-reaction-contents-' + id, height: 35 },
      '.data-reaction': {id: 'reaction-contents-' + id, 'data-remark': id}
    }
  });
  
  if(id != ""){
    contentsList.push({
      id: id,
      contents: contents
    });
  }
  
  if(cell.attributes.attrs['.emotion'].text == ""){
    cell.attributes.attrs['.name'].x = 40;
    if(document.getElementById("hDemoEmotionEnable").value == "true"){
      noEmotion_RemarkIDList.push(id);
    }
  }
  
  // 音声ファイルがあれば再生アイコン出します。
  
  if(cell.attributes.attrs['.btn.audioPlay'].audioPath != undefined && cell.attributes.attrs['.btn.audioPlay'].audioPath != ""){
    cell.attributes.attrs['.btn.audioPlay>image']['xlink:href'] = "images/voice_recognition.svg";
  }
  
  
  return cell;
};

/****************************************
 * 表情の絵文字取得処理
 ****************************************/
function getEmotion(emotion){
  
  let result = "";
  switch(emotion){
    
    case EMOTION_KIND_HAPPINESS:
      result = EMOTION_HAPPINESS;
      break;
    
    case EMOTION_KIND_NEUTRAL:
      result = EMOTION_NEUTRAL;
      break;
    
    case EMOTION_KIND_SURPRISE:
      result = EMOTION_SURPRISE;
      break;
    
    case EMOTION_KIND_ANGER:
      result = EMOTION_ANGER;
      break;
    
    case EMOTION_KIND_SCARED:
      result = EMOTION_SCARED;
      break;
    
    case EMOTION_KIND_SADNESS:
      result = EMOTION_SADNESS;
      break;
    
    case EMOTION_KIND_CONTEMPT:
      result = EMOTION_CONTEMPT;
      break;
    
    case EMOTION_KIND_DISGUST:
      result = EMOTION_DISGUST;
      break;
    
    case EMOTION_KIND_NOTHING:
      result = EMOTION_NOTHING;
      break;
    
    case EMOTION_KIND_DEFAULT:
    default:
      result = EMOTION_DEFAULT;
      break;
    
    case undefined:
      result = "";
      break;
  }
  
  return result;
  
}

/****************************************
 * 付箋追加アイコンクリック時の処理
 ****************************************/
async function noteAddImg_Click(color, pointX, pointY, copyCell){
  
  let res = await createNoteCell(color, pointX, pointY, copyCell);
  AddObject(res.sendData, res.cell);
  let colorKey = listColorSticky.indexOf(color);
  $('.joint-toolbar.joint-theme-modern button[data-name="changeColor"]').attr('class', `joint-widget joint-theme-modern color-${colorKey}`);
}

async function createNoteCell(color, pointX, pointY, copyCellParam){
  // アクティブ表示用の枠を全ての付箋から消す
  //paper.removeTools();
  
  return new Promise((resolve, reject) => {
    
    let res = {};
    let now = new Date();
    let now_str = dateToStr24HPad0(now, 'YYYY/MM/DD hh:mm:ss');
    
    let x = pointX;
    let y = pointY;
    let contents = "";
    
    if(copyCellParam){
      contents = copyCellParam.contents;
    }
    
    // $.ajax({
    //     type: 'POST',
    //     url: `/yyknowledgeboard/add-Note`,
    //     processData: false,
    //     data : JSON.stringify({count: 1, user_code: userInfo.response[0].code, contents: contents}),
    //     contentType: "application/json; charset=utf-8",
    // success: function (response) {
    // let res = response;
    
    // if(checkResponseErrer("", res)== false){
    //     return;
    // }
    
    let image_str = "";
    // 顔画像作成
    image_str = GetUserIconStr();
    
    // 自分の付箋の色取得
    let idx = participantsData.findIndex(a => a.code == userInfo.response[0].code);
    let sticky_note_color = undefined;
    if(idx != -1){
      sticky_note_color = participantsData[idx]["sticky_note_color"];
    }
    if(sticky_note_color == null || sticky_note_color == undefined){
      sticky_note_color = "#FFF6D5";
    }
    
    let boardIndex = getBoardsIndex(activeBoardId);
    let z = boards[boardIndex].graph.maxZIndex() + 1;
    
    let isInit = true;
    isChangeSizeSticky = true;
    if(copyCellParam){
      isInit = false;
    }
    let templete = createStickyNoteTemplete(note_header_type, stickyNoteType.normal, isInit);
    //chg Start ymd
    //let cell = AddMember(x, y, z, res.insertData[0].remark_id, userInfo.response[0].name, now_str, res.insertData[0].contents, image_str, "#FFF6D5", OBJECT_KIND_STICKY_NOTE, "会議ID:" + res.meeting_id, undefined, OPACITY_DISP, undefined);
    let cell = undefined;
    if(copyCellParam){
      // chg start 20201111 kobayashi
      // cell = AddMember(x + 10 + (Math.floor(Math.random() * 11 ) * 5), y + 10 + (Math.floor(Math.random() * 11 ) * 5), z, res.insertData[0].remark_id, userInfo.response[0].name, now_str, contents, image_str, color, OBJECT_KIND_STICKY_NOTE, "会議ID:" + res.meeting_id, undefined, OPACITY_DISP, undefined, copyCell.attributes.size.width, copyCell.attributes.size.height);
      cell = AddMember(x, y, z, "", userInfo.response[0].name, now_str, contents, image_str, color, OBJECT_KIND_STICKY_NOTE, "会議ID:" + selectMeeting, undefined, OPACITY_DISP, undefined, copyCellParam.width, copyCellParam.height);
      cell.attr('copyObjectId/value', copyCellParam.copyObjectId);
      // chg end 20201111
      
    } else {
      cell = AddMember(x, y, z, "", userInfo.response[0].name, now_str, contents, image_str, color, OBJECT_KIND_STICKY_NOTE, "会議ID:" + selectMeeting, undefined, OPACITY_DISP, undefined);
    }
    
    //chg End ymd
    
    cell.markup = templete;
    
    // 付箋情報
    let cellInfo = {
      type: cell.attributes.type,
      x : cell.attributes.position.x,
      y : cell.attributes.position.y,
      z : z,
      kind : cell.attributes.kind,
      // id : res.insertData[0].remark_id,
      name : cell.attributes.attrs[".name"].text,
      background : cell.attributes.attrs[".card"].fill,
      meeting_id : res.meeting_id,
      date : now_str,
      opacity : OPACITY_DISP,
      contents : contents
      //imageは容量が大きいので参加者情報で保持する。
    };
    cell.attributes.attrs['.contents'].value = contents;
    
    // DBに付箋情報保存
    let data = {
      data : cellInfo,
      boardId : activeBoardId,
      // remark_id : res.insertData[0].remark_id,
      kind: OBJECT_KIND_STICKY_NOTE,
      user_code: userInfo.response[0].code,
      meetingId: selectMeeting
    };
    
    let result = {
      sendData: data,
      cell: cell
    };
    
    resolve(result);
    
    // }
    // });
  });
  
  
}

// オブジェクト情報をDBに追加処理
// * nakamatsu
function AddObject(data, cell, recBuffuer = undefined, finishNotify = () => {}){
// nakamatsu *
  
  // 選択状態解除
  releaseSelectToolbar();
  
  // let type = Object.prototype.toString.call(data);
  // let contentType = false;
  // let sendData = data;
  // let datatype = "html";
  // if(type.indexOf("FormData") == -1){
  //     sendData = JSON.stringify(data);
  //     contentType = "application/json; charset=utf-8";
  //     datatype = "json";
  // }
  
  $.ajax({
    type: 'POST',
    url: `/yyknowledgeboard/add-Object`,
    data : JSON.stringify(data),
    processData: false,
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      
      if(Object.prototype.toString.call(response).indexOf("String") != -1){
        response = JSON.parse(response);
      }
      $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"]').addClass('d-none');
      changeCursor();
      let len = cell.length;
      let addCells = [];
      if(len == undefined){
        len = 1;
        let array = [];
        array.push(cell);
        cell = array;
      }
      let data_len = data.length;
      if(data_len == undefined){
        data_len = 1;
        let datas = [];
        datas.push(data);
        data = datas;
      }
      
      // サーバやDBで作成した情報を付与
      for(let i = 0; i < len; i++){
        
        if(cell[i].attributes.attrs[".objectId"] == undefined){
          cell[i].attributes.attrs[".objectId"] = {
            value: undefined
          };
        }
        
        cell[i].attributes.attrs[".objectId"].value = response.insertId[i];
        if(recBuffuer == undefined){
          addCells.push(cell[i]);
        }
        
        if(cell[i].attributes.kind == OBJECT_KIND_STICKY_NOTE){
          // remarkID
          cell[i].attributes.attrs[".id"].text = "ID:" + response.insertRemarkId[i];
          cell[i].attributes.attrs[".contents"].id = "contents-" + response.insertRemarkId[i];
          if(cell[i].attributes.attrs[".contents"].class.indexOf("contents-" + response.insertRemarkId[i]) == -1){
            cell[i].attributes.attrs[".contents"].class = cell[i].attributes.attrs[".contents"].class.replace("contents-","contents-" + response.insertRemarkId[i]);
          }
          cell[i].attributes.attrs[".areaReaction"].id = 'area-reaction-contents-' + response.insertRemarkId[i];
          cell[i].attributes.attrs[".data-reaction"].id = 'reaction-contents-' + response.insertRemarkId[i];
          cell[i].attributes.attrs[".data-reaction"]['data-remark'] = response.insertRemarkId[i];
          contentsList.push({
            id : response.insertRemarkId[i],
            contents : data[i].data.contents
          });
        } else if(cell[i].attributes.kind == OBJECT_KIND_COMMENT){
          cell[i].attributes.remark_id = response.insertRemarkId[i];
          
        } else if(cell[i].attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
          if(cell[i].attributes.remark_type  != stickyNoteType.image){
            
            cell[i].attributes.attrs[".id"].text = "ID:" + response.insertRemarkId[i];
            cell[i].attributes.attrs[".contents"].id = "contents-" + response.insertRemarkId[i];
            cell[i].attributes.attrs[".file-name"].id = "file-name-" + response.insertRemarkId[i];
            cell[i].attributes.attrs[".areaReaction"].id = 'area-reaction-contents-' + response.insertRemarkId[i];
            cell[i].attributes.attrs[".data-reaction"].id = 'reaction-contents-' + response.insertRemarkId[i];
            cell[i].attributes.attrs[".data-reaction"]['data-remark'] = response.insertRemarkId[i];
            cell[i].attributes.attrs[".download"].id = "download-" + response.insertRemarkId[i];
            cell[i].attributes.attrs[".download"].href = response.s3Paths[i];
          }
          
          if(cell[i].attributes.remark_type  == stickyNoteType.sound){
            cell[i].attributes.attrs['.content-audio'].src = response.s3Paths[i];
          }
          else if(cell[i].attributes.remark_type  == stickyNoteType.movie){
            cell[i].attributes.attrs['.content-movie'].src = response.s3Paths[i];
          }
          cell[i].attributes.file_info.url = response.s3Paths[i];
          
          if(cell[i].attributes.remark_type != stickyNoteType.document){
            cell[i].attributes.file_info.src = response.s3Paths[i];
            cell[i].attributes.file_info.url = response.s3Paths[i];
          }
        }
      }
      
      let selected = true;
      if(addCells.length > 0){
        if(addCells[0].attributes.addselect != undefined){
          selected = addCells[0].attributes.addselect;
        }
      }
      
      addNotes(addCells, addCells.length, selected, activeBoardId);
      // * nakamatsu
      finishNotify(cell);
      // nakamatsu *
      
      if(recBuffuer){
        let boardId = activeBoardId;
        let objectId = response.insertId[0];
        $.ajax({
          type: 'POST',
          url: `/yyknowledgeboard/comment-soundfile-upload`,
          processData: false,
          data : JSON.stringify({
            bufferData: recBuffuer,
            objectId: response.insertId[0],
            remark_id: cell[0].attributes.remark_id
          }),
          contentType: "application/json; charset=utf-8",
          success: async function (response) {
            window_blur();
            let boardIndex = getBoardsIndex(boardId);
            boards[boardIndex].graph.addCell(cell[0]);
            let cellIndex = boards[boardIndex].graph.attributes.cells.models.findIndex(a => {
              let res = false;
              if(a.attributes.attrs[".objectId"]){
                if( a.attributes.attrs[".objectId"].value == objectId){
                  res = true;
                }
              }
              return res;
            });
            
            if(cellIndex != -1){
              let cellView = boards[boardIndex].paper.findViewByModel(boards[boardIndex].graph.attributes.cells.models[cellIndex]);
              cellView.model.attributes.attrs.audioPlay.audioPath = response.audioPath;
              cellView.update();
              selectedObject(cell);
            }
            
            addUserOperationLog(USER_OPERATION_ADD, cell[0], undefined);
            
            let param = {
              targetId: commentTargetCell.id,
              sourceId: cell[0].id
            };
            let commentLinkData = await createCommentLinkCell(param);
            
            // let targetObj = getLinkTargetSourceSaveData({id: commentTargetCell.id});
            // let sourceObj = getLinkTargetSourceSaveData({id: cell[0].id});
            
            // // ターゲットとソースどちらかか不足していたら線作成をやめる
            // if(targetObj.objectId == undefined || targetObj.objectId == "" ||
            //    sourceObj.objectId == undefined || sourceObj.objectId == ""){
            //     return;
            // }
            
            // // let boardIndex = getBoardsIndex(activeBoardId);
            // let link = new joint.shapes.standard.Link({
            //     kind: OBJECT_KIND_LINE,
            //     connector: { name: CONNECTOR_TYPE_STRAIGHT },
            //     smooth: false,
            //     addselect: false,
            //     attrs: {
            //         line: {
            //             strokeDasharray: LINE_STYLE_DASHED_STROKE_DASHARRAY,
            //             stroke: "#4B71A6",
            //             /* strokeWidth: 3, */
            //             strokeWidth: 1,
            //             sourceMarker: {
            //                 /* 'stroke-width': 5, */
            //                 strokeWidth: 0.1,
            //                 stroke : TRANSPARENT,
            //                 fill : TRANSPARENT,
            //                 //change　Start 20201019
            //                 /* d : 'M 5 -10 L -15 0 L 5 10 Z'*/
            //                 d : TRIANGLE_POINT
            //                 //change　END 20201019
            //             },
            //             targetMarker: {
            //                 /* 'stroke-width': 5, */
            //                 strokeWidth: 0.1,
            //                 stroke : TRANSPARENT,
            //                 fill : TRANSPARENT,
            //                 //change　Start 20201019
            //                 /* d : 'M 5 -10 L -15 0 L 5 10 Z' */
            //                 d : TRIANGLE_POINT
            //                 //change　END 20201019
            //             }
            //         }
            //     },
            //     source: {id: cell[0].id},
            //     target: {id: commentTargetCell.id}
            // });
            // link.appendLabel({
            //     attrs: {
            //         text: {
            //             type: 'textarea',
            //             text: ' ',
            //             style:{whiteSpace: 'pre',
            //             'xml:space': "preserve",
            //             'word-break': 'break-all'},
            //         },
            //         rect: {
            //             fill: TRANSPARENT,
            //         },
            //         position: {
            //             distance: 0.3,
            //             args: {
            //                 keepGradient: true
            //             }
            //         }
            //     }
            // });
            
            // // DBに付箋情報保存
            // let cellinfo = {
            //     target: targetObj,
            //     source: sourceObj,
            //     z: link.attributes.z,
            //     attrs_line : link.attributes.attrs.line,
            //     type: link.attributes.type,
            //     smooth: link.attributes.smooth,
            //     connector: link.attributes.connector,
            //     kind: OBJECT_KIND_LINE
            // };
            // let sendData = {
            //     data : cellinfo,
            //     boardId : activeBoardId,
            //     remark_id : undefined,
            //     kind: OBJECT_KIND_LINE
            // };
            AddObject(commentLinkData.sendData, commentLinkData.cell);
          }
        });
      } else {
        if(len > 0){ // Add 20201111 kobayashi
          addUserOperationLog(USER_OPERATION_ADD, cell, undefined);
        }
      }
    }
  });
  
}

/****************************************
 * 発言内容保存処理
 ****************************************/
function noteContentsSave(cellView, content){
  
  updateTextarea(cellView.model.attributes.attrs[".contents"].id, content);
  
}

/****************************************
 * 選択中の付箋から保持してる発言データのインデックスを取得
 ****************************************/
function getNotesResDataIndex(cellView){
  
  let index = -1;
  
  
  if(cellView.model.attributes.kind == NOTE_KIND_MEMO){
    
    index = memoCotents.findIndex(a => a.cid == cellView.model.attributes.attrs["id"].value);
    
  } else {
    
    let remark_id = cellView.model.attributes.attrs[".id"].text.replace("ID:","");
    index =  notesResData.findIndex(a => a.remark_id == remark_id);
  }
  
  return index;
}


/****************************************
 * 発言内容保存処理
 ****************************************/
function updateTextarea(ctrlId, content){
  
  // 発言内容変更する場合はセットする
  if(content != undefined){
    
    let ctrls = document.getElementsByClassName(ctrlId);
    for(let i = 0; i < ctrls.length; i++){
      
      ctrls[i].innerText = content;
    }
    // document.getElementById(ctrlId).innerText = content;
  }
}

/****************************************
 * 操作ログ情報追加処理(付箋)
 ****************************************/
// function addUserOperationLog(kind, cell, bef_x, bef_y, bef_content, aft_x, aft_y, aft_content){
function addUserOperationLog(kind, cell, selectObjectId){
  
  let logObj = {
    kind: kind,
    selectObjectId: selectObjectId,
    cell : cell,
    // beforeData: {
    //     content : bef_content,
    //     x : bef_x,
    //     y : bef_y
    // },
    // afterData: {
    //     content : aft_content,
    //     x : aft_x,
    //     y : aft_y
    // }
  }
  
  // 操作ログ情報追加処理
  addUserOperationLogs(logObj);
  
}

/****************************************
 * 操作ログ情報追加処理(線)
 ****************************************/
function addUserOperationLogLink(kind, cell, befCell, aftCell){
  
  // if(cell.attributes.type == CELL_TYPE_STANDARD_LINK){
  //     return;
  // }
  
  let logObj = {
    kind: kind,
    cell : cell,
    selectObjectId: cell.attributes.attrs[".objectId"].value,
    beforeData: {
      
      target: undefined,
      source: undefined,
      vertices: undefined
      
    },
    afterData: {
      target: undefined,
      source: undefined,
      vertices: undefined
    }
  }
  
  if(befCell != undefined){
    logObj.beforeData.target = befCell.target;
    logObj.beforeData.source = befCell.source;
    
    if(befCell.vertices != undefined){
      logObj.beforeData.vertices = befCell.vertices;
    }
  }
  
  if(aftCell != undefined){
    logObj.afterData.target = aftCell.target;
    logObj.afterData.source = aftCell.source;
    
    if(aftCell.vertices != undefined){
      logObj.afterData.vertices = aftCell.vertices;
    }
  }
  
  // 操作ログ情報追加処理
  addUserOperationLogs(logObj);
  
}

/****************************************
 * 操作ログ情報追加処理
 ****************************************/
function addUserOperationLogs(logObj){
  
  // Redo要素があれば削除する
  if(userOperationIndex < userOperationLogs.length - 1 ){
    userOperationLogs.splice(userOperationIndex + 1,  userOperationLogs.length - (userOperationIndex + 1));
  }
  
  // オブジェクト追加
  userOperationLogs.push(logObj);
  
  // インデックス更新
  userOperationIndex = userOperationLogs.length - 1;
  
  // JSON保存処理(線削除時は線削除後に保存するためここではスキップする)
  if(logObj.kind != USER_OPERATION_LINK_DELETE){
    
    //let graphIndex
    jsonSave(logObj.cell);
    
    // 付箋の変更情報をブロードキャスト通信で共有
    sendBroadCastByNoteObject(logObj);
    
  }
  
}

/****************************************
 * 付箋をボードに追加する処理
 ****************************************/
function addNotes(cells, count, selected, boardId){
  
  let cells_len = cells.length;
  if(cells_len == 0){
    return;
  }
  
  if(boardId == undefined){
    return;
  }
  let boardIndex = getBoardsIndex(boardId);
  
  let noDispRowsIndexes_len = noDispRowsIndexes.length;
  setTimeout(() => {
    let addCount = 0;
    boards[boardIndex].graph.startBatch('add');
    cells.forEach(function(cell) {
      boards[boardIndex].graph.addCell(cell);
      
      if(cell.attributes.type == CELL_TYPE_NOTE){
        
        let cv = boards[boardIndex].paper.findViewByModel(cell);
        // cv.scalableNode.scale(1,1);
        contentsDivResize(cell, false, false, false);
        
        if(cell.attributes.kind == OBJECT_KIND_STICKY_NOTE){
          
          // 発言内容を反映
          let index = notesResData.findIndex(a => a.remark_id == cell.attributes.attrs[".id"].text.replace("ID:",""));
          
          // 発言内容の反映
          if(index != -1){
            updateTextarea(cell.attributes.attrs[".contents"].id, notesResData[index].contents);
          } else {
            index = contentsList.findIndex(a => a.id == cell.attributes.attrs[".id"].text.replace("ID:",""));
            if(index != -1){
              updateTextarea(cell.attributes.attrs[".contents"].id, contentsList[index].contents);
            }
          }
          
          document.getElementById(cell.attributes.attrs[".contents"].id).addEventListener("keydown", function(e) {
            setTimeout(function () {
              let scaleValue = parseFloat($(`#${cell.attributes.attrs[".contents"].id}`).parent().parent().children('.scalable').attr('transform').split('scale(')[1].split(',')[1]);
              let boardIndex = getBoardsIndex(activeBoardId);
              let listCell = getBoundaryCellList(boardIndex, CELL_TYPE_NOTE);
              for(let i = 0; i < listCell.length; i++){
                let cellView = boards[boardIndex].paper.findViewByModel(listCell[i]);
                if (cell.cid === cellView.model.cid) {
                  cellView.model.attributes.size.height = scaleValue * 175;
                  cell.attributes.size.height = scaleValue * 175;
                  let param = {
                    cellView: cellView,
                    allowRotation: false,
                    maxWidth: MAX_OBJECT_WIDTH,
                    maxHeight: MAX_OBJECT_HEIGHT
                  };
                  
                  if(cellView.model.attributes.type == CELL_TYPE_NOTE){
                    param.minWidth = MIN_OBJECT_WIDTH;
                    param.minHeight = MIN_OBJECT_HEIGHT;
                    
                    if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
                      param.preserveAspectRatio = true;
                    }
                  };
                  
                  let freeTransform = new joint.ui.FreeTransform(param);
                  let tools = new joint.dia.ToolsView({
                    tools: [new joint.elementTools.Boundary({
                      useModelGeometry: true,
                      padding : 7,
                      attributes: {
                        'fill': 'none',
                        'stroke-width': 4,
                        'stroke': ' #31d0c6',
                        'z-index': 3,
                        'rx': 5,
                        'ry': 5
                      }
                    })]
                  });
                  freeTransform.render();
                  cellView.addTools(tools);
                }
              }
            }, 100);
            // 変更前のデータがなければ保持します。
            if(selectContentsCellInfo == undefined){
              selectContentsCellInfo = {cell: boards[boardIndex].paper.findViewByModel(cell), contents_bef: document.getElementById(e.currentTarget.id).innerText};
            }
          });
          
        } else if(cell.attributes.kind == NOTE_KIND_MEMO){
          
          let index = memoCotents.findIndex(a => a.cid == cell.attributes.attrs["id"].value);
          
          // 発言内容の反映
          if(index != -1){
            updateTextarea(cell.attributes.attrs[".contents"].id, memoCotents[index].contents);
          }
        }
        else if(cell.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
          // Y.Akasaka 20201019
          //document.getElementById(cell.attributes.attrs[".download"].id).innerText = cell.attributes.file_info.file_name;
          document.getElementById(cell.attributes.attrs[".file-name"].id).innerText = cell.attributes.file_info.file_name;
        }
      }
      addCount++;
      
      // ロード処理性能改善 -->
      if(addCount >= cells_len){
        boards[boardIndex].graph.stopBatch('add');
      }
      // <-- ロード処理性能改善
      
      if(count != undefined){
        
        if(selected){
          if(count <= addCount){
            selectedObject(cells);
          }
        }
        
        // if(noDispRowsIndexes_len){
        // if(count <= boards[boardIndex].graph.attributes.cells.models.length + noDispRowsIndexes_len){
        if(count <= boards[boardIndex].graph.attributes.cells.models.length){
          
          // ロード処理性能改善 -->
          // boards[boardIndex].graph.stopBatch('add');
          // <-- ロード処理性能改善
          
          // コントロール操作有効
          loadingContralDisabled(false);
          
          // if(jsonSaveValidFlg == false){
          if(boards[boardIndex].jsonSaveValid == false){
            // 表示終わったときにオブジェクトがある中央をセンターにする。
            boards[boardIndex].paperScroller.centerContent();
          }
          
          // json保存有効
          boards[boardIndex].jsonSaveValid = true;
          // jsonSaveValidFlg = true;
          //jsonSave(boardId);
          
          commonDefine.setVisible('#yykb-loading-objectOperation', false);
          if(nowPasteingFlg){
            
            // グループ化解除
            if(old_cellview != undefined){
              let embedList = old_cellview.model.getEmbeddedCells();
              let len = embedList.length;
              for(let i = 0; i < len; i++){
                if(embedList[i]){
                  old_cellview.model.unembed(embedList[i]);
                }
              }
              old_cellview = undefined;
            }
            
            // グループ化
            let selectCells = getBoundaryCellList(boardIndex, "all");
            old_cellview = boards[boardIndex].paper.findViewByModel(selectCells[0]);
            let selectCells_len = selectCells.length;
            if(selectCells_len > 1){
              for(let i = 0; i < selectCells_len; i++){
                if(i>0){
                  selectCells[0].embed(selectCells[i]);
                }
              }
            }
            // old_cellview = boards[boardIndex].paper.findViewByModel(cells[0]);
            // let selectCells_len = cells.length;
            // if(selectCells_len > 1){
            //     for(let i = 0; i < selectCells_len; i++){
            //         if(i>0){
            //             cells[0].embed(cells[i]);
            //         }
            //     }
            // }
            setTimeout(() => {
              nowPasteingFlg = false;
            }, 300);
          }
        }
        // }
        
        // リアクションアイコンの無効化対応 -->
        // if (cell.attributes.attrs['.data-reaction'] && cell.attributes.attrs['.data-reaction']['data-remark']) yyKnowledgeBoard.updateReaction(cell.attributes.attrs['.data-reaction']['data-remark']);
        // <-- リアクションアイコンの無効化対応
        
        // if(selected){
        //     if(count <= addCount){
        //         // let noteList = cells.filter((val) => {
        //         //     val.attributes.type ==
        //         // });
        //         selectedObject(cells);
        //     }
        // }
      }
      
    }, boards[boardIndex].graph);
    // ロード処理性能改善 -->
    // boards[boardIndex].graph.stopBatch('add');
    // <-- ロード処理性能改善
    
    // 最後に追加した付箋のElementIDを保持
    let cellView = boards[boardIndex].paper.findViewByModel(cells[cells.length - 1]);
    lastAddNoteId = cellView.id;
    
  }, 10);
  
}

// オブジェクトを選択状態にする
function selectedObject(cells){
  // let boardIndex = getBoardsIndex(activeBoardId);
  let len = cells.length;
  let multiSelect = false;
  if(len > 1){
    multiSelect = true;
  }
  for(let i = 0; i < len; i++){
    
    if(cells[i].graph){ // Add 20201111 kobayashi
      
      let boardIndex = boards.findIndex(a => a.graph.cid == cells[i].graph.cid);
      
      let cellView = boards[boardIndex].paper.findViewByModel(cells[i]);
      
      if(cellView == undefined){
        continue;
      }
      if(cellView._toolsView == undefined){
        noteBoundary(cellView);
      }
      
      if(i == 0){
        
        if(cellView.model.attributes.type != CELL_TYPE_STANDARD_LINK){
          
          selectCell = cells[i];
          if(cellView.model.attributes.type == CELL_TYPE_NOTE){
            let color = $(`#${cellView.id}`).find('.card').attr('fill');
            let colorKey = listColorSticky.indexOf(color);
            $('.joint-toolbar.joint-theme-modern button[data-name="changeColor"]').attr('class', `joint-widget joint-theme-modern color-${colorKey}`);
            selectStickyNote = cells[i];
          }
          
          let toolbarOn = true;
          if(nowPasteingFlg){
            if(copyPaste.getCopyObjectCount() != len){
              toolbarOn = false;
            }
          }
          
          if(toolbarOn){
            // サイズ変更枠表示
            setFreeTransform(cellView);
            
            // ツールバー表示
            setMenuBarPosition(cellView, multiSelect);
          }
          
        } else {
          
          // 他のツールバーが非表示の時のみ表示(ペースト時にダブるため)
          if(document.getElementById(ID_MULTI_SELECT_MENUBAR + activeBoardId).style.display == "none" &&
            document.getElementById(ID_SHAPE_SUB_MENUBAR + activeBoardId).style.display == "none" &&
            document.getElementById(ID_TEXT_MENUBAR + activeBoardId).style.display == "none" &&
            document.getElementById(ID_FUSEN_SUB_MENUBAR + activeBoardId).style.display == "none"){
            selectCell = cells[i];
            setDisplay(ID_LINE_MENUBAR, activeBoardId);
            toolsViewDisp(cellView, multiSelect);
          }
        }
      }
    }
  }
  
}


/****************************************
 * 戻るアイコンクリック時の処理
 ****************************************/
function undoImg_Click(){
  
  let idx = commandManagers.findIndex(a => a.boardCtrlId == activeBoardId);
  if(idx != -1)
  {
    commandManagers[idx].commandManager.undo();
  }
  return;
}


/****************************************
 * 進むアイコンクリック時の処理
 ****************************************/
function redoImg_Click(){
  
  
  let idx = commandManagers.findIndex(a => a.boardCtrlId == activeBoardId);
  if(idx != -1)
  {
    commandManagers[idx].commandManager.redo();
  }
  return;
  
  
}

/****************************************
 * 付箋削除処理
 ****************************************/
function noteRemove(cellView){
  
  // ボード上から付箋削除
  cellView.model.remove();
  
  let graphIndex = getBoardsIndex(activeBoardId);
  menuDisplayDisabled(boards[graphIndex].id);
  
  if(cellView.model.attributes.attrs[".objectId"] == undefined){
    return;
  }
  
  let remark_id = undefined;
  if(cellView.model.attributes.attrs[".id"] != undefined){
    if(cellView.model.attributes.attrs[".id"].text != undefined){
      remark_id = cellView.model.attributes.attrs[".id"].text.replace("ID:","")
    }
  }
  
  let data = {
    objectId : cellView.model.attributes.attrs[".objectId"].value,
    remark_id : remark_id
  }
  
  // DBの付箋情報を削除する
  $.ajax({
    type: 'POST',
    url: `/yyknowledgeboard/delete-object`,
    processData: false,
    data : JSON.stringify(data),
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      let res = response;
      
      if(checkResponseErrer("", res)== false){
        return;
      }
    }
  });
  
  // 変更情報をブロードキャスト通信で共有
  let obj = {
    kind: USER_OPERATION_DELETE,
    cell: cellView.model
  }
  
  sendBroadCastByNoteObject(obj);
  
}

/****************************************
 * hsl形式を16進RGB形式に変換する処理
 * 引数の形式(最大値)："hsl(360, 100%, 100%)"
 ****************************************/
function hslTo16Rgb(hsl_str){
  
  let result = "";
  
  try{
    
    // 不要な文字列削除
    hsl_str = hsl_str.replace("hsl(", "");
    hsl_str = hsl_str.replace(")", "");
    hsl_str = hsl_str.replace(/ /g, "");
    hsl_str = hsl_str.replace(/%/g, "");
    
    // カンマ区切りで分ける
    let hsl_split = hsl_str.split(",");
    
    if(hsl_split.length != 3){
      return;
    }
    
    let red   = 0;
    let green = 0;
    let blue  = 0;
    
    let hue = Number(hsl_split[0]) / 360;
    let saturation = Number(hsl_split[1]) / 100;
    let lightness = Number(hsl_split[2]) / 100;
    
    // hslの各値の範囲チェック
    if((hue >= 0 && 360 >= hue) &&
      (saturation >= 0 && 100 >= saturation) &&
      (lightness >= 0 && 100 >= lightness)){
      
      // 彩度が0の時はRGBに輝度の値を入れる
      if (saturation == 0) {
        red   = lightness;
        green = lightness;
        blue  = lightness;
      } else {
        
        let q = 0;
        let p = 0;
        let hueToRgb;
        
        // 色相をRGBに変換
        hueToRgb = function(p, q, t) {
          if (t < 0) {
            t += 1;
          }
          
          if (t > 1) {
            t -= 1;
          }
          
          if (t < 1 / 6) {
            p += (q - p) * 6 * t;
          } else if (t < 1 / 2) {
            p = q;
          } else if (t < 2 / 3) {
            p += (q - p) * (2 / 3 - t) * 6;
          }
          
          return p;
        };
        
        if (lightness < 0.5) {
          q = lightness * (1 + saturation);
        } else {
          q = lightness + saturation - lightness * saturation;
        }
        p = 2 * lightness - q;
        
        red   = hueToRgb(p, q, hue + 1 / 3);
        green = hueToRgb(p, q, hue);
        blue  = hueToRgb(p, q, hue - 1 / 3);
      }
      
      result = `#${Math.round(red * 255).toString(16)}${Math.round(green * 255).toString(16)}${Math.round(blue * 255).toString(16)}`
    }
    
  }
  catch(err){
    writeYYKnowledgeBoardErrorLog(err);
  }
  
  return result;
}


/****************************************
 * オブジェクト情報をDBに保存する処理
 ****************************************/
async function jsonSave(cell){
  
  // return;
  
  if(activeBoardId == undefined){
    return;
  }
  
  // if(jsonSaveValidFlg == false){
  let boardIndex = getBoardsIndex(activeBoardId);
  if(boards[boardIndex].jsonSaveValid == false){
    return;
  }
  
  if(cell == undefined){
    return;
  }
  
  let datas = [];
  let len = cell.length;
  if(len == undefined){
    len = 1;
    let array = [];
    array.push(cell);
    cell = array;
  } else if(len == 0){ // Add 20201111 kobayashi
    return;
  }
  for(let i = 0; i < len; i++){
    // 保存データ取得
    let objectData = await getObjectData(cell[i]);
    
    if(objectData == undefined){
      return;
    }
    
    let contents = "";
    if(cell[i].attributes.type == CELL_TYPE_NOTE && cell[i].attributes.kind == OBJECT_KIND_STICKY_NOTE){
      
      if(document.getElementById(cell[i].attributes.attrs['.contents'].id) != null){
        contents = document.getElementById(cell[i].attributes.attrs['.contents'].id).innerText;
      }
      
      // テキスト入力DOMに反映されてない時用
      if(cell[i].attributes.attrs['.contents'].value != undefined && cell[i].attributes.attrs['.contents'].value != "" && contents == ""){
        contents = cell[i].attributes.attrs['.contents'].value;
      }
    } else if(cell[i].attributes.kind == OBJECT_KIND_COMMENT){
      contents = cell[i].attributes.attrs.label.text;
      objectData.attrs.label.text = undefined;
    }
    
    // サムネイル画像は更新しません
    if(objectData.thumbnail_image){
      delete objectData.thumbnail_image;
    }
    
    let data = {
      objectId : cell[i].attributes.attrs['.objectId'].value,
      contents : contents,
      type: cell[i].attributes.type,
      objectData : objectData
    };
    datas.push(data);
  }
  
  $.ajax({
    type: 'POST',
    url: `/yyknowledgeboard/save`,
    processData: false,
    // data : JSON.stringify(data),
    data : JSON.stringify(datas),
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      let res = response;
      if (res.dataWebSocket) {
        let dataWS = {
          remark_id: res.dataWebSocket.remarkId,
          language: 'japanese',
          remark_registered: true,
        }
        if (res.dataWebSocket.kind === 0) {
          dataWS.type_board_remarks = res.dataWebSocket.audioPath ? TYPE_BOARD_REMARK_STICKY_AUDIO : TYPE_BOARD_REMARK_STICKY
        } else {
          dataWS.type_board_remarks = res.dataWebSocket.audioPath ? TYPE_BOARD_REMARK_COMMENT_AUDIO : TYPE_BOARD_REMARK_COMMENT
        }
        wsCom.send(selectMeeting, voiceRemark.actionPutMessage, dataWS);
      }
      if(checkResponseErrer("", res)== false){
        return;
      }
    }
  });
  
}

// オブジェクトの保存データ取得
async function getObjectData(cell){
  
  let data = undefined;
  
  if(cell.attributes.type == CELL_TYPE_NOTE){
    
    if(cell.attributes.kind == OBJECT_KIND_STICKY_NOTE){
      // 付箋情報
      data = {
        type: cell.attributes.type,
        x : cell.attributes.position.x,
        y : cell.attributes.position.y,
        z : cell.attributes.z,
        width: cell.attributes.size.width,
        height: cell.attributes.size.height,
        kind : cell.attributes.kind,
        id : cell.attributes.attrs[".id"].text.replace("ID:", ""),
        objectId : cell.attributes.attrs[".objectId"].value,
        name : cell.attributes.attrs[".name"].text,
        background : cell.attributes.attrs[".card"].fill,
        meeting_id : cell.attributes.attrs[".meeting_id"].text.replace("会議ID:", ""),
        date : cell.attributes.attrs[".date"].text,
        opacity : cell.attributes.attrs[".card"].opacity,
        audioPath : cell.attributes.attrs[".btn.audioPlay"].audioPath,
      };
    }
    else if(cell.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
      
      data = {
        type: cell.attributes.type,
        x : cell.attributes.position.x,
        y : cell.attributes.position.y,
        z : cell.attributes.z,
        width: cell.attributes.size.width,
        height: cell.attributes.size.height,
        kind : cell.attributes.kind,
        id : cell.attributes.attrs[".id"].text.replace("ID:", ""),
        objectId : cell.attributes.attrs[".objectId"].value,
        name : cell.attributes.attrs[".name"].text,
        background : cell.attributes.attrs[".card"].fill,
        meeting_id : cell.attributes.attrs[".meeting_id"].text.replace("会議ID:", ""),
        date : cell.attributes.attrs[".date"].text,
        opacity : cell.attributes.attrs[".card"].opacity,
        remark_type : cell.attributes.remark_type,
        file_name : cell.attributes.file_info.file_name,
        src : cell.attributes.file_info.src,
        download_url : cell.attributes.file_info.url
      };
      if(cell.attributes.remark_type == stickyNoteType.image){
        data.thumbnail_image = cell.attributes.attrs[".content-image"]['xlink:href'];
      }
    }
    
  } else if(cell.attributes.type == CELL_TYPE_STANDARD_LINK){
    // 線
    data = {
      target: getLinkTargetSourceSaveData(cell.attributes.target),
      source: getLinkTargetSourceSaveData(cell.attributes.source),
      z: cell.attributes.z,
      attrs_line : cell.attributes.attrs.line,
      type: cell.attributes.type,
      smooth: cell.attributes.smooth,
      connector: cell.attributes.connector,
      vertices: cell.attributes.vertices,
      kind: OBJECT_KIND_LINE,
      objectId : cell.attributes.attrs[".objectId"].value,
      label : ' ',  // Add 20201111 kobayashi
    };
    
    // Add Start 20201111 kobayashi
    if(cell.attributes.labels){
      data.label = cell.attributes.labels[0].attrs.text.text;
      data.label_position = cell.attributes.labels[0].position;
    }
    // Add End 20201111
    
  } else if(cell.attributes.type == CELL_TYPE_RECT){
    // 四角
    if(cell.attributes.attrs[".objectId"] == undefined){
      return undefined;
    }
    if(cell.attributes.kind == OBJECT_KIND_COMMENT){
      
      data = {
        position: cell.attributes.position,
        z: cell.attributes.z,
        type: cell.attributes.type,
        size: cell.attributes.size,
        id: cell.attributes.remark_id,
        attrs: JSON.parse(JSON.stringify(cell.attributes.attrs)),
        objectId: cell.attributes.attrs[".objectId"].value,
        kind:OBJECT_KIND_COMMENT,
      };
    } else if(cell.attributes.kind == OBJECT_KIND_TEXT){
      
      data = {
        x: cell.attributes.position.x,
        y: cell.attributes.position.y,
        z: 0,
        width: cell.attributes.size.width,
        height: cell.attributes.size.height,
        attrs_body : cell.attributes.attrs.body,
        attrs_label : cell.attributes.attrs.label,
        type: cell.attributes.type,
        objectId: cell.attributes.attrs[".objectId"].value,
        kind: OBJECT_KIND_TEXT
      };
      
    } else {
      data = {
        x: cell.attributes.position.x,
        y: cell.attributes.position.y,
        z: cell.attributes.z,
        type: cell.attributes.type,
        width: cell.attributes.size.width,
        height: cell.attributes.size.height,
        fill: cell.attributes.attrs.body.fill,
        stroke: cell.attributes.attrs.body.stroke,
        strokeWidth: cell.attributes.attrs.body.strokeWidth,
        objectId: cell.attributes.attrs[".objectId"].value,
        kind:OBJECT_KIND_SHAPE
      };
    }
  }
  else if(cell.attributes.type == CELL_TYPE_IMAGE){
    data = {
      type: cell.attributes.type,
      x : cell.attributes.position.x,
      y : cell.attributes.position.y,
      z : cell.attributes.z,
      width: cell.attributes.size.width,
      height: cell.attributes.size.height,
      kind : OBJECT_KIND_STICKY_NOTE_FILE,
      //kind : cell.attributes.attrs.kind,
      objectId : cell.attributes.attrs[".objectId"].value,
      remark_type : cell.attributes.remark_type,
      file_name : cell.attributes.file_info.file_name,
      src : cell.attributes.file_info.src,
      download_url : cell.attributes.file_info.url,
      thumbnail_image : cell.attributes.attrs.image['xlinkHref']
    };
    
    
  }
  
  // * nakamatsu
  if (Number(data.id) <= 0) {
    return undefined;
  }
  // nakamatsu *
  return data;
}

/****************************************
 * 読み込んだ付箋情報をボード上に追加する。
 ****************************************/

function addLoadNotesData(cellsInfo, getlimit, boardId){
  let cells = [];
  
  // 付箋から処理する(付箋同士を結ぶ線に付箋のIDが必要なため)
  // 付箋
  let notes = cellsInfo.filter((val) => {
    return val.data.type === CELL_TYPE_NOTE;
  });
  let notes_len = notes.length;
  for(let i = 0; i < notes_len; i++){
    
    let cell = undefined;
    if(notes[i].data.kind == OBJECT_KIND_STICKY_NOTE){
      
      // ユーザーの付箋の色と顔画像を取得
      let userData = getParticipantsData(notes[i].data.name, notes[i].user_code);
      
      let templete = createStickyNoteTemplete(note_header_type, stickyNoteType.normal);
      
      let audioPath = "";
      if(notes[i].audioPath){
        audioPath = notes[i].audioPath;
      }
      
      cell = AddMember(notes[i].data.x, notes[i].data.y, notes[i].data.z, notes[i].data.id, notes[i].data.name, notes[i].data.date, notes[i].contents, userData.face_image, notes[i].data.background, notes[i].data.kind, "会議ID:" + notes[i].data.meeting_id, undefined, notes[i].data.opacity, notes[i].id, notes[i].data.width, notes[i].data.height, audioPath);
      cell.markup = templete;
      
      // 追加
      if(cell){
        cells.push(cell);
      }
    }
    else if(notes[i].data.kind== OBJECT_KIND_STICKY_NOTE_FILE){
      
      let userData = getParticipantsData(notes[i].data.name, notes[i].user_code);
      
      let templete =  createStickyNoteTemplete(note_header_type, notes[i].data.remark_type);
      
      cell = AddMemberForFiles(
        notes[i].data.x, notes[i].data.y, notes[i].data.z,
        notes[i].data.id,
        notes[i].data.name,
        notes[i].data.date,
        notes[i].data.file_name,
        notes[i].data.src,
        userData.face_image,
        notes[i].data.background,
        notes[i].data.kind,
        "会議ID:" + notes[i].data.meeting_id,
        notes[i].data.opacity,
        notes[i].id,
        notes[i].data.remark_type,
        notes[i].data.download_url,
        notes[i].data.width,
        notes[i].data.height,
        notes[i].thumbnail_image
      );
      
      cell.markup = templete;
      
      // 追加
      if(cell){
        cells.push(cell);
      }
      
    }
    
  }
  
  // Image
  let image = cellsInfo.filter((val) => {
    return val.data.type === CELL_TYPE_IMAGE;
  });
  for(let i = 0; i < image.length; i++){
    
    let cell = undefined;
    cell = new joint.shapes.standard.Image();
    //cell.resize(image[i].data.width, image[i].data.height);
    
    // 画像が1ドットになる対策 -->
    // cell.size(image[i].data.width, image[i].data.height);
    
    let width = image[i].data.width;
    if(!width){
      width = 150;
    }
    let height = image[i].data.height;
    if(!height){
      height = 150;
    }
    cell.size(width, height);
    // <-- 画像が1ドットになる対策
    
    cell.position( image[i].data.x, image[i].data.y);
    cell.attributes.z = image[i].data.z;
    cell.attr('.objectId/value', image[i].id);
    // Del start 20201111 kobayashi
    // cell.attr('root/title', image[i].data.file_name);
    // cell.attr('label/text', getWrapFileName(image[i].data.file_name));
    // Del end 20201111
    cell.attr('image/xlinkHref', image[i].thumbnail_image);
    cell.attributes.kind = image[i].data.kind;
    cell.attributes.remark_type = image[i].data.remark_type;
    cell.attributes.file_info = {file_name:image[i].data.file_name ,src: image[i].data.src, url:image[i].data.download_url};
    
    // 画像追加処理改善 -->
    let index = imageList.findIndex(a => a.url == image[i].data.download_url);
    if(index == -1){
      imageList.push({
        url: image[i].data.download_url,
        imageData: image[i].thumbnail_image
      });
    }
    // <-- 画像追加処理改善
    
    if(cell){
      cells.push(cell);
    }
  }
  
  
  // 四角
  let rect = cellsInfo.filter((val) => {
    return val.data.type === CELL_TYPE_RECT;
  });
  let rect_len = rect.length;
  for(let i = 0; i < rect_len; i++){
    
    // 四角作成
    let cell = undefined;
    
    if(rect[i].kind == OBJECT_KIND_SHAPE){
      
      cell = new joint.shapes.standard.Rectangle();
      cell.attr('body/fill', rect[i].data.fill);
      cell.attr('body/stroke', rect[i].data.stroke);
      cell.attr('body/strokeWidth', rect[i].data.strokeWidth);
      cell.attr('.objectId/value', rect[i].data.objectId);
      cell.position(rect[i].data.x, rect[i].data.y);
      cell.size(rect[i].data.width, rect[i].data.height);
      cell.attributes.kind = rect[i].kind;
      
    } else if(rect[i].kind == OBJECT_KIND_COMMENT) {
      
      let data = {
        kind: OBJECT_KIND_COMMENT,
        z: rect[i].data.z,
        position: rect[i].data.position,
        size: rect[i].data.size,
        remark_id: rect[i].remark_id,
        attrs: rect[i].data.attrs
      }
      
      if(data.attrs.label.text == undefined){
        if(rect[i].contents == ""){
          rect[i].contents = " ";
        }
        data.attrs.label.text = rect[i].contents;
      }
      
      if(rect[i].data.attrs[".objectId"] == undefined){
        data.attrs[".objectId"] = {
          value: rect[i].id
        }
      }
      
      data.attrs.audioPlay = {
        'xlink:href' : "images/voice_recognition.svg",
        audioPath : rect[i].audioPath,
        event: 'element:audioPlay',
      };
      if(data.attrs.audioPlay.audioPath == undefined || data.attrs.audioPlay.audioPath == ""){
        data.attrs.audioPlay['xlink:href'] = "images/commentEdit.svg";
        data.attrs.audioPlay.event = 'element:commentEdit';
      }
      
      cell = new joint.shapes.standard.Rectangle(data);
      if(cell.markup.length == 2){
        cell.markup.push({
          tagName: "g",
          children:[{
            selector: "audioPlay",
            tagName: "image"
          }]
        });
      }
    } else {
      cell = new joint.shapes.standard.Rectangle({
        kind: OBJECT_KIND_TEXT,
        position: { x: rect[i].data.x, y: rect[i].data.y },
        z: rect[i].data.z,
        size: { width: rect[i].data.width, height: rect[i].data.height},
        attrs:{
          '.objectId': {value: rect[i].data.objectId},
          label: rect[i].data.attrs_label,
          body: rect[i].data.attrs_body
        }
      });
      cell.attributes.attrs.label.fontSize = 28;
    }
    
    // 追加
    if(cell){
      cells.push(cell);
    }
  }
  
  // 線
  let links = cellsInfo.filter((val) => {
    return (val.data.type === CELL_TYPE_STANDARD_LINK);
    // return (val.type === CELL_TYPE_LINK || val.type === CELL_TYPE_AREALINE || val.type === CELL_TYPE_STANDARD_LINK);
  });
  cells = cells.concat(getLinkFromReadData(links, cells, boardId));
  
  // 作成した付箋と線の情報をボードに追加
  let cells_len = cells.length;
  let data_len = cells_len + noDispRowsIndexes.length;
  if(cells_len == 0){
    let boardIndex = getBoardsIndex(boardId);
    loadingContralDisabled(false);
    boards[boardIndex].jsonSaveValid = true;
  }
  
  setTimeout(() => {
    addCellsforLoad(cells, data_len, cells_len, getlimit, 0, boardId);
  }, 10);
  
}

function addCellsforLoad(cells, data_len, cells_len, getlimit, startIndex, boardId){
  
  let list = [];
  let len = startIndex + getlimit;
  
  if(len > cells_len){
    len = cells_len;
  }
  
  for(let i = startIndex; i < len; i++){
    
    list.push(cells[i]);
  }
  
  addNotes(list, data_len, false, boardId);
  list = [];
  
  
  let stIndex = startIndex + getlimit;
  if(stIndex < cells_len){
    setTimeout(() => {
      addCellsforLoad(cells, data_len, cells_len, getlimit, stIndex, boardId);
    }, 100);
  }
  
}

/****************************************
 * 付箋や線の内部IDからオブジェクトIDを取得する処理
 ****************************************/
function getCellIDToObjectID(cellId){
  
  let result = "";
  let boardIndex = getBoardsIndex(activeBoardId);
  let targetNoteIndex = boards[boardIndex].graph.attributes.cells.models.findIndex(a => a.id == cellId);
  if(targetNoteIndex != -1){
    if(boards[boardIndex].graph.attributes.cells.models[targetNoteIndex].attributes.attrs[".objectId"] != undefined){
      result = boards[boardIndex].graph.attributes.cells.models[targetNoteIndex].attributes.attrs[".objectId"].value;
    }
  }
  
  return result;
}

/****************************************
 * 描画したオブジェクトから付箋や線の内部IDを取得する処理
 ****************************************/
function getObjectIDToCellID(cells, objectId){
  let res = undefined;
  
  let targetNoteIndex = cells.findIndex(a => {
    let res = false;
    if(a.attributes.attrs[".objectId"] != undefined){
      if(a.attributes.attrs[".objectId"].value == objectId){
        res = true;
      }
    }
    return res;
  });
  if(targetNoteIndex != -1){
    res = cells[targetNoteIndex].id;
  }
  return res;
}

// 線のターゲットとソースの保存用情報を返します。 data: 線のtargetオブジェクト or sourceオブジェクト
function getLinkTargetSourceSaveData(data){
  
  let res = undefined;
  
  if(data.x != undefined && data.y != undefined){
    
    res = {
      x : data.x,
      y : data.y,
    };
    
  } else if(data.id != undefined){
    
    res = {
      objectId : getCellIDToObjectID(data.id)
    };
  }
  
  return res;
  
}

/****************************************
 * DBから読み込んだデータから線を追加する処理
 ****************************************/
function getLinkFromReadData(links, cells, boardId){
  
  let result = [];
  let notDrawLine = []; // 描画出来なかった線
  // let graphIndex = getBoardsIndex(boardId);
  let links_len = links.length;
  for(let i = 0; i < links_len; i++){
    
    if(links[i].id == undefined ||
      links[i].data.source == undefined ||
      links[i].data.target == undefined ){
      notDrawLine.push({objectId: links[i].id});
      continue;
    }
    
    links[i].data.attrs_line.class = "link-corsor";
    
    let cell = undefined;
    if(links[i].data.type == CELL_TYPE_STANDARD_LINK){
      cell =  new joint.shapes.standard.Link({
        connector: links[i].data.connector,
        smooth: links[i].data.smooth,
        attrs: {
          '.objectId': {value: links[i].id},
          line: links[i].data.attrs_line
        }
      });
      cell.attributes.attrs.wrapper.class = "link-corsor";
      
      let linkLabel = ' ';  // Add 20201111 kobayashi
      if(links[i].data.label){
        linkLabel = links[i].data.label;
      }
      
      // if(links[i].data.labels){
      //     cell.appendLabel(links[i].data.labels);
      // } else {
      cell.appendLabel({
        attrs: {
          text: {
            type: 'textarea',
            text: linkLabel,
            style:{whiteSpace: 'pre',
              'xml:space': "preserve",
              'word-break': 'break-all'},
          },
          rect: {
            class: "link-label-corsor",
            fill: TRANSPARENT,
          },
          position: {
            distance: 0.3,
            args: {
              keepGradient: true
            }
          }
        }
      });
      // Add Start 20201111 kobayashi
      if(links[i].data.label_position){
        cell.attributes.labels[0].position = links[i].data.label_position;
      }
      // Add End 20201111
      // }
      // cell = groupingLine(0, links[i].attrs.label.text);
    }
    
    // source
    if(links[i].data.source.objectId == undefined){
      cell.attributes.source.x = links[i].data.source.x;
      cell.attributes.source.y = links[i].data.source.y;
    } else {
      cell.attributes.source.id = getObjectIDToCellID(cells, links[i].data.source.objectId);
      if(cell.attributes.source.id == undefined){
        notDrawLine.push({objectId: links[i].id});
        continue;
      }
    }
    
    // target
    if(links[i].data.target.objectId == undefined){
      cell.attributes.target.x = links[i].data.target.x;
      cell.attributes.target.y = links[i].data.target.y;
    } else {
      cell.attributes.target.id = getObjectIDToCellID(cells, links[i].data.target.objectId);
      if(cell.attributes.target.id == undefined){
        notDrawLine.push({objectId: links[i].id});
        continue;
      }
    }
    
    // Z軸
    cell.attributes.z = links[i].data.z;
    
    // 種別
    cell.attributes.kind = links[i].kind;
    
    // コネクタ
    cell.attributes.vertices = links[i].data.vertices;
    
    // 追加
    if(cell){
      result.push(cell);
    }
  }
  
  // 描画出来なかった線は削除する
  if(notDrawLine.length > 0){
    deleteObject(notDrawLine);
  }
  
  return result;
  
}

/****************************************
 * 右クリックメニュー マウスオーバーイベント
 ****************************************/
function onmouseover_ContentMenu(className){
  
  let ctrls = document.getElementsByClassName(className);
  let len = ctrls.length;
  for(let i = 0; i < len; i++){
    ctrls[i].style['background-color'] = "gray";
  }
  
}

/****************************************
 * 右クリックメニュー マウスアウトイベント
 ****************************************/
function onmouseout_ContentMenu(className){
  
  let ctrls = document.getElementsByClassName(className);
  let len = ctrls.length;
  for(let i = 0; i < len; i++){
    ctrls[i].style['background-color'] = "white";
  }
}


/****************************************
 * 枠付きの付箋リストを取得する処理　※type = "all"で全て取得
 ****************************************/
function getBoundaryCellList(boardIndex, type){
//    function getBoundaryCellList(graphIndex){
  
  let list = [];
  
  if(boards[boardIndex].graph != undefined && boards[boardIndex].paper != undefined){
    
    list = boards[boardIndex].graph.attributes.cells.models.filter((val) => {
      
      let res = false;
      if(val.attributes.type == type || type == "all"){
        //    if(val.attributes.type == CELL_TYPE_NOTE){
        let view = boards[boardIndex].paper.findViewByModel(val);
        if(view._toolsView != undefined){
          res = true;
        }
      }
      return res;
    });
  }
  
  return list;
  
}
/****************************************
 * 枠付きの矩形リストを取得する処理
 ****************************************/
function getBoundaryShapeCellList(graphIndex){
  
  let list = [];
  
  if(boards[graphIndex].graph != undefined && boards[graphIndex].paper != undefined){
    
    list = boards[graphIndex].graph.attributes.cells.models.filter((val) => {
      
      let res = false;
      if(val.attributes.type == CELL_TYPE_CIRCLE ||val.attributes.type == CELL_TYPE_PATH || val.attributes.type == CELL_TYPE_RECT){
        let view = boards[graphIndex].paper.findViewByModel(val);
        if(view._toolsView != undefined){
          res = true;
        }
      }
      return res;
    });
  }
  
  return list;
  
}

// 発言内容エリアからアクティブが離れた時の処理
function contents_onblur(){
  
  if(selectContentsCellInfo == undefined){
    return;
  }
  if(selectContentsCellInfo.cell == undefined){
    return;
  }
  
  if(textLockTimer != undefined){
    clearTimeout(textLockTimer);
  }
  
  // 発言内容保存処理
  let cellView = Object.create(selectContentsCellInfo.cell);
  let contents_bef = selectContentsCellInfo.contents_bef;
  selectContentsCellInfo = undefined;
  let ctr = document.getElementById(cellView.model.attributes.attrs[".contents"].id);
  let contents_aft = document.getElementById(cellView.model.attributes.attrs[".contents"].id).innerText;
  
  if(contents_aft.length > MAX_WORD_LENGTH){
    contents_aft = contents_aft.substring(0,MAX_WORD_LENGTH);
    document.getElementById(cellView.model.attributes.attrs[".contents"].id).innerText = contents_aft;
    let currentHeight = parseFloat(document.getElementById(cellView.model.attributes.attrs[".contents"].id).clientHeight) + 25;
    ctr.parentElement.parentElement.getElementsByClassName('scalable')[0].setAttribute('transform', `scale(1, ${currentHeight / 175})`);
    ctr.parentElement.parentElement.getElementsByClassName('custom-scrollbar-fusen')[0].setAttribute('height', currentHeight);
    cellView.model.attributes.size.height = currentHeight;
  }
  
  // 発言内容保存
  //noteContentsSave(cellView, contents_aft);
  
  // 発言内容に変更があったときのみ操作ログ追加
  if(contents_bef != contents_aft){
    // addUserOperationLog(USER_OPERATION_EDIT, cellView.model,
    //                     cellView.model.attributes.position.x, cellView.model.attributes.position.y, contents_bef,
    //                     cellView.model.attributes.position.x, cellView.model.attributes.position.y, contents_aft);
    addUserOperationLog(USER_OPERATION_EDIT, cellView.model, cellView.model.attributes.attrs[".objectId"].value);
  }
}

// 発言内容変更時の処理
function contents_onChange(){
  
  let keyword = document.getElementById('labelYYKBKeyword').innerText.replace("ハイライト対象キーワード：", "");
  let cellView = selectContentsCellInfo.cell;
  let contents = document.getElementById(cellView.model.attributes.attrs[".contents"].id).innerText;
  
  contents = contents.replace(new RegExp(CONTENTS_HIGHLIGHT_SPAN_START, 'g'), '');
  contents = contents.replace(new RegExp(CONTENTS_HIGHLIGHT_SPAN_END, 'g'), '');
  
  // 関連するキーワード情報取得
  let keywordRelationWords = JSON.parse(document.getElementById('hKeywordRelationWords').value);
  
  
  // 発言内容のハイライト更新
  let c_index = contents.indexOf(keyword);
  let krw_index = keywordRelationWords.findIndex(a => a.keyword == keyword);
  
  if(c_index != -1){
    contents = contents.replace(new RegExp(keyword, 'g'), `${CONTENTS_HIGHLIGHT_SPAN_START}${keyword}${CONTENTS_HIGHLIGHT_SPAN_END}`);
  }
  
  if(krw_index != -1){
    
    // 関連するワードリスト分処理する
    let relationWords_length = keywordRelationWords[krw_index].relationWords.length;
    for(let i = 0; i < relationWords_length; i++){
      
      if(contents.indexOf(keywordRelationWords[krw_index].relationWords[i]) != -1){
        contents = contents.replace(new RegExp(keywordRelationWords[krw_index].relationWords[i], 'g'), `${CONTENTS_HIGHLIGHT_SPAN_START}${keywordRelationWords[krw_index].relationWords[i]}${CONTENTS_HIGHLIGHT_SPAN_END}`);
      }
    }
  }
  
  
  // 発言内容保存
  noteContentsSave(cellView, contents);
  
}

// 拡大縮小処理
function zoom(boardId, scale, zoomValue){
  
  let graphIndex = getBoardsIndex(boardId);
  
  // 選択中状態を解除する。
  // joint.ui.FreeTransform.clear(boards[graphIndex].paper);
  // boards[graphIndex].paper.removeTools();
  
  // // メニューバー非表示
  // menuDisplayDisabled(boards[graphIndex].id);
  
  // boards[graphIndex].paper.scale(scale);
  let old_scale = boards[graphIndex].paperScroller.zoom();
  boards[graphIndex].paperScroller.zoom(zoomValue, {max:2, min:0.1});
  
  
  let cellView = boards[graphIndex].paper.findViewByModel(selectCell);
  if(cellView){
    if(cellView._toolsView != undefined){
      if(cellView.model.isLink()){
        let list = getBoundaryCellList(graphIndex, "all");
        toolsViewDisp(cellView, list.length > 1 ? true : false);
      } else {
        setMenuBarPosition(cellView);
      }
    }
  }
  
  // ロックオブジェクトのサイズを合わせる
  let lockDivs = document.getElementsByClassName("object-lock");
  let len = lockDivs.length;
  for(let i = 0; i < len; i++){
    
    let ctrl = lockDivs[i];
    if(ctrl.style.display == "none"){
      continue;
    }
    if(ctrl.parentElement.id != boardId){
      continue;
    }
    
    // ズーム率100%のときの座標を一旦取得
    let defzoompos = {
      top: Number(ctrl.style.top.replace("px","")) / old_scale,
      left: Number(ctrl.style.left.replace("px","")) / old_scale,
      width: ctrl.clientWidth / old_scale,
      height: ctrl.clientHeight / old_scale,
    };
    
    // 100%の座標ベースで拡大率を計算
    ctrl.style.top = defzoompos.top * scale;
    ctrl.style.left = defzoompos.left * scale;
    ctrl.style.width = defzoompos.width * scale;
    ctrl.style.height = defzoompos.height * scale;
    
    // let lockDivInfo = JSON.parse(ctrl.value);
    // let lockdata = {
    //         boardId : lockDivInfo.boardId,
    //         objectIds : ctrl.lockObjectid,
    //         userName : lockDivInfo.userName,
    //         userCode : lockDivInfo.userCode,
    //         lock: true // true:ロック　false:解除
    // }
    // reciveNoteObjectLock(lockdata);
  }
  
  // 他の人のマウスカーソルの位置を更新
  let mouseCursorCtrls = document.getElementsByClassName("mouseCursor-container");
  let mouseCursor_len = mouseCursorCtrls.length;
  for(let i = 0; i < mouseCursor_len; i++){
    
    let ctrl = mouseCursorCtrls[i];
    if(ctrl.parentElement.id == boardId){
      
      // ズーム率100%のときの座標を一旦取得
      let offsetX = ctrl.clientWidth / 2
      let defzoompos = {
        top: Number(ctrl.style.top.replace("px","")) / old_scale,
        left: (Number(ctrl.style.left.replace("px","")) + offsetX) / old_scale,
      };
      
      // 100%の座標ベースで拡大率を計算
      ctrl.style.top = defzoompos.top * scale;
      ctrl.style.left = (defzoompos.left * scale) - offsetX;
    }
  }
}

function groupingLine(y, keyword){
  
  let link = new joint.shapes.standard.Link({
    source: { x: 0, y: y },
    target: { x: 400, y: y },
    markup: [
      {
        tagName: 'path',
        selector: 'line',
        attributes: {
          'fill': 'none'
        }
      }
      ,{
        tagName: 'text',
        selector: 'label'
      }
    ],
    
    attrs: {
      line: {
        'fill': 'none',
        stroke: '#3c4260',
        'stroke-width': '20'
        
        
      },
      label: {
        textPath: {
          selector: 'line',
          startOffset: '50%'
        },
        textAnchor: 'middle',
        textVerticalAnchor: 'middle',
        text: keyword,
        fill: '#f6f6f6',
        fontSize: 15,
      }
    }
  });
  
  return link;
}

function DeleteGroupingLink(){
  
  let list = jointJSGraph.attributes.cells.models.filter((val) => {
    return val.attributes.type == CELL_TYPE_STANDARD_LINK;
  });
  
  let len = list.length;
  for(let i = 0; i < len; i++){
    let cellView = paper.findViewByModel(list[i]);
    cellView.model.remove();
  }
}

function loadingContralDisabled(enable){
  
  loadingFlg = enable;
  
}

//ボード一覧表示<<,>>イベント
async function boardSelectDoubleAngle_click(){
  
  let zoomAreaPosition = function(enable){
    let ctrls = document.getElementsByClassName("zoomArea-container");
    let len = ctrls.length;
    let leftValue = "";
    if(enable){
      leftValue = "290px";
    } else {
      leftValue = "70px";
    }
    for(let i = 0; i < len; i++){
      ctrls[i].style.left = leftValue;
    }
  };
  
  if($("#boardSelect-double-angle").hasClass("fa-angle-double-left")){
    
    $('#boardSelect-double-angle').removeClass('fa-angle-double-left').addClass('fa-angle-double-right');
    $('#boardSelect-double-angle').attr('style', 'margin-left: 15px;');
    $('#yyKnowledgeBoard-teamName').attr('style', 'left: 70;');
    zoomAreaPosition(false);
    
    $('#boardSelect-main').attr('style', 'display: none;');
    $('#boardSelect-td').attr('style', 'width: 0px;');
    document.getElementById("boardSelect-double-angle-img").src = "images/double_arrow_right.svg";
    
  } else if($("#boardSelect-double-angle").hasClass("fa-angle-double-right")){
    
    $('#boardSelect-double-angle').removeClass('fa-angle-double-right').addClass('fa-angle-double-left');
    $('#boardSelect-double-angle').attr('style', 'margin-left: 0px;');
    $('#yyKnowledgeBoard-teamName').attr('style', 'left: 290;');
    zoomAreaPosition(true);
    
    $('#boardSelect-main').attr('style', 'display: block;');
    $('#boardSelect-td').attr('style', 'width: 220px;');
    document.getElementById("boardSelect-double-angle-img").src = "images/double_arrow_left.svg";
    
    // サムネイル画像更新
    // let shareBoardId = getShareBoardId();
    // await boardThumbnailTimer(100, shareBoardId);
    
  }
  
}

// ボード一覧でサムネイルクリック時にボードを切り替える処理
function board_dblclick(boardId, boardName){
  
  let board = document.getElementById(`${ID_KNOWLEDGEBOARD_FRAME + boardId}`);
  let td = document.getElementById("yyKnowledgeBoard-td1");
  
  // if(document.getElementById(`${boardId}-disp`).src.indexOf("images/visibility.svg") != -1){
  //     return;
  // }
  
  // 既にあれば消す
  let td_children_len = td.children.length;
  for(let i = 0; i < td_children_len; i++){
    
    let hiddenBoardId = td.children[i].id.replace(ID_KNOWLEDGEBOARD_FRAME, "");
    td.children[i].style.display = "";
    document.getElementById(`toolbar-container-${hiddenBoardId}`).style.display = "none";
    document.getElementById(`toolbarShort-container-${hiddenBoardId}`).style.display = "none";
    document.getElementById(`zoomArea-container-${hiddenBoardId}`).style.display = "none";
    document.getElementById(`boardNameArea-container-${hiddenBoardId}`).style.display = "none";
    document.getElementById(`${hiddenBoardId}-disp`).src = "images/visibility_off.svg";
    
    while(document.getElementById("shareBoradName-td0").children.length > 0){
      document.getElementById("shareBoradName-td0").children[0].remove();
    }
    document.getElementById("yyKnowledgeBoard-hidden-boards").appendChild(td.children[i]);
  }
  
  // 追加
  board.style.display = "";
  td.appendChild(board);
  document.getElementById(`${boardId}-disp`).src = "images/visibility.svg";
  document.getElementById(`boardNameArea-container-${boardId}`).style.display = "";
  document.getElementById(`boardDispChange-${boardId}`).setAttribute("data-name" , "boardDisp-enable");
  
  // 個人ボード表示中かチェック
  if(getKojinBoardDisplayStatus() == false){
    // 非表示
    setShareBoardDisplay("default",boardId,"");
  } else {
    // 表示中
    let kojinBoardId = getKojinBoardId();
    setShareBoardDisplay("ALL",boardId,"");
    setKojinBoardDisplay("ALL",kojinBoardId);
  }
  
  activeBoardId = boardId;
  
  // ボードの初期位置中央にする
  let boardIndex = getBoardsIndex(activeBoardId);
  // boards[boardIndex].paperScroller.center();
  if(boards[boardIndex].graph.attributes.cells.models.length > 0){
    boards[boardIndex].paperScroller.centerContent();
  } else {
    boards[boardIndex].paperScroller.center();
  }
  
}

// ボード一覧のサムネイル表示クリック時の処理
function boardImg_click(ctrlId, e){
  let ctrl = document.getElementById(ctrlId.replace("drag-",""));
  if(ctrl == null){
    return;
  }
  
  // 選択状態解除
  window_blur();
  
  // シフトキーが押されてない時は他の選択状態解除
  // if(e.shiftKey == false){
  let boardImgCtrls = document.getElementsByClassName("board-img");
  let len = boardImgCtrls.length;
  for(let i = 0; i < len; i++){
    boardImgCtrls[i].style.border = "solid 1px #aaa";
  }
  // }
  
  // Chg 20201027 ボード一覧　選択枠のborder、サイズと色を変更
  ctrl.style.border = "solid 5px";
  ctrl.style.borderColor = SELECTED_THUMBNAIL_COLOR;
  
}

// ボード一覧にボードを追加時の処理
async function addSelectBoard(boardId, boardName, boardKind, boardVisible, boardThumbnailPath = undefined){
  
  let boardsImgList = document.getElementsByClassName("board-img");
  let nowCount = boardsImgList.length;
  
  let id_str = "";
  if(boardId == undefined){
    
    // IDからボード番号を取り出す
    let str_tmp = boardsImgList[nowCount - 1].id.replace("-img","");
    str_tmp　= str_tmp.substring(str_tmp.length - 4);
    let no = Number(str_tmp);
    id_str = "yyKnowledgeBoard-" + $('#selectMeetingYYKB').val() + "_board" + ("0000" + (no + 1)).slice(-4);
  } else {
    id_str = boardId;
  }
  
  let newDiv = document.createElement("div");
  let newThumbnailDiv = document.createElement("div");
  let newThumbnailImg = document.createElement("img");
  // Chg start 20201111 kobayashi
  // let newLbl = document.createElement("label");
  let newLbl = document.createElement("div");
  // Chg end 20201111
  let newtxt = document.createElement("input");
  let newDispDiv = document.createElement("img");
  let boardZoomImg = document.createElement("img");
  
  // img
  newThumbnailDiv.className = "board-img";
  newThumbnailDiv.id = `${id_str}-img`;
  newThumbnailDiv.style.border = "solid 1px #aaa";
  
  // loading
  let divThumbnailLoading = document.createElement("div");
  divThumbnailLoading.id = `${id_str}-loading`;
  // newSmallDiv.src = "images/board2_img.png";
  
  // thumneil
  newThumbnailImg.id = `thumbnail-${id_str}-img`;
  newThumbnailImg.className = "board-thumbnail-img";
  if(boardThumbnailPath){
    newThumbnailImg.src = boardThumbnailPath;
  }
  // label
  if(boardName){
    newLbl.innerText = boardName;
  } else {
    newLbl.innerText = "新規ボード";
  }
  newLbl.for = newThumbnailDiv.id;
  newLbl.style.height = "auto";
  newLbl.style.display = "block";
  newLbl.id = `lbl-${id_str}`;
  newLbl.className = "board-lbl";
  newLbl.title = newLbl.innerText; // Add 20201111 kobayashi
  
  // text
  newtxt.type = "text";
  newtxt.className = "board-txt";
  newtxt.id = `txt-${id_str}`;
  
  // div
  newDiv.className = "drag";
  newDiv.id = `${id_str}-drag`;
  // 非表示設定
  if(boardVisible == 0){
    newDiv.style.display = "none";
  }
  
  // 表示中表示
  newDispDiv.className = "board-disp";
  newDispDiv.id = `${id_str}-disp`;
  newDispDiv.src = "images/visibility_off.svg";
  // Add 20201027 Y.Akasaka アイコンを一旦非表示にする
  newDispDiv.style.display = "none";
  
  boardZoomImg.id = `boardZoomImg-${id_str}`;
  boardZoomImg.className = "board-zoom";
  boardZoomImg.src = "images/zoom_in.svg";
  // Add 20201027 Y.Akasaka アイコンを一旦非表示にする
  boardZoomImg.style.display = "none";
  
  if(boardKind == BOARD_KIND_SHARE){//add YMD
    newThumbnailDiv.appendChild(newThumbnailImg);
    newDiv.appendChild(newThumbnailDiv);
    newDiv.appendChild(newLbl);
    newDiv.appendChild(divThumbnailLoading);
    newDiv.appendChild(newtxt);
    newDiv.appendChild(newDispDiv);
    newDiv.appendChild(boardZoomImg);
    
    // document.getElementById("boardSelect-main").appendChild(newDiv);
  }
  // ボード本体追加処理
  let board_Frame = document.createElement("div");
  let board_sideToolbar = document.createElement("div");
  let board_sideToolbarShort = document.createElement("div"); //CHG
// let board_bottomToolbar = document.createElement("div");     //DELL YMD
  let board_contextmenu = document.createElement("div");
  let board_zoomArea = document.createElement("div");
  let board_contextmenu_ul = document.createElement("ul");
  let board_contextmenu_memo = document.createElement("li");
  let board_contextmenu_line = document.createElement("li");
  let board_contextmenu_boardHidden = document.createElement("li");
  let board_zoomRange = document.createElement("input");
  let board_mainDiv = document.createElement("div");
  let board_nameArea = document.createElement("div");
  let board_nameLbl = document.createElement("label");
  //add START ymd
  let board_fusenMenubar = document.createElement("div");
  let board_voiceFusenMenubar = document.createElement("div");
  let board_fusenSubMenubar = document.createElement("div");
  let board_textMenubar = document.createElement("div");
  let board_addFigureMenubar = document.createElement("div");
  let board_addLineMenu = document.createElement("div");
  let board_lineMenubar = document.createElement("div");
  let board_lineTypeSettingMenubar = document.createElement("div");
  let board_lineLeftMenubar = document.createElement("div");
  let board_lineRightMenubar = document.createElement("div");
  let board_textOptionMenu = document.createElement("div");
  let board_shapeSubMenubar = document.createElement("div");
  let board_multiSelectMenubar = document.createElement("div");
  let reactionArea = document.createElement('div');
  let changeColorArea = document.createElement('div');
  //add END YMD
  let board_nameBorderLine = document.createElement("div");
  let board_dispChange = document.createElement("button");
  let board_zoomUp = document.createElement("img");
  let board_zoomOut = document.createElement("img");
  let board_zoomRateLbl = document.createElement("label");
  //add START ymd
  let board_zoom_Separator = document.createElement("img");
  let board_zoom_DoubleArrow = document.createElement("img");
  //add END YMD
  
  // ボードの大枠
  board_Frame.id = "yyKnowledgeBoard-frame-" + id_str;
  board_Frame.className = "yyKnowledgeBoard-frame custom-scrollbar";
  // board_Frame.style.display = "none";
  if(boardKind == BOARD_KIND_REMARK){
    board_Frame.style.overflow = "auto";
  }
  
  // ボードにドラッグ＆ドロップイベントを追加
  board_mainDiv.addEventListener('dragover', handleBoardDragOver, false);
  board_mainDiv.addEventListener('drop', handleBoardDrop, false);
  
  // ツールバー
  board_sideToolbar.id = "toolbar-container-" + id_str;
  board_sideToolbar.className = "toolbar-container";


//CHG ST ymd
  board_sideToolbar.style.display = "none";
  
  board_sideToolbarShort.id = "toolbarShort-container-" + id_str;
  board_sideToolbarShort.className = "toolbarShort-container";
  board_sideToolbarShort.style.display = "none";
  
  board_fusenMenubar.id = "fusenMenubar-container-" + id_str;
  board_fusenMenubar.className = "fusenMenubar-container";
  board_fusenMenubar.style.display = "none";
  
  board_voiceFusenMenubar.id = ID_VOICE_FUSEN_MENUBAR + id_str;
  board_voiceFusenMenubar.className = "voiceFusenMenubar-container";
  board_voiceFusenMenubar.style.display = "none";
  
  board_fusenSubMenubar.id = ID_FUSEN_SUB_MENUBAR + id_str;
  board_fusenSubMenubar.className = "fusenSubMenubar-container";
  board_fusenSubMenubar.style.display = "none";
  
  board_textMenubar.id = ID_TEXT_MENUBAR + id_str;
  board_textMenubar.className = "textMenubar-container";
  board_textMenubar.style.display = "none";
  
  board_shapeSubMenubar.id = ID_SHAPE_SUB_MENUBAR + id_str;
  board_shapeSubMenubar.className = "shapeSubMenubar-container";
  board_shapeSubMenubar.style.display = "none";
  
  board_multiSelectMenubar.id = ID_MULTI_SELECT_MENUBAR + id_str;
  board_multiSelectMenubar.className = "multiSelectMenubar-container";
  board_multiSelectMenubar.style.display = "none";

//CHG END ymd
//YMD DELL
//   board_bottomToolbar.id = "bottomToolbar-container-" + id_str;
//   board_bottomToolbar.className = "bottomToolbar-container";
  
  //Add ymd
  board_addFigureMenubar.id = ID_ADD_FIGURE_MENUBAR + id_str;
  board_addFigureMenubar.className = "addFigureMenubar-container";
  board_addFigureMenubar.style.display = "none";
  
  board_addLineMenu.id = ID_ADD_LINE_MENU + id_str;
  board_addLineMenu.className = "addLineMenu-container";
  board_addLineMenu.style.display = "none";
  
  board_lineMenubar.id = ID_LINE_MENUBAR + id_str;
  board_lineMenubar.className = "lineMenubar-container";
  board_lineMenubar.style.display = "none";
  
  board_lineTypeSettingMenubar.id = ID_LINE_TYPE_SETTING_MENUBAR + id_str;
  board_lineTypeSettingMenubar.className = "lineTypeSettingMenubar-container";
  board_lineTypeSettingMenubar.style.display = "none";
  
  board_lineLeftMenubar.id = ID_LINE_LEFT_MENUBAR + id_str;
  board_lineLeftMenubar.className = "lineLeftMenubar-container";
  board_lineLeftMenubar.style.display = "none";
  
  board_lineRightMenubar.id = ID_LINE_RIGHT_MENUBAR + id_str;
  board_lineRightMenubar.className = "lineRightMenubar-container";
  board_lineRightMenubar.style.display = "none";
  
  board_textOptionMenu.id = "textOptionMenu-container-" + id_str;
  board_textOptionMenu.className = "textOptionMenu-container";
  //Add end ymd
  
  // 拡大率変更バー
  board_zoomArea.id = "zoomArea-container-" + id_str;
  board_zoomArea.className = "zoomArea-container";
  // if(boardKind == 0){
  board_zoomArea.style.display = "none";
  // }
  
  board_zoomUp.id = "zoomUp-" + id_str;
  board_zoomUp.className = "zoomicon";
  board_zoomUp.src='images/zoomup.svg';
  
  board_zoomOut.id = "zoomOut-" + id_str;
  board_zoomOut.className = "zoomicon";
  board_zoomOut.src='images/zoomout.svg';
  
  board_zoomRateLbl.id = "zoomRateLbl-"  + id_str;
  board_zoomRateLbl.className = "zoomRateLbl";
  board_zoomRateLbl.innerText = "100%";
  board_zoomRateLbl.style["vertical-align"] = "sub";
  board_zoomRateLbl.style["margin-left"] = "10px";
  
  board_zoomRange.type = "range";
  board_zoomRange.className = "zoom-range";
  board_zoomRange.min = "10";
  board_zoomRange.max = "200";
  board_zoomRange.step = "10";
  board_zoomRange.value = "100";
  board_zoomRange.id = "zoom-range-" + id_str;
  board_zoomRange.style["margin-top"] = "18px";
  board_zoomRange.style["vertical-align"] = "sub";
  board_zoomRange.style["margin-left"] = "10px";
  
  //Add start ymd
  board_zoom_Separator.style["margin-left"] = "18px";
  board_zoom_Separator.src = 'images/separatorVertical.svg';
  board_zoom_DoubleArrow.className = "zoom_DoubleArrow";
  board_zoom_DoubleArrow.id = "zoom_DoubleArrow-" + id_str;
  board_zoom_DoubleArrow.src = double_arrow_svg.left.src;
  // board_zoom_DoubleArrow.addEventListener("click",changeZoomMenubarDisplay);
  //Add end ymd
  
  // ボード名表示
  board_nameArea.id = "boardNameArea-container-" + id_str;
  board_nameArea.className = "boardNameArea-container";
  board_nameArea.style.display = "none";
  
  board_nameLbl.id = `lbl-boardname-${id_str}`;
  board_nameLbl.innerText = newLbl.innerText;
  board_nameLbl.className = " header-text-4 font-source-han-san-bold text-heading";
  board_nameLbl.style.margin = "10px 30px 10px 30px";
  board_nameLbl.style.float = "left";
  
  board_nameBorderLine.className = "toolbar-border";
  
  board_dispChange.id = "boardDispChange-" + id_str;
  board_dispChange.className = "joint-widget joint-theme-modern my-board-bar-button";
  //board_dispChange.src = double_arrow_svg.up.src;
  board_dispChange.setAttribute("data-name", "boardDisp-enable");
  board_dispChange.addEventListener("click", function(){
    
    let name = "新規ボード";
    if(boardName != ""){
      name = boardName;
    }
    
    shareBoardDisplayChange(id_str,name);
  });
  
  // 右クリックメニュー
  board_contextmenu.id = "contextmenu-" + id_str;
  board_contextmenu.className = "contextmenu";
  board_contextmenu_ul.className = "contentmenu_ul";
  board_contextmenu_memo.id = "contextmenu-memo-" + id_str;
  board_contextmenu_memo.innerText = "付箋追加";
  board_contextmenu_memo.className = "contextmenu_memo contentmenu_li";
  board_contextmenu_line.id = "contextmenu-line-" + id_str;
  board_contextmenu_line.innerText = "領域線追加";
  board_contextmenu_line.className = "contextmenu_line contentmenu_li";
  board_contextmenu_boardHidden.id = "contextmenu-boardHidden-" + id_str;
  board_contextmenu_boardHidden.innerText = "ボード非表示";
  board_contextmenu_boardHidden.className = "contextmenu_boardHidden contentmenu_li";
  
  // ボード本体
  board_mainDiv.id = id_str;
  board_mainDiv.className = "yyKnowledgeBoard custom-scrollbar";
  
  //DEHA_EQR-210
  reactionArea.id = ID_REACTION_STICKY_NOTE + id_str;
  reactionArea.className = 'reaction-sticky';
  reactionArea.style.display = 'none';
  //DEHA_EQR-210
  
  //DEHA_EQR-233
  changeColorArea.id = ID_CHANGE_COLOR_STICKY_NOTE + id_str;
  changeColorArea.className = 'change-color-sticky';
  changeColorArea.style.display = 'none';
  //DEHA_EQR-233
  
  // DEHA_EQR-244 start
  
  //Item Collapse Expand
  let menuExpand = document.createElement('div');
  menuExpand.id = 'menu-collapse';
  
  let menuCollapse = document.createElement('div');
  menuCollapse.id = 'menu-expand';
  menuCollapse.className = 'd-none';
  
  let itemCollapse = document.createElement('div');
  itemCollapse.className = 'flex-layout-menu-top item-menu-top change-size-menu tooltip-right';
  itemCollapse.id = 'item-menu-expand';
  let imgItemCollapse = document.createElement('img');
  imgItemCollapse.src = '/images/icon-menu-top/icon_expand.svg';
  itemCollapse.appendChild(imgItemCollapse);
  
  let itemExpand = document.createElement('div');
  itemExpand.className = 'flex-layout-menu-top item-menu-top change-size-menu tooltip-right';
  itemExpand.id = 'item-menu-collapse';
  itemExpand.setAttribute('aria-label', 'tooltip');
  let imgItemExpand = document.createElement('img');
  imgItemExpand.src = '/images/icon-menu-top/icon_collapse.svg';
  itemExpand.appendChild(imgItemExpand);
  
  //Item Search
  let itemSearch = document.createElement('div');
  itemSearch.className = 'item-search flex-layout-menu-top item-menu-top change-size-menu event-coming-soon';
  itemSearch.setAttribute('data-tooltip','まもなく！');
  itemSearch.setAttribute('data-tooltip-position', 'top');
  let imgItemSearch = document.createElement('img');
  imgItemSearch.src = '/images/icon-menu-top/icon_search.svg';
  itemSearch.appendChild(imgItemSearch)
  
  //Item slide show
  let itemSlideShow = document.createElement('div');
  itemSlideShow.className = 'item-slide-show flex-layout-menu-top item-menu-top change-size-menu event-coming-soon';
  itemSlideShow.setAttribute('data-tooltip','まもなく！');
  itemSlideShow.setAttribute('data-tooltip-position', 'top');
  let imgItemSlideShow = document.createElement('img');
  imgItemSlideShow.src = '/images/icon-menu-top/icon_slide_show.svg';
  itemSlideShow.appendChild(imgItemSlideShow);
  
  //Item record voice over
  let itemRecordVoiceOver = document.createElement('div');
  // 常時音認を一時的に停止 -->
  itemRecordVoiceOver.className = 'item-record-voice-over flex-layout-menu-top item-menu-top change-size-menu event-coming-soon';
  itemRecordVoiceOver.setAttribute('data-tooltip','まもなく！');
  itemRecordVoiceOver.setAttribute('data-tooltip-position', 'top');
  // <-- 常時音認を一時的に停止
  let imgItemRecordVoiceOver = document.createElement('img');
  imgItemRecordVoiceOver.src = '/images/icon-menu-top/icon_record_voice_over.svg';
  itemRecordVoiceOver.appendChild(imgItemRecordVoiceOver);
  
  // deha_eqr-266 start
  
  let popupVoiceRecord = document.createElement('div');
  popupVoiceRecord.id = 'manage-remark-record-' + id_str;
  popupVoiceRecord.className = 'parent-popup-voice';
  setTimeout(function () {
    $(`#manage-remark-record-${id_str}`).html(
      '<div class="popup-voice-record font-source-han-san d-none">' +
      '<div class="header-popup-voice d-flex flex-column align-item-flex-start">' +
      '<div class="voice-record-action d-flex justify-content-center align-items-center">' +
      '<div class="identification-voice d-flex justify-content-start align-items-center mic-on">' +
      '<img>' +
      '<div class="font-source-han-san-bold title-identification medium-text"></div>' +
      '</div>' +
      '<div class="download-csv-voice d-flex justify-content-start align-items-center">' +
      '<img src="/images/icon-menu-top/download-csv.svg">' +
      '<div class="font-source-han-san-bold medium-text text-heading">CSVダウンロード</div>' +
      '</div>' +
      '</div>' +
      '<div class="filter-remark-voice d-flex justify-content-around align-items-center">' +
      '<div class="d-flex justify-content-start align-items-center">' +
      `<input class="input-filter-remark custom-control-input" id="input-filter-remark-${id_str}" type="checkbox">` +
      `<label class="label-filter-remark custom-control-label font-source-han-san-bold checked-off medium-text" for="input-filter-remark-${id_str}">自分の発言だけ表示する</label>` +
      '</div>' +
      '<div class="d-flex justify-content-start align-items-center">' +
      `<input class="input-display-sticky-note custom-control-input" id="display-sticky-note-${id_str}" type="checkbox">` +
      `<label class="label-display-sticky-note custom-control-label font-source-han-san-bold checked-off medium-text" for="display-sticky-note-${id_str}">ふせんを表示する</label>` +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div class="list-voice-remark d-flex flex-column align-item-flex-start"></div>' +
      '<div class="footer-popup-voice d-flex justify-content-center align-items-center">' +
      '<div class="voice-record">' +
      '<div class="parent-voice-animation d-flex justify-content-center align-items-center">' +
      // 常時音声認識対応 -->
      '<div class="voice-animation d-flex justify-content-center align-items-center bg-voice-mic-on">' +
      `<div class="voice-mic-toggle on-voice-mic" id="on-off-mic-voice"></div>` +
      // <-- 常時音声認識対応
      '</div>' +
      '</div>' +
      '</div>' +
      '<textarea class="area-text-voice" readonly="true"></textarea>' +
      '</div>' +
      '</div>'
    );
  });
  
  
  //Item preview
  let itemPreview = document.createElement('div');
  itemPreview.className = 'item-preview flex-layout-menu-top item-menu-top change-size-menu event-coming-soon';
  itemPreview.setAttribute('data-tooltip','まもなく！');
  itemPreview.setAttribute('data-tooltip-position', 'top');
  let imgItemPreview = document.createElement('img');
  imgItemPreview.src = '/images/icon-menu-top/icon_preview.svg';
  itemPreview.appendChild(imgItemPreview);
  
  //Item Share
  let itemShare = document.createElement('div');
  itemShare.className = 'item-share flex-layout-menu-top item-menu-top change-size-menu';
  itemShare.id = 'share-board'
  let imgItemShare = document.createElement('img');
  imgItemShare.src = '/images/icon-menu-top/icon_share.svg';
  itemShare.appendChild(imgItemShare);
  
  //Item Download
  let itemDownload = document.createElement('div');
  itemDownload.className = 'item-download flex-layout-menu-top item-menu-top change-size-menu';
  itemDownload.id = 'download-board'
  let imgItemDownload = document.createElement('img');
  imgItemDownload.src = '/images/icon-menu-top/icon_download.svg';
  itemDownload.appendChild(imgItemDownload);
  
  menuExpand.appendChild(itemExpand);
  menuExpand.appendChild(itemSearch);
  menuExpand.appendChild(itemSlideShow);
  menuExpand.appendChild(itemRecordVoiceOver);
  menuExpand.appendChild(popupVoiceRecord);
  menuExpand.appendChild(itemPreview);
  menuExpand.appendChild(itemShare);
  menuExpand.appendChild(itemDownload);
  
  menuCollapse.appendChild(itemCollapse);
  
  board_nameArea.appendChild(menuExpand);
  board_nameArea.appendChild(menuCollapse);
  
  let downloadContainer = document.createElement('div');
  downloadContainer.className = 'download-area hide-it'
  
  let itemDownloadImage =  document.createElement('div');
  itemDownloadImage.className = 'item-menu-download';
  itemDownloadImage.id = 'download-image';
  itemDownloadImage.setAttribute("data-id", id_str);
  
  let imageDownloadImage = document.createElement('div');
  imageDownloadImage.className = 'icon-menu-download';
  let iconDownloadImage = document.createElement('img');
  iconDownloadImage.src = '/images/icon-menu-top/icon_image.svg';
  
  let textDownloadImage = document.createElement('div');
  textDownloadImage.className = 'font-source-han-san-bold text-heading large-text';
  textDownloadImage.innerText = '画像で保存する';
  
  imageDownloadImage.appendChild(iconDownloadImage);
  itemDownloadImage.appendChild(imageDownloadImage);
  itemDownloadImage.appendChild(textDownloadImage);
  
  //Item downloadPDF
  let itemDownloadPDF =  document.createElement('div');
  itemDownloadPDF.className = 'item-menu-download';
  itemDownloadPDF.id = 'download-pdf';
  itemDownloadPDF.setAttribute("data-id", id_str);
  // メニューの形状統一化対応 -->
  itemDownloadPDF.setAttribute('data-tooltip', 'まもなく！');
  itemDownloadPDF.setAttribute('data-tooltip-position', 'right');
  // <-- メニューの形状統一化対応
  
  let imageDownloadPDF = document.createElement('div');
  imageDownloadPDF.className = 'icon-menu-download';
  let iconDownloadPDF = document.createElement('img');
  iconDownloadPDF.src = '/images/icon-menu-top/icon_pdf.svg';
  
  let textDownloadPDF = document.createElement('div');
  textDownloadPDF.className = 'font-source-han-san-bold text-heading large-text';
  textDownloadPDF.innerText = 'PDFで保存する';
  
  imageDownloadPDF.appendChild(iconDownloadPDF);
  itemDownloadPDF.appendChild(imageDownloadPDF);
  itemDownloadPDF.appendChild(textDownloadPDF);
  
  // Item download CSV
  let itemDownloadCSV =  document.createElement('div');
  itemDownloadCSV.className = 'item-menu-download';
  itemDownloadCSV.id = 'download-csv';
  itemDownloadCSV.setAttribute("data-id", id_str);
  // メニューの形状統一化対応 -->
  itemDownloadCSV.setAttribute('data-tooltip', 'まもなく！');
  itemDownloadCSV.setAttribute('data-tooltip-position', 'right');
  // <-- メニューの形状統一化対応
  
  let imageDownloadCSV = document.createElement('div');
  imageDownloadCSV.className = 'icon-menu-download';
  let iconDownloadCSV = document.createElement('img');
  iconDownloadCSV.src = '/images/icon-menu-top/icon_csv.svg';
  
  let textDownloadCSV = document.createElement('div');
  textDownloadCSV.className = 'font-source-han-san-bold text-heading large-text';
  textDownloadCSV.innerText = 'CSVファイルで保存する';
  
  imageDownloadCSV.appendChild(iconDownloadCSV);
  itemDownloadCSV.appendChild(imageDownloadCSV);
  itemDownloadCSV.appendChild(textDownloadCSV);
  
  downloadContainer.appendChild(itemDownloadImage);
  downloadContainer.appendChild(itemDownloadPDF);
  downloadContainer.appendChild(itemDownloadCSV);
  
  // DEHA_EQR-244 end
  
  // Append
  board_contextmenu_ul.appendChild(board_contextmenu_memo);
  board_contextmenu_ul.appendChild(board_contextmenu_line);
  board_contextmenu_ul.appendChild(board_contextmenu_boardHidden);
  board_contextmenu.appendChild(board_contextmenu_ul);
  
  // board_toolbar.appendChild(board_nameLbl);
  
  board_zoomArea.appendChild(board_zoomOut);
  board_zoomArea.appendChild(board_zoomRange);
  board_zoomArea.appendChild(board_zoomUp);
  board_zoomArea.appendChild(board_zoomRateLbl);
  board_zoomArea.appendChild(board_zoom_Separator);//ymd
  board_zoomArea.appendChild(board_zoom_DoubleArrow);//ymd
  
  // board_nameArea.appendChild(menuTopContainer);
  // board_nameArea.appendChild(board_nameBorderLine);
  // board_nameArea.appendChild(board_dispChange);
  
  board_Frame.appendChild(board_mainDiv);
  
  // 個人ボードツールバー表示
  let myBoardName = undefined;
  if(boardKind == BOARD_KIND_KOJIN){
    
    let kojinBoardChange = function(boardId){
      let board = document.getElementById(`${ID_KNOWLEDGEBOARD_FRAME + boardId}`);
      let td = document.getElementById("yyKnowledgeBoard-kojin-boards");
      
      // 既にあれば消す
      let td_children_len = td.children.length;
      for(let i = 0; i < td_children_len; i++){
        
        let hiddenBoardId = td.children[i].id.replace(ID_KNOWLEDGEBOARD_FRAME, "");
        td.children[i].style.display = "";
        document.getElementById(`toolbar-container-${hiddenBoardId}`).style.display = "none";
        document.getElementById(`toolbarShort-container-${hiddenBoardId}`).style.display = "none";
        document.getElementById(`zoomArea-container-${hiddenBoardId}`).style.display = "none";
        
        document.getElementById("yyKnowledgeBoard-hidden-boards").appendChild(td.children[i]);
      }
      
      td.appendChild(board);
      
      // 共有ボード無ければツールバー出す
      let shareBoardId = getShareBoardId();
      if(shareBoardId != -1){
        if(getShareBoardDisplayStatus(shareBoardId) == false){
          document.getElementById(`toolbar-container-${boardId}`).style.display = "";
          document.getElementById(`zoomArea-container-${boardId}`).style.display = "";
        }
      }
      
      activeBoardId = boardId;
      
      // ボードの初期位置中央にする
      let boardIndex = getBoardsIndex(activeBoardId);
      boards[boardIndex].paperScroller.center();
      
    };
    
    myBoardName = document.createElement('div');
    myBoardName.id = "my-board-name-" + id_str;
    myBoardName.className = "my-board-name header-text-4 font-source-han-san-bold text-heading my-board-disable";
    myBoardName.innerText = boardName;
    myBoardName.title = boardName;
    
    let myBoardArea = document.getElementById("my-board-name-area");
    if(myBoardArea){
      myBoardArea.prepend(myBoardName);
    }
    
    $(`#${myBoardName.id}`).click(function(e){
      
      if(myBoardName.className.indexOf("my-board-disable") == -1){
        return;
      }
      
      // 名前の色変え
      let myBoards = document.getElementsByClassName("my-board-name");
      let len = myBoards.length;
      for(var i = 0; i < len; i++){
        let index = myBoards[i].className.indexOf("my-board-disable");
        if(index == -1){
          myBoards[i].className = myBoards[i].className + " my-board-disable";
        }
      }
      myBoardName.classList.remove("my-board-disable");
      
      // ボードの入れ替え
      kojinBoardChange(id_str);
    });
  }
  
  let paperScrollAppendFunc = function(){
    let paperScrollDiv  = document.getElementById(ID_PAPER_SCROLLER + id_str);
    paperScrollDiv.appendChild(board_zoomArea);
    board_Frame.appendChild(board_nameArea);
    board_Frame.appendChild(downloadContainer);
    paperScrollDiv.appendChild(board_sideToolbar);
    paperScrollDiv.appendChild(board_contextmenu);
    //add st ymd
    paperScrollDiv.appendChild(board_sideToolbarShort);
    paperScrollDiv.appendChild(board_addFigureMenubar);
    paperScrollDiv.appendChild(board_addLineMenu);
    paperScrollDiv.appendChild(board_lineTypeSettingMenubar);
    paperScrollDiv.appendChild(board_lineLeftMenubar);
    paperScrollDiv.appendChild(board_lineRightMenubar);
    paperScrollDiv.appendChild(board_textOptionMenu);
    paperScrollDiv.appendChild(board_fusenMenubar);//ymd
    paperScrollDiv.appendChild(board_voiceFusenMenubar);
    
    board_mainDiv.appendChild(board_lineMenubar);
    board_mainDiv.appendChild(board_fusenSubMenubar);//ymd
    board_mainDiv.appendChild(board_textMenubar);//ymd
    board_mainDiv.appendChild(board_shapeSubMenubar);//ymd
    board_mainDiv.appendChild(board_multiSelectMenubar);
    board_mainDiv.appendChild(reactionArea);
    board_mainDiv.appendChild(changeColorArea);
  }
  
  if(boardKind == BOARD_KIND_KOJIN && document.getElementById(ID_KOJIN_BOARDS).children.length == 0){
    document.getElementById(ID_KOJIN_BOARDS).appendChild(board_Frame);
    myBoardName.classList.remove("my-board-disable");
  }else{
    //add end YMD
    document.getElementById("yyKnowledgeBoard-hidden-boards").appendChild(board_Frame);
  }
  
  // if(boardKind == BOARD_KIND_SHARE){//add YMD
  //     board_Frame.appendChild(board_nameArea);
  //     //document.getElementById("yyKnowledgeBoard-shareBoradName").appendChild(board_nameArea);
  // }
  // ボード初期化
  addBoard(board_mainDiv.id, newLbl.innerText, id_str, boardKind, paperScrollAppendFunc);
  viewMenuTop = $('.boardNameArea-container').html()
  
  // Add 20201027 Y.Akasaka　シングルクリックでボード切替するように変更
  $(`#${newDiv.id}`).click(function(e){
    
    board_dblclick(id_str, newLbl.innerText);
  });
  
  $(`#${newDiv.id}`).dblclick(async function(e){
    
    // Add 20201027 Y.Akasaka ダブルクリックで拡大表示に変更
    //board_dblclick(id_str, newLbl.innerText);
    let boardZoomWindowOpen = function(){
      let imgCtrl = document.getElementById(newThumbnailImg.id);
      if(imgCtrl != null){
        
        boardZoomWindow = window.open("", "board-zoom-disp", `width=600px,height=300px,location=0,directories=0,toolbar=0,status=0,menubar=0,scrollbars=1`);
        boardZoomWindow.document.open();
        boardZoomWindow.document.write("<html>");
        boardZoomWindow.document.write("<head>");
        boardZoomWindow.document.write("<title>拡大ウインドウ</title>");
        boardZoomWindow.document.write('<link rel="icon" href="images/logo.png" sizes="32x32">');
        boardZoomWindow.document.write("</head>");
        boardZoomWindow.document.write("<body style='margin:0px;padding:0px'>");
        boardZoomWindow.document.write("</body>");
        boardZoomWindow.document.write("</html>");
        boardZoomWindow.document.close();
        
        let img = document.createElement('img');
        img.src = imgCtrl.src;
        img.style.width = "100%";
        img.style.height = "100%";
        img.style["object-fit"] = "contain";
        boardZoomWindow.document.body.append(img);
      }
    }
    
    // // サムネイル画像更新
    // await boardThumbnailTimer(100, id_str, boardZoomWindowOpen);
    
  });
  
  
  $(`#${newThumbnailDiv.id}`).click(function (e) {
    boardImg_click(e.currentTarget.id, e);
  });
  // 右クリックメニューのボード非表示クリック
  $(`#${board_contextmenu_boardHidden.id}`).click(function () {
    
    boardHidden_Click();
    
  });
  
  // 右クリックメニューのボード非表示マウスオーバー
  $(`#${board_contextmenu_boardHidden.id}`).mouseover(function () {
    
    onmouseover_ContentMenu("contextmenu_boardHidden");
    
  });
  
  // 右クリックメニューのボード非表示マウスアウト
  $(`#${board_contextmenu_boardHidden.id}`).mouseout(function () {
    
    onmouseout_ContentMenu("contextmenu_boardHidden");
    
  });
  
  // 拡大率変更イベント
  $(`#${board_zoomRange.id}`).on('input', function(e) {
    
    // YMD commentアウト　20201015　Requa問題点ID：289
    // if(loadingFlg){
    //      return;
    // }
    let len = boards.length;
    for(let i = 0; i < len; i++){
      // let boardId = e.currentTarget.id.replace("zoom-range-", "");
      let boardId = boards[i].id;
      let lbl = document.getElementById("zoomRateLbl-" + boardId);
      let range = document.getElementById("zoom-range-" + boardId);
      if(lbl){
        let nowScale = Number(lbl.innerText.replace("%", "")) / 100;
        let scale = ($(this).val() / 100);
        if(boardId != ""){
          zoom(boardId, scale, scale - nowScale);
          lbl.innerText = $(this).val() + "%";
          range.value = scale * 100;
        }
        
      }
      
    }
    
  });
  
  // ズームの「+」ボタンクリック時の処理
  $(`#${board_zoomUp.id}`).on('click', function(e) {
    
    // YMD commentアウト　20201015　Requa問題点ID：289
    // if(loadingFlg){
    //     return;
    // }
    let len = boards.length;
    for(let i = 0; i < len; i++){
      let zoomRange = document.getElementById("zoom-range-" + boards[i].id);
      zoomRange.value = (Number(zoomRange.value) + Number(zoomRange.step));
      let scale = Number(zoomRange.value) / 100;
      
      if(boards[i].id != ""){
        zoom(boards[i].id, scale, Number(zoomRange.step) / 100);
        document.getElementById("zoomRateLbl-"+boards[i].id).innerText = zoomRange.value + "%";
      }
    }
  });
  
  // ズームの「-」ボタンクリック時の処理
  $(`#${board_zoomOut.id}`).on('click', function(e) {
    
    // YMD commentアウト　20201015　Requa問題点ID：289
    // if(loadingFlg){
    //     return;
    // }
    
    let len = boards.length;
    for(let i = 0; i < len; i++){
      let zoomRange = document.getElementById("zoom-range-" + boards[i].id);
      zoomRange.value = (Number(zoomRange.value) - Number(zoomRange.step));
      let scale = Number(zoomRange.value) / 100;
      
      if(boards[i].id != ""){
        zoom(boards[i].id, scale, -(Number(zoomRange.step) / 100));
        document.getElementById("zoomRateLbl-"+boards[i].id).innerText = zoomRange.value + "%";
      }
    }
    
  });
  
  // Zooｍツールの<<、>>
  $(`#${board_zoom_DoubleArrow.id }`).on('click', function(e) {
    let boardId = e.currentTarget.id.replace("zoom_DoubleArrow-", "");
    
    var fullPath =  e.currentTarget.src;
    var filename = fullPath.replace(/^.*[\\\/]/, '');
    
    //alert(boardId);
    
    if("images/" + filename == double_arrow_svg.left.src){
      document.getElementById("zoom_DoubleArrow-" + boardId).src = double_arrow_svg.right.src;
      document.getElementById("zoomOut-" + boardId).style.display = "none";
      document.getElementById("zoomUp-" + boardId).style.display = "none";
      document.getElementById("zoom-range-" + boardId).style.display = "none";
      document.getElementById("zoomArea-container-" + boardId).style.width = '125px';
    }else{
      document.getElementById("zoom_DoubleArrow-" + boardId).src = double_arrow_svg.left.src;
      document.getElementById("zoomOut-" + boardId).style.display = "";
      document.getElementById("zoomUp-" + boardId).style.display = "";
      document.getElementById("zoom-range-" + boardId).style.display = "";
      document.getElementById("zoomArea-container-" + boardId).style.width = '350px';
    }
  });
  
  // ボード上をマウスカーソルが移動したときの処理
  // $(`#${board_Frame.id}`).on('mousemove', function(e) {
  $(`#${board_mainDiv.id}`).on('mousemove', function(e) {
    
    // Add start 20201111 kobayashi
    let scrolltop = $(this).scrollTop();
    let scrollleft = $(this).scrollLeft();
    let os = $(this).offset();
    let zoomRange = document.getElementById("zoom-range-" + id_str);
    let scale = Number(zoomRange.value) / 100;
    objectPastePosition = {
      boardId : id_str,
      x : (e.pageX  - os.left + scrollleft) / scale,
      y : (e.pageY - os.top + scrolltop) / scale
    }
    // Add end 20201111 kobayashi
    
    // 個人ボード操作中は更新しない
    if(getBoardKind(id_str) == BOARD_KIND_KOJIN){
      return;
    }
    
    // Del start 20201111 kobayashi
    // let scrolltop = $(this).scrollTop();
    // let scrollleft = $(this).scrollLeft();
    
    // let os = $(this).offset();
    // Del end 20201111 kobayashi
    
    if(userInfo.peerid == undefined){
      userInfo.peerid = "";
    }
    if(userInfo.peerid == ""){
      userInfo.peerid = getMyPeerId(selectMeeting);
    }
    
    // Del start 20201111 kobayashi
    // let zoomRange = document.getElementById("zoom-range-" + id_str);
    // let scale = Number(zoomRange.value) / 100;
    // Del end 20201111
    
    // ニックネームあれば反映
    let username = userInfo.response[0].name;
    if(userInfo.response[0].nickname){
      username  = userInfo.response[0].nickname ? userInfo.response[0].nickname : userInfo.response[0].name;
    }
    mousePointerInfo = {
      boardId : id_str,
      mousePointer : {
        x : (e.pageX  - os.left + scrollleft) / scale,
        y : (e.pageY - os.top + scrolltop) / scale
      },
      userCode : userInfo.response[0].code,
      userName : username,
      peerid : userInfo.peerid,
      mouseCursorColor : userInfo.mouseCursorColor
    }
  });
  
  $(`#${board_mainDiv.id}`).on('dblclick', function(e) {
    // ボード上をダブルクリックで編集モードにする。
    if(boardEditMode == false){
      changeBoardEditMode(!boardEditMode);
    }
  });
}

/*
disable =　非表示
default = デフォルト位置のみ
defaultAndVideo = デフォルト位置　+　カメラ
defaultAndShare = デフォルト位置　+  個人Board
ALL
*/
var shareBoardDisplaySetting　=
  {
    disable: {frameDisplayHeight : "0%", frameHeight :'0%', tdPadding:'0px', boardNameAreaColor: '#aaa', boardNameFontColor: 'white', toolbarDisp: "none", splitterDisp: "none"},
    default: {frameDisplayHeight : "100%", frameHeight : '100%', tdPadding:'.25rem', boardNameAreaColor: 'white', boardNameFontColor: '', toolbarDisp: "", splitterDisp: "none"},
    defaultAndVideo: {frameDisplayHeight : "100%", frameHeight :'100%', tdPadding:'.25rem', boardNameAreaColor: 'white', boardNameFontColor: '', toolbarDisp: "", splitterDisp: "none"},
    defaultAndKojin: {frameDisplayHeight : "65%", frameHeight : '100%', tdPadding:'.25rem', boardNameAreaColor: 'white', boardNameFontColor: '', toolbarDisp: "", splitterDisp: ""},
    ALL:{frameDisplayHeight : "65%", frameHeight :'100%', tdPadding:'.25rem', boardNameAreaColor: 'white', boardNameFontColor: '', toolbarDisp: "", splitterDisp: ""},
  };

/************************************************************
 //type   :shareBoardDisplaySetting
 //boardId:
 //size   :half半分の場合
 *************************************************************/
function setShareBoardDisplay(type,boardId,size){
  document.getElementById(ID_KNOWLEDGEBOARD_FRAME + boardId).style.height = shareBoardDisplaySetting[type].frameHeight;
  document.getElementById("yyKnowledgeBoard-sharearea").style.height = shareBoardDisplaySetting[type].frameDisplayHeight;
  // document.getElementById("yyKnowledgeBoard-td1").style.padding = shareBoardDisplaySetting[type].tdPadding;
  // document.getElementById("boardSelect-td").style.padding = shareBoardDisplaySetting[type].tdPadding;
  document.getElementById('boardNameArea-container-'+ boardId).style.backgroundColor = shareBoardDisplaySetting[type].boardNameAreaColor;
  
  document.getElementById("zoomArea-container-" + boardId).style.display = shareBoardDisplaySetting[type].toolbarDisp;
  document.getElementById("toolbar-container-" + boardId).style.display = shareBoardDisplaySetting[type].toolbarDisp;
  
  document.getElementById("table-split-bar").style.display = shareBoardDisplaySetting[type].splitterDisp;
  
  if(shareBoardDisplaySetting[type].splitterDisp == ""){
    setBoardSplitSize();
  }
  
}


function boardHidden_Click(){
  
  let board = document.getElementById("yyKnowledgeBoard-frame-" + activeBoardId);
  board.style.display = "none";
  document.getElementById("yyKnowledgeBoard-hidden-boards").appendChild(board);
  document.getElementById(activeBoardId + "-disp").style.display = "none";
}

//-共有ボードのID取得
function getShareBoardDisplayid(){
  
  var boardIDList = new Array();
  var newBoardIDList = new Array(-1,-1,-1);
  var td0 = document.getElementById('yyKnowledgeBoard-td0');
  
  if (td0.children.length>=1){
    var children = td0.children;
    var boardId = (children[0].id).replace(ID_KNOWLEDGEBOARD_FRAME, "");
    boardIDList[0]=(boardId);
  }
  
  var td1 = document.getElementById('yyKnowledgeBoard-td1');
  
  if (td1.children.length>=1){
    for(var i=0; i<td1.children.length; i++){
      var children = td1.children;
      var boardId = (children[i].id).replace(ID_KNOWLEDGEBOARD_FRAME, "");
      boardIDList[i+1]=(boardId);
    }
  }
  
  newBoardIDList[0] = boardIDList[1];
  newBoardIDList[1] = boardIDList[0];
  newBoardIDList[2] = boardIDList[2];
  
  return newBoardIDList;
}

/*
ビデオ通話表示エリアの表示状態取得
return true:表示中、false：非表示
*/
function getVideoAreaDisplayStatus(){
  
  if(document.getElementById('videocall-main').style.display == 'flex'){
    return true;
  }else{
    return false;
  }
  
}

/*
個人ボードエリアの表示状態取得
return true:表示中、false：非表示
*/
function getKojinBoardDisplayStatus(){
  
  if(document.getElementById("yyKnowledgeBoard-kojinarea").style.height == '0%' || document.getElementById(ID_KOJIN_BOARDS).style.height == '0%'){
    return false;
  }else{
    return true;
  }
}
/*
共有ボードエリアの表示状態取得
return true:表示中、false：非表示
*/
function getShareBoardDisplayStatus(boardCtrlId){
  
  // if(document.getElementById(ID_KNOWLEDGEBOARD_FRAME + boardCtrlId).style.display != "none" ){
  if(document.getElementById(ID_KNOWLEDGEBOARD_FRAME + boardCtrlId).style.height != "0%" ){
    return true;
  }else{
    return false;
  }
}

/*ビデオ表示エリアの切り替え */
function changeVideoAreaDisplay()
{
  let ctrlTop = {
    toolbar: "",
    boardNameArea: ""
  }
  
  if(document.getElementById('videocall-main').style.display == 'undefined'){
    return;
  }else if(getVideoAreaDisplayStatus() == true){
    //ビデオ通話表示中
    document.getElementById('videocall-main').style.display = 'none';
    document.getElementById('Video_arrow_img').src = "images/double_arrow_down.svg";
    document.getElementById("shareBoradName-td0").style.top = "7%";
    document.getElementById(ID_KNOWLEDGEBOARD_BODY).style.height = '93%';
    document.getElementById("yyKnowledgeBoard-videocall").style.height = '0%';
    document.getElementById("yyKnowledgeBoard-videocall").style.paddingTop = '0px';
    document.getElementById("yyKnowledgeBoard-body").style.height = 'calc(100vh - 70px)';
    
    ctrlTop.toolbar = "95px";
    ctrlTop.boardNameArea = "95px";
    
  } else {
    //ビデオ通話非表示
    document.getElementById('videocall-main').style.display = 'flex';
    document.getElementById('Video_arrow_img').src = "images/double_arrow_up.svg";
    document.getElementById(ID_KNOWLEDGEBOARD_BODY).style.height = '81%';
    document.getElementById("shareBoradName-td0").style.top = "19%";
    document.getElementById("yyKnowledgeBoard-videocall").style.height = '130px';
    document.getElementById("yyKnowledgeBoard-videocall").style.paddingTop = '8px';
    document.getElementById("yyKnowledgeBoard-body").style.height = 'calc(100vh - 200px)';
    
    // ctrlTop.toolbar = "20%";
    // ctrlTop.boardNameArea = "19%";
  }
  
  let len = boards.length;
  for(let i = 0; i < len; i++){
    if(boards[i].kind == BOARD_KIND_SHARE){
      document.getElementById('toolbar-container-'+ boards[i].id).style.top = ctrlTop.toolbar;
      document.getElementById('boardNameArea-container-'+ boards[i].id).style.top = ctrlTop.boardNameArea;
    }
  }
}


/*
disable =　非表示
default = デフォルト位置のみ
defaultAndShare = デフォルト位置　+  共有Board
ALL
*/
var kojinBoardDisplaySetting　=
  {
    disable: {frameHeight : '0%', toolbarDisp: "none", splitterDisp: "none"},
    default: {frameHeight : '100%', toolbarDisp: "", splitterDisp: "none"},
    defaultAndVideo: {frameHeight :'100%', toolbarDisp: "", splitterDisp: "none"},
    defaultAndShare: {frameHeight : '35%', toolbarDisp: "none", splitterDisp: ""},
    ALL:{frameHeight : '35%', toolbarDisp: "none", splitterDisp: ""},
  };

function setKojinBoardDisplay(type,boardId){
  
  document.getElementById(ID_KOJIN_BOARDS).style.height = kojinBoardDisplaySetting[type].frameHeight;
  document.getElementById("yyKnowledgeBoard-kojinarea").style.height = kojinBoardDisplaySetting[type].frameHeight;
  document.getElementById("zoomArea-container-" + boardId).style.display = kojinBoardDisplaySetting[type].toolbarDisp;
  document.getElementById("toolbar-container-" + boardId).style.display = kojinBoardDisplaySetting[type].toolbarDisp;
  document.getElementById("table-split-bar").style.display = kojinBoardDisplaySetting[type].splitterDisp;
  
  if(kojinBoardDisplaySetting[type].splitterDisp == ""){
    setBoardSplitSize();
  }
  
}

function setBoardSplitSize(){
  
  let ctrl = document.getElementById("table-split-bar");
  let top = Number(ctrl.style.top.replace("%", ""));
  
  let shareHeight = top + "%";
  let kojinHeight = (100 - top) + "%";
  
  // document.getElementById("table-split-bar").style.top = shareHeight;
  document.getElementById("yyKnowledgeBoard-sharearea").style.height = shareHeight;
  document.getElementById(ID_KOJIN_BOARDS).style.height = kojinHeight;
  document.getElementById("yyKnowledgeBoard-kojinarea").style.height = kojinHeight;
};

/*個人ボードの切り替え*/
//マイボード表示
function changeMyBoardDisplay() {
  
  let shareBoardList = getShareBoardDisplayid();
  let boardCtrlId = shareBoardList[0];    //現在共有ボードが一枚の為強制的に入力している
  let kojinBoardId =　getKojinBoardId();
  let myBoardDispCtrl = document.getElementById('my-board-disp');
  
  // ショートツールバーは使わないので非表示
  document.getElementById("toolbarShort-container-" + boardCtrlId).style.display = "none";
  document.getElementById("toolbarShort-container-" + kojinBoardId).style.display = "none";
  
  if(boardCtrlId != kojinBoardId){
    //個人ボード表示
    if(getKojinBoardDisplayStatus() == false){
      //ボタン切り替え
      myBoardDispCtrl.setAttribute("data-name" ,"boardDisp-disable");
      
      // 共有ボード非表示
      if(getShareBoardDisplayStatus(boardCtrlId) == false){
        setShareBoardDisplay("disable",boardCtrlId,"");
        setKojinBoardDisplay("defaultAndVideo", kojinBoardId);
        
      }else{
        // 共有ボード表示
        //ビデオ通話非表示
        if(getVideoAreaDisplayStatus() == false){
          setShareBoardDisplay("defaultAndKojin",boardCtrlId,"");
          setKojinBoardDisplay("defaultAndShare", kojinBoardId);
          
        }else{
          setShareBoardDisplay("ALL",boardCtrlId,"");
          setKojinBoardDisplay("ALL", kojinBoardId);
        }
      }
    }else{
      myBoardDispCtrl.setAttribute("data-name", "boardDisp-enable");
      
      setKojinBoardDisplay('disable', kojinBoardId);
      document.getElementById("yyKnowledgeBoard-sharearea").style.height = "100%";
      
      // 共有ボード非表示
      if(getShareBoardDisplayStatus(boardCtrlId) == false){
        //nothing
      }else{
        //ビデオ通話非表示
        if(getVideoAreaDisplayStatus() == false){
          setShareBoardDisplay("default",boardCtrlId,"");
        }else{
          setShareBoardDisplay("defaultAndVideo",boardCtrlId,"");
        }
      }
    }
  }else{
    //nothing
  }
}

/*個人ボードのID取得 */
function getKojinBoardId(){
  let myBoards = document.getElementsByClassName("my-board-name");
  let len = myBoards.length;
  for(var i = 0; i < len; i++){
    let index = myBoards[i].className.indexOf("my-board-disable");
    if(index == -1){
      return myBoards[i].id.replace("my-board-name-", "");
    }
  }
}

//Boardのツールバーの表示をshortから通常に切替
function changeToolber(boardId){
  setDisplay("toolbar-container-",boardId);
  setDisplayNone("toolbarShort-container-",boardId);
}

//Boardのツールバーの表示を通常からshortに切替
function changeShortToolber(boardId){
  setDisplayNone("toolbar-container-",boardId);
  setDisplay("toolbarShort-container-",boardId);
}

// 付箋やオブジェクトの変更情報をブロードキャスト通信で送信する処理
async function sendBroadCastByNoteObject(logObj){
  
  let len = logObj.cell.length;
  if(len == undefined){
    len = 1;
    let array = [];
    array.push(logObj.cell);
    logObj.cell = array;
  }
  
  // 不具合修正 -->
  let username = userInfo.response[0].name;
  if(userInfo.response[0].nickname){
    username  = userInfo.response[0].nickname ? userInfo.response[0].nickname : userInfo.response[0].name;
  }
  // <-- 不具合修正
  
  let boardIndex = getBoardsIndex(activeBoardId);
  if(boards[boardIndex].kind != BOARD_KIND_SHARE){
    return;
  }
  
  for(let i = 0; i < len; i++){
    let sendData = {
      kind : BROADCAST_KIND_CHANGE_OBJECT,
      data : {
        operation : 0,
        boardId : activeBoardId,
        objectData : undefined,
        userCode: userInfo.response[0].code,
        // 不具合修正 -->
        userName: username,
        // <-- 不具合修正
        selectObjectId : logObj.selectObjectId,
        addselect: logObj.cell[i].attributes.addselect
      }
    }
    
    // operation
    switch(logObj.kind){
      
      // 変更
      case USER_OPERATION_EDIT:
      case USER_OPERATION_MOVE:
      case USER_OPERATION_LINK_MOVE:
        sendData.data.operation = 0;
        break;
      
      // 追加
      case USER_OPERATION_ADD:
      case USER_OPERATION_LINK_ADD:
        sendData.data.operation = 1;
        break;
      
      // 削除
      case USER_OPERATION_DELETE:
      case USER_OPERATION_LINK_DELETE:
      case USER_OPERATION_RANGEDELETE:
        sendData.data.operation = 2;
        break;
    }
    
    // オブジェクト情報
    sendData.data.objectData = await getObjectData(logObj.cell[i]);
    if(sendData.data.objectData == undefined){
      return;
    }
    
    // 追加以外画像のバイナリ情報送らない対応 -->
    // 画像追加処理改善 -->
    //  if(sendData.data.operation !== 1 && sendData.data.objectData.kind == OBJECT_KIND_STICKY_NOTE_FILE){
    if(sendData.data.objectData.kind == OBJECT_KIND_STICKY_NOTE_FILE){
      // <-- 画像追加処理改善
      if(sendData.data.objectData.remark_type == stickyNoteType.image){
        delete sendData.data.objectData.thumbnail_image;
      }
    }
    // <-- 追加以外画像のバイナリ情報送らない対応
    
    let contents = "";
    if(logObj.cell[i].attributes.type == CELL_TYPE_NOTE && logObj.cell[i].attributes.kind == OBJECT_KIND_STICKY_NOTE){
      if(document.getElementById(logObj.cell[i].attributes.attrs[".contents"].id) != null){
        contents = document.getElementById(logObj.cell[i].attributes.attrs[".contents"].id).innerText;
      }
      // テキスト入力DOMに反映されてない時用
      if(logObj.cell[i].attributes.attrs['.contents'].value != "" && contents == ""){
        if(logObj.cell[i].attributes.attrs['.contents'].value){
          contents = logObj.cell[i].attributes.attrs['.contents'].value;
        }
      }
      
    } else if(logObj.cell[i].attributes.kind == OBJECT_KIND_COMMENT){
      contents = logObj.cell[i].attributes.attrs.label.text;
      sendData.data.objectData.attrs.label.text = undefined;
    }
    sendData.data.objectData.contents = contents;
    // 画像追加処理改善 -->
    //  sendData.data.objectData.cell = logObj.cell[i];
    if(sendData.data.objectData.kind == OBJECT_KIND_STICKY_NOTE ||
      sendData.data.objectData.kind == OBJECT_KIND_STICKY_NOTE_FILE ){
      if( note_header_type == stickyNoteHeaderType.headerVisible){
        sendData.data.objectData.face_image = "";
      } else {
        sendData.data.objectData.face_image = logObj.cell[i].attributes.attrs.image['xlink:href'];
      }
    }
    // <-- 画像追加処理改善
    
    //  sendDataBroadcast(sendData);
    changeObjectDataList.push(sendData.data);
  }
  
}

// 付箋やオブジェクトの変更情報をブロードキャスト通信で受信したときの処理
function reciveNoteObject(data){
  
  switch(data.operation){
    case 0:
      shareNoteInfo(data);
      break;
    
    case 1:
      shareAddNote(data);
      break;
    
    case 2:
      shareDeleteNote(data);
      break;
    
  }
}

// 付箋情報共有：変更
function shareNoteInfo(data){
  
  let boardIndex = getBoardsIndex(data.boardId);
  if(boardIndex == -1){
    return;
  }
  let objIdx = boards[boardIndex].graph.attributes.cells.models.findIndex(a => {
    let res = false;
    if(a.attributes.attrs[".objectId"] != undefined){
      if(a.attributes.attrs[".objectId"].value == data.objectData.objectId){
        res = true;
      }
    }
    return res;
  });
  if(objIdx != -1){
    
    let cell = boards[boardIndex].graph.attributes.cells.models[objIdx];
    let cellView = boards[boardIndex].paper.findViewByModel(cell);
    
    // 移動対象のオブジェクトに選択枠が付いていたら消す
    if(cellView._toolsView != undefined){
      cellView.removeTools();
    }
    
    // //自由変形ウィジェットが付いていたら消す
    // let freeTransform = new joint.ui.FreeTransform({ cellView: cellView });
    // freeTransform.remove();
    
    if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE){
      
      // 座標更新
      let tx = data.objectData.x - cell.attributes.position.x;
      let ty = data.objectData.y - cell.attributes.position.y;
      cell.translate(tx, ty);
      cell.resize(data.objectData.width, data.objectData.height);
      
      if(data.objectData.audioPath){
        cell.attributes.attrs[".btn.audioPlay"].audioPath = data.objectData.audioPath;
        if(cell.attributes.attrs[".btn.audioPlay"].audioPath != "" &&
          cell.attributes.attrs[".btn.audioPlay>image"]['xlink:href'] == "images/dummy.png"){
          cellView.model.attributes.attrs[".btn.audioPlay>image"]['xlink:href'] = "images/voice_recognition.svg";
          cellView.update();
        }
      }
      cellView.model.attributes.attrs[".card"].fill = data.objectData.background;
      $(`#${cellView.id}`).find('.card').attr('fill', data.objectData.background);
      
      updateTextarea(cell.attributes.attrs[".contents"].id, data.objectData.contents);
      
    }else if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
      
      // 座標更新
      let tx = data.objectData.x - cell.attributes.position.x;
      let ty = data.objectData.y - cell.attributes.position.y;
      cell.translate(tx, ty);
      cell.resize(data.objectData.width, data.objectData.height);
      
    } else if(cellView.model.attributes.kind == OBJECT_KIND_SHAPE){
      
      // 座標とサイズ更新
      let tx = data.objectData.x - cell.attributes.position.x;
      let ty = data.objectData.y - cell.attributes.position.y;
      cell.translate(tx, ty);
      cell.resize(data.objectData.width, data.objectData.height);
      
      cellView.model.attributes.attrs.body.stroke = data.objectData.stroke;
      cellView.update();
      
    } else if(cellView.model.attributes.kind == OBJECT_KIND_COMMENT){
      cellView.model.attributes.position.x = data.objectData.position.x;
      cellView.model.attributes.position.y = data.objectData.position.y;
      cellView.model.attributes.size.width = data.objectData.size.width;
      cellView.model.attributes.size.height = data.objectData.size.height;
      cellView.model.attributes.attrs.label.text = data.objectData.contents;
      cellView.updateTransformation();
      cellView.update();
    }　else if(cellView.model.attributes.kind == OBJECT_KIND_LINE){
      let changeingCell_buf = changeingCell;
      cellView.model.attributes.z =  data.objectData.z;
      cellView.model.attributes.attrs.line = data.objectData.attrs_line;
      // cellView.model.attributes.target = getLinkTargetSourceSaveData(data.objectData.target),
      // cellView.model.attributes.source = getLinkTargetSourceSaveData(data.objectData.source),
      cellView.model.attributes.smooth = data.objectData.smooth;
      cellView.model.attributes.connector = data.objectData.connector;
      cellView.model.attributes.vertices = data.objectData.vertices;
      // Add Start 20201111 kobayashi
      if(cellView.model.attributes.labels){
        cellView.el.children[2].lastElementChild.lastChild.textContent = data.objectData.label;
        cellView.model.attributes.labels[0].attrs.text.text = data.objectData.label;
        cellView.model.attributes.labels[0].position = data.objectData.label_position;
      }
      // Add End 20201111
      cellView.update();
      
      // target
      if(data.objectData.target.x != undefined && data.objectData.target.y != undefined){
        
        cell.target({
          x : data.objectData.target.x,
          y : data.objectData.target.y,
        });
        
      } else if(data.objectData.target.objectId != undefined){
        
        cell.target({
          id : getObjectIDToCellID(boards[boardIndex].graph.attributes.cells.models, data.objectData.target.objectId)
        });
      }
      
      // source
      if(data.objectData.source.x != undefined && data.objectData.source.y != undefined){
        
        cell.source({
          x : data.objectData.source.x,
          y : data.objectData.source.y,
        });
        
      } else if(data.objectData.source.objectId != undefined){
        
        cell.source({
          id : getObjectIDToCellID(boards[boardIndex].graph.attributes.cells.models, data.objectData.source.objectId)
        });
      }
      changeingCell = changeingCell_buf;
      
    }　else if(cellView.model.attributes.kind == OBJECT_KIND_TEXT){
      cellView.model.attributes.position.x = data.objectData.x;
      cellView.model.attributes.position.y = data.objectData.y;
      cellView.model.attributes.size.width = data.objectData.width;
      cellView.model.attributes.size.height = data.objectData.height;
      cellView.model.attributes.attrs.label = data.objectData.attrs_label;
      cellView.model.attributes.attrs.body = data.objectData.attrs_body;
      cellView.updateTransformation();
      cellView.update();
    }
    
    // // 選択しているオブジェクトを最前面に持ってくる
    // if(data.selectObjectId){
    //     if(cell.attributes.attrs[".objectId"].value == data.selectObjectId){
    //         cell.toFront();
    //     }
    // }
    
    // オブジェクトに繋がっている線の情報を更新します。
    if(cell.isLink() == false){
      let links = boards[boardIndex].graph.getConnectedLinks(cell);
      let len = links.length;
      for(let i = 0; i < len; i++){
        let linkView =  boards[boardIndex].paper.findViewByModel(links[i]);
        linkView.update();
      }
    }
  }
  
}

// 付箋情報共有：追加
function shareAddNote(data){
  
  let boardIndex = getBoardsIndex(data.boardId);
  if(boardIndex == -1){
    return;
  }
  
  let z = boards[boardIndex].graph.maxZIndex() + 1;
  let cell = undefined;
  if(data.objectData.kind == OBJECT_KIND_STICKY_NOTE){
    
    let templete =  createStickyNoteTemplete(note_header_type, stickyNoteType.normal);
    
    // 画像追加処理改善 -->
    // cell = AddMember(data.objectData.x, data.objectData.y, z, data.objectData.id, data.objectData.name, data.objectData.date, data.objectData.contents, data.objectData.cell.attrs.image['xlink:href'], data.objectData.background, OBJECT_KIND_STICKY_NOTE, "会議ID:" + data.objectData.meeting_id, data.objectData.emotion, data.objectData.opacity, data.objectData.objectId, data.objectData.width, data.objectData.height );
    cell = AddMember(data.objectData.x, data.objectData.y, z, data.objectData.id, data.objectData.name, data.objectData.date, data.objectData.contents, data.objectData.face_image, data.objectData.background, OBJECT_KIND_STICKY_NOTE, "会議ID:" + data.objectData.meeting_id, data.objectData.emotion, data.objectData.opacity, data.objectData.objectId, data.objectData.width, data.objectData.height );
    // <-- 画像追加処理改善
    
    cell.markup = templete;
    
  } else if(data.objectData.kind == OBJECT_KIND_STICKY_NOTE_FILE){
    
    if(data.objectData.remark_type == stickyNoteType.image){
      
      cell = new joint.shapes.standard.Image();
      cell.size(data.objectData.width, data.objectData.height);
      cell.position(data.objectData.x, data.objectData.y);
      cell.attributes.z = data.objectData.z;
      cell.attr('.objectId/value', data.objectData.objectId);
      // Del start 20201111 kobayashi
      // cell.attr('root/title', data.objectData.file_name);
      // cell.attr('label/text', getWrapFileName(data.objectData.file_name));
      // Del end 20201111
      // 画像追加処理改善 -->
      // cell.attr('image/xlinkHref', data.objectData.thumbnail_image);
      // <-- 画像追加処理改善
      cell.attributes.kind = data.objectData.kind;
      cell.attributes.remark_type = data.objectData.remark_type;
      cell.attributes.file_info = {file_name: data.objectData.file_name ,src:data.objectData.src, url:data.objectData.download_url};
      
      // 画像追加処理改善 -->
      let index =imageList.findIndex(a => a.url == data.objectData.download_url);
      if(index == -1){
        // cell.attr('image/xlinkHref', "/images/files_icon/default.png");
        getBase64ImageToObjectId({
          boardId: data.boardId,
          objectId: data.objectData.objectId,
          url: data.objectData.download_url
        });
      } else {
        cell.attr('image/xlinkHref', imageList[index].imageData);
      }
      // <-- 画像追加処理改善
    }
    else{
      let templete =  createStickyNoteTemplete(note_header_type, data.objectData.remark_type);
      
      cell = AddMemberForFiles(
        data.objectData.x, data.objectData.y, data.objectData.z,
        data.objectData.id,
        data.objectData.name,
        data.objectData.date,
        data.objectData.file_name,
        data.objectData.src,
        // 画像追加処理改善 -->
        // data.objectData.cell.attrs.image['xlink:href'],
        data.objectData.face_image,
        // <-- 画像追加処理改善
        data.objectData.background,
        data.objectData.kind,
        "会議ID:" + data.objectData.meeting_id,
        data.objectData.opacity,
        data.objectData.objectId,
        data.objectData.remark_type,
        data.objectData.download_url,
        data.objectData.width,
        data.objectData.height,
        data.objectData.thumbnail_image);
      
      cell.markup = templete;
      contentsDivResize(cell, false, false, false);
    }
    
  } else if(data.objectData.kind == OBJECT_KIND_SHAPE){
    cell = new joint.shapes.standard.Rectangle();
    cell.attr('body/fill', data.objectData.fill);
    cell.attr('body/stroke', data.objectData.stroke);
    cell.attr('body/strokeWidth', data.objectData.strokeWidth);
    cell.attr('.objectId/value', data.objectData.objectId);
    cell.position(data.objectData.x, data.objectData.y);
    cell.size(data.objectData.width, data.objectData.height);
    cell.attributes.kind = OBJECT_KIND_SHAPE;
  } else if(data.objectData.kind == OBJECT_KIND_COMMENT){
    let celldata = {
      kind: OBJECT_KIND_COMMENT,
      z: z,
      position: data.objectData.position,
      size: data.objectData.size,
      remark_id: data.objectData.id,
      attrs: data.objectData.attrs
    }
    celldata.attrs.label.text = data.objectData.contents;
    cell = new joint.shapes.standard.Rectangle(celldata);
    if(cell.markup.length == 2){
      
      cell.markup.push({
        tagName: "g",
        children:[{
          selector: "audioPlay",
          tagName: "image"
        }]
      });
    }
    
  } else if(data.objectData.kind == OBJECT_KIND_LINE){
    
    cell =  new joint.shapes.standard.Link({
      connector: data.objectData.connector,
      smooth: data.objectData.smooth,
      attrs: {
        '.objectId': {value: data.objectData.objectId},
        line: data.objectData.attrs_line
      }
    });
    // Add Start 20201111 kobayashi
    cell.appendLabel({
      attrs: {
        text: {
          type: 'textarea',
          text: data.objectData.label,
          style:{whiteSpace: 'pre',
            'xml:space': "preserve",
            'word-break': 'break-all'},
        },
        rect: {
          fill: TRANSPARENT,
        },
        position: {
          distance: 0.3,
          args: {
            keepGradient: true
          }
        }
      }
    });
    // Add End 20201111
    cell.toFront();
    
    
    // コネクタ
    cell.attributes.vertices = data.objectData.vertices;
    
    // source
    if(data.objectData.source != undefined){
      // cell.attributes.source.x = 0;
      // cell.attributes.source.y = 0;
      if(data.objectData.source.objectId == undefined){
        cell.attributes.source.x = data.objectData.source.x;
        cell.attributes.source.y = data.objectData.source.y;
      } else {
        cell.attributes.source.id = getObjectIDToCellID(boards[boardIndex].graph.attributes.cells.models, data.objectData.source.objectId);
      }
    }
    
    // target
    if(data.objectData.target != undefined){
      if(data.objectData.target.objectId == undefined){
        cell.attributes.target.x = data.objectData.target.x;
        cell.attributes.target.y = data.objectData.target.y;
      } else {
        cell.attributes.target.id = getObjectIDToCellID(boards[boardIndex].graph.attributes.cells.models, data.objectData.target.objectId);
      }
    }
    
    // Z軸
    if(data.objectData.z != undefined){
      cell.attributes.z = data.objectData.z;
    }
    
    // 種別
    cell.attributes.kind = data.objectData.kind;
  } else if(data.objectData.kind == OBJECT_KIND_TEXT){
    cell = new joint.shapes.standard.Rectangle({
      kind: data.objectData.kind,
      position: { x: data.objectData.x, y: data.objectData.y },
      z: data.objectData.z,
      size: { width: data.objectData.width, height: data.objectData.height},
      attrs:{
        '.objectId': {value: data.objectData.objectId},
        label: data.objectData.attrs_label,
        body: data.objectData.attrs_body
      }
    });
  }
  
  
  // グラフに付箋追加
  if(cell != undefined){
    boards[boardIndex].graph.addCell(cell);
    
    if(data.objectData.kind == OBJECT_KIND_STICKY_NOTE){
      contentsDivResize(cell, false, false, true);
      
      let index = contentsList.findIndex(a => a.id == cell.attributes.attrs[".id"].text.replace("ID:",""));
      if(index != -1){
        updateTextarea(cell.attributes.attrs[".contents"].id, contentsList[index].contents);
      }
    }
    
    if(cell.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
      // Y.Akasaka 20201019
      //document.getElementById(cell.attributes.attrs[".download"].id).innerText = cell.attributes.file_info.file_name;
      if(data.objectData.remark_type != stickyNoteType.image){
        document.getElementById(cell.attributes.attrs[".file-name"].id).innerText = cell.attributes.file_info.file_name;
      }
    }
    
    // // ロック状態にする。
    // if(data.addselect == undefined || data.addselect == true){
    //     let ids = [];
    //     ids.push(data.objectData.objectId);
    //     let username = document.getElementById(`mouseCursor-${data.userCode}`).innerText;
    //     let lockdata = {
    //             boardId : data.boardId,
    //             objectIds : ids,
    //             userName : username,
    //             userCode : data.userCode,
    //             lock: true // true:ロック　false:解除
    //     }
    //     reciveNoteObjectLock(lockdata);
    // }
    
    // let cells = [];
    // cells.push(cell);
    // addNotes(cells, "", true, data.boardId);
  }
  
}

// 付箋情報共有：削除
function shareDeleteNote(data){
  
  let boardIndex = getBoardsIndex(data.boardId);
  if(boardIndex == -1){
    return;
  }
  
  let len = data.objectData.length;
  if(len){
    for(let i = 0; i < len; i++){
      let objIdx = boards[boardIndex].graph.attributes.cells.models.findIndex(a => {
        let res = false;
        if(a.attributes.attrs[".objectId"] != undefined){
          if(a.attributes.attrs[".objectId"].value == data.objectData[i].objectId){
            res = true;
          }
        }
        return res;
      });
      if(objIdx != -1){
        
        let cellView = boards[boardIndex].paper.findViewByModel(boards[boardIndex].graph.attributes.cells.models[objIdx]);
        
        // 移動対象のオブジェクトに選択枠が付いていたら消す
        if(cellView._toolsView != undefined){
          cellView.removeTools();
        }
        // //自由変形ウィジェットが付いていたら消す
        // let freeTransform = new joint.ui.FreeTransform({ cellView: cellView });
        // freeTransform.remove();
        
        if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE ||
          cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE ||
          cellView.model.attributes.kind == OBJECT_KIND_SHAPE ||
          cellView.model.attributes.kind == OBJECT_KIND_COMMENT ||
          cellView.model.attributes.kind == OBJECT_KIND_LINE ||
          cellView.model.attributes.kind == OBJECT_KIND_TEXT
        ){
          // 付箋削除
          cellView.model.remove();
          
          let lockDiv = document.getElementById("object-lock-" + data.userCode);
          if(lockDiv != null){
            lockDiv.style.display = "none";
          }
        }
      }
    }
  } else {
    let objIdx = boards[boardIndex].graph.attributes.cells.models.findIndex(a => {
      let res = false;
      if(a.attributes.attrs[".objectId"] != undefined){
        if(a.attributes.attrs[".objectId"].value == data.objectData.objectId){
          res = true;
        }
      }
      return res;
    });
    if(objIdx != -1){
      
      let cellView = boards[boardIndex].paper.findViewByModel(boards[boardIndex].graph.attributes.cells.models[objIdx]);
      
      // 移動対象のオブジェクトに選択枠が付いていたら消す
      if(cellView._toolsView != undefined){
        cellView.removeTools();
      }
      //自由変形ウィジェットが付いていたら消す
      let freeTransform = new joint.ui.FreeTransform({ cellView: cellView });
      freeTransform.remove();
      
      if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE ||
        cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE ||
        cellView.model.attributes.kind == OBJECT_KIND_SHAPE ||
        cellView.model.attributes.kind == OBJECT_KIND_COMMENT ||
        cellView.model.attributes.kind == OBJECT_KIND_LINE ||
        cellView.model.attributes.kind == OBJECT_KIND_TEXT
      ){
        // 付箋削除
        cellView.model.remove();
        
        let lockDiv = document.getElementById("object-lock-" + data.userCode);
        if(lockDiv != null){
          lockDiv.style.display = "none";
        }
      }
    }
  }
  
  
}

// マウスカーソル共有化のタイマー制御処理
async function mousePointerTimerEnable(enable) {
  
  let chk = document.getElementById("hMousePointerTimerEnable");
  chk.value = enable;
  if (chk.value == 'true') {
    
    if(timerIdMousePointerSend == undefined){
      
      while (chk.value) {
        
        await mousePointerTimer(MOUSEPOINTER_SEND_TIMER);
        
      }
    }
    
  } else {
    
    if (timerIdMousePointerSend) {
      clearTimeout(timerIdMousePointerSend);
    }
    timerIdMousePointerSend = undefined;
  }
}

// マウスカーソル共有化のタイマー処理
function mousePointerTimer(timer) {
  
  return new Promise((resolve) => {
    
    timerIdMousePointerSend = setTimeout(async () => {
      
      if(mousePointerInfo != undefined){
        
        // 前回値取得
        let oldMousePointer = undefined;
        let oldMousePointer_str = document.getElementById("hOldMousePointer").value;
        if(oldMousePointer_str == ""){
          oldMousePointer = {
            x : undefined,
            y : undefined,
            boardId : undefined,
            
          }
        } else {
          oldMousePointer = JSON.parse(oldMousePointer_str);
        }
        
        // 前回値と差があるときのみ送信する。(5秒に1回は変化が無くても送信する)
        if(mousePointerInfo.mousePointer.x != oldMousePointer.x ||
          mousePointerInfo.mousePointer.y != oldMousePointer.y ||
          mousePointerInfo.boardId != oldMousePointer.boardId ||
          mousePointerNoChangeCount > (5000 / timer)
        ){
          mouseCursorDataList.push(mousePointerInfo);
          mousePointerNoChangeCount = 0;
        } else {
          mousePointerNoChangeCount++;
        }
        
        if(mouseCursorTimerCount >= MOUSEPOINTER_SEND_TIMER_COUNT_MAX){
          
          if(changeObjectDataList.length > 0 ||
            lockObjectDataList.length > 0 ||
            mouseCursorDataList.length > 0
          ){
            
            let sendData = {
              kind : BROADCAST_KIND_MOUSECURSOR_SHARE,
              data : mouseCursorDataList,
              objectData : changeObjectDataList,
              lockObjectData : lockObjectDataList
            };
            
            sendDataBroadcast(sendData);
            mouseCursorDataList = [];
            changeObjectDataList = [];
            lockObjectDataList = [];
          }
          
          mouseCursorTimerCount = 0;
        }
        
        // 前回値更新
        document.getElementById("hOldMousePointer").value = JSON.stringify({
          boardId : mousePointerInfo.boardId,
          x : mousePointerInfo.mousePointer.x,
          y : mousePointerInfo.mousePointer.y
        });
        
        mouseCursorTimerCount++;
        
      }
      resolve();
      
    }, timer);
  })
}

// マウスカーソルの座標受信処理
function reciveMousePointer(data, objectData, lockObjectData){
  
  // マウスカーソル作成
  let data_len = data.length;
  if(data_len > 0){
    
    let mouseCursorCtrl = document.getElementById(`mouseCursor-${data[0].userCode}`);
    let color = "#E95856";
    
    if(mouseCursorCtrl == null){
      
      let boardCtrl =  document.getElementById(`${data[0].boardId}`);
      if(boardCtrl == null){
        return;
      }
      
      // マウスカーソルオブジェクト作成
      mouseCursorCtrl = document.createElement("div");
      mouseCursorCtrl.id = `mouseCursor-${data[0].userCode}`;
      mouseCursorCtrl.className = "mouseCursor-container";
      boardCtrl.appendChild(mouseCursorCtrl);
      let corsorSvg = `<svg id="mouseCursorSvg-${data[0].userCode}" class="mouseCursor-img" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">`
        + ` <path id="mouseCursorPath-${data[0].userCode}" d="M12.93,4.26l6.15,14.99a1,1,0,0,1-1.33,1.29l-5.34-2.36a1.035,1.035,0,0,0-.81,0L6.26,20.54a1,1,0,0,1-1.33-1.29L11.08,4.26A1,1,0,0,1,12.93,4.26Z" fill="#202020"/>`
        + `</svg>`;
      
      $('#' + mouseCursorCtrl.id).html(corsorSvg);
      color =　data[0].mouseCursorColor;
      
      let newLbl = document.createElement("label");
      newLbl.id = `mouseCursorLbl-${data[0].userCode}`;
      newLbl.className = "mouseCursor-lbl";
      newLbl.innerText = data[0].userName;
      newLbl.style["background-color"] = color;
      
      // 描画処理
      mouseCursorCtrl.appendChild(newLbl);
      
      // マウスカーソルの色変更
      let ctrl = document.getElementById(`mouseCursorPath-${data[0].userCode}`);
      if(ctrl != null){
        ctrl.style.fill = color;
      }
    } else {
      if(mouseCursorCtrl.parentElement.id != data[0].boardId){
        document.getElementById(`${data[0].boardId}`).appendChild(mouseCursorCtrl);
      }
    }
    
    // マウスカーソルの色とビデオ通話の帯の色に差異があったら合わせる処理
    if(data[0].peerid != ""){
      let userPlateCtrl = document.getElementById('video-thumbnail-user-plate-' + data[0].peerid);
      if(userPlateCtrl){
        let videoNameColor = hex2rgb(data[0].mouseCursorColor, 0.35);
        if(userPlateCtrl.style["background-color"].toUpperCase() != videoNameColor.toUpperCase()){
          userPlateCtrl.style["background-color"] = videoNameColor;
        }
      }
    }
    
    // ユーザ名(ニックネーム)が変更されたら反映します。
    let lbl = document.getElementById(`mouseCursorLbl-${data[0].userCode}`);
    if(lbl.innerText != data[0].userName){
      lbl.innerText = data[0].userName;
    }
    
    // 表示位置更新
    let offsetX = 0;
    let ctrl =  document.getElementById(`mouseCursor-${data[0].userCode}`);
    if(ctrl != null){
      offsetX = ctrl.clientWidth / 2;
    }
    setTimeout(async () => {
      for(let i = 0; i < data_len; i++){
        
        let zoomRange = document.getElementById("zoom-range-" + data[i].boardId);
        let scale = Number(zoomRange.value) / 100;
        $(`#mouseCursor-${data[0].userCode}`).animate({
          top : (data[i].mousePointer.y) * scale,
          left : (data[i].mousePointer.x  * scale) - offsetX
        }, {duration: MOUSEPOINTER_ANIMETION_INTERVAL, queue: false});
        
        await new Promise((resolve) => {
          setTimeout(() => {
            resolve();
          }, MOUSEPOINTER_ANIMETION_INTERVAL);
        });
      }
      
    }, 10);
  }
  
  // 編集中の時は変更内容をストックする
  if(contentsEditFlg == true || changeingCell != undefined){
    if(objectData.length > 0 ||
      lockObjectData.length > 0){
      
      let data = {
        objectData : [],
        lockObjectData : []
      }
      data.objectData = data.objectData.concat(objectData);
      data.lockObjectData = data.lockObjectData.concat(lockObjectData);
      
      reciveDataStock.push(data);
    }
    
    return;
  }
  
  setTimeout(() => {
    
    // ロック状態反映
    let lockObjectData_len = lockObjectData.length;
    if(lockObjectData_len > 0){
      for(let i = 0; i < lockObjectData_len; i++){
        reciveNoteObjectLock(lockObjectData[i]);
      }
    }
    
    // オブジェクト変更情報反映
    let objectData_len = objectData.length;
    if(objectData_len != undefined && objectData_len > 0){
      let boardIndex = getBoardsIndex(objectData[0].boardId);
      boards[boardIndex].graph.startBatch('add');
      for(let i = 0; i < objectData_len; i++){
        reciveNoteObject(objectData[i]);
      }
      boards[boardIndex].graph.stopBatch('add');
      
      // ロック状態を更新します。
      let lockDiv = document.getElementById("object-lock-" + objectData[0].userCode);
      let objectIds = [];
      if(objectData[0].operation == 1){
        for(let i = 0; i < objectData_len; i++){
          objectIds.push(objectData[i].objectData.objectId);
        }
      } else {
        if(lockDiv){
          objectIds = lockDiv.lockObjectid;
        } else {
          for(let i = 0; i < objectData_len; i++){
            objectIds.push(objectData[i].objectData.objectId);
          }
        }
      }
      // 不具合修正 -->
      // let username = document.getElementById(`mouseCursor-${objectData[0].userCode}`).innerText;
      // <-- 不具合修正
      
      let lockdata = {
        userCode: objectData[0].userCode,
        // 不具合修正 -->
        // userName: username,
        userName: objectData[0].userName,
        // <-- 不具合修正
        boardId: objectData[0].boardId,
        objectIds: objectIds,
        lock: true
      }
      reciveNoteObjectLock(lockdata);
    }
  }, 10);
}

// 線をDBに追加する処理
function addLinkObject(cell){
  
  let source = {};
  let target = {};
  
  // 線の位置情報取得
  // source
  if(cell.attributes.source.x != undefined
    && cell.attributes.source.y != undefined){
    
    source.x = cell.attributes.source.x;
    source.y = cell.attributes.source.y;
    
  } else if(cell.attributes.source.id != undefined){
    
    source.remark_id = getCellIDToObjectID(cell.attributes.source.id);
    if(cell.attributes.source.selector != undefined){
      source.selector = cell.attributes.source.selector;
    }
  }
  
  // target
  if(cell.attributes.target.x != undefined
    && cell.attributes.target.y != undefined){
    
    target.x = cell.attributes.target.x;
    target.y = cell.attributes.target.y;
    
  } else if(cell.attributes.target.id != undefined){
    
    target.remark_id = getCellIDToObjectID(cell.attributes.target.id);
    if(cell.attributes.target.selector != undefined){
      target.selector = cell.attributes.target.selector;
    }
  }
  
  
  // attrs/lineプロパティ
  let attrs_line = {};
  
  // 線情報
  let cellInfo = {
    type: cell.attributes.type,
    source : cell.attributes.source.x,
    target : cell.attributes.source.y,
    z : cell.attributes.z,
    attrs_line: cell.attributes.attrs.line,
    attrs_wrapper: cell.attributes.attrs.wrapper,
    connector_name: cell.attributes.connector.name,
  };
  
  let data = {
    data : cellInfo,
    boardId : activeBoardId,
    remark_id : undefined,
    kind: OBJECT_KIND_LINE
  };
  AddObject(data, cell);
  
}

// オブジェクトのロックON/OFFを通知
// function noteObjectLock(cell, lock){
function noteObjectLock(cells, lock, selectObjectId = undefined){
  let sendData = {
    data : {
      boardId : activeBoardId,
      // objectId : undefined,
      objectIds : [],
      userName : userInfo.response[0].name,
      userCode : userInfo.response[0].code,
      selectObjectId : selectObjectId,
      lock: lock // true:ロック　false:解除
    }
  };
  
  if(lock){
    // if(cell.attributes.attrs[".objectId"] == undefined){
    //     return;
    // }
    
    // sendData.data.objectId = cell.attributes.attrs[".objectId"].value;
    
    let len = cells.length;
    for(let i = 0; i < len; i++){
      
      if(cells[i].attributes.attrs[".objectId"] == undefined){
        continue;
      }
      
      sendData.data.objectIds.push(cells[i].attributes.attrs[".objectId"].value);
    }
  }
  
  lockObjectDataList.push(sendData.data);
  // sendDataBroadcast(sendData);
  
  if(lock == false){
    
    // グループ化解除
    if(old_cellview != undefined){
      let embedList = old_cellview.model.getEmbeddedCells();
      let len = embedList.length;
      for(let i = 0; i < len; i++){
        if(embedList[i]){
          old_cellview.model.unembed(embedList[i]);
        }
      }
      old_cellview = undefined;
    }
    
    // ストックしていた共有情報を反映します。
    while(reciveDataStock.length > 0){
      
      let stock = reciveDataStock[0];
      
      // ロック状態反映
      let lockObjectData_len = stock.lockObjectData.length;
      if(lockObjectData_len > 0){
        for(let n = 0; n < lockObjectData_len; n++){
          reciveNoteObjectLock(stock.lockObjectData[n]);
        }
      }
      
      // オブジェクト変更情報反映
      let objectData_len = stock.objectData.length;
      if(objectData_len > 0){
        // ストック反映時のロック表示修正 -->
        let boardIndex = getBoardsIndex(stock.objectData[0].boardId);
        if(boardIndex != -1){
          boards[boardIndex].graph.startBatch('add');
        }
        
        // <-- ストック反映時のロック表示修正
        
        for(let n = 0; n < objectData_len; n++){
          reciveNoteObject(stock.objectData[n]);
        }
        
        // ストック反映時のロック表示修正 -->
        if(boardIndex != -1){
          boards[boardIndex].graph.stopBatch('add');
        }
        
        // ロック状態を更新します。
        let lockDiv = document.getElementById("object-lock-" + stock.objectData[0].userCode);
        let objectIds = [];
        if(stock.objectData[0].operation == 1){
          for(let i = 0; i < objectData_len; i++){
            objectIds.push(stock.objectData[i].objectData.objectId);
          }
        } else {
          if(lockDiv){
            objectIds = lockDiv.lockObjectid;
          } else {
            for(let i = 0; i < objectData_len; i++){
              objectIds.push(stock.objectData[i].objectData.objectId);
            }
          }
        }
        // 不具合修正 -->
        // let username = document.getElementById(`mouseCursor-${stock.objectData[0].userCode}`).innerText;
        // <-- 不具合修正
        let lockdata = {
          userCode: stock.objectData[0].userCode,
          // 不具合修正 -->
          // userName: username,
          userName: stock.objectData[0].userName,
          // <-- 不具合修正
          boardId: stock.objectData[0].boardId,
          objectIds: objectIds,
          lock: true
        }
        reciveNoteObjectLock(lockdata);
        // <-- ストック反映時のロック表示修正
        
      }
      
      // 配列の先頭の要素を削除
      reciveDataStock.shift();
    }
    
    // 編集中フラグOFF
    contentsEditFlg = false;
    // ストックしていた共有情報を反映します。
    let stock_len = reciveDataStock.length;
    for(let i = 0; i < stock_len; i++){
      // ロック状態反映
      let lockObjectData_len = reciveDataStock[i].lockObjectData.length;
      if(lockObjectData_len > 0){
        for(let n = 0; n < lockObjectData_len; n++){
          reciveNoteObjectLock(reciveDataStock[i].lockObjectData[n]);
        }
      }
      
      // オブジェクト変更情報反映
      let objectData_len = reciveDataStock[i].objectData.length;
      if(objectData_len > 0){
        // ストック反映時のロック表示修正 -->
        let boardIndex = getBoardsIndex(reciveDataStock[i].objectData[0].boardId);
        boards[boardIndex].graph.startBatch('add');
        // <-- ストック反映時のロック表示修正
        for(let n = 0; n < objectData_len; n++){
          reciveNoteObject(reciveDataStock[i].objectData[n]);
        }
        // ストック反映時のロック表示修正 -->
        boards[boardIndex].graph.stopBatch('add');
        
        // ロック状態を更新します。
        let lockDiv = document.getElementById("object-lock-" + reciveDataStock[i].objectData[0].userCode);
        let objectIds = [];
        if(reciveDataStock[i].objectData[0].operation == 1){
          for(let i = 0; i < objectData_len; i++){
            objectIds.push(reciveDataStock[i].objectData[i].objectData.objectId);
          }
        } else {
          if(lockDiv){
            objectIds = lockDiv.lockObjectid;
          } else {
            for(let i = 0; i < objectData_len; i++){
              objectIds.push(reciveDataStock[i].objectData[i].objectData.objectId);
            }
          }
        }
        // 不具合修正 -->
        // let username = document.getElementById(`mouseCursor-${reciveDataStock[i].objectData[0].userCode}`).innerText;
        // <-- 不具合修正
        
        let lockdata = {
          userCode: reciveDataStock[i].objectData[0].userCode,
          // 不具合修正 -->
          // userName: username,
          userName: reciveDataStock[i].objectData[0].userName,
          // <-- 不具合修正
          boardId: reciveDataStock[i].objectData[0].boardId,
          objectIds: objectIds,
          lock: true
        }
        reciveNoteObjectLock(lockdata);
        // <-- ストック反映時のロック表示修正
      }
    }
    
    reciveDataStock = [];
  }
  
}

// 受信したロック情報からオブジェクトにロックかける処理
function reciveNoteObjectLock(data){
  
  let lockDiv = document.getElementById("object-lock-" + data.userCode);
  // ニックネーム対応 -->
  let mouseUserName = data.userName;
  let mouseCurCtrl = document.getElementById(`mouseCursor-${data.userCode}`);
  if (mouseCurCtrl && mouseCurCtrl.innerText) {
    mouseUserName = mouseCurCtrl.innerText;
  }
  // <-- ニックネーム対応
  
  if(lockDiv == null){
    lockDiv = document.createElement("div");
    lockDiv.id = "object-lock-" + data.userCode;
    lockDiv.className = "object-lock";
    
    let nameLbl = document.createElement("label");
    nameLbl.id = "object-lock-lbl-" + data.userCode;
    nameLbl.className = "object-lock-lbl";
    // ニックネーム対応 -->
    nameLbl.innerText = mouseUserName;
    // <-- ニックネーム対応
    lockDiv.appendChild(nameLbl);
    
  }
  
  if(data.lock){
    if(document.getElementById(data.boardId) == undefined){
      return;
    }
    document.getElementById(data.boardId).appendChild(lockDiv);
    // ニックネーム対応 -->
    lockDiv.children[0].innerText = mouseUserName;
    // <-- ニックネーム対応
    
    let boardIndex = getBoardsIndex(data.boardId);
    let lockDivInfo = undefined;
    
    let len = data.objectIds.length;
    let lockObjects = document.getElementsByClassName("object-lock");
    let lockObjects_len = lockObjects.length;
    let zoomRange = document.getElementById("zoom-range-" + boards[boardIndex].id);
    let scale = Number(zoomRange.value) / 100;
    for(let i = 0; i < len; i++){
      
      // 既にロックされているかチェック
      let nowLock = false;
      for(let n = 0; n < lockObjects_len; n++){
        
        if(lockObjects[n].lockObjectid){
          let lockObjectid_len = lockObjects[n].lockObjectid;
          for(let m = 0; m < lockObjectid_len; m++){
            
            if(data.userCode != lockObjects[n].id.replace("object-lock-", "") && data.objectIds[i] == lockObjects[n].lockObjectid[m]){
              nowLock = true;
              break;
            }
          }
        }
        if(nowLock){
          break;
        }
      }
      if(nowLock){
        continue;
      }
      
      
      let objIdx = boards[boardIndex].graph.attributes.cells.models.findIndex(a => {
        let res = false;
        if(a.attributes.attrs[".objectId"] != undefined){
          if(a.attributes.attrs[".objectId"].value == data.objectIds[i]){
            res = true;
          }
        }
        return res;
      });
      if(objIdx != -1){
        
        let cell = boards[boardIndex].graph.attributes.cells.models[objIdx];
        if(data.selectObjectId){
          if(data.objectIds[i] == data.selectObjectId){
            cell.toFront();
          }
        }
        
        let objPos = {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
        };
        
        if(cell.attributes.kind == OBJECT_KIND_LINE){
          
          let cellView =  boards[boardIndex].paper.findViewByModel(cell);
          
          // 不具合対応 -->
          // noteBoundary(cellView);
          if(cellView._toolsView == undefined){
            noteBoundary(cellView);
          }
          // <-- 不具合対応
          for(let n = 0; n < cellView._toolsView.tools.length; n++){
            
            if(cellView._toolsView.tools[n].tagName == "rect"){
              
              objPos.left = (cellView._toolsView.tools[n].el.x.baseVal.value) * scale;
              objPos.top = (cellView._toolsView.tools[n].el.y.baseVal.value) * scale;
              objPos.right = (cellView._toolsView.tools[n].el.width.baseVal.value + cellView._toolsView.tools[n].el.x.baseVal.value) * scale;
              objPos.bottom  = (cellView._toolsView.tools[n].el.height.baseVal.value + cellView._toolsView.tools[n].el.y.baseVal.value ) * scale;
            }
            
          }
          cellView.removeTools();
          
        } else {
          
          objPos.left = (cell.attributes.position.x) * scale;
          objPos.top = (cell.attributes.position.y) * scale;
          objPos.right = (cell.attributes.size.width + cell.attributes.position.x) * scale;
          objPos.bottom  = (cell.attributes.size.height + cell.attributes.position.y) * scale;
        }
        lockDiv.value = JSON.stringify({
          userCode: data.userCode,
          userName: data.userName,
          boardId: data.boardId,
          objectId: data.objectIds
        });
        
        if(lockDivInfo == undefined){
          lockDivInfo = {
            left: objPos.left,
            right: objPos.right,
            top: objPos.top,
            bottom: objPos.bottom,
          }
        } else {
          
          if(lockDivInfo.top >  objPos.top){
            lockDivInfo.top = objPos.top;
          }
          if(lockDivInfo.left >  objPos.left){
            lockDivInfo.left = objPos.left;
          }
          if(lockDivInfo.right <  objPos.right){
            lockDivInfo.right = objPos.right;
          }
          if(lockDivInfo.bottom <  objPos.bottom){
            lockDivInfo.bottom = objPos.bottom;
          }
        }
        
      }
    }
    
    if(lockDivInfo){
      lockDiv.style.display = "block";
      lockDiv.lockObjectid = data.objectIds;
      lockDiv.style.left = lockDivInfo.left - 5;
      lockDiv.style.top = lockDivInfo.top - 5;
      lockDiv.style.width = lockDivInfo.right - lockDivInfo.left + 10;
      lockDiv.style.height = lockDivInfo.bottom - lockDivInfo.top + 10;
      lockDiv.children[0].style.fontSize = 16 * scale;
    }
    
  } else {
    lockDiv.style.display = "none";
    lockDiv.lockObjectid = [];
  }
  
}

// 個人ボードエリアのボード切替
function setMyBoardDisplay(boardName){
  
  // 全て非表示
  let td = document.getElementById(ID_KOJIN_BOARDS);
  let td_len = td.children.length;
  for(let i = 0; i < td_len; i++){
    document.getElementById("yyKnowledgeBoard-hidden-boards").appendChild(td.children[i]);
  }
  
  // ボード表示
  let index = boards.findIndex(a => a.name == boardName);
  if(index != -1){
    let ctrl = document.getElementById(ID_KNOWLEDGEBOARD_FRAME + boards[index].id);
    ctrl.style.display = "";
    td.appendChild(ctrl);
  }
  
}

/* Add Y.Akasaka Team Icon Edit modal */
// チーム名変更ダイアログ表示
function popUpTeanNameEditShow(){
  
  if(userInfo.role == 3){
    return;
  }
  
  $.ajax({
    type: "GET",
    url: "/yyknowledgeboard/get-sub-room-team-info",
    contentType: false,
    success: async function (response) {
      
      if (response === -1) $('#error-system').modal('show');
      else {
        renderDataSubRoomIcon(response.response, response.listIcons);
      }
    }
  });
}

// チーム名、チームアイコンを更新
function updateTeanName(dataUpdateSubRoom,teamName,iconSrc){
  
  //commonDefine.setVisible('#yykb-loading', true);
  $.ajax({
    type: "POST",
    url: "/yyknowledgeboard/update-sub-room-team-info",
    data: JSON.stringify(dataUpdateSubRoom),
    processData: false,
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      
      //commonDefine.setVisible('#yykb-loading', false);
      
      if (response === -1) $('#error-system').modal('show');
      else {
        $('#teamIcon-img').attr('src',iconSrc);
        $('#teamName-lbl').text(teamName);
        $(".popup-edit-team-name").addClass("hide-it");
        $('.yyKnowledgeBoard-teamName .my-board-bar-button#btn-edit-team').removeClass('active');
        
        // Add Y.Akasaka 20201008 チーム名共有
        let sendData = {kind : 3, data : {meetingID : dataUpdateSubRoom.meeting_id, teamIconID : dataUpdateSubRoom.team_icon_id, teamName : dataUpdateSubRoom.team_name, iconPath : iconSrc}};
        sendDataBroadcast(sendData);
      }
    }
  });
}

function renderDataSubRoomIcon (response, listIcons) {
  
  let curMeetingId = response[0].meeting_id;
  let curTeamName = response[0].team_name;
  let curIconId = response[0].team_icon_id;
  
  let viewSubRoomIcon ="";
  viewSubRoomIcon += `
            <div class="popup-edit-team-name">
                <button class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <div class="team-content-edit">
                    <div class="list-team-icon">`;
  listIcons.forEach(val => {
    if(val.id == curIconId){
      viewSubRoomIcon += `
                                    <div class="team-icon-select team-icon-active" data-icon=${val.id}><img src="${val.icon_file_path}"></div>`
    }
    else{
      viewSubRoomIcon += `
                                    <div class="team-icon-select" data-icon=${val.id}><img src="${val.icon_file_path}"></div>`
    }
  });
  viewSubRoomIcon += `</div>
                    <div class="edit-sub-room-name"><input class="ipt-team-name-sub-room font-source-han-san custom-input ipt-team-name-sub-room-${curMeetingId}" type="text"></div>
                    <button class="button-primary btn-change-team-name font-source-han-san-bold" type="button" data-meeting = "${curMeetingId}">変更する</button>
                </div>
            </div>`
  $('#team-edit-modal').html(viewSubRoomIcon);
  
  $('.ipt-team-name-sub-room-' + curMeetingId).val(curTeamName);
  
  //add Start 20201016 YMD Requa問題点ID:290
  if(document.getElementById('boardSelect-main').style.display !="none"){
    var boardListWidth = $('#boardSelect-td').css('width');
    var beforeLeft = $('.popup-edit-team-name').css('left');
    $('.popup-edit-team-name').css('left', (parseInt((boardListWidth).replace("px", "")) + parseInt((beforeLeft).replace("px", "")) + "px"));
  }
  //add End 20201016
}

// チームアイコン更新ブロードキャスト受信
function reciveTeamChange(data){
  try{
    $('#teamIcon-img').attr('src',data.iconPath);
    $('#teamName-lbl').text(data.teamName);
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// 付箋テンプレート更新
function createStickyNoteTemplete(headerType, stickyType, isInit){
  
  try{
    // ボードの初期化処理
    document.getElementById('yyKnowledgeBoard-body').style.display = "block";
    joint.setTheme('modern');
    
    //-----------------
    // Create Base
    //-----------------
    let baseHtml = `<filter id="filter1"><feGaussianBlur stdDeviation="5" /></filter>
                        <g class="scalable" transform="scale(1,1)">
                        <rect class="shadow"/>
                        <rect class="card"/>
                        </g>`;
    
    if(stickyType != stickyNoteType.normal){
      baseHtml = `<filter id="filter1"><feGaussianBlur stdDeviation="5" /></filter>
                        <g class="scalable" transform="scale(1,1)">
                        <rect class="shadow"/>
                        <rect class="card"/>`;
    }
    
    //-----------------
    // Create Header
    //-----------------
    let headerHtml = ``;
    let foreignObjectStyle = `width="175px" height="175px"`;
    let contentDivStyle = ``;
    let fileNameMargin = 'margin-top: 145px;';
    let longTextSyle = "text-overflow: ellipsis; overflow: hidden; white-space: nowrap;"
    
    // header hidden
    if(headerType == stickyNoteHeaderType.headerHidden){
      // headerHtml = `<image/>
      //               <image class="background"/>
      //               <text class="name" style="margin-left: 50px; font-size: 12; font-family: ${Menber_font};"/>
      //               <text class="date" style="margin-left: 20px; font-size: 10; font-family: ${Menber_font};"/>
      //               <text class="meeting_id" style="font-size: 10; font-family: ${Menber_font};" />
      //               <text class="emotion"/>`;
      headerHtml = `<image/>
                          <image class="background"/>
                          <text class="name" style="margin-left: 50px; font-size: 13; font-family: ${Menber_font};"/>
                          <text class="date" style="margin-left: 20px; font-size: 12; font-family: ${Menber_font};"/>
                          <text class="emotion"/>`;
      contentDivStyle = 'margin-top: 36px; padding-left: 5px; height:95px';
      
      if(stickyType != stickyNoteType.normal){
        foreignObjectStyle += ` y="20"`;
      }
    }
    // icon only
    else if(headerType == stickyNoteHeaderType.headerIconOnly){
      headerHtml = `<image/>
                          <image class="background"/>`;
      contentDivStyle = 'margin-top: 36px; padding-left: 5px; width:165px; height:95px';
      
      if(stickyType != stickyNoteType.normal){
        foreignObjectStyle += ` y="20"`;
      }
    }
    // header visible
    else{
      if(stickyType == stickyNoteType.normal){
        foreignObjectStyle += ` y="17"`;
      }
      contentDivStyle = 'padding-left: 5px; width:175px; height:124px';
    }
    
    //-----------------
    // Create Content
    //-----------------
    // 付箋のプレースホルダ対応 -->
    // let contentHtml = `<foreignObject class="foreignObject custom-scrollbar-fusen" ${foreignObjectStyle} style ="overflow-y:auto; ">
    //                    <div contenteditable = "true" class="contents note-contents-cursor" xmlns="http://www.w3.org/1999/xhtml" onblur = "contents_onblur()" oninput="inputContent(event, this, ${isInit})" style = "resize: none; font-family: ${Menber_font}; background-color:transparent; border:none; ${contentDivStyle}; outline:none; font-size: 20px; padding-right: 5px"></div>
    //                    </foreignObject>`;
    let contentHtml = `<foreignObject class="foreignObject custom-scrollbar-fusen" ${foreignObjectStyle} style ="overflow-y:auto; ">
                            <div contenteditable = "true" data-text="1000文字まで入力出来ます。" class="contents note-contents-cursor" xmlns="http://www.w3.org/1999/xhtml" onblur = "contents_onblur()" oninput="inputContent(event, this, ${isInit})" style = "resize: none; font-family: ${Menber_font}; background-color:transparent; border:none; ${contentDivStyle}; outline:none; font-size: 20px; padding-right: 5px"></div>
                            </foreignObject>`;
    // <-- 付箋のプレースホルダ対応
    
    
    
    switch(stickyType){
      case stickyNoteType.normal:
        break;
      case stickyNoteType.image:
        contentHtml = `<image class="content-image"/>
                                <foreignObject class="foreignObject" ${foreignObjectStyle}>
                                    <div xmlns="http://www.w3.org/1999/xhtml" style="width: 175px; display: inline-block; text-align: center; ${fileNameMargin}">
                                        <p xmlns="http://www.w3.org/1999/xhtml" class="file-name" style="font-size: 14; word-wrap: break-word; ${longTextSyle}"><a class="download"></a></p>
                                    </div>
                                </foreignObject>`;
        break;
      case stickyNoteType.document:
        contentHtml = ` <image  class="content-image"/>
                                <foreignObject class="foreignObject" ${foreignObjectStyle}>
                                    <div xmlns="http://www.w3.org/1999/xhtml" style="width: 175px; display: inline-block; text-align: center; ${fileNameMargin}">
                                        <p xmlns="http://www.w3.org/1999/xhtml" class="file-name" style="font-size: 14; word-wrap: break-word; ${longTextSyle}"><a class="download"></a></p>
                                    </div>
                                </foreignObject>`;
        break;
      case stickyNoteType.sound:
        
        contentHtml = `<foreignObject class="foreignObject" ${foreignObjectStyle}>
                                <div xmlns="http://www.w3.org/1999/xhtml" style="width: 175px; height: 175px; display: inline-block; text-align: center;">
                                <audio class="content-audio" xmlns="http://www.w3.org/1999/xhtml" style="width:165px; margin-top:40px;"></audio>
                                    <div xmlns="http://www.w3.org/1999/xhtml" style="width: 165px; display: inline-block; text-align: center;">
                                        <p xmlns="http://www.w3.org/1999/xhtml" class="file-name" style="font-size: 14; word-wrap: break-word; ${longTextSyle}"><a class="download"></a></p>
                                    </div>
                                </div>
                                </foreignObject>`;
        break;
      case stickyNoteType.movie:
        
        contentHtml = `<foreignObject class="foreignObject" ${foreignObjectStyle}>
                                <div xmlns="http://www.w3.org/1999/xhtml" style="width: 175px; height: 175px; display: inline-block; text-align: center;">
                                    <video class="content-movie" xmlns="http://www.w3.org/1999/xhtml" style="margin-top:24px; z-index:0;"></video>
                                    <div xmlns="http://www.w3.org/1999/xhtml" style="width: 165px; display: inline-block; text-align: center;">
                                        <p xmlns="http://www.w3.org/1999/xhtml" class="file-name" style="font-size: 14; word-wrap: break-word; ${longTextSyle}"><a class="download"></a></p>
                                    </div>
                                </div>
                                </foreignObject>`;
        break;
      default:
        break;
    }
    
    //-----------------
    // Create Footer
    //-----------------
    // let footerHtml = `<g class="btn audioPlay"><circle class="audioPlay"/><text class="audioPlay" style = "cursor: hand; font-size: 18; font-family: Times New Roman"></text></g>`;
    let footerHtml = `<g class="btn audioPlay"><circle class="audioPlay"/><image class="audioPlay" style = "cursor: hand;"></image></g>`;
    
    
    if(stickyType != stickyNoteType.normal){
      // footerHtml = `</g><g class="btn audioPlay"><circle class="audioPlay"/><text class="audioPlay" style = "cursor: hand; font-size: 18; font-family: Times New Roman"></text></g>`;
      footerHtml = `</g>`;
    }
    
    let reactionHtml =
      `<foreignObject class="areaReaction">
                <div xmlns="http://www.w3.org/1999/xhtml" class="data-reaction"></div>
            </foreignObject>`
    
    stickyNoteHtml = baseHtml + headerHtml + contentHtml + footerHtml + reactionHtml;
    //joint.shapes.org.Member.prototype.markup = stickyNoteHtml;
    return stickyNoteHtml;
    
  } catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
    return "";
  }
}

// ファイル用の付箋追加
function AddMemberForFiles(x, y, z, id, name, date, srcfileName, srcfile, image, background, kind, meetingId, opacity, objectId, stickyType, downloadUrl, width, height, thumbnail_image) {
  
  if(width == undefined){
    width = CARD_SHADOW_WIDTH;
  }
  if(height == undefined){
    height = CARD_SHADOW_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT);
  }
  
  let textColor = "#000";
  let attribute = {
    '.shadow': { fill: '#000000', 'fill-opacity': 0.15, rx: 0, ry: 0, filter: "url(#filter1)", 'style':`width: ${CARD_SHADOW_WIDTH}px; height: ${CARD_SHADOW_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT)}px;`},
    '.card': {fill: background, stroke: 'none', 'stroke-width': 3, 'style':`width: ${CARD_WIDTH}px; height: ${(CARD_HEIGHT + (CARD_WORD_HEIGHT * CARD_OMISSION_NEWLINE_LIMIT))}px;`, opacity:opacity,  'pointer-events': 'visiblePainted', ref: '.shadow', rx: 0, ry: 0},
    image: {'xlink:href':  image, ref: '.card',  opacity: opacity, width: "28px", height:"28px", 'ref-x': 4, 'ref-y': 4},
    '.id' : { ref: '.card', text: "ID:" + id, fill: textColor, 'x':240, 'y':30, 'text-anchor': 'end', opacity:opacity},
    '.objectId' : {value : objectId},
    '.contents': {id:"contents-"+ id},
    '.meeting_id' : { text: meetingId, fill: textColor, 'text-anchor': 'end', 'font-family': Menber_font, 'font-size': '11px', 'x':155, 'y':15, opacity:opacity},
    '.name': { "font-weight":"0", ref:".card","ref-x":0,"ref-y":0,"text-anchor":"start", text: name, fill: textColor, 'x': 31, 'y':36, opacity:opacity},
    '.date': { ref: '.card', text: date, fill: textColor, 'x': 31, 'y': 15, 'letter-spacing': 0, opacity:opacity},
    '.foreignObject':{width:175, height:175},
    '.file-name':{id:"file-name-"+ id},
    '.areaReaction': { id: 'area-reaction-contents-' + id, height: 35 },
    '.data-reaction': {id: 'reaction-contents-' + id, 'data-remark': id}
    
  }
  
  if(stickyType == stickyNoteType.image){
    attribute['.content-image']={'xlink:href': thumbnail_image, width:"165px", height:"140px", y:5, transform:"matrix(1,0,0,1,0,0)"};
    if(note_header_type != stickyNoteHeaderType.headerVisible){
      attribute['.content-image']={'xlink:href': thumbnail_image, width:"165px", height:"120px", y:36, transform:"matrix(1,0,0,1,0,0)"};
    }
  }
  else if(stickyType == stickyNoteType.document){
    attribute['.content-image']={'xlink:href': srcfile, width:"100px", height:"100px", x:32, y:20, transform:"matrix(1,0,0,1,0,0)"};
    if(note_header_type != stickyNoteHeaderType.headerVisible){
      attribute['.content-image']={'xlink:href': srcfile, width:"100px", height:"100px", x:32, y:36, transform:"matrix(1,0,0,1,0,0)"};
    }
  }
  else if(stickyType == stickyNoteType.sound){
    attribute['.content-audio']={src: srcfile, controls:"true"};
  }
  else if(stickyType == stickyNoteType.movie){
    attribute['.content-movie']={src: srcfile, controls:"true", width:"160px"};
  }
  attribute['.download']={id:"download-"+ id, href: downloadUrl, download: srcfileName};
  
  // 付箋を作成
  let cell = new joint.shapes.org.Member({
    position: { x: x, y: y },
    z: z,
    size : {width : width, height: height},
    kind: kind,
    remark_type : stickyType,
    file_info : {file_name:srcfileName ,src:srcfile, url:downloadUrl},
    attrs: attribute
  });
  
  return cell;
};


function handleBoardDragOver(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy';
}

function handleBoardDrop(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  activeBoardId = evt.currentTarget.id;
  // マウスカーソルが復帰しないバグ対応 -->
  figureFlag = FIGURE_MENU_BAR_UNSELECTED;
  releaseSelectToolbar();
  $('.joint-toolbar.joint-theme-modern button').removeClass('active bg-btn-active');
  changeCursor();
  // <-- マウスカーソルが復帰しないバグ対応
  
  // ボードの座標に変換する
  let zoomRange = document.getElementById("zoom-range-" + activeBoardId);
  let scale = Number(zoomRange.value) / 100;
  addStickyFiles(evt.dataTransfer.files, evt.layerX / scale, evt.layerY / scale);
  
  // * nakamatsu
  if (analysisCount.isAnalysisCountDragEvent(evt)) {
    analysisCount.createStickyContent(evt, scale);
  }
  // nakamatsu *
}

async function addStickyFiles(files,posX,posY){
  
  try{
    // ファイルのエラーチェック
    if(checkUploadFiles(files) == false){
      return;
    }
    
    for (var i = 0; i < files.length; i++) {
      
      let remarkType = getRemarkType(files[i].name);
      if( remarkType == -1){
        // ※※※　後でメッセージダイアログを記載する
        alert("付箋の追加に失敗しました。");
        return;
      }
      
      let fd = new FormData( );
      fd.append('upFile', files[i]);
      fd.append('user_code', userInfo.response[0].code);
      fd.append('remark_type', remarkType);
      
      // 画像のサムネイルを作成
      let thumbnail_image_base64 = "";
      if(remarkType == stickyNoteType.image){
        thumbnail_image_base64 = await readImageFileForBase64(files[i]);
      }
      
      let sticky_note_color = GetStickyColor();
      
      let param = {
        x: posX,
        y: posY,
        remarkType: remarkType,
        sticky_note_color: sticky_note_color,
        thumbnail_image: thumbnail_image_base64,
        fileName: files[i].name
      }
      
      let res = await createNoteForFile(fd, param);
      AddObject(res.sendData, res.cell);
    }
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// 拡張子を取得する
function getExtention(filename){
  var pos = filename.lastIndexOf('.');
  if (pos === -1) return '';
  return filename.slice(pos + 1);
}

// アップロード可能かどうかをチェック
function checkUploadFiles(files){
  
  if(files){
    if(files.length > 1){
      alert(`複数同時にファイルの追加は出来ません。`);
      return false;
    }
  }
  
  let VALID_EXTENTIONS = DOCUMENT_EXTENTIONS.ALL.concat(VALID_MOVIE_EXTENTIONS).concat(VALID_SOUND_EXTENTIONS).concat(VALID_IMAGE_EXTENTIONS);
  let totalFileSize = 0;
  
  for (var i = 0; i < files.length; i++) {
    
    // 拡張子を取得
    let extention = getExtention(files[i].name);
    if(extention == ''){
      // ※※※　後でメッセージダイアログを記載する
      alert(`拡張子のついたファイルを指定してください。:[${files[i].name}]`);
      return false;
    }
    
    // 拡張子チェック
    if (!VALID_EXTENTIONS.includes(extention.toLowerCase())) {
      // ※※※　後でメッセージダイアログを記載する
      alert(`取り扱えないファイルが含まれています。:[${files[i].name}]`);
      return false;
    }
    
    // サイズチェック
    totalFileSize += files[i].size;
    if (totalFileSize > MAX_UPLOAD_FILE_SIZE) {
      // ※※※　後でメッセージダイアログを記載する
      let mb = MAX_UPLOAD_FILE_SIZE / MEGA_BYTE;
      alert(`読み込めるファイルサイズを超えました。${mb}MB以内で指定してください。`);
      return false;
    }
  }
  return true;
}

// アップロードするファイルの発言種別を取得
function getRemarkType(fileName){
  try{
    if(fileName == null || fileName == undefined || fileName == ''){
      return stickyNoteType.normal;
    }
    
    let extention = getExtention(fileName);
    
    if (DOCUMENT_EXTENTIONS.ALL.includes(extention.toLowerCase())) {
      return stickyNoteType.document;
    }
    else if(VALID_MOVIE_EXTENTIONS.includes(extention.toLowerCase())){
      return stickyNoteType.movie;
    }
    else if(VALID_SOUND_EXTENTIONS.includes(extention.toLowerCase())){
      return stickyNoteType.sound;
    }
    else if(VALID_IMAGE_EXTENTIONS.includes(extention.toLowerCase())){
      
      return stickyNoteType.image;
    }
    return -1;
  }
  catch(err) {
    return -1;
  }
}

// アップロードしたファイルがドキュメントの場合、ドキュメントのアイコンを取得する
function getDocumentFileIconPath(fileName){
  
  try{
    
    let extention = getExtention(fileName);
    
    if (!DOCUMENT_EXTENTIONS.ALL.includes(extention.toLowerCase())) {
      return '';
    };
    
    // doc
    if (DOCUMENT_EXTENTIONS.WORD.includes(extention.toLowerCase())) {
      return 'images/files_icon/doc.png';
    }
    // xls
    else if(DOCUMENT_EXTENTIONS.EXCEL.includes(extention.toLowerCase())){
      return 'images/files_icon/xls.png';
    }
    // ppt
    else if(DOCUMENT_EXTENTIONS.POWERPOINT.includes(extention.toLowerCase())){
      return 'images/files_icon/ppt.png';
    }
    // pdf
    else if(DOCUMENT_EXTENTIONS.PDF.includes(extention.toLowerCase())){
      return 'images/files_icon/pdf.png';
    }
    
    return ''
  }
  catch(err) {
    return '';
  }
}

// ユーザーアイコン取得
function GetUserIconStr()
{
  try{
    let image_str = "";
    let canvasInfo = {
      // 画像のサイズ
      // 幅
      WIDTH: 60,
      // 高さ
      HEIGHT: 60,
      // 背景色
      BACK_COLOR: "",
      // 文字色
      FORE_COLOR: "",
      // 文字サイズ(px)
      FONT_SIZE: 44,
      // フォント
      FONT: 'sans-serif',
      // ベースラインの位置ぞろえ
      TEXT_BASE_LINE: 'hanging'
    }
    
    if(userInfo.response[0].code_color == "" || userInfo.response[0].code_color == undefined || userInfo.response[0].code_color == null){
      canvasInfo.BACK_COLOR = DEFAULT_FACE_IMAGE_BACKCOLOR;
      canvasInfo.FORE_COLOR = DEFAULT_FACE_IMAGE_FONTCOLOR;
      
    } else {
      canvasInfo.BACK_COLOR = userInfo.response[0].code_color;
      canvasInfo.FORE_COLOR = fontColorBlackorWhite(userInfo.response[0].code_color);
    }
    
    // 顔画像作成
    return  createImageFromText(canvasInfo, userInfo.response[0].name.substr(0, 1));
  }
  catch(err) {
    return '';
  }
}

// 自分の付箋の色を取得する
function GetStickyColor()
{
  let sticky_note_color = "#FFF6D5";
  try{
    let idx = participantsData.findIndex(a => a.code == userInfo.response[0].code);
    if(idx != -1){
      sticky_note_color = participantsData[idx]["sticky_note_color"];
    }
    if(sticky_note_color == null || sticky_note_color == undefined){
      sticky_note_color = "#FFF6D5";
    }
  }
  catch(err) {}
  return sticky_note_color;
}

// ボード一覧のサムネイル画像を更新します。
async function setBoardthumbnail(boardId, endFnc, isTimer){
  
  let boardIndex = getBoardsIndex(boardId);
  // setTimeout(() => {
  
  // スクロール位置保持
  let dispBoardId = activeBoardId;
  let boardTd = document.getElementById("yyKnowledgeBoard-td1");
  if(boardTd.children.length > 0){
    dispBoardId = boardTd.children[0].id.replace(ID_KNOWLEDGEBOARD_FRAME, "");
  }
  
  let frameCtrl = document.getElementById(ID_PAPER_SCROLLER + dispBoardId);
  let scrollX = frameCtrl.scrollLeft;
  let scrollY = frameCtrl.scrollTop;
  
  // 個人ボード
  let kojinBoardId =　getKojinBoardId();
  let kojinFrameCtrl = document.getElementById(ID_PAPER_SCROLLER + kojinBoardId);
  let kojinScrollX = kojinFrameCtrl.scrollLeft;
  let kojinScrollY = kojinFrameCtrl.scrollTop;
  
  let templateImage = undefined;
  let bordListIndex = getActiveBoardListInfoIndex(boardId);
  let templateUrl = boardListInfo[bordListIndex].background_image_base64;
  
  if(templateUrl != ""){
    templateImage = new joint.shapes.standard.Image();
    templateImage.resize(TEMPLATE_WIDTH, TEMPLATE_HEIGHT);
    // templateImage.position(BOARD_WIDTH * 0.00535, BOARD_HEIGHT * 0.0083);
    templateImage.position(BOARD_WIDTH * 0.3876, BOARD_HEIGHT * 0.4484);
    templateImage.attributes.z = -1;
    templateImage.attributes.kind = "template";
    templateImage.attr('image/xlink:href', templateUrl);
    templateImage.set('stopDelegation', false);
    templateImage.set('elementMove', false);
    templateImage.addTo(boards[boardIndex].graph);
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  }
  boards[boardIndex].paper.toPNG(function(dataURL) {
    
    // frameCtrl.scrollLeft = scrollX;
    // frameCtrl.scrollTop = scrollY;
    
    // 画像保存用に追加したテンプレート消す
    if(templateImage){
      let cellIndex = boards[boardIndex].graph.attributes.cells.models.findIndex(a => a.attributes.kind == "template");
      let cellView = boards[boardIndex].paper.findViewByModel(boards[boardIndex].graph.attributes.cells.models[cellIndex]);
      cellView.model.remove();
    }
    
    let thumImg = document.getElementById("thumbnail-" + boardId + "-img");
    if(thumImg == null){
      return;
    }
    
    thumImg.onload = function() {
      
      let resizeFlg = false;
      let rw = thumImg.naturalWidth;
      let rh = thumImg.naturalHeight;
      if(thumImg.naturalWidth > BOARD_THUMBNAIL_RESIZE_MAX_WIDTH){
        resizeFlg = true;
        let scale = 0;
        scale = (BOARD_THUMBNAIL_RESIZE_MAX_WIDTH / thumImg.naturalWidth);
        rw = thumImg.naturalWidth * scale;
        rh = thumImg.naturalHeight * scale;
        
        if(rh > BOARD_THUMBNAIL_RESIZE_MAX_HEIGHT){
          scale = (BOARD_THUMBNAIL_RESIZE_MAX_HEIGHT / rh);
          rw = rw * scale;
          rh = rh * scale;
        }
        
      } else if(thumImg.naturalHeight > BOARD_THUMBNAIL_RESIZE_MAX_HEIGHT){
        resizeFlg = true;
        let scale = 0;
        scale = (BOARD_THUMBNAIL_RESIZE_MAX_HEIGHT / thumImg.naturalHeight);
        rw = thumImg.naturalWidth * scale;
        rh = thumImg.naturalHeight * scale;
      }
      
      let sendThumbnailData = function(resize_img_b64){
        
        let thumbnailData = "";
        if(resize_img_b64){
          thumbnailData = resize_img_b64;
        } else {
          thumbnailData = dataURL;
        }
        
        let sendData = {
          kind : BROADCAST_KIND_BOARD_THUMBNAIL_CHANGE,
          data : {
            imgData: thumbnailData,
            meetingID: selectMeeting,
            boardID: boards[boardIndex].id
          },
        };
        sendDataBroadcast(sendData);
        
        // if(boardThumbnailDBUpdateTimeCount >= ((BOARD_THUMBNAIL_DB_UPDATE_TIME * 1000) / (BOARD_THUMBNAIL_UPDATE_TIME * 1000)) - 1){
        // if(boardThumbnailDBUpdateTimeCount >= (BOARD_THUMBNAIL_DB_UPDATE_TIME * 1000) / (BOARD_THUMBNAIL_UPDATE_TIME * 1000)){
        let divThumbnailLoading = document.getElementById(`${boardId}-loading`);
        divThumbnailLoading.classList.add('board-loading');
        
        let data = {
          boardId: boardId,
          screenImageData: dataURL,
          thumbnailData: thumbnailData
        };
        
        $.ajax({
          type: "POST",
          url: '/yyknowledgeboard/upload-board-thumbnail',
          data: JSON.stringify(data),
          processData: false,
          contentType: "application/json; charset=utf-8",
          success: function (response) {
            divThumbnailLoading.classList.remove('board-loading');
          }
        });
        
        // boardThumbnailDBUpdateTimeCount = 0;
        // }
        
        // boardThumbnailDBUpdateTimeCount++;
        
        if(endFnc){
          endFnc();
        }
      };
      
      if(resizeFlg){
        resizeBase64Img(dataURL, rw, rh, sendThumbnailData);
      } else {
        
        sendThumbnailData();
      }
      
    };
    
    thumImg.src = dataURL;
  });
  frameCtrl.scrollLeft = scrollX;
  frameCtrl.scrollTop = scrollY;
  kojinFrameCtrl.scrollLeft = kojinScrollX;
  kojinFrameCtrl.scrollTop = kojinScrollY;
  
  
  // }, 100);
  
  
}

// base64画像のリサイズ
function resizeBase64Img(imgB64_src, width, height, callback) {
  
  let img_type = imgB64_src.substring(5, imgB64_src.indexOf(";"));
  let img = new Image();
  img.onload = function() {
    let canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    let ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, width, height);
    let imgB64_dst = canvas.toDataURL(img_type);
    canvas.remove();
    callback(imgB64_dst);
  };
  img.src = imgB64_src;
}


// ボードのサムネイル画像更新タイマー制御処理
// async function boardThumbnailTimerEnable(enable) {
//
//     let chk = document.getElementById("hBoardThumbnailTimerEnable");
//     chk.value = enable;
//     if (chk.value == 'true') {
//
//         if(timerIdBoardThumbnail == undefined){
//             while (chk.value) {
//                 await boardThumbnailTimer(BOARD_THUMBNAIL_UPDATE_TIME * 1000, activeBoardId);
//             }
//         }
//
//     } else {
//
//         if (timerIdBoardThumbnail) {
//             clearTimeout(timerIdBoardThumbnail);
//         }
//         timerIdBoardThumbnail = undefined;
//     }
// }


// ボードのサムネイル画像更新タイマー処理
// function boardThumbnailTimer(timer, boardId, endFnc) {
//
//     return new Promise((resolve) => {
//
//         timerIdBoardThumbnail = setTimeout(async () => {
//
//             let ctrl = document.getElementById(ID_KNOWLEDGEBOARD_FRAME + boardId);
//
//             if(ctrl.style.display == "none"){
//
//                 if(ctrl.parentElement.id == "yyKnowledgeBoard-hidden-boards"){
//                     ctrl.parentElement.style.display = "";
//                     ctrl.style.display = "";
//                     setBoardthumbnail(boardId, endFnc);
//                     ctrl.style.display = "none"
//                     // ctrl.parentElement.style.display = "none";
//                 } else {
//                     ctrl.style.display = "";
//                     setBoardthumbnail(boardId, endFnc);
//                     ctrl.style.display = "none"
//                 }
//
//             } else {
//                 setBoardthumbnail(boardId, endFnc);
//             }
//
//             resolve();
//
//         }, timer);
//     });
// }

// ブラウザのエラーイベント
window.onerror = (message, file, lineNo, colNo, err) => {
  writeYYKnowledgeBoardErrorLog(err);
};

window.onpointerup = async function(e){
  if (!$('.download-area').hasClass('hide-it')) {
    $('.download-area').addClass('hide-it');
    $('.item-download').removeClass('active');
  }
  
  // nakamatsu 未実装メニューの押下不可対応 -->
  // $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').removeClass('bg-btn-active');
  // <-- nakamatsu 未実装メニューの押下不可対応
  // $('.toolbar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').removeClass('bg-btn-active');
  
  if(document.getElementById('table-split-bar')){
    document.getElementById('table-split-bar').classList.remove("split-drag");
  }
  
  // ダブルクリック時に先に処理されてしまうため一瞬wait入れる。
  await new Promise((resolve) => {
    setTimeout(() => resolve(), 10);
  });
  
  let boardIndex = getBoardsIndex(activeBoardId);
  if (boardIndex !== -1) {
    let listCell = getBoundaryCellList(boardIndex, CELL_TYPE_NOTE);
    if (listCell.length > 0) {
      jsonSave(listCell);
    }
  }
  
  if(changeingCell){
    let kind = "";
    if(changeingCell.attributes.type == CELL_TYPE_STANDARD_LINK){
      kind = USER_OPERATION_LINK_MOVE;
      // 線オブジェクトの最小値設定対応 -->
      // サイズチェック
      let result = calcStandardLinkSize(changeingCell);
      if (result.isChange) {
        changeingCell = result.cell;
        let linkView = boards[boardIndex].paper.findViewByModel(changeingCell);
        linkView.update();
      }
      // <-- 線オブジェクトの最小値設定対応
    } else {
      kind = USER_OPERATION_MOVE;
    }
    
    // 領域外チェック
    checkObjectOutBoard(changeingCell);
    
    addUserOperationLogs({
      cell: changeingCell,
      selectObjectId: changeingCell.attributes.attrs[".objectId"].value,
      kind: kind
    });
    
    let cv = boards[boardIndex].paper.findViewByModel(changeingCell);
    selectCell = changeingCell;
    if(cv.model.attributes.type != CELL_TYPE_STANDARD_LINK){
      // ツールバー表示
      setMenuBarPosition(cv);
    } else {
      // 他のツールバーが非表示の時のみ表示(ペースト時にダブるため)
      if(document.getElementById(ID_MULTI_SELECT_MENUBAR + activeBoardId).style.display == "none" &&
        document.getElementById(ID_SHAPE_SUB_MENUBAR + activeBoardId).style.display == "none" &&
        document.getElementById(ID_TEXT_MENUBAR + activeBoardId).style.display == "none" &&
        document.getElementById(ID_FUSEN_SUB_MENUBAR + activeBoardId).style.display == "none" ){
        toolsViewDisp(cv);
      }
    }
    
    setTimeout(() => {
      
      // ストックしていた共有情報を反映します。
      let stock_len = reciveDataStock.length;
      for(let i = 0; i < stock_len; i++){
        // ロック状態反映
        let lockObjectData_len = reciveDataStock[i].lockObjectData.length;
        if(lockObjectData_len > 0){
          for(let n = 0; n < lockObjectData_len; n++){
            reciveNoteObjectLock(reciveDataStock[i].lockObjectData[n]);
          }
        }
        
        // オブジェクト変更情報反映
        let objectData_len = reciveDataStock[i].objectData.length;
        if(objectData_len > 0){
          for(let n = 0; n < objectData_len; n++){
            reciveNoteObject(reciveDataStock[i].objectData[n]);
          }
        }
      }
      reciveDataStock = [];
      if(changeingCell){
        delete changeingCell.attributes.isAllMove;
        changeingCell = undefined;
      }
    }, 100);
  }
};

// 領域外チェック
function checkObjectOutBoard(cell){
  
  const offset = 10;
  let isOut = false;
  
  // 線
  if(cell.isLink()){
    
    // 個別移動時
    if(cell.attributes.isAllMove == undefined){
      
      // source
      if(cell.attributes.source.x != undefined && cell.attributes.source.y != undefined){
        let isChange = false;
        let source = Object.assign({}, cell.attributes.source);
        
        if(cell.attributes.source.x < 0){
          source.x = offset;
          isChange = true;
        } else if(cell.attributes.source.x > BOARD_WIDTH){
          source.x = BOARD_WIDTH - offset;
          isChange = true;
        }
        
        if(cell.attributes.source.y < 0){
          source.y = offset;
          isChange = true;
        } else if(cell.attributes.source.y > BOARD_HEIGHT){
          source.y = BOARD_HEIGHT - offset;
          isChange = true;
        }
        
        if(isChange){
          cell.source(source);
        }
      }
      
      // target
      if(cell.attributes.target.x != undefined && cell.attributes.target.y != undefined){
        let isChange = false;
        let target = Object.assign({}, cell.attributes.target);
        
        if(cell.attributes.target.x < 0){
          target.x = offset;
          isChange = true;
        } else if(cell.attributes.target.x > BOARD_WIDTH){
          target.x = BOARD_WIDTH - offset;
          isChange = true;
        }
        
        if(cell.attributes.target.y < 0){
          target.y = offset;
          isChange = true;
        } else if(cell.attributes.target.y > BOARD_HEIGHT){
          target.y = BOARD_HEIGHT - offset;
          isChange = true;
        }
        
        if(isChange){
          cell.target(target);
        }
      }
      
      // vertices
      if(cell.attributes.vertices != undefined){
        let isChange = false;
        let len = cell.attributes.vertices.length;
        let vertices = [];
        for(let i = 0; i < len; i++){
          let pos = Object.assign({}, cell.attributes.vertices[i]);
          
          if(pos.x < 0){
            pos.x = offset;
            isChange = true;
          } else if(pos.x > BOARD_WIDTH){
            pos.x = BOARD_WIDTH - offset;
            isChange = true;
          }
          if(pos.y < 0){
            pos.y = offset;
            isChange = true;
          } else if(pos.y > BOARD_HEIGHT){
            pos.y = BOARD_HEIGHT - offset;
            isChange = true;
          }
          vertices.push(pos);
        }
        
        if(isChange){
          cell.vertices(vertices);
        }
      }
    }
    
    // 緑枠超えてないかチェック
    let boardIndex = getBoardsIndex(activeBoardId);
    let cellView = boards[boardIndex].paper.findViewByModel(cell);
    let bbox = cellView.getBBox();
    let scale = boards[boardIndex].paperScroller.zoom();
    bbox.width = (bbox.width / scale);
    bbox.height = (bbox.height / scale);
    bbox.x = (bbox.x / scale);
    bbox.y = (bbox.y / scale);
    
    let overValue = {
      x: 0,
      y: 0
    };
    if(bbox.x + bbox.width > BOARD_WIDTH){
      overValue.x = bbox.x + bbox.width - (BOARD_WIDTH - offset);
      isOut = true;
    } else if(bbox.x < 0 ){
      overValue.x = bbox.x - offset;
      isOut = true;
    }
    
    if(bbox.y + bbox.height > BOARD_HEIGHT){
      overValue.y = bbox.y + bbox.height - (BOARD_HEIGHT - offset);
      isOut = true;
    } else if(bbox.y < 0 ){
      overValue.y = bbox.y - offset;
      isOut = true;
    }
    
    if(cell.attributes.source.x != undefined && cell.attributes.source.y != undefined){
      cell.attributes.source.x -= overValue.x;
      cell.attributes.source.y -= overValue.y;
    }
    if(cell.attributes.target.x != undefined && cell.attributes.target.y != undefined){
      cell.attributes.target.x -= overValue.x;
      cell.attributes.target.y -= overValue.y;
    }
    
    // vertices
    if(cell.attributes.vertices != undefined){
      let len = cell.attributes.vertices.length;
      for(let i = 0; i < len; i++){
        cell.attributes.vertices[i].x -= overValue.x;
        cell.attributes.vertices[i].y -= overValue.y;
      }
    }
    
    if(isOut){
      cellView.update();
    }
    
  } else {
    
    // 線以外
    let position = Object.assign({}, cell.attributes.position);
    let size = Object.assign({}, cell.attributes.size);
    let changeRes = {
      x: 0,
      y: 0,
      width: size.width,
      height: size.height,
    }
    
    if(position.x + size.width > BOARD_WIDTH){
      changeRes.width = BOARD_WIDTH - position.x;
      isOut = true;
    } else if(position.x < 0) {
      // cell.attributes.position.x = 0;
      changeRes.x = -cell.attributes.position.x;
      changeRes.width = size.width - changeRes.x;
      isOut = true;
    }
    
    if(position.y + size.height > BOARD_HEIGHT){
      changeRes.height = BOARD_HEIGHT - position.y;
      isOut = true;
    } else if(position.y < 0) {
      // cell.attributes.position.y = 0;
      changeRes.y = -cell.attributes.position.y;
      changeRes.height = size.height - changeRes.y;
      isOut = true;
    }
    
    if(isOut){
      cell.translate(changeRes.x, changeRes.y);
      cell.resize(changeRes.width, changeRes.height);
    }
  }
}

// オブジェクトクリック時のメニューバーの表示処理
function setMenuBarPosition(cellView, multiSelect = false){
  
  // let graphIndex = getBoardsIndex(activeBoardId);
  let graphIndex = boards.findIndex(a => a.graph.cid == cellView.model.graph.cid);
  let zoomRange = document.getElementById("zoom-range-" + boards[graphIndex].id);
  let scale = Number(zoomRange.value) / 100;
  let offsetY = 60;
  if(scale > 1){
    offsetY += (scale - 1) * 15;
  }
  
  //add Start 20201016 Requa問題点ID：284
  //let positionY = (cellView.model.attributes.position.y) * scale - offsetY;
  let positionY = (cellView.model.attributes.position.y) * scale - offsetY - TOOLBAR_Y_OFFSET;
  if( positionY < 0){
    //    positionY = (cellView.model.attributes.position.y) * scale + (cellView.model.attributes.size.height) + 15;
    // positionY = (cellView.model.attributes.position.y) * scale + (cellView.model.attributes.size.height) + 15 + TOOLBAR_Y_OFFSET;
    positionY = (cellView.model.attributes.position.y + cellView.model.attributes.size.height) * scale + TOOLBAR_Y_OFFSET;
  }else{
    //nothing
  }
  //add End 20201016
  
  let selectCells = getBoundaryCellList(graphIndex, "all");
  if(selectCells.length > 1 || multiSelect){
    
    document.getElementById(ID_MULTI_SELECT_MENUBAR + boards[graphIndex].id).style.display = "";
    document.getElementById(ID_MULTI_SELECT_MENUBAR + boards[graphIndex].id).style.left = (cellView.model.attributes.position.x) * scale;
    document.getElementById(ID_MULTI_SELECT_MENUBAR + boards[graphIndex].id).style.top = positionY;
    
  } else if(cellView.model.attributes.type == CELL_TYPE_PATH || cellView.model.attributes.type == CELL_TYPE_CIRCLE){
    document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.display = "";
    
    document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.left = (cellView.model.attributes.position.x) * scale;
    //change Start 20201016 Requa問題点ID：284
    //document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.top = (cellView.model.attributes.position.y) * scale - offsetY;
    document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.top = positionY;
    //change End 20201016
  } else if(cellView.model.attributes.type == CELL_TYPE_RECT){
    
    if(cellView.model.attributes.attrs.label.text != undefined){
      
      let toolbar = document.getElementById(ID_TEXT_MENUBAR + boards[graphIndex].id);
      toolbar.style.display = "";
      
      let posX = (cellView.model.attributes.position.x) * scale;
      if(boards[graphIndex].paper.options.width < posX + toolbar.clientWidth){
        posX = boards[graphIndex].paper.options.width - toolbar.clientWidth;
      }
      
      // document.getElementById(ID_TEXT_MENUBAR + boards[graphIndex].id).style.left = (cellView.model.attributes.position.x) * scale;
      toolbar.style.left = posX;
      //change Start 20201016 Requa問題点ID：284
      //document.getElementById(ID_TEXT_MENUBAR + boards[graphIndex].id).style.top =  (cellView.model.attributes.position.y) * scale - offsetY;
      toolbar.style.top = positionY;
      //change End 20201016
      //add Start 20201020　ツールバーのテキストエリアにテキストコンポーネントの値を表示
      var textMenuChildrenList = toolbar.children[0].children;
      //矢印のメニューと見た目調整　Start
      if(cellView.model.attributes.attrs.label.text == ""){
        cellView.model.attributes.attrs.label.text = " ";
        cellView.selectors.label.textContent = " ";
      }
      
      textMenuChildrenList[1].children[0].childNodes[1].firstElementChild.value = cellView.model.attributes.attrs.label.text;
      //textMenuChildrenList[0].children[0].childNodes[1].firstElementChild.value = cellView.selectors.label.textContent;
      //矢印のメニューと見た目調整　END
      //add End 20201020
    } else {
      
      let toolbar = document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id);
      toolbar.style.display = "";
      
      let posX = (cellView.model.attributes.position.x) * scale;
      if(boards[graphIndex].paper.options.width < posX + toolbar.clientWidth){
        posX = boards[graphIndex].paper.options.width - toolbar.clientWidth;
      }
      
      // document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.left = (cellView.model.attributes.position.x) * scale;
      toolbar.style.left = posX;
      //change Start 20201016 Requa問題点ID：284
      //document.getElementById(ID_SHAPE_SUB_MENUBAR + boards[graphIndex].id).style.top = (cellView.model.attributes.position.y) * scale - offsetY;
      toolbar.style.top = positionY;
      //change End 20201016
    }
    
  } else if(cellView.model.attributes.type == CELL_TYPE_NOTE){
    changeHusenSubMenuItems(graphIndex, cellView.model.attributes.kind, false);
    
    let toolbar = document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id);
    
    toolbar.style.display = "";
    
    let posX = (cellView.model.attributes.position.x) * scale;
    if(boards[graphIndex].paper.options.width < posX + toolbar.clientWidth){
      posX = boards[graphIndex].paper.options.width - toolbar.clientWidth;
    }
    
    // document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id).style.left = (cellView.model.attributes.position.x) * scale;
    toolbar.style.left = posX;
    
    //change Start 20201016 Requa問題点ID：284
    //document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id).style.top =  (cellView.model.attributes.position.y) * scale - offsetY;
    toolbar.style.top = positionY;
    //change End 20201016
  }else if(cellView.model.attributes.type == CELL_TYPE_IMAGE){
    changeHusenSubMenuItems(graphIndex, cellView.model.attributes.kind, true);
    
    let toolbar = document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id);
    
    toolbar.style.display = "";
    
    let posX = (cellView.model.attributes.position.x) * scale;
    if(boards[graphIndex].paper.options.width < posX + toolbar.clientWidth){
      posX = boards[graphIndex].paper.options.width - toolbar.clientWidth;
    }
    
    // document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id).style.left = (cellView.model.attributes.position.x) * scale;
    toolbar.style.left = posX;
    toolbar.style.top = positionY;
  } else {
    let toolbar = document.getElementById(ID_TEXT_MENUBAR + boards[graphIndex].id);
    toolbar.style.display = "";
    
    let posX = (cellView.model.attributes.position.x) * scale;
    if(boards[graphIndex].paper.options.width < posX + toolbar.clientWidth){
      posX = boards[graphIndex].paper.options.width - toolbar.clientWidth;
    }
    toolbar.style.left = (cellView.model.attributes.position.x) * scale;
    //change Start 20201016 Requa問題点ID：284
    //document.getElementById(ID_TEXT_MENUBAR + boards[graphIndex].id).style.top =  (cellView.model.attributes.position.y) * scale - offsetY;
    toolbar.style.top = positionY;
    //change End 20201016
  }
}

function changeHusenSubMenuItems(graphIndex, kind, isImage = false)
{
  // 付箋用のサブメニュー取得
  let toolbar = document.getElementById(ID_FUSEN_SUB_MENUBAR + boards[graphIndex].id);
  if(toolbar == undefined){
    return ;
  }
  let buttonElem = toolbar.getElementsByTagName('button');
  
  // ファイルの付箋、画像
  if(kind == OBJECT_KIND_STICKY_NOTE_FILE){
    for (var i = 0; i < buttonElem.length; i++) {
      buttonElem[i].style.display = "";
      if (isImage) {
        if(buttonElem[i].attributes['data-name'].value == 'reactionStickyNote' || buttonElem[i].attributes['data-name'].value == 'changeColor'){
          buttonElem[i].style.display = "none";
        }
      }
      
      // * nakamatsu
      if (buttonElem[i].attributes['data-name'].value == 'imageSearch') {
        buttonElem[i].style.display = 'none';
      }
      // nakamatsu *
    }
  }else{
    for (var i = 0; i < buttonElem.length; i++) {
      // * nakamatsu
      switch (buttonElem[i].attributes['data-name'].value) {
        case 'download':
          buttonElem[i].style.display = "none";
          break;
        case 'reactionStickyNote':
        case 'imageSearch':
          buttonElem[i].style.display = '';
          break;
        case 'changeColor':
          buttonElem[i].style.display = '';
          break;
      }
      // nakamatsu *
    }
  }
}

// ブラウザからフォーカスが外れた時の処理
function window_blur(){
  
  // 選択状態を外す
  let boards_len = boards.length;
  for(let i = 0; i < boards_len; i++){
    boards[i].paper.removeTools();
    joint.ui.FreeTransform.clear(boards[i].paper);
    menuDisplayDisabled(boards[i].id);
  }
  // テキスト直接入力対応 -->
  jointUiTextEditorClose(activeBoardId);
  // <-- テキスト直接入力対応
  
  // 更新停止を解除
  contentsEditFlg = false;
  contents_onblur();
  
  // オブジェクトのロック状態を外す
  noteObjectLock(undefined, false);
  
  // 音声認識停止処理
  if(onseiNinshikiCommentStopFnc){
    onseiNinshikiCommentStopFnc();
  }
}


/****************************************
 * 画像保存アイコンクリック
 ****************************************/
async function knowledgeDlImg_Click(){
  
  // 表示中のボードのみ保存
  // 共有ボード
  let shareBoardId = getShareBoardId();
  if(getShareBoardDisplayStatus(shareBoardId)){
    await boardImageSave(shareBoardId);
  }
  
  // // 個人ボード
  // let kojinBoardId = getKojinBoardId();
  // if(getKojinBoardDisplayStatus()){
  //     await boardImageSave(kojinBoardId);
  // }
}

/****************************************
 * ボードの画像保存
 ****************************************/
async function boardImageSave(boardId){
  
  let boardIndex = getBoardsIndex(boardId);
  let templateImage = undefined;
  
  let bordListIndex = getActiveBoardListInfoIndex(boardId);
  let templateUrl = boardListInfo[bordListIndex].background_image_base64;
  
  if(templateUrl != ""){
    templateImage = new joint.shapes.standard.Image();
    templateImage.resize(TEMPLATE_WIDTH, TEMPLATE_HEIGHT);
    // Chg start 20201111 kobayashi
    // templateImage.position(BOARD_WIDTH * 0.00535, BOARD_HEIGHT * 0.0083);
    templateImage.position(BOARD_WIDTH * 0.3876, BOARD_HEIGHT * 0.4484);
    // Chg end 20201111
    templateImage.attributes.z = -1;
    templateImage.attributes.kind = "template";
    templateImage.set('stopDelegation', false);
    templateImage.set('elementMove', false);
    templateImage.attr('image/xlink:href', templateUrl);
    templateImage.addTo(boards[boardIndex].graph);
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  }
  
  // スクロール位置保持
  let shareBoardId =　getKojinBoardId();
  let frameCtrl = document.getElementById(ID_PAPER_SCROLLER + shareBoardId);
  let scrollX = frameCtrl.scrollLeft;
  let scrollY = frameCtrl.scrollTop;
  
  let kojinBoardId =　getKojinBoardId();
  let kojinFrameCtrl = document.getElementById(ID_PAPER_SCROLLER + kojinBoardId);
  let kojinScrollX = kojinFrameCtrl.scrollLeft;
  let kojinScrollY = kojinFrameCtrl.scrollTop;
  
  boards[boardIndex].paper.toPNG(function(dataURL) {
    
    // 画像保存用に追加したテンプレート消す
    if(templateImage){
      let cellIndex = boards[boardIndex].graph.attributes.cells.models.findIndex(a => a.attributes.kind == "template");
      let cellView = boards[boardIndex].paper.findViewByModel(boards[boardIndex].graph.attributes.cells.models[cellIndex]);
      cellView.model.remove();
    }
    
    let image = new Image();
    image.onload = function(){
      $("body").append(`<canvas id='canvas1' class='hidden' width=${image.naturalWidth} height=${image.naturalHeight} ></canvas>`);
      let canvas = $("#canvas1")[0];
      let ctx = canvas.getContext("2d");
      
      ctx.drawImage(image, 0, 0);
      $("body").append("<a id='image-file' class='hidden' type='application/octet-stream' href='"
        + canvas.toDataURL("image/png") + `' download='board.png'>Donload Image</a>`);
      $("#image-file")[0].click();
      
      // 後処理
      $("#canvas1").remove();
      $("#image-file").remove();
      
    }
    image.src = dataURL;
  });
  frameCtrl.scrollLeft = scrollX;
  frameCtrl.scrollTop = scrollY;
  kojinFrameCtrl.scrollLeft = kojinScrollX;
  kojinFrameCtrl.scrollTop = kojinScrollY;
}

// メインルームへ移動
// function btnMainRoom_click(){
//     // 2020/10/14 連打防止 -->
//     $('#btnMainRoom').prop("disabled", true);
//     commonDefine.setVisible('#yykb-loading', true);
//     // <-- 2020/10/14 連打防止
//     $.ajax({
//         type: "POST",
//         url: '/yyknowledgeboard/get-main-room-meetingid',
//         data: JSON.stringify({subRoomId: selectMeeting}),
//         processData: false,
//         contentType: "application/json; charset=utf-8",
//         success: function (response) {
//             mainRoomId = response;
//
//             if(mainRoomId != -1){
//
//                 // ビデオ通話のクローズ処理
//                 closeVideoCall();
//
//                 // 2020/10/15 再対応
//                 //     window.location.href = '/yy-knowledge-board-main-room/' + mainRoomId;
//                 window.location.href = '/yy-knowledge-board-main-room/' + mainRoomId;
//                 // 2020/10/15 再対応
//             }
//         }
//     });
// }

// 2020/10/15 再対応
// function mainRoomTransition(){
//     window.location.href = '/yy-knowledge-board-main-room/' + mainRoomId;
// }
// 2020/10/15 再対応

// 2020/10/14 微調整 -->
function initFinishNotify() {
  
  // 制限機能対応 -->
  if (limitFunctions.isSoundOnly()) {
    $('#my-desktopImg').attr('title', 'ベータ版ではルームに入った最初の4人だけ画面共有が可能です。');
    $('#my-videoImg').attr('title', 'ベータ版ではルームに入った最初の4人だけビデオ通話が可能です。');
  }
  // <-- 制限機能対応
  $('#btnMainRoom').prop('disabled', false);
  commonDefine.setVisible('#yykb-loading', false);
}
// <-- 2020/10/14 微調整

// 線オブジェクトの最小値設定対応 -->
function calcStandardLinkSize(cell) {
  
  let ret = {
    isChange: false,
    cell: cell,
  };
  
  if (cell.attributes.source.id || cell.attributes.target.id) {
    return ret;
  }
  
  let width = cell.attributes.target.x - cell.attributes.source.x;
  let isReverseX = false;
  if (width < 0) {
    width = cell.attributes.source.x - cell.attributes.target.x;
    isReverseX = true;
  }
  
  let isReverseY = false;
  let height = cell.attributes.target.y - cell.attributes.source.y;
  if (height < 0) {
    height = cell.attributes.source.y - cell.attributes.target.y;
    isReverseY = true;
  }
  
  if (width < LINE_OBJECT_MIN_SIZE.WIDTH && height < LINE_OBJECT_MIN_SIZE.HEIGHT) {
    
    if (width === height) {
      
      if (isReverseX) {
        cell.attributes.source.x = cell.attributes.target.x + LINE_OBJECT_MIN_SIZE.WIDTH;
      } else {
        cell.attributes.target.x = cell.attributes.source.x + LINE_OBJECT_MIN_SIZE.WIDTH;
      }
      
      if (isReverseY) {
        cell.attributes.source.y = cell.attributes.target.y + LINE_OBJECT_MIN_SIZE.HEIGHT;
      } else {
        cell.attributes.target.y = cell.attributes.source.y + LINE_OBJECT_MIN_SIZE.HEIGHT;
      }
    } else if (width > height) {
      if (isReverseX) {
        cell.attributes.source.x = cell.attributes.target.x + LINE_OBJECT_MIN_SIZE.WIDTH;
      } else {
        cell.attributes.target.x = cell.attributes.source.x + LINE_OBJECT_MIN_SIZE.WIDTH;
      }
    } else {
      if (isReverseY) {
        cell.attributes.source.y = cell.attributes.target.y + LINE_OBJECT_MIN_SIZE.HEIGHT;
      } else {
        cell.attributes.target.y = cell.attributes.source.y + LINE_OBJECT_MIN_SIZE.HEIGHT;
      }
    }
    ret.isChange = true;
  }
  
  return ret;
}
// <-- 線オブジェクトの最小値設定対応

// エラー情報をサーバ側でログに書き込みます
function writeYYKnowledgeBoardErrorLog(err){
  if(err){
    
    let errorStr = "";
    if(typeof(err) == "string"){
      errorStr = err;
    } else {
      if(err.stack){
        errorStr = err.stack;
      } else if(err.message){
        errorStr = err.message;
      }
    }
    
    let data = {
      source: "yyknowledgeboard.js",
      errorMsg:errorStr
    };
    
    $.ajax({
      type: "POST",
      url: '/yyknowledgeboard/write-errorlog',
      data: JSON.stringify(data),
      processData: false,
      contentType: "application/json; charset=utf-8",
      success: function (response) {
      }
    });
  }
}

// ボードの背景をDBに登録する
function　setBoardBackGroundImage(boardId, imagePath){
  try{
    let param = {
      boardId: boardId,
      path: imagePath,
    };
    
    $.ajax({
      type: "POST",
      url: "/yyknowledgeboard/set-board-background-image",
      data: JSON.stringify(param),
      processData: false,
      contentType: "application/json; charset=utf-8",
      success: function (response) {
        //commonDefine.setVisible('#yykb-loading', false);
        if (response === -1) $('#error-system').modal('show');
        else {
          // boardListInfo[n].background_image_pathを更新する
          let bordListIndex = getActiveBoardListInfoIndex(activeBoardId);
          if(bordListIndex >= 0){
            boardListInfo[bordListIndex].background_image_path = imagePath;
            boardListInfo[bordListIndex].background_image_base64 = response.base64Image;
          }
          
          // 背景描画
          drawBoardBackgroundImage(boardId, imagePath);
          
          // 操作ログに追加　※※※
          
          // 共有
          let sendData = {kind : 20, data : {boardID : boardId, backgroundImagePath : imagePath, backgroundImageBase64: response.base64Image}};
          sendDataBroadcast(sendData);
        }
      }
    });
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// ボードの背景を表示
function drawBoardBackgroundImage(boardId, imagePath){
  
  try{
    if(isBlank(imagePath) == true){
      document.getElementById(boardId).style.backgroundImage = '';
    }
    else{
      document.getElementById(boardId).style.backgroundImage = `url(${imagePath})`;
      document.getElementById(boardId).style.backgroundRepeat = "no-repeat";
      // document.getElementById(boardId).style.backgroundSize = "100% auto";
      // document.getElementById(boardId).style.backgroundSize = "1920px";
      document.getElementById(boardId).style.backgroundSize = "23.25%";
      // document.getElementById(boardId).style.backgroundPosition = "0.7% 1%";
      document.getElementById(boardId).style.backgroundPosition = "50.5% 54.3%";
      
    }
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// テンプレートダイアログ表示
function showTemplateModal(){
  
  try{
    // activeなボードのデータのインデックスを取得
    let nothingImagePath = "images/image_nothing.png";
    let forcusImagePath = nothingImagePath;
    let bordListIndex = getActiveBoardListInfoIndex(activeBoardId);
    if(bordListIndex >= 0){
      if(isBlank(boardListInfo[bordListIndex].background_image_path) == false){
        forcusImagePath = boardListInfo[bordListIndex].background_image_path;
      }
    }
    
    $.ajax({
      type: "POST",
      url: "/yyknowledgeboard/get-template-file-list",
      //data: JSON.stringify(param),
      processData: false,
      contentType: "application/json; charset=utf-8",
      success: function (response) {
        //commonDefine.setVisible('#yykb-loading', false);
        if (response === -1) $('#error-system').modal('show');
        else {
          let viewTemplateModal = ``;
          let divImageClass = "setting-template-img";
          
          // 背景無し
          if(nothingImagePath == forcusImagePath){
            divImageClass = "setting-template-img selected-templete";
          }
          
          viewTemplateModal += `
                    <div class="setting-board-template-wrapper">
                        <div class="${divImageClass}">
                            <img src="${nothingImagePath}">
                        </div>
                        <label class="lb-template-type.font-source-han-san" >なし</label>
                    </div>
                    `;
          
          // S3のテンプレート
          for (let i = 0; i < response.fileUrlList.length; i++) {
            
            divImageClass = "setting-template-img";
            if(forcusImagePath == response.fileUrlList[i].url){
              divImageClass = "setting-template-img selected-templete";
            }
            
            viewTemplateModal += `
                        <div class="setting-board-template-wrapper">
                            <div class="${divImageClass}">
                                <img src="${response.fileUrlList[i].url}">
                            </div>
                            <label class="lb-template-type.font-source-han-san" >${response.fileUrlList[i].name}</label>
                        </div>
                        `;
          }
          $('#board-template-content').html(viewTemplateModal);
          $('#modal-board-template').modal('show');
        }
      }
    });
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// テンプレート選択完了
async function selectedTemplate(filename, imgsrc){
  
  //alert(filename);
  if(filename == undefined || imgsrc == undefined){
    alert("テンプレートの設定に失敗しました");
    return ;
  }
  
  let shareBoardId = getShareBoardId();
  if(shareBoardId == -1){
    alert("テンプレートの設定に失敗しました");
    return;
  }
  
  if(imgsrc == "images/image_nothing.png"){
    setBoardBackGroundImage(shareBoardId, "");
  }
  else{
    setBoardBackGroundImage(shareBoardId, imgsrc);
  }
}

// テンプレート画像変更をブロードキャスト通信で受信したときの処理
function reciveTemplateChange(data){
  try{
    // boardListInfo[n].background_image_pathを更新する
    let bordListIndex = getActiveBoardListInfoIndex(data.boardID);
    if(bordListIndex >= 0){
      boardListInfo[bordListIndex].background_image_path = data.backgroundImagePath;
    }
    
    // 背景描画
    drawBoardBackgroundImage(data.boardID, data.backgroundImagePath);
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
}

// 指定のボードIDに対応するボードの情報のインデックスを返す
function getActiveBoardListInfoIndex(boardId){
  
  let index = -1;
  try{
    for(let i = 0; i < boardListInfo.length; i++){
      if(boardListInfo[i].boardId == boardId){
        index = i;
        break;
      }
    }
  }
  catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
  return index;
}

function isBlank(str){
  if( (str != null) && (str != '') && (str != undefined)){
    return false;
  }
  return true;
}

// ニックネーム変更
function changeNickName(nickName, userName){
  if(nickName){
    userInfo.response[0].nickname = nickName;
  } else {
    userInfo.response[0].nickname = "";
  }
  mousePointerInfo.userName = nickName ? nickName : userName;
}

async function readImageFileForBase64(file){
  
  return new Promise((resolve) => {
    let reader = new FileReader();
    reader.onload = function(evt) {
      let base64Str = this.result;
      let image = new Image();
      image.onload = function(){
        let resizeFlg = false;
        let rw = image.naturalWidth;
        let rh = image.naturalHeight;
        if(image.naturalWidth > IMAGE_THUMBNAIL_WIDTH){
          resizeFlg = true;
          let scale = 0;
          scale = (IMAGE_THUMBNAIL_WIDTH / image.naturalWidth);
          rw = image.naturalWidth * scale;
          rh = image.naturalHeight * scale;
          
          if(rh > IMAGE_THUMBNAIL_HEIGHT){
            scale = (IMAGE_THUMBNAIL_HEIGHT / rh);
            rw = rw * scale;
            rh = rh * scale;
          }
          
        } else if(image.naturalHeight > IMAGE_THUMBNAIL_HEIGHT){
          resizeFlg = true;
          let scale = 0;
          scale = (IMAGE_THUMBNAIL_HEIGHT / image.naturalHeight);
          rw = image.naturalWidth * scale;
          rh = image.naturalHeight * scale;
        }
        if(resizeFlg){
          resizeBase64Img(base64Str, rw, rh, function(resize_img_b64){
            resolve(resize_img_b64);
          });
        } else {
          resolve(base64Str);
        }
        
      };
      image.src = base64Str;
    };
    reader.readAsDataURL(file);
  });
  
}


function getWrapFileName(name){
  return joint.util.breakText(name, { width: 150, height: 25 }, { 'font-size': 16 }, { ellipsis: true })
}

/****************************************
 * DELETEキー押された時の処理
 ****************************************/
function keyDownDelete(){
  
  let boardIndex = getBoardsIndex(activeBoardId);
  if(boardIndex == -1){
    return;
  }
  
  if(boards[boardIndex].graph != undefined){
    
    nowPasteingFlg = true;
    commonDefine.setVisible('#yykb-loading-objectOperation', true);
    
    // グループ化解除
    if(old_cellview != undefined){
      let embedList = old_cellview.model.getEmbeddedCells();
      let len = embedList.length;
      for(let i = 0; i < len; i++){
        if(embedList[i]){
          old_cellview.model.unembed(embedList[i]);
        }
      }
    }
    
    let removeList = [];
    let removeList_Link = getBoundaryCellList(boardIndex, CELL_TYPE_STANDARD_LINK);
    let removeList_Note = getBoundaryCellList(boardIndex, CELL_TYPE_NOTE);
    let removeList_Rect = getBoundaryCellList(boardIndex, CELL_TYPE_RECT);
    let removeList_Image = getBoundaryCellList(boardIndex, CELL_TYPE_IMAGE);
    
    // 線の削除
    let removeList_Link_len = removeList_Link.length;
    for(let i = 0; i < removeList_Link_len; i++){
      
      removeList.push({
        objectId : removeList_Link[i].attributes.attrs['.objectId'].value,
      });
      
      // 線の削除
      let cellView = boards[boardIndex].paper.findViewByModel(removeList_Link[i]);
      // Chg start 20201111 kobayashi
      if(cellView){
        cellView.model.remove();
      }
      // Chg end 20201111
      
    }
    
    // 付箋の削除
    let removeList_Note_len = removeList_Note.length;
    for(let i = 0; i < removeList_Note_len; i++){
      
      removeList.push({
        objectId : removeList_Note[i].attributes.attrs['.objectId'].value,
        remark_id : removeList_Note[i].attributes.attrs[".id"].text.replace("ID:", "")
      });
      
      // 音声入力中の付箋があれば音声認識をストップさせる。
      if(selectStickyNote){
        if(selectStickyNote.attributes.attrs['.objectId'].value == removeList_Note[i].attributes.attrs['.objectId'].value){
          onseininshikiStickyNote.stop();
        }
      }
      
      // 付箋削除
      let cellView = boards[boardIndex].paper.findViewByModel(removeList_Note[i]);
      // Chg start 20201111 kobayashi
      if(cellView){
        cellView.model.remove();
      }
      // Chg end 20201111
    }
    
    // Rectの削除
    let removeList_Rect_len = removeList_Rect.length;
    for(let i = 0; i < removeList_Rect_len; i++){
      
      let data = {
        objectId : removeList_Rect[i].attributes.attrs['.objectId'].value,
      };
      if(removeList_Rect[i].attributes.kind == OBJECT_KIND_COMMENT){
        data.remark_id = removeList_Rect[i].attributes.remark_id;
      }
      removeList.push(data);
      
      // Rect削除
      let cellView = boards[boardIndex].paper.findViewByModel(removeList_Rect[i]);
      // Chg start 20201111 kobayashi
      if(cellView){
        cellView.model.remove();
      }
      // Chg end 20201111
      
    }
    
    // 画像の削除
    let removeList_Image_len = removeList_Image.length;
    for(let i = 0; i < removeList_Image_len; i++){
      
      let data = {
        objectId : removeList_Image[i].attributes.attrs['.objectId'].value,
        remark_id : removeList_Image[i].attributes.remark_id
      };
      
      removeList.push(data);
      
      // 画像削除
      let cellView = boards[boardIndex].paper.findViewByModel(removeList_Image[i]);
      // Chg start 20201111 kobayashi
      if(cellView){
        cellView.model.remove();
      }
      // Chg end 20201111
      
    }
    
    // ツールバー非表示
    menuDisplayDisabled(boards[boardIndex].id);
    
    if(removeList.length == 0){
      old_cellview = undefined;
      nowPasteingFlg = false;
      commonDefine.setVisible('#yykb-loading-objectOperation', false);
      return;
    }
    
    // $.ajax({
    //     type: 'POST',
    //     url: `/yyknowledgeboard/delete-object`,
    //     processData: false,
    //     data : JSON.stringify(removeList),
    //     contentType: "application/json; charset=utf-8",
    //     success: function (response) {
    //         let res = response;
    //         old_cellview = undefined;
    //         nowPasteingFlg = false;
    //         commonDefine.setVisible('#yykb-loading-objectOperation', false);
    
    //         if(checkResponseErrer("", res)== false){
    //             return;
    //         }
    //     }
    // });
    deleteObject(removeList);
    
    // 個人ボード以外の時に共有情報飛ばす。
    if(getBoardKind(activeBoardId) != BOARD_KIND_KOJIN){
      
      let sendData = {
        kind : BROADCAST_KIND_CHANGE_OBJECT,
        data : {
          operation : 2,
          boardId : activeBoardId,
          objectData : removeList,
          userCode: userInfo.response[0].code
        }
      }
      
      changeObjectDataList.push(sendData.data);
    }
    
  }
}

function deleteObject(removeList){
  $.ajax({
    type: 'POST',
    url: `/yyknowledgeboard/delete-object`,
    processData: false,
    data : JSON.stringify(removeList),
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      let res = response;
      old_cellview = undefined;
      nowPasteingFlg = false;
      commonDefine.setVisible('#yykb-loading-objectOperation', false);
      
      if(checkResponseErrer("", res)== false){
        return;
      }
    }
  });
}

// 編集中フラグを返します。
function getContentEditFlg(){
  return contentsEditFlg;
}


let yyKnowledgeBoard = {
  
  init: () => {
    $("#yykb-loading").addClass('d-none');
    let elem = document.getElementById("myBar");
    let width = 1;
    let id = setInterval(frame, 30);
    function frame() {
      if (width >= 100) {
        clearInterval(id);
        $('#loading-bar').addClass('d-none');
      } else {
        width++;
        elem.style.width = width + '%';
      }
    }
    
    $(document).on('click', '.icon-reaction-sticky-note, .total-reaction', async function () {
      await yyKnowledgeBoard.insertOrUpdateReactionStickyNote($(this).data('id'), $(this).parent().attr('data-remark'));
    });
    
    $(document).on('click', '.confirm-go-to-subroom', function () {
      let mainMeetingId = $(this).data('meeting');
      let subRoomId = $(this).data('id');
      commonDefine.setVisible('#yykb-loading', true);
      closeVideoCall(false);
      if (mainMeetingId === subRoomId) {
        window.location.href = `/yy-knowledge-board-main-room/${mainMeetingId}`
      } else {
        window.location.href = '/redirect-sub-room?mainMeetingId=' + mainMeetingId + '&subRoomId=' + subRoomId;
      }
    });
    
    // $(document).on('click','#btnMainRoom', function(){
    //     if (roleUser === role.PARTICIPANT) {
    //         closeVideoCall(false);
    //         window.location.href = `/yy-knowledge-board-main-room/${$(this).data('id')}`;
    //     } else {
    //         $('#modal-list-subroom').modal('show');
    //     }
    // });
    
    $(document).on('change', '#list-sub-room-main .detail-content', function () {
      let meetingId = $('input[name="radioSubRoom"]:checked').val();
      $('.confirm-go-to-subroom').attr('data-id', meetingId);
    });
    
    $(document).on('click', '.custom-color-select', async function () {
      let newColor = $(this).data('color');
      let boardIndex = getBoardsIndex(activeBoardId);
      let listCell = getBoundaryCellList(boardIndex, CELL_TYPE_NOTE);
      for(let i = 0; i < listCell.length; i++){
        let cellView = boards[boardIndex].paper.findViewByModel(listCell[i]);
        cellView.model.attributes.attrs['.card'].fill = newColor;
        $(`#${cellView.id}`).find('.card').attr('fill', newColor);
        // cellView.update();
        // jsonSave(cellView.model);
        
        // let sendData = {
        //     kind : BROADCAST_KIND_CHANGE_COLOR_STICKY_NOTE,
        //     data : {
        //         idStickyNote: cellView.id,
        //         newColor: newColor,
        //     },
        // };
        // sendDataBroadcast(sendData);
      }
      
      addUserOperationLog(USER_OPERATION_EDIT, listCell, undefined);
      
      $('.custom-select-wrapper').removeClass('color-active');
      $(this).parent().addClass('color-active');
      let colorKey = $(this).attr('data-color-key');
      $('.joint-toolbar.joint-theme-modern button[data-name="changeColor"]').attr('class', `joint-widget joint-theme-modern ${colorKey}`)
    });
    
    $(document).on('click', '#expandMenu', async function () {
      $(`#toolbar-container-${activeBoardId} .joint-toolbar .joint-toolbar-group[data-group="default"]`).css('display', 'block');
      $(`.joint-toolbar-group[data-group="extends"]`).css('display', 'none');
    });
    
    $(document).on('show.bs.modal', '#modal-list-subroom', function () {
      $('#first-option').prop("checked", true);
    });
    
    // $(document).on('click', '#menu-expand-collapse', () => {
    //     let contentMenuTop = $('.boardNameArea-container');
    //     contentMenuTop.toggleClass('change-width-menu');
    //     let iconMenuMainRoom = $('#menu-expand-collapse img');
    //     let src = (iconMenuMainRoom.attr("src") === "/images/icon-menu-top/icon_collapse.svg")
    //         ? "/images/icon-menu-top/icon_expand.svg"
    //         : "/images/icon-menu-top/icon_collapse.svg";
    //     iconMenuMainRoom.attr("src", src);
    //
    //     if (contentMenuTop.hasClass('change-width-menu'))
    //         $('.change-size-menu').remove('.item-search, .item-slide-show, .item-record-voice-over, .item-preview, .item-share, .item-download');
    //     else contentMenuTop.html(viewMenuTop);
    // });
    
    
    $(document).on('click', '#item-menu-collapse', () => {
      $('#menu-collapse').addClass('d-none');
      $('#menu-expand').removeClass('d-none');
    });
    
    $(document).on('click', '#item-menu-expand', () => {
      $('#menu-expand').addClass('d-none');
      $('#menu-collapse').removeClass('d-none');
    });
    
    $(document).on('click', '#share-board', () => {
      $('#share-board').addClass('active');
      $('#invite-modal').modal('show');
    });
    
    $(document).on('click', '#download-board', () => {
      $('.download-area').toggleClass('hide-it');
      $('.item-download').toggleClass('active');
    });
    
    $(document).on('click', '#download-image', () => {
      knowledgeDlImg_Click();
    });
    
    yyKnowledgeBoard.appendSelectTime('#select-setting-share-1', '.scroll-setting-share-default-1', ['ボードの編集ができる']);
    yyKnowledgeBoard.appendSelectTime('#select-setting-share-2', '.scroll-setting-share-default-2', ['ボードの編集ができる']);
    
    $(document).on('keydown', '#input-email', function (e) {
      if (e.keyCode === 9 || e.keyCode === 13 || e.keyCode === 188) {
        let validateClass = 'invalid';
        $(this).remove();
        if (yyKnowledgeBoard.validateEmail($(this).val())) {
          validateClass = 'valid';
        }
        $('.all-mail').append(`<div class="email-ids ${validateClass}"><div class="email-input">${$(this).val().trim()}</div><button class="cancel-email"><img src="/images/icon_cancel.svg"/> </button></div><input id="input-email" type="text">`);
        
        setTimeout(function () {
          $('#input-email').val('');
          $('#input-email').focus();
        });
      }
    });
    
    $(document).on('click','.cancel-email',function(){
      $(this).parent().remove();
    });
    
    $(document).on('click','.email-area',function(){
      $('#input-email').focus();
    });
    
    $(document).on('click','#invite-btn',function(){
      let arrayEmail = $('.email-ids.valid .email-input').map(function () {
        return $(this).text();
      }).get();
      if (yyKnowledgeBoard.validateEmail($('#input-email').val())) {
        arrayEmail.push($('#input-email').val());
        $('#input-email').val('');
      }
      yyKnowledgeBoard.sendEmailInviteUser(arrayEmail);
      $('#invite-modal').modal('hide');
    });
    
    $(document).on('click','#copy-link',function(){
      let copyText = $('#invite-link')[0];
      copyText.select();
      copyText.setSelectionRange(0, 99999)
      document.execCommand("copy");
    });
    
    $(document).on('click','#hidden-survey',function(){
      $('.survey-yykb').css('bottom', '-100%');
      yyKnowledgeBoard.timerShowPopupSurvey();
    });
    
    $(document).on('click','#btn-join-survey',function(){
      $('.survey-yykb').css('bottom', '-100%');
      window.open('https://forms.gle/wvDvJmHDv2fxknku5');
    });
    
    $('#invite-modal').on('hidden.bs.modal', function () {
      $('.all-mail').html(`<input id="input-email" type="text">`);
      $('#input-email').val('');
      $('#share-board').removeClass('active');
    });
    
    yyKnowledgeBoard.timerShowPopupSurvey();
    yyKnowledgeBoard.getMeetingInvite();
    
    $(document).on('input', '.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] .input-wrapper .textarea', function () {
      let imgEnterKey = $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] img');
      if ($(this).val().trim().length > 0) imgEnterKey.attr('src', '/images/icon-toolbar/keyboard_return-active.png');
      else imgEnterKey.attr('src', '/images/icon-toolbar/keyboard_return.png');
      this.style.height = 'auto';
      this.style.height = (this.scrollHeight) + 'px';
    });
    
    $(document).on('dblclick', '.lineMenubar-container .joint-widget.joint-theme-modern[data-name="addTextLabel"]', function () {
      $(this).removeClass('active bg-btn-active');
      $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"]').addClass('d-none');
      $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] .input-wrapper .textarea').val(textEditedLine).focus();
    });
    
    // deha_eqr-266 start
    
    // $(document).on('click', '.item-record-voice-over', function () {
    //     $('.popup-voice-record').toggleClass('d-none');
    // });
    
    // deha_eqr-266 end
  },
  
  checkCheckedVoice: () => {
    let isChecked = true;
    if (!$('.filter-remark-voice .custom-control-input').is(":checked")) {
      $('.filter-remark-voice .custom-control-input').attr('disabled', false);
      isChecked = false;
    }
    return isChecked;
  },
  
  appendLayoutChangeColor: () => {
    let htmlColor = '';
    htmlColor +=
      `<div class="list-color-sticky-note">
                <div class="custom-color">`;
    for (let i=0; i<Object.keys(fusenColor).length;i++) {
      htmlColor += `<div class="custom-select-wrapper">
                                    <div class="custom-color-select" data-color-key="color-${i}" data-color="${fusenColor[Object.keys(fusenColor)[i]].color}" style="background-color: ${fusenColor[Object.keys(fusenColor)[i]].color}"></div>
                                </div>`;
    }
    htmlColor += `</div>
            </div>`;
    $('.change-color-sticky').html(htmlColor)
  },
  
  getIconReaction: () => {
    return $.ajax({
      type: 'GET',
      url: '/get-icon-reaction',
      success: function (response) {
        checkCookieAjax(response);
        if (response === -1) return $('#error-system-yykb').modal('show');
        else {
          return response;
        }
      }
    });
  },
  
  reactionStickyNote: (cellView, graphIndex) => {
    
    let scale = $(`#zoom-range-${boards[graphIndex].id}`).val() / 100;
    let offsetY = 60;
    // chg start 20201109 kobayashi
    // let reaction = $('.reaction-sticky');
    let reaction = $(`#reaction-sticky-note-` + boards[graphIndex].id);
    // chg end 20201109 kobayashi
    let remark_id = undefined;
    
    if(scale > 1){
      offsetY += (scale - 1) * 15;
    }
    
    if(cellView.model.attributes.attrs[".id"] != undefined){
      if(cellView.model.attributes.attrs[".id"].text != undefined){
        remark_id = cellView.model.attributes.attrs[".id"].text.replace("ID:","")
      }
    }
    
    reaction.attr('style', `left: ${(cellView.model.attributes.position.x) * scale}; top: ${(cellView.model.attributes.position.y) * scale - offsetY}; display: flex`).attr('data-remark', remark_id);
    // reaction.addClass('show');
  },
  
  getReactionOfMeeting: (remarkId) => {
    return $.ajax({
      type: 'GET',
      url: '/get-reaction-of-meeting',
      data: { remarkId: remarkId },
      success: function (response) {
        checkCookieAjax(response);
        if (response === -1) return $('#error-system-yykb').modal('show');
        else {
          return response;
        }
      }
    });
  },
  
  insertOrUpdateReactionStickyNote: (reactionId, remarkId) => {
    let data = {
      reactionId: reactionId,
      remarkId: remarkId,
    };
    $.ajax({
      type: 'POST',
      url: '/reaction-sticky-note',
      processData: false,
      data : JSON.stringify(data),
      contentType: "application/json; charset=utf-8",
      success: function (response) {
        checkCookieAjax(response);
        if (response === -1) return $('#error-system-yykb').modal('show');
        else {
          yyKnowledgeBoard.updateReaction(response.remark_id);
          let sendData = {
            kind : BROADCAST_KIND_UPDATE_REACTION_STICKY_NOTE,
            data : {
              remarkId: response.remark_id,
            },
          };
          sendDataBroadcast(sendData);
        }
      }
    });
  },
  
  updateReaction: async (remarkId) => {
    let dataReaction = await yyKnowledgeBoard.getReactionOfMeeting(remarkId);
    let html = '';
    
    if (dataReaction.response) {
      dataReaction.response.forEach(reaction => {
        let color = 'none';
        let background = 'rgba(61, 60, 64, 0.08)';
        if (reaction['user.code'].includes(dataReaction.user)) {
          color = '#166de0';
          background = 'rgba(22,109,224,0.08)';
        }
        html +=
          `<button xmlns="http://www.w3.org/1999/xhtml" data-toggle="tooltip" title="${reaction['user.name']}" class="total-reaction" data-id="${reaction['board_reaction_icon.id']}" style="background-color: ${background}; border: 1px solid ${color}">
                    <div>
                        <span class="reactions"> ${reaction['board_reaction_icon.reaction_icon']} </span>
                        <span class="reactions total-reactions" style="color: ${color}">${reaction['user.totalReaction']}</span>
                    </div>
                </button>`
      });
      // 不具合修正 -->
      $(`#area-reaction-contents-${remarkId}`).attr('width', dataReaction.response.length * 40);
      // <-- 不具合修正
    }
    
    // 不具合修正 -->
    // $(`#area-reaction-contents-${remarkId}`).attr('width', dataReaction.response.length * 40);
    // <-- 不具合修正
    
    $(`#reaction-contents-${remarkId}`).html(html);
  },
  
  // checkRoleUserYYKB: async (mainMeetingId) => {
  checkRoleUserYYKB: async (userRole) => {
    // let roleUserYYKB = await commonDefine.getRoleUserYYKB(mainMeetingId);
    let roleUserYYKB = userRole;
    switch (roleUserYYKB) {
      case role.CREATOR_MEETING:
        roleUser = role.CREATOR_MEETING;
        $('.title-go-to-sub-room').text('ルームを移動する');
        $('.icon-sub-room img').attr('src', '/images/icon-sub-room.png');
        break;
      case role.VIEWER:
        roleUser = role.VIEWER;
        $('.title-go-to-sub-room').text('ルームを移動する');
        $('.icon-sub-room img').attr('src', '/images/icon-sub-room.png');
        break;
      case role.PARTICIPANT:
        roleUser = role.PARTICIPANT;
        $('.title-go-to-sub-room').text('メインルームへ行く');
        $('.icon-sub-room img').attr('src', '/images/home-btn.png');
        break;
    }
  },
  
  ajaxGetListSubRoom: (mainMeetingId, currentSubRoom) => {
    return $.ajax({
      type: 'GET',
      url: '/main-room/get-list-sub-room',
      data: {id: mainMeetingId},
      success: function (response) {
        commonDefine.checkCookieAjax(response);
        if (response === -1) $('#error-system-yykb').modal('show');
        else {
          if (roleUser === role.VIEWER || roleUser === role.CREATOR_MEETING) {
            let str= '';
            str +=
              `<label class="label-custom-radio main-text font-source-han-san large-text">メインルーム
                                    <input id="first-option" type="radio" name="radioSubRoom" value="${response.resultMainMeeting.id}" checked>
                                    <span class="span-custom-radio"></span>
                                </label>`
            $('.confirm-go-to-subroom').attr('data-meeting', response.resultMainMeeting.id).attr('data-id', response.resultMainMeeting.id);
            if (response.resultListSubRoom.length !== 0) {
              response.resultListSubRoom.forEach((meeting, index) => {
                if (meeting.id !== currentSubRoom) {
                  str +=
                    `<label class="label-custom-radio main-text font-source-han-san large-text">${meeting.name.replace(/</g, '&lt;')}（${meeting['knowledge_teams.team_name'].replace(/</g, '&lt;')}）
                                        <input type="radio" name="radioSubRoom" value="${meeting.id}">
                                        <span class="span-custom-radio"></span>
                                    </label>`
                }
              });
            }
            $('.detail-content').html(str);
          } else {
            $('#btnMainRoom').attr('data-id', mainMeetingId);
          }
        }
      },
    });
  },
  
  appendSelectTime: (elementSelect1, elementSelect2, typeSelect) => {
    let html = '';
    let elementParentDropDown, setElement, elementInputDropDown, elementParentOption, elementValueOption;
    
    for (let i = 0; i < typeSelect.length; i++) {
      html += `<option>${typeSelect[i]}</option>`
    }
    $(elementSelect1).html(html);
    elementParentDropDown = $(elementSelect2);
    
    for (let i = 0; i < elementParentDropDown.length; i++) {
      setElement = elementParentDropDown[i].getElementsByClassName("select-schedule")[0];
      
      elementInputDropDown = document.createElement("DIV");
      elementInputDropDown.setAttribute("class", `select-selected custom-input select-schedule`);
      elementInputDropDown.innerHTML = '';
      elementParentDropDown[i].appendChild(elementInputDropDown);
      
      elementParentOption = document.createElement("DIV");
      elementParentOption.setAttribute("class", "select-items select-hide");
      elementInputDropDown.innerHTML = setElement.options[0].innerHTML;
      
      for (let j = 0; j < setElement.length; j++) {
        elementValueOption = document.createElement("DIV");
        elementValueOption.className = 'font-source-han-san-bold text-heading large-text'
        elementValueOption.innerHTML = setElement.options[j].innerHTML;
        if (j !== 0) elementValueOption.style.cursor = 'not-allowed';
        if (j == selectedTime.indexOf(elementInputDropDown.innerHTML)) {
          elementValueOption.setAttribute("class", "font-source-han-san-bold text-heading large-text same-as-selected");
        }
        
        elementValueOption.addEventListener("click", function (e) {
          let getSelect, parentElement;
          getSelect = this.parentNode.parentNode.getElementsByTagName("select")[0];
          for (let i = 0; i < getSelect.length; i++) {
            if (getSelect.options[i].innerHTML == this.innerHTML) {
              if (i !== 0) return;
              getSelect.selectedIndex = i;
              this.parentNode.previousSibling.innerHTML = this.innerHTML;
              parentElement = this.parentNode.getElementsByClassName("same-as-selected");
              for (let k = 0; k < parentElement.length; k++) {
                parentElement[k].classList.remove("same-as-selected");
              }
              this.setAttribute("class", "font-source-han-san-bold text-heading large-text same-as-selected");
              break;
            }
          }
          this.parentNode.previousSibling.click();
        });
        
        elementParentOption.appendChild(elementValueOption);
      }
      
      elementParentDropDown[i].appendChild(elementParentOption);
      
      elementInputDropDown.addEventListener("click", function (e) {
        e.stopPropagation();
        yyKnowledgeBoard.closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        if ($('.custom-select-schedule').eq(i).children().children('.same-as-selected')[0]) {
          $('.custom-select-schedule').eq(i).children().children('.same-as-selected')[0].scrollIntoView();
        }
      });
      document.addEventListener("click", yyKnowledgeBoard.closeAllSelect);
    }
  },
  
  closeAllSelect: function(element) {
    let elementItem, elementSelect, array = [];
    elementItem = document.getElementsByClassName("select-items");
    elementSelect = document.getElementsByClassName("select-selected");
    for (let i = 0; i < elementSelect.length; i++) {
      if (element == elementSelect[i]) {
        array.push(i)
      } else {
        elementSelect[i].classList.remove("select-arrow-active");
      }
    }
    for (let i = 0; i < elementItem.length; i++) {
      if (array.indexOf(i)) {
        elementItem[i].classList.add("select-hide");
      }
    }
  },
  
  validateEmail: (email) => {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  },
  
  sendEmailInviteUser(email) {
    let data = {
      email: email,
      url: $('#invite-link').val(),
    }
    return $.ajax({
      type: 'POST',
      url: '/send-email-invite-user',
      data : JSON.stringify(data),
      contentType: "application/json; charset=utf-8",
      processData: false,
      success: function (response) {
        checkCookieAjax(response);
        if (response === -1) return $('#error-system-yykb').modal('show');
        else {
          return response;
        }
      }
    });
  },
  
  timerShowPopupSurvey: () => {
    setTimeout(function () {
      $('.survey-yykb').css('bottom', 30);
    }, 15 * 60 * 1000);
  },
  
  getMeetingInvite: () => {
    $.ajax({
      type: 'GET',
      url: '/get-meeting-invite',
      success: function (response) {
        checkCookieAjax(response);
        if (response === -1) return $('#error-system-yykb').modal('show');
        else {
          let meeting = {
            meetingId: response.meetingId
          }
          $('#invite-link').val(`${response.domain}/invitation?yy=${btoa(`${JSON.stringify(meeting)}`)}`);
        }
      }
    });
  },
  
  logout: () => {
    localStorage.clear();
    window.location.href = '/logout';
  }
}

// setInterval(async function () {
//     if (window.location.pathname !== '/yyknowledgeboard') return;
//     let boardId = $('table.boder-table-keyword').find('div.yyKnowledgeBoard').attr('id');
//     let boardIndex = getBoardsIndex(boardId);
//     boards[boardIndex].paper.toPNG(async function (imgData) {
//         let sendData = {
//             kind: BROADCAST_KIND_BOARD_THUMBNAIL_CHANGE,
//             data: {
//                 imgData: imgData,
//                 meetingID: selectMeeting,
//                 boardID: boardId
//             },
//         };
//         sendDataBroadcast(sendData);
//     });
// }, 5000);


function setFreeTransform(cellView){
  // We don't want to transform links.
  if (cellView.model instanceof joint.dia.Link) return;
  
  let param = {
    cellView: cellView,
    allowRotation: false,
    maxWidth: MAX_OBJECT_WIDTH,
    maxHeight: MAX_OBJECT_HEIGHT
  };
  
  if(cellView.model.attributes.type == CELL_TYPE_NOTE){
    param.minWidth = MIN_OBJECT_WIDTH;
    param.minHeight = MIN_OBJECT_HEIGHT;
    
    if(cellView.model.attributes.kind == OBJECT_KIND_STICKY_NOTE_FILE){
      param.preserveAspectRatio= true;
    }
    // コメントのサイズ変更時に音声再生アイコンと被る対応 -->
  } else if(cellView.model.attributes.kind == OBJECT_KIND_COMMENT){
    
    let textArray = cellView.model.attributes.attrs.label.text.split(/\n/);
    let lengthText = textArray.length;
    let heightTextVoice = lengthText * 24 + 28;
    let heightComment =  lengthText < 3 ? heightTextVoice + 50 : heightTextVoice;
    param.minWidth = document.getElementById(cellView.selectors.label.id).clientWidth + 20;
    param.minHeight = heightComment;
  }
  // <-- コメントのサイズ変更時に音声再生アイコンと被る対応
  
  
  
  let freeTransform = new joint.ui.FreeTransform(param);
  //自由変形ウィジェットをレンダリング
  freeTransform.render();
  
  // リサイズ開始イベント登録
  $('.resize').off('pointerdown');
  $('.resize').on('pointerdown', function(){
    changeingCell = cellView.model;
  });
  
}

// 個人ボード追加アイコンクリック
function myBoardAdd_click(){
  
  let boardName = "";
  let inc = 1;
  let kojinBoards = boards.filter((val) => {
    return val.kind == BOARD_KIND_KOJIN;
  });
  
  let len = kojinBoards.length;
  boardName = KOJIN_BOARD_NAME + (len + inc);
  let index = -1;
  
  do{
    
    index = kojinBoards.findIndex(a => a.name == boardName);
    if(index != -1){
      inc++;
      boardName = KOJIN_BOARD_NAME + (len + inc);
    }
    
  }while(index != -1)
  
  let data = {
    isAllBoard: false,
    kind: 1,
    meetingId: selectMeeting,
    visible: 1,
    nameBoard: boardName,
    owner: userInfo.response[0].code
  }
  
  $.ajax({
    type: 'POST',
    url: `/create-board`,
    processData: false,
    data : JSON.stringify(data),
    contentType: "application/json; charset=utf-8",
    success: async function (response) {
      
      if(response == -1){
        return;
      }
      
      await addSelectBoard(response[0].id, response[0].name, response[0].kind, response[0].visible);
      $("#my-board-name-" + response[0].id).click();
    }
  });
}

// ボード名変更アイコンクリック
function boardNameEdit_click(){
  document.getElementById("board-name-edit").style.display = "";
  let kojinBoardId = getKojinBoardId();
  let index = getBoardsIndex(kojinBoardId);
  $('.input-board-name').val(boards[index].name);
}

// 変更したボード名の保存
function boardNameSave_click(){
  let kojinBoardId = getKojinBoardId();
  let newBoardName = $('.input-board-name').val();
  let ctrl = document.getElementById("my-board-name-" + kojinBoardId);
  
  if(ctrl == undefined){
    return;
  }
  
  if(ctrl.innerText == newBoardName){
    return;
  }
  
  let data = {
    id: kojinBoardId,
    name: newBoardName,
    visible: 1
  }
  
  $.ajax({
    type: 'POST',
    url: `/update-board`,
    processData: false,
    data : JSON.stringify(data),
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      
      if(response == -1){
        return;
      }
      
      let index = getBoardsIndex(kojinBoardId);
      boards[index].name = newBoardName;
      ctrl.innerText = newBoardName;
      ctrl.title = newBoardName;
    }
  });
  document.getElementById('board-name-edit').style.display = 'none';
  $('#my-board-name-edit').removeClass('active');
}

// 個人ボード削除
function myBoardDelete_click(){
  
  let myBoards = document.getElementById("my-board-name-area").children;
  let len = myBoards.length;
  let deleteBoardId = -1;
  let nextActiveBoardId = -1;
  
  if(len == 1){
    alert("個人ボードを全て削除することは出来ません。");
    return;
  }
  
  // 削除するボードID取得
  for(var i = 0; i < len; i++){
    let index = myBoards[i].className.indexOf("my-board-disable");
    if(index == -1){
      deleteBoardId = myBoards[i].id.replace("my-board-name-", "");
      if(i + 1 >= len){
        nextActiveBoardId = myBoards[i - 1].id.replace("my-board-name-", "");
      } else {
        nextActiveBoardId = myBoards[i + 1].id.replace("my-board-name-", "");
      }
      // myBoards[i].id.remove();
      break;
    }
  }
  
  if(deleteBoardId == -1 || nextActiveBoardId == -1){
    return;
  }
  
  let boardName = document.getElementById("my-board-name-" + deleteBoardId).innerText;
  if(window.confirm(`「${boardName}」を削除します。よろしいですか？`) == false){
    return;
  }
  
  
  let data = [];
  data.push(deleteBoardId);
  
  
  $.ajax({
    type: "POST",
    url: '/yyknowledgeboard/delete-board',
    data: JSON.stringify({boardIds: data}),
    processData: false,
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      if(response == -1){
        return;
      }
      
      // ボードの要素削除
      document.getElementById(ID_KNOWLEDGEBOARD_FRAME + deleteBoardId).remove();
      document.getElementById("my-board-name-" + deleteBoardId).remove();
      let boardIndex = getBoardsIndex(deleteBoardId);
      boards.splice(boardIndex, 1);
      
      // 削除後にアクティブにする個人ボード
      document.getElementById("my-board-name-" + nextActiveBoardId).click();
    }
  });
  
}

// ボードIDからボード種別を取得します。
function getBoardKind(boardId){
  
  let res = -1;
  try{
    let index = boards.findIndex(a => a.id == boardId);
    if(index != -1){
      res = boards[index].kind;
    }
  } catch (err){
    writeYYKnowledgeBoardErrorLog(err);
  }
  return res;
  
}

// 表示している共有ボードのIDを取得します。
function getShareBoardId(){
  let res = -1;
  try{
    let shareTd = document.getElementById("yyKnowledgeBoard-td1");
    res = shareTd.children[0].id.replace(ID_KNOWLEDGEBOARD_FRAME, "");
  } catch(err) {
    writeYYKnowledgeBoardErrorLog(err);
  }
  
  return res;
}

// VertexAddingプロパティを更新する
function setVertexAdding(){
  
  let boardIndex = getBoardsIndex(activeBoardId);
  let selectList = getBoundaryCellList(boardIndex, CELL_TYPE_STANDARD_LINK);
  let len = selectList.length;
  for(let i = 0; i < len; i++){
    let cellView = boards[boardIndex].paper.findViewByModel(selectList[i]);
    if(cellView._toolsView != undefined){
      cellView.removeTools();
      noteBoundary(cellView);
    }
  }
  
}

// paperのInteractiveプロパティの関数
function paperInteractive(view) {
  return {
    elementMove: view.model.get('elementMove'),
    stopDelegation: view.model.get('stopDelegation')
  }
}
function inputContent(event, selector, isInit) {
  if (isInit === true) {
    selector.style.height = 'auto';
    let currentHeight = parseFloat(selector.clientHeight);
    if (currentHeight > (175 - 25)) {
      selector.parentElement.parentElement.getElementsByClassName('scalable')[0].setAttribute('transform', `scale(1, ${(currentHeight + 25) / 175})`);
      $(`#area-reaction-${selector.id}`).attr('transform', `translate (0, ${currentHeight + 30})`);
      selector.parentElement.setAttribute("height", currentHeight);
    } else {
      selector.parentElement.parentElement.getElementsByClassName('scalable')[0].setAttribute('transform', `scale(1, 1)`);
    }
  }
}

function getTextEditedLine(evt, paper) {
  let selector = $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"] .input-wrapper');
  if (!selector.is(evt.target) && selector.has(evt.target).length === 0) {
    let lineMenuBarName = ID_LINE_MENUBAR + activeBoardId;
    let lineMenuChildrenList = document.getElementById(lineMenuBarName).children[0].children;
    let editComment = lineMenuChildrenList[0].children[8].childNodes[1].firstElementChild.value;
    
    let boradId = lineMenuBarName.replace(ID_LINE_MENUBAR, "");
    let graphIndex = getBoardsIndex(boradId);
    let removeList_Note = getBoundaryCellList(graphIndex, CELL_TYPE_STANDARD_LINK);
    if (removeList_Note[0] != null || removeList_Note[0] != undefined) {
      removeList_Note[0].attributes.labels[0].attrs.text.text = editComment;
      let cellView = paper.findViewByModel(removeList_Note[0]);
      if (cellView) {
        cellView.el.children[2].lastElementChild.lastChild.textContent = editComment;
        cellView.model.appendLabel({
          attrs: {
            text: {
              type: 'textarea',
              style: {
                whiteSpace: 'pre',
                'xml:space': "preserve",
                'word-break': 'break-all'
              },
            },
          }
        });
        
        $('.lineMenubar-container .joint-widget.joint-theme-modern').removeClass('bg-btn-active active');
        $('.joint-widget.joint-theme-modern[data-name="textEditAreaLine"]').addClass('d-none');
        addUserOperationLogLink(USER_OPERATION_EDIT, cellView.model);
      }
    }
  }
}

function addTextFromTextMenuBar(event, paper) {
  let selector = $('.textarea');
  let toolBar = $('.joint-toolbar-group.joint-theme-modern');
  if (!$('.text-edit-area-custom').hasClass('d-none') && !selector.is(event.target) && !toolBar.is(event.target) && toolBar.has(event.target).length === 0 && selector.has(event.target).length === 0) {
    let textMenuBarName = ID_TEXT_MENUBAR + activeBoardId;
    let textMenuChildrenList = document.getElementById(textMenuBarName).children[0].children[1].children;
    let editComment = textMenuChildrenList[0].childNodes[1].firstElementChild.value;
    let boardId = (ID_TEXT_MENUBAR + activeBoardId).replace(ID_TEXT_MENUBAR,"");
    let graphIndex = getBoardsIndex(boardId);
    let removeList_Note = getBoundaryCellList(graphIndex,CELL_TYPE_TEXT);
    if (removeList_Note.length > 0) {
      if (removeList_Note[0].attributes.attrs.label.text != null) {
        removeList_Note[0].attributes.attrs.label.text = editComment;
      }
      let cellView = paper.findViewByModel(removeList_Note[0]);
      if (cellView) {
        let html = "";
        let textArray = editComment.split(/\n/);
        let lengthText = textArray.length;
        let heightText = lengthText * 35;
        let dy = 0.3 - (lengthText - 1) * 0.5;
        let transform = lengthText * 17.5;
        for (let i = 0; i < lengthText; i++) {
          if (textArray[i] === '') textArray[i] = '&nbsp;';
          let x = parseInt(i) === 0 ? "" : "x='0'";
          html += `<tspan dy="${parseInt(i) === 0 ? dy : 1.2}em" ${x} class="v-line">${textArray[i]}</tspan>`
        }
        $(`#${cellView.selectors.label.id}`).html(html);
        $(`#${cellView.selectors.label.id}`).attr({
          'transform': `matrix(1,0,0,1,10,${transform})`
        });
        cellView.model.attributes.size.width = document.getElementById(cellView.selectors.label.id).clientWidth + 20;
        cellView.model.attributes.size.height = heightText;
        if (cellView.model.attributes.attrs.body.stroke !== "transparent") {
          let heightTextVoice = lengthText * 24 + 28;
          let yTransform = lengthText * 12 + 28;
          let heightComment =  lengthText < 3 ? heightTextVoice + 50 : heightTextVoice;
          $(`#${cellView.selectors.label.id}`).attr({
            'transform': `matrix(1,0,0,1,10,${yTransform})`
          });
          $(`#${cellView.selectors.body.id}`).attr({
            'stroke': '#D1DFF4',
            'width': parseInt(`${document.getElementById(cellView.selectors.label.id).clientWidth}`) + 20,
            'height': `${heightComment}`,
          });
          cellView.model.attributes.size.width = document.getElementById(cellView.selectors.label.id).clientWidth + 20;
          cellView.model.attributes.size.height = heightComment;
        }
        
        cellView.model.attributes.attrs.label.text = editComment;
        // 接続線が離れる件対応 -->
        cellView.update();
        if(cellView.model.isLink() == false){
          let links = boards[graphIndex].graph.getConnectedLinks(cellView.model);
          let len = links.length;
          for(let i = 0; i < len; i++){
            let linkView =  boards[graphIndex].paper.findViewByModel(links[i]);
            linkView.update();
          }
        }
        // <-- 接続線が離れる件対応
        addUserOperationLog(USER_OPERATION_EDIT, cellView.model, undefined);
      }
    }
    oldTextArea = 'text';
    $('.text-edit-area-custom').addClass('d-none');
    // メニューの形状統一化対応 -->
    // $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').removeClass('active').css('background-color', '#fff');
    $('.textMenubar-container .joint-toolbar-group .joint-theme-modern[data-name=addTextLabel]').removeClass('active bg-btn-active');
    // <-- メニューの形状統一化対応
    setDisplayNone(ID_TEXT_MENUBAR, activeBoardId);
    window_blur();
  }
}

function changeCursor(type) {
  $('#yyKnowledgeBoard-td1').find('.joint-paper-scroller.joint-theme-modern')
    .attr('class', `joint-paper-scroller joint-theme-modern ${type}`);
}

function addTextLabel_Click(){
  $('.input-wrapper .textarea').focus();
  $('.text-edit-area-custom').removeClass('d-none');
  $('.joint-them-modern').css('background', 'none');
  $(`#textMenubar-${activeBoardId}-addTextLabel`).addClass('active bg-btn-active');
  let text = $(`#textMenubar-${activeBoardId}-addTextLabel`).parent().parent().find('.text-edit-area-custom').children().find('.input-wrapper').find('.textarea').val();
  if (text && text.length > 0) $('.input-wrapper img').attr('src', '/images/keyboard_return-active.png');
  else $('.input-wrapper img').attr('src', '/images/keyboard_return.png');
  contentsEditFlg = true;
  $('.text-edit-area-custom .textarea').css('height', '52px');
  
  $(`#text-edit-area-custom-${activeBoardId}-textarea`).off('input');
  $(`#text-edit-area-custom-${activeBoardId}-textarea`).on('input', function () {
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
    if ($(this).val().length > 0) $('.input-wrapper img').attr('src', '/images/keyboard_return-active.png');
    else $('.input-wrapper img').attr('src', '/images/keyboard_return.png');
  });
  
  let ctrl = document.getElementById(`text-edit-area-custom-${activeBoardId}-textarea`);
  ctrl.style.height = 'auto';
  ctrl.style.height = (ctrl.scrollHeight) + 'px';
  if ($(`#text-edit-area-custom-${activeBoardId}-textarea`).val().length > 0) $('.input-wrapper img').attr('src', '/images/keyboard_return-active.png');
  else $('.input-wrapper img').attr('src', '/images/keyboard_return.png');
  $('.input-wrapper .textarea').focus();
}

$(document).ready(function () {
  yyKnowledgeBoard.init();
  $('#yy-knowledge-board-logout').on('click', (e) => {
    e.preventDefault();
    meetingManage.showNotificationModal('#warning-notification-modal',
      'ログアウトしますか？', '', yyKnowledgeBoard.logout);
  });
});

// テキスト直接入力対応 -->
function jointUiTextEditorClose(boardId){
  let graphIndex = getBoardsIndex(boardId);
  joint.ui.TextEditor.close();
  if(graphIndex == -1){
    return;
  }
  if(textEditerBef.objectId != undefined){
    
    let objIdx = boards[graphIndex].graph.attributes.cells.models.findIndex(a => {
      let res = false;
      if(a.attributes.attrs[".objectId"] != undefined){
        if(a.attributes.attrs[".objectId"].value == textEditerBef.objectId){
          res = true;
        }
      }
      return res;
    });
    
    if(objIdx != -1){
      let cellView = boards[graphIndex].paper.findViewByModel(boards[graphIndex].graph.attributes.cells.models[objIdx]);
      
      if(cellView.model.attributes.attrs.label.text != textEditerBef.befText){
        
        let html = "";
        let editComment = cellView.model.attributes.attrs.label.text;
        let textArray = editComment.split(/\n/);
        let lengthText = textArray.length;
        let heightText = lengthText * 35;
        let dy = 0.3 - (lengthText - 1) * 0.5;
        let transform = lengthText * 17.5;
        for (let i = 0; i < lengthText; i++) {
          if (textArray[i] === '') textArray[i] = '&nbsp;';
          let x = parseInt(i) === 0 ? "" : "x='0'";
          html += `<tspan dy="${parseInt(i) === 0 ? dy : 1}em" ${x} class="v-line">${textArray[i]}</tspan>`
        }
        $(`#${cellView.selectors.label.id}`).html(html);
        $(`#${cellView.selectors.label.id}`).attr({
          'transform': `matrix(1,0,0,1,10,${transform})`
        });
        cellView.model.attributes.size.width = document.getElementById(cellView.selectors.label.id).clientWidth + 20;
        cellView.model.attributes.size.height = heightText;
        cellView.model.attributes.attrs.label.text = editComment;
        cellView.update();
        if(cellView.model.isLink() == false){
          let links = boards[graphIndex].graph.getConnectedLinks(cellView.model);
          let len = links.length;
          for(let i = 0; i < len; i++){
            let linkView =  boards[graphIndex].paper.findViewByModel(links[i]);
            linkView.update();
          }
        }
        
        $(`#text-edit-area-custom-${activeBoardId}-textarea`).val(cellView.model.attributes.attrs.label.text);
        let d = {
          kind: USER_OPERATION_EDIT,
          selectObjectId: cellView.model.attributes.attrs[".objectId"].value,
          cell : cellView.model
        }
        
        // オブジェクト情報保存
        addUserOperationLogs(d);
      }
    }
  }
  textEditerBef = {};
}
// <-- テキスト直接入力対応

// 画像追加処理改善 -->
// オブジェクトIDから画像データを取得
function getBase64ImageToObjectId(param){
  
  $.ajax({
    type: "POST",
    url: '/yyknowledgeboard/get-imageData-to-objectId',
    data: JSON.stringify(param),
    processData: false,
    contentType: "application/json; charset=utf-8",
    success: function (response) {
      if(response.status == -1){
        return;
      }
      
      let boardIndex = getBoardsIndex(param.boardId);
      let cellIndex = boards[boardIndex].graph.attributes.cells.models.findIndex(a => {
        let res = false;
        if(a.attributes.attrs[".objectId"]){
          if( a.attributes.attrs[".objectId"].value == param.objectId){
            res = true;
          }
        }
        return res;
      });
      
      if(cellIndex != -1){
        let cellView = boards[boardIndex].paper.findViewByModel(boards[boardIndex].graph.attributes.cells.models[cellIndex]);
        cellView.model.attr('image/xlinkHref', response.data[0].thumbnail_image);
        cellView.update();
      }
      
      let idx = imageList.findIndex(a => a.url == param.url);
      if(idx == -1){
        imageList.push({
          url: param.url,
          imageData: response.data[0].thumbnail_image
        });
      }
    }
  });
  
}
// <-- 画像追加処理改善